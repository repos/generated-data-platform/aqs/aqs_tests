#BDD steps for the Common Impact Metrics (CIM) Pageviews per media file monthly
@aqs_tests @aqs_tests.common_impact_metrics @aqs_tests.cim_pageviews_per_media_file_monthly

Feature: Tests for Common Impact Metrics : Pageviews per media file monthly
    # Business Need: Positive Scenarios

    # These are tests for Pageviews per media file monthly

  Scenario Outline: Be able to run the CIM Pageviews per media file monthly endpoint with varying media file
        Given request is made to the Pageviews per media file monthly endpoint with varying media file <media_file>, 20231101 and 20240601
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that response body with context key is of type dict
        And verify that the sub_key media-file in the context key response is equal to <media_file>
        And verify that the sub_key wiki in the context key response is equal to all-wikis
        And verify that the sub_key start in the context key response is equal to 2023110100
        And verify that the sub_key end in the context key response is equal to 2024060100
        Examples:
            | media_file |
            | !!!_Palazzo_Scicluna,_now_Parisio_06.jpg |
            | !_Valletta_3951_04.jpg |
            | !mohyla_N1_13.JPG |
            | !-2011-debowa-leka-kosciol-abri.jpg      |


  Scenario Outline: Be able to run the CIM Pageviews per media file monthly endpoint with varying wiki
        Given request is made to the Pageviews per media file monthly endpoint with varying wiki <wiki>, 20231101 and 20240601
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that response body with context key is of type dict
        And verify that the sub_key media-file in the context key response is equal to !-2009-lgin-palac-abri.jpg
        And verify that the sub_key wiki in the context key response is equal to <wiki>
        And verify that the sub_key start in the context key response is equal to 2023110100
        And verify that the sub_key end in the context key response is equal to 2024060100
        Examples:
            | wiki |
            |all-wikis|
            | en.wikipedia|
            | nl.wikipedia|


  Scenario: Be able to run the CIM Pageviews per media file monthly endpoint with invalid Route
        Given request is made to the Pageviews per media file monthly endpoint with invalid route
        Then the request should be unsuccessful with status code of 404
        And verify that the content-type is application/problem+json
        And verify that title key contains Not Found
        And verify that the detail in response is equal to invalid route
        And verify that the method in response is equal to get


  Scenario: Be able to run the CIM Pageviews per media file monthly endpoint with invalid file
        Given request is made to the Pageviews per media file monthly endpoint with invalid file
        Then the request should be unsuccessful with status code of 404
        And verify that the content-type is application/problem+json
        And verify that title key contains Not Found
        And verify that the method in response is equal to get
        And verify that the detail in response contains media file you asked for is not loaded yet

  # The error message for invalid wiki needs to be updated
  Scenario: Be able to run the CIM Pageviews per media file monthly endpoint with invalid wiki
        Given request is made to the Pageviews per media file monthly endpoint with invalid wiki
        Then the request should be unsuccessful with status code of 404
        And verify that the content-type is application/problem+json
        And verify that title key contains Not Found
        And verify that the method in response is equal to get
        And verify that the detail in response contains media file you asked for is not loaded yet

  Scenario: Be able to run the CIM Pageviews per media file monthly endpoint with invalid start time
        Given request is made to the Pageviews per media file monthly endpoint with invalid start time
        Then the request should be unsuccessful with status code of 400
        And verify that the content-type is application/problem+json
        And verify that title key contains Bad Request
        And verify that the detail in response contains start timestamp is invalid, must be a valid date in YYYYMMDD format


  Scenario: Be able to run the CIM Pageviews per media file monthly endpoint with invalid end time
        Given request is made to the Pageviews per media file monthly endpoint with invalid end time
        Then the request should be unsuccessful with status code of 400
        And verify that the content-type is application/problem+json
        And verify that title key contains Bad Request
        And verify that the detail in response contains end timestamp is invalid, must be a valid date in YYYYMMDD format

   Scenario Outline: Be able to run the CIM Pageviews per media file monthly endpoint with invalid characters
        Given request is made to the CIM Pageviews per media file monthly endpoint with invalid characters <wiki>
        Then the request should be successful with status code of 400
        And verify that the content-type is application/problem+json
        And verify that detail key contains the parameter `wiki` contains invalid characters

        Examples:
            | wiki |
            | `all-wikis |
            | all-wikis` |
            | `all-wikis` |
            | en.wiki*pedia |
            | es.wiki(pedia |
            | ^.wikipedia |
            | *.wikipedia |
            | !wiki*pedia |
            | $wikipedia* |

# DATA Test
  Scenario: Be able to run the CIM Pageviews per media file monthly endpoint to validate category rank and pageview count
        Given request is made to the Pageviews per media file monthly endpoint to validate timestamp and edit count
        Then the request should be unsuccessful with status code of 200
        And verify that the sub_key wiki in the context key response is equal to all-wikis
        And verify that the sub_key media-file in the context key response is equal to !-2009-lgin-palac-abri.jpg
        And verify the value of the timestamp key in the items object first json is equal to text 2023110100
        And verify the value of the pageview-count key in the first json is equal to 7
        And verify the value of the timestamp key in the items object last json is equal to text 2024050100
        And verify the value of the pageview-count key in the last json is equal to 26