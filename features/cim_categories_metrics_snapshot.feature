#BDD steps for the Common Impact Metrics (CIM) scenarios
@aqs_tests @aqs_tests.common_impact_metrics @aqs_tests.cim_categories_metrics_snapshot

Feature: Tests for Common Impact Metrics (CIM)
    # Business Need: Positive Scenarios

    # These are tests for Categories Metrics Snapshot
    Scenario Outline: Be able to run the CIM Categories Metrics Snapshot endpoint
        Given request is made to the Categories Metrics Snapshot endpoint with varying category <category> 20231101 and 20240601 times
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that response body with context key is of type dict
        And verify that the sub_key category in the context key response is equal to <category>
        And verify that the sub_key start in the context key response is equal to 2023110100
        And verify that the sub_key end in the context key response is equal to 2024060100
        Examples:
            | category |
            |Arbetets_museum|


    Scenario: Be able to run the CIM Categories Metrics Snapshot endpoint with invalid Route
        Given request is made to the Categories Metrics Snapshot endpoint with and invalid route
        Then the request should be unsuccessful with status code of 404
        And verify that the content-type is application/problem+json
        And verify that title key contains Not Found
        And verify that the detail in response is equal to invalid route

    Scenario: Be able to run the CIM Categories Metrics Snapshot endpoint with invalid category
        Given request is made to the Categories Metrics Snapshot endpoint with and invalid category
        Then the request should be unsuccessful with status code of 404
        And verify that the content-type is application/problem+json
        And verify that title key contains Not Found
        And verify that the detail in response contains category you asked for is not loaded yet

    Scenario: Be able to run the CIM Categories Metrics Snapshot endpoint with invalid start time
        Given request is made to the Categories Metrics Snapshot endpoint with and invalid start time
        Then the request should be unsuccessful with status code of 400
        And verify that the content-type is application/problem+json
        And verify that title key contains bad request
        And verify that the detail in response contains start timestamp is invalid

    Scenario: Be able to run the CIM Categories Metrics Snapshot endpoint with invalid end time
        Given request is made to the Categories Metrics Snapshot endpoint with and invalid end time
        Then the request should be unsuccessful with status code of 400
        And verify that the content-type is application/problem+json
        And verify that title key contains bad request
        And verify that the detail in response contains end timestamp is invalid


 #Data Test

    Scenario: Be able to run the CIM Categories Metrics Snapshot endpoint to validate counts and deep
        Given request is made to the Categories Metrics Snapshot endpoint to validate counts and deep
        Then the request should be unsuccessful with status code of 200
        And verify that the sub_key category in the context key response is equal to Arbetets_museum
        And verify the value of the timestamp key in the items object first json is equal to text 2023110100
        And verify the value of the media-file-count key in the first json is equal to 6
        And verify the value of the media-file-count-deep key in the first json is equal to 6
        And verify the value of the used-media-file-count key in the first json is equal to 2
        And verify the value of the used-media-file-count-deep key in the first json is equal to 2
        And verify the value of the leveraging-wiki-count key in the first json is equal to 2
        And verify the value of the leveraging-wiki-count-deep key in the first json is equal to 2
        And verify the value of the leveraging-page-count key in the first json is equal to 2
        And verify the value of the leveraging-page-count-deep key in the first json is equal to 2
        And verify the value of the media-file-count key in the last json is equal to 8
        And verify the value of the media-file-count-deep key in the last json is equal to 8
        And verify the value of the used-media-file-count key in the last json is equal to 3
        And verify the value of the used-media-file-count-deep key in the last json is equal to 3
        And verify the value of the leveraging-wiki-count key in the last json is equal to 3
        And verify the value of the leveraging-wiki-count-deep key in the last json is equal to 3
        And verify the value of the leveraging-page-count key in the last json is equal to 4
        And verify the value of the leveraging-page-count-deep key in the last json is equal to 4
        And verify that the sub_key start in the context key response is equal to 2023110100
        And verify that the sub_key end in the context key response is equal to 2024060100