#BDD test steps for the editor data aggregate api scenarios

    # Editor analytics was formerly called Editors data

@aqs_tests @aqs_tests.editors_analytics_data @aqs_tests.editor_analytics_data_aggregate

Feature: Tests for Editor Data Aggregate endpoint

    # Business Need: Positive Scenarios

    # This is to test for Editor data aggregate endpoint with en.wikipedia, diff editor types, diff page types, diff activity levels, diff granularity,
    # 20210302 and 20210901
    # Success cases
    Scenario Outline: Be able to run the editor analytics data aggregate endpoint with projects, editor types, page types, activity levels, granularities, start dates and end dates
        Given request is made to the editor analytics data aggregate endpoint with <project>, <editor_type>, <page_type>, <activity_level>, <granularity>, <start_date> and <end_date>
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that items key contains project in list of arrays
        And verify that items key contains editor-type in list of arrays
        And verify that items key contains page-type in list of arrays
        And verify that items key contains activity-level in list of arrays
        And verify that items key contains granularity in list of arrays
        And verify that items key with project in is equal to <response_project>
        And verify that items key with editor-type in is equal to <editor_type>
        And verify that items key with page-type in is equal to <page_type>
        And verify that items key with activity-level in is equal to <activity_level>
        And verify that items key with granularity in is equal to <granularity>
        And verify that items key with sub_key results sub_key contains editors of type int
        And verify that items key with sub_key results sub_key contains timestamp of type string
        And verify the value of editors key of the first json of the results array is equal to <first_editors>
        And verify the value of editors key of the last json of the results array is equal to <last_editors>

        Examples:
            | project              | response_project | editor_type      | page_type      | activity_level      | granularity | start_date | end_date   | first_editors | last_editors  |
            | sw.wikipedia         | sw.wikipedia     | all-editor-types | all-page-types | all-activity-levels | daily       | 20210302   | 20210901   | 14            | 17            |
            | sw.wikipedia.org     | sw.wikipedia     | anonymous        | content        | 1..4-edits          | monthly     | 20210302   | 20210901   | 72            | 60            |
            | www.sw.wikipedia.org | sw.wikipedia     | group-bot        | non-content    | 5..24-edits         | daily       | 20210302   | 20210901   | 1             | 1             |
            | www.ab.wikipedia     | ab.wikipedia     | name-bot         | all-page-types | all-activity-levels | monthly     | 20190227   | 20211231   | 2             | 1             |
            | sw.wikipedia         | sw.wikipedia     | user             | content        | 100..-edits         | monthly     | 20210302   | 20210901   | 5             | 4             |
            | sw.wikipedia         | sw.wikipedia     | user             | content        | 100..-edits         | monthly     | 2021030200 | 20210901   | 5             | 4             |
            | ab.wikipedia.org     | ab.wikipedia     | user             | content        | 100..-edits         | monthly     | 20200302   | 2021090100 | 1             | 1             |


    # Business Need: Negative Scenarios

    # Bad Request when using all-... filter
    Scenario Outline: Be able to run the editor analytics data aggregate endpoints using invalid filters for project, all-editor-types, content, 1..4-edits, monthly, 20210302 and 20210901
        Given request is made to the editor analytics data aggregate endpoint with <project>, all-editor-types, content, 1..4-edits, monthly, 20210302 and 20210901
        Then the request should be unsuccessful with status code of 400
        And verify that the content-type is application/problem+json
        And verify that title key contains Bad Request
        And verify that method key contains get
        And verify that detail key contains all-...` project values are not accepted for this metric

        Examples:
            | project                |
            | all-projects           |
            | all-wikipedia-projects |

    # Keep in mind that AQS 1.0 title error response for these cases is equal to "Invalid parameters".
    # Bad request when requesting invalid parameters (dates not included)
    Scenario Outline: Run the editor analytics data aggregate endpoints with sw.wikipedia, editor types, page types, activity_levels, granularities, 20210302 and 20210901 using an invalid parameter for each case
        Given request is made to the editor analytics data aggregate endpoint with sw.wikipedia, <editor_type>, <page_type>, <activity_level>, <granularity>, 20210302 and 20210901
        Then the request should be unsuccessful with status code of 400
        And verify that the content-type is application/problem+json
        And verify that title key contains Bad Request
        And verify that method key contains get
        And verify that detail key contains <detail>

        Examples:
            | editor_type         | page_type         | activity_level         | granularity          | detail                                                                                                                            |
            | invalid_editor_type | all-page-types    | all-activity-levels    | daily                | editor-type should be equal to one of the allowed values: [all-editor-types, anonymous, group-bot, name-bot, user] |
            | anonymous           | invalid_page_type | 1..4-edits             | monthly              | page-type should be equal to one of the allowed values: [all-page-types, content, non-content]                     |
            | group-bot           | non-content       | invalid_activity_level | daily                | activity-level should be equal to one of the allowed values: [all-activity-levels, 1..4-edits, 5..24-edits, 25..99-edits, 100..-edits]|
            | name-bot            | all-page-types    | 25..99-edits           | invalid_granularity  | granularity should be equal to one of the allowed values: [daily, monthly]                                                               |

    # Keep in mind that there is no 'title' key for errors when invalid dates. That part will fail with AQS 1.0
    # Keep in mind that AQS 1.0 title error response for these cases is equal to "Invalid parameters".
    # Bad request when requesting invalid dates
    Scenario Outline: Run the editor analytics data aggregate endpoints with sw.wikipedia, all-editor-types, all-page-types, all-activity-levels, granularities, start dates and end dates using invalid date for each case
        Given request is made to the editor analytics data aggregate endpoint with sw.wikipedia, all-editor-types, all-page-types, all-activity-levels, <granularity>, <start_date> and <end_date>
        Then the request should be unsuccessful with status code of 400
        And verify that the content-type is application/problem+json
        And verify that title key contains Bad Request
        And verify that method key contains get
        And verify that detail key contains <detail>

    Examples:
        | granularity   | start_date         | end_date         | detail                                                              |
        | daily         | 20210302333        | 20210901         | start timestamp is invalid, must be a valid date in YYYYMMDD format |
        | daily         | 20210302           | 20210901aaa      | end timestamp is invalid, must be a valid date in YYYYMMDD format   |
        | daily         | 20210302           | 20200901         | start timestamp should be before the end timestamp                  |
        | monthly       | 20210302           | 20210401         | no full months found in specified date range                        |

    # Invalid characters
    Scenario: Be able to run the editor analytics data aggregate endpoints with an invalid project, all-editor-types, content, 1..4-edits, monthly, 20210302 and 20210901
        Given request is made to the editor analytics data aggregate endpoint with sw.wi*kipedia, all-editor-types, content, 1..4-edits, monthly, 20210302 and 20210901
        Then the request should be unsuccessful with status code of 400
        And verify that the content-type is application/problem+json
        And verify that title key contains Bad request
        And verify that method key contains get
        And verify that detail key contains the parameter `project` contains invalid characters

    # Keep in mind that AQS 1.0 responds with a 200 status code and zero values when no data is found due to an invalid project
    # Not Found when requesting invalid project
    Scenario: Be able to run the editor analytics data aggregate endpoints with an invalid project, all-editor-types, content, 1..4-edits, monthly, 20210302 and 20210901
    Given request is made to the editor analytics data aggregate endpoint with invalid_project, all-editor-types, content, 1..4-edits, monthly, 20210302 and 20210901
    Then the request should be unsuccessful with status code of 404
    And verify that the content-type is application/problem+json
    And verify that title key contains Not Found
    And verify that method key contains get
    And verify that detail key contains The date(s) you used are valid, but we either do not have data for those date(s), or the project you asked for is not loaded yet. Please check documentation for more information

    # No data found
    Scenario: Be able to run the editor analytics data aggregate endpoints with a project, all-editor-types, content, 1..4-edits, monthly, 20000302 and 20000901
    Given request is made to the editor analytics data aggregate endpoint with sw.wikipedia, all-editor-types, content, 1..4-edits, monthly, 20000302 and 20000901
    Then the request should be unsuccessful with status code of 404
    And verify that the content-type is application/problem+json
    And verify that title key contains Not Found
    And verify that method key contains get
    And verify that detail key contains The date(s) you used are valid, but we either do not have data for those date(s), or the project you asked for is not loaded yet. Please check documentation for more information

    # Invalid route
    Scenario: Be able to run the editor analytics data aggregate endpoints with sw.wikipedia, all-editor-types, content, 1..4-edits, monthly, 20210302 and 20210901 using an invalid route
    Given request is made to the editors data aggregate endpoint using an invalid route
    Then the request should be unsuccessful with status code of 404
    And verify that the content-type is application/problem+json
    And verify that title key contains Not Found
    And verify that method key contains get
    And verify that detail key contains Invalid route