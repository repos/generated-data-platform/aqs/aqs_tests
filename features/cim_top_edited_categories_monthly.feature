#BDD steps for the Common Impact Metrics (CIM) Top Edited Categories Monthly scenarios
@aqs_tests @aqs_tests.common_impact_metrics @aqs_tests.cim_top_edited_categories_monthly

Feature: Tests for Common Impact Metrics : Top Edited Categories Monthly
    # Business Need: Positive Scenarios

    # These are tests for Top Edited Categories Monthly
    Scenario Outline: Be able to run the CIM Top Edited categories monthly endpoint with varying category scope
        Given request is made to the Top Edited categories monthly endpoint with varying scope <scope>, 2023 and 11
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that response body with context key is of type dict
        And verify that the sub_key category-scope in the context key response is equal to <scope>
        And verify that the sub_key edit-type in the context key response is equal to all-edit-types
        And verify that the sub_key year in the context key response is equal to 2023
        And verify that the sub_key month in the context key response is equal to 11
        Examples:
            | scope |
            |shallow|
            | deep|

    Scenario Outline: Be able to run the CIM Top Edited categories monthly endpoint with varying edit type
        Given request is made to the Top Edited categories monthly endpoint with varying edit_type <edit_type>, 2023 and 11
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that response body with context key is of type dict
        And verify that the sub_key category-scope in the context key response is equal to shallow
        And verify that the sub_key edit-type in the context key response is equal to <edit_type>
        And verify that the sub_key year in the context key response is equal to 2023
        And verify that the sub_key month in the context key response is equal to 11
        Examples:
            | edit_type |
            | create |
            | update |
            | all-edit-types |

    Scenario: Be able to run the CIM Top Edited categories monthly endpoint with invalid Route
        Given request is made to the Top Edited categories monthly endpoint with invalid route
        Then the request should be unsuccessful with status code of 404
        And verify that the content-type is application/problem+json
        And verify that title key contains Not Found
        And verify that the detail in response is equal to invalid route
        And verify that the method in response is equal to get

    Scenario: Be able to run the CIM Top Edited categories monthly endpoint with invalid scope
        Given request is made to the CIM Top Edited categories monthly endpoint with invalid scope
        Then the request should be unsuccessful with status code of 400
        And verify that the content-type is application/problem+json
        And verify that title key contains Bad Request
        And verify that the detail in response contains allowed values: [shallow,deep]

    Scenario: Be able to run the CIM Top Edited categories monthly endpoint with invalid year
        Given request is made to the Top Edited categories monthly endpoint with invalid year
        Then the request should be unsuccessful with status code of 400
        And verify that the content-type is application/problem+json
        And verify that title key contains bad request
        And verify that the detail in response contains given year/month is invalid date

    Scenario: Be able to run the CIM Top Edited categories monthly endpoint with invalid month
        Given request is made to the Top Edited categories monthly endpoint with invalid month
        Then the request should be unsuccessful with status code of 400
        And verify that the content-type is application/problem+json
        And verify that title key contains bad request
        And verify that the detail in response contains given year/month is invalid date

    # DATA TEST
    Scenario: Be able to run the CIM Top Edited categories monthly endpoint to validate category rank and edit count
        Given request is made to the Top Edited categories monthly endpoint to validate category rank and edit count
        Then the request should be unsuccessful with status code of 200
        And verify the value of the edit-count key in the first json is equal to 767641
        And verify the value of the category key in the items object first json is equal to text Files_from_Gallica
        And verify the value of the rank key in the first json is equal to 1
        And verify the value of the edit-count key in the last json is equal to 5536
        And verify the value of the category key in the items object last json is equal to text Agritechnica_2023_(supported_by_Wikimedia_Deutschland)
        And verify the value of the rank key in the last json is equal to 100
        And verify that the sub_key year in the context key response is equal to 2023
        And verify that the sub_key month in the context key response is equal to 11
        And verify the order of the rank key is numerical