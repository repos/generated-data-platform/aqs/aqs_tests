#BDD steps for the API SPEC JSON SCHEMAs scenarios

@aqs_tests @aqs_tests.api_spec_json

Feature: Tests for API SPEC Json for all services

    # Business Need: Positive Scenarios
    Scenario: Be able to run the Api Spec Json for the Editor Analytics
        Given request to the api spec json for editor analytics
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that the header contains the cache-control
        And verify that the header contains the etag
        And verify that the header contains the date
        And verify that the header contains the server
        And verify that the header contains the access-control-allow-origin
        And verify that the header contains the access-control-allow-methods
        And verify that the header contains the access-control-allow-headers
        And verify that the header contains the x-content-type-options
        And verify that the header contains the content-security-policy
        And verify that the header contains the content-encoding
        And verify that the header contains the access-control-expose-headers
        And verify that the header contains the referrer-policy
        And validate that the editor_analytics schema is valid

    Scenario: Be able to run the Api Spec Json for the editor analytics with invalid route
        Given request to the api spec json for editor by country with invalid route
        Then the request should be successful with status code of 404
        And verify that the content-type is application/problem+json
        And verify that the detail in response is equal to Invalid route
        And verify that the title in response is equal to Not Found


    Scenario: Be able to run the Api Spec Json for the Media Analytics
        Given request to the api spec json for media analytics
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that the header contains the cache-control
        And verify that the header contains the etag
        And verify that the header contains the date
        And verify that the header contains the server
        And verify that the header contains the access-control-allow-origin
        And verify that the header contains the access-control-allow-methods
        And verify that the header contains the access-control-allow-headers
        And verify that the header contains the x-content-type-options
        And verify that the header contains the content-security-policy
        And verify that the header contains the content-encoding
        And verify that the header contains the access-control-expose-headers
        And verify that the header contains the referrer-policy
        And validate that the media_analytics schema is valid

    Scenario: Be able to run the Api Spec Json for the media analytics with invalid route
        Given request to the api spec json for media analytics with invalid route
        Then the request should be successful with status code of 404
        And verify that the content-type is application/problem+json
        And verify that the detail in response is equal to Invalid route
        And verify that the title in response is equal to Not Found

    Scenario: Be able to run the Api Spec Json for the Page Analytics
        Given request to the api spec json for page analytics
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that the header contains the cache-control
        And verify that the header contains the etag
        And verify that the header contains the date
        And verify that the header contains the server
        And verify that the header contains the access-control-allow-origin
        And verify that the header contains the access-control-allow-methods
        And verify that the header contains the access-control-allow-headers
        And verify that the header contains the x-content-type-options
        And verify that the header contains the content-security-policy
        And verify that the header contains the content-encoding
        And verify that the header contains the access-control-expose-headers
        And verify that the header contains the referrer-policy
        And validate that the page_analytics schema is valid

    Scenario: Be able to run the Api Spec Json for the page analytics with invalid route
        Given request to the api spec json for page analytics with invalid route
        Then the request should be successful with status code of 404
        And verify that the content-type is application/problem+json
        And verify that the detail in response is equal to Invalid route
        And verify that the title in response is equal to Not Found

    Scenario: Be able to run the Api Spec Json for the Edit Analytics
        Given request to the api spec json for edit analytics
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that the header contains the cache-control
        And verify that the header contains the etag
        And verify that the header contains the date
        And verify that the header contains the server
        And verify that the header contains the access-control-allow-origin
        And verify that the header contains the access-control-allow-methods
        And verify that the header contains the access-control-allow-headers
        And verify that the header contains the x-content-type-options
        And verify that the header contains the content-security-policy
        And verify that the header contains the content-encoding
        And verify that the header contains the access-control-expose-headers
        And verify that the header contains the referrer-policy
        And validate that the edit_analytics schema is valid

    Scenario: Be able to run the Api Spec Json for the edit analytics with invalid route
        Given request to the api spec json for edit analytics with invalid route
        Then the request should be successful with status code of 404
        And verify that the content-type is application/problem+json
        And verify that the detail in response is equal to Invalid route
        And verify that the title in response is equal to Not Found

    Scenario: Be able to run the Api Spec Json for the Geo Analytics
        Given request to the api spec json for geo analytics
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that the header contains the cache-control
        And verify that the header contains the etag
        And verify that the header contains the date
        And verify that the header contains the server
        And verify that the header contains the access-control-allow-origin
        And verify that the header contains the access-control-allow-methods
        And verify that the header contains the access-control-allow-headers
        And verify that the header contains the x-content-type-options
        And verify that the header contains the content-security-policy
        And verify that the header contains the content-encoding
        And verify that the header contains the access-control-expose-headers
        And verify that the header contains the referrer-policy
        And validate that the geo_analytics schema is valid


    Scenario: Be able to run the Api Spec Json for the Geo analytics with invalid route
        Given request to the api spec json for Geo analytics with invalid route
        Then the request should be successful with status code of 404
        And verify that the content-type is application/problem+json
        And verify that the detail in response is equal to Invalid route
        And verify that the title in response is equal to Not Found


    Scenario: Be able to run the Api Spec Json for the Device Analytics
        Given request to the api spec json for device analytics
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that the header contains the cache-control
        And verify that the header contains the etag
        And verify that the header contains the date
        And verify that the header contains the server
        And verify that the header contains the access-control-allow-origin
        And verify that the header contains the access-control-allow-methods
        And verify that the header contains the access-control-allow-headers
        And verify that the header contains the x-content-type-options
        And verify that the header contains the content-security-policy
        And verify that the header contains the content-encoding
        And verify that the header contains the access-control-expose-headers
        And verify that the header contains the referrer-policy
        And validate that the device_analytics schema is valid


    Scenario: Be able to run the Api Spec Json for the Device analytics with invalid route
        Given request to the api spec json for device analytics with invalid route
        Then the request should be successful with status code of 404
        And verify that the content-type is application/problem+json
        And verify that the detail in response is equal to Invalid route
        And verify that the title in response is equal to Not Found



    Scenario: Be able to run the Api Spec Json for the Common Impact Metrics
        Given request to the api spec json for Common Impact Metrics
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that the header contains the cache-control
        And verify that the header contains the etag
        And verify that the header contains the date
        And verify that the header contains the server
        And verify that the header contains the access-control-allow-origin
        And verify that the header contains the access-control-allow-methods
        And verify that the header contains the access-control-allow-headers
        And verify that the header contains the x-content-type-options
        And verify that the header contains the content-security-policy
        And verify that the header contains the content-encoding
        And verify that the header contains the access-control-expose-headers
        And verify that the header contains the referrer-policy
        And validate that the commons_impact_metrics schema is valid