#BDD steps for the editor by country api test scenarios

@aqs_tests @aqs_tests.editors @aqs_tests.geo_analytics_by_country

Feature: Tests for Geo Analytics by country endpoint

    # Business Need: Positive Scenarios


    # This is to test for the month of jan for years 2018 - 2021 with 100..-edits activity level
    Scenario Outline: Be able to run the geo analytics by country endpoints
        Given request is made to the geo analytics by country endpoint with en.wikipedia.org, 100..-edits, <year>, 01
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that items key contains project in list of arrays
        And verify that items key contains activity-level in list of arrays
        And verify that items key contains year in list of arrays
        And verify that items key contains month in list of arrays
        And verify that items key contains countries in list of arrays
        And verify that items key with sub_key project is of type string
        And verify that items key with sub_key activity-level is of type string
        And verify that items key with sub_key year is of type string
        And verify that items key with sub_key month is of type string
        And verify that items key with sub_key countries is of type list
        And verify that items key with sub_key countries sub_key contains country of type string
        And verify that items key with sub_key countries sub_key contains editors-ceil of type int
        And verify that items key with sub_key countries sub_key contains editors-ceil that is divisible by 10

        Examples:
            | year |
            | 2018 |
            | 2019 |
            | 2020 |
            | 2021 |


    # This is to test for the month of jan for years 2018 - 2021 with 100..-edits activity level
    Scenario Outline: Be able to run the geo analytics by country endpoints
        Given request is made to the geo analytics by country endpoint with en.wikipedia.org, 100..-edits, <year>, 12
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that items key contains project in list of arrays
        And verify that items key contains activity-level in list of arrays
        And verify that items key contains year in list of arrays
        And verify that items key contains month in list of arrays
        And verify that items key contains countries in list of arrays
        And verify that items key with sub_key project is of type string
        And verify that items key with sub_key activity-level is of type string
        And verify that items key with sub_key year is of type string
        And verify that items key with sub_key month is of type string
        And verify that items key with sub_key countries is of type list
        And verify that items key with sub_key countries sub_key contains country of type string
        And verify that items key with sub_key countries sub_key contains editors-ceil of type int
        And verify that items key with sub_key countries sub_key contains editors-ceil that is divisible by 10

        Examples:
            | year |
            | 2018 |
            | 2019 |
            | 2020 |
            | 2021 |

    # This is to test for the month of jan for years 2018 - 2021 with 5.99..-edits activity level
    Scenario Outline: Be able to run the geo analytics by country endpoints
        Given request is made to the geo analytics by country endpoint with en.wikipedia.org, 5.99..-edits, <year>, 01
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that items key contains project in list of arrays
        And verify that items key contains activity-level in list of arrays
        And verify that items key contains year in list of arrays
        And verify that items key contains month in list of arrays
        And verify that items key contains countries in list of arrays
        And verify that items key with sub_key project is of type string
        And verify that items key with sub_key activity-level is of type string
        And verify that items key with sub_key year is of type string
        And verify that items key with sub_key month is of type string
        And verify that items key with sub_key countries is of type list
        And verify that items key with sub_key countries sub_key contains country of type string
        And verify that items key with sub_key countries sub_key contains editors-ceil of type int
        And verify that items key with sub_key countries sub_key contains editors-ceil that is divisible by 10

        Examples:
            | year |
            | 2018 |
            | 2019 |
            | 2020 |
            | 2021 |

        # This is to test for the month of Dec for years 2018 - 2021 with 5.99..-edits activity level
    Scenario Outline: Be able to run the geo analytics by country endpoints with en.wikipedia.org, 5.99..-edits
        Given request is made to the geo analytics by country endpoint with en.wikipedia.org, 5.99..-edits, <year>, 12
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that items key contains project in list of arrays
        And verify that items key contains activity-level in list of arrays
        And verify that items key contains year in list of arrays
        And verify that items key contains month in list of arrays
        And verify that items key contains countries in list of arrays
        And verify that items key with sub_key project is of type string
        And verify that items key with sub_key activity-level is of type string
        And verify that items key with sub_key year is of type string
        And verify that items key with sub_key month is of type string
        And verify that items key with sub_key countries is of type list
        And verify that items key with sub_key countries sub_key contains country of type string
        And verify that items key with sub_key countries sub_key contains editors-ceil of type int
        And verify that items key with sub_key countries sub_key contains editors-ceil that is divisible by 10

        Examples:
            | year |
            | 2018 |
            | 2019 |
            | 2020 |
            | 2021 |

      # This is to test for the month of jan for year 2018 with 100..-edits activity level with different countries
    Scenario Outline: Be able to run the geo analytics by country endpoints with 100..-edits, 2018, 01 and diff projects
        Given request is made to the geo analytics by country endpoint with <projects>, 100..-edits, 2018, 01
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that items key contains project in list of arrays
        And verify that items key contains activity-level in list of arrays
        And verify that items key contains year in list of arrays
        And verify that items key contains month in list of arrays
        And verify that items key contains countries in list of arrays
        And verify that items key with sub_key project is of type string
        And verify that items key with sub_key activity-level is of type string
        And verify that items key with sub_key year is of type string
        And verify that items key with sub_key month is of type string
        And verify that items key with sub_key countries is of type list
        And verify that the <projects> used in request is same as project in response body

        Examples:
            | projects |
            | en.wikipedia |
            | es.wikipedia |
            | fr.wikipedia |
            | ru.wikipedia |
            | li.wikipedia |
            | ba.wikipedia |
            | be.wikipedia |
            | ca.wikipedia |


    # Business Need: Negative Scenarios


    # This is to test for the validations using wrong project, years 2018 with 100..-edits activity level
    Scenario: Be able to run the geo analytics by country endpoints
        Given request is made to the geo analytics by country endpoint with invalid_project, 100..-edits, 2018, 01
        Then the request should be successful with status code of 404
        And verify that title key contains Not found
        And verify that method key contains get
        And verify that detail key from the error response displays valid project or data warning

     # This is to test for the validations using valid project, years 2018 with invalid activity level
    Scenario: Be able to run the geo analytics by country endpoints
        Given request is made to the geo analytics by country endpoint with valid project, invalid activity level, 2018, 01
        Then the request should be successful with status code of 400
        And verify that title key contains Bad Request
        And verify that method key contains get
        And verify that detail key from the error response displays activity level validation


     # This is to test for the month of jun for years 2000 - 2017 ( years with no data) with 5.99..-edits activity level
    Scenario Outline: Be able to run the geo analytics by country endpoints with en.wikipedia.org, 5.99..-edits
        Given request is made to the geo analytics by country endpoint with en.wikipedia.org, 5.99..-edits, <year>, 06
        Then the request should be successful with status code of 404
        And verify that title key contains Not found
        And verify that method key contains get
        And verify that detail key from the error response displays validation for the date(year)
        And verify the entered <year> is in the uri key in the error response

        Examples:
            | year |
            | 2000 |
            | 2000 |
            | 2001 |
            | 2002 |
            | 2003 |
            | 2004 |
            | 2005 |
            | 2006 |
            | 2007 |
            | 2008 |
            | 2009 |
            | 2010 |
            | 2011 |
            | 2012 |
            | 2013 |
            | 2014 |
            | 2015 |
            | 2016 |
            | 2017 |

    # This is to test for the validations when using invalid year
    Scenario: Be able to run the geo analytics by country endpoints with invalid year
        Given request is made to the geo analytics by country endpoint with project, 100..-edits, invalid_year, month
        Then the request should be successful with status code of 400
        And verify that method key contains get
        And verify that detail key from the error response displays invalid year/month warning

    # This is to test for the validations when using invalid month
    Scenario: Be able to run the geo analytics by country endpoints
        Given request is made to the geo analytics by country endpoint with project, 100..-edits, year, invalid_month
        Then the request should be successful with status code of 400
        And verify that method key contains get
        And verify that detail key from the error response displays invalid year/month warning

    # This is to test for the specific first and last data points in a response body
    Scenario: Be able to run the geo analytics by country endpoints with year, month and 5..99-edits activity level
        Given request is made to the geo analytics by country endpoint with project, 5..99-edits, year, month
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that items key with sub_key countries sub_key contains editors-ceil where first data is equal to integer number 23350
        And verify that items key with sub_key countries sub_key contains editors-ceil where last data is equal to integer number 10

    # This is to test for invalid characters in the project parameter
    Scenario Outline: Be able to throw a Bad Request error when invalid characters are included in the project name
        Given request is made to the geo analytics by country endpoint with <project>, 100..-edits, 2018, 01
        Then the request should be successful with status code of 400
        And verify that the content-type is application/problem+json
        And verify that detail key contains the parameter `project` contains invalid characters

        Examples:
            | project |
            | en.wiki*pedia |
            | es.wiki(pedia |
            | ^.wikipedia |
            | *.wikipedia |
            | !wiki*pedia |
            | $wikipedia* |

    Scenario Outline: Be able to run the geo analytics by country endpoints with 5..99-edits, 2018, 01 and no normalized projects
        Given request is made to the geo analytics by country endpoint with <full_project>, 100..-edits, 2018, 01
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that project sub_key in items key is same as the <normalized_project> in the request

        Examples:
            | full_project          | normalized_project |
            | www.en.wikipedia.org  | en.wikipedia       |
            | www.en.wikipedia      | en.wikipedia       |
            | en.wikipedia.org      | en.wikipedia       |
            | www.ru.wikipedia      | ru.wikipedia       |
            | sw.wikipedia.org      | sw.wikipedia       |