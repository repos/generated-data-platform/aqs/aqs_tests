#BDD steps for the Common Impact Metrics (CIM) Top pages per media file monthly
@aqs_tests @aqs_tests.common_impact_metrics @aqs_tests.cim_top_pages_per_media_file_monthly

Feature: Tests for Common Impact Metrics : Top pages per media file monthly
    # Business Need: Positive Scenarios

    # These are tests for Top pages per media file Monthly

   Scenario Outline: Be able to run the CIM Top pages per media file monthly endpoint with varying media file scope
        Given request is made to the Top pages per media file monthly endpoint with varying file <file>, 2023 and 11
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that response body with context key is of type dict
        And verify that the sub_key media-file in the context key response is equal to <file>
        And verify that the sub_key year in the context key response is equal to 2023
        And verify that the sub_key month in the context key response is equal to 11
        Examples:
            | file |
            |!-2014-wschowa-zamek-abri.jpg|
            | !-2009-lgin-palac-abri.jpg|
            |   !Nara.jpg                        |
            |   !_Floriana_Lines_03.jpg|
            |   !_Valletta_3951_04.jpg                       |
            |   !fotokolbin_IMG_0249_copy.jpg                                             |


   Scenario Outline: Be able to run the CIM Top pages per media file monthly endpoint with varying wiki
        Given request is made to the Top pages per media file monthly endpoint with varying wiki <wiki>, 2023 and 11
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that response body with context key is of type dict
        And verify that the sub_key media-file in the context key response is equal to !-2009-lgin-palac-abri.jpg
        And verify that the sub_key wiki in the context key response is equal to <wiki>
        And verify that the sub_key year in the context key response is equal to 2023
        And verify that the sub_key month in the context key response is equal to 11
        Examples:
            | wiki |
            |all-wikis|
            | en.wikipedia|
            | fa.wikipedia |

   Scenario: Be able to run the CIM Top pages per media file monthly endpoint with invalid Route
        Given request is made to the Top pages per media file monthly endpoint with invalid route
        Then the request should be unsuccessful with status code of 404
        And verify that the content-type is application/problem+json
        And verify that title key contains Not Found
        And verify that the detail in response is equal to invalid route
        And verify that the method in response is equal to get

   Scenario: Be able to run the CIM Top pages per media file monthly endpoint with invalid file
        Given request is made to the Top pages per media file monthly endpoint with invalid file
        Then the request should be unsuccessful with status code of 404
        And verify that the content-type is application/problem+json
        And verify that title key contains Not Found
        And verify that the method in response is equal to get
        And verify that the detail in response contains media file you asked for is not loaded yet

# The error message for invalid wiki needs to be updated
     Scenario: Be able to run the CIM Top pages per media file monthly endpoint with invalid wiki
        Given request is made to the Top pages per media file monthly endpoint with invalid wiki
        Then the request should be unsuccessful with status code of 404
        And verify that the content-type is application/problem+json
        And verify that title key contains Not Found
        And verify that the method in response is equal to get
        And verify that the detail in response contains media file you asked for is not loaded yet

    Scenario: Be able to run the CIM Top pages per media file monthly endpoint with invalid year
        Given request is made to the Top pages per media file monthly endpoint with invalid year
        Then the request should be unsuccessful with status code of 400
        And verify that the content-type is application/problem+json
        And verify that title key contains bad request
        And verify that the detail in response contains given year/month is invalid date

   Scenario: Be able to run the CIM Top pages per media file monthly endpoint with invalid month
        Given request is made to the Top pages per media file monthly endpoint with invalid month
        Then the request should be unsuccessful with status code of 400
        And verify that the content-type is application/problem+json
        And verify that title key contains bad request
        And verify that the detail in response contains given year/month is invalid date


  Scenario Outline: Be able to run the CIM Top pages per media file monthly endpoint with invalid characters
        Given request is made to the CIM Top pages per media file monthly endpoint with invalid characters <wiki>
        Then the request should be successful with status code of 400
        And verify that the content-type is application/problem+json
        And verify that detail key contains the parameter `wiki` contains invalid characters

        Examples:
            | wiki |
            | `all-wikis |
            | all-wikis` |
            | `all-wikis` |
            | en.wiki*pedia |
            | es.wiki(pedia |
            | ^.wikipedia |
            | *.wikipedia |
            | !wiki*pedia |
            | $wikipedia* |

   # DATA TEST
  Scenario: Be able to run the CIM Top pages per media file monthly endpoint to validate category rank and edit count
        Given request is made to the Top pages per media file monthly endpoint to validate pageview rank and title
        Then the request should be unsuccessful with status code of 200
        And verify that the sub_key media-file in the context key response is equal to !-2009-lgin-palac-abri.jpg
        And verify the value of the pageview-count key in the first json is equal to 6
        And verify the value of the rank key in the first json is equal to 1
        And verify the value of the pageview-count key in the last json is equal to 1
        And verify the value of the rank key in the last json is equal to 2
        And verify that the sub_key year in the context key response is equal to 2023
        And verify that the sub_key month in the context key response is equal to 11
        And verify the order of the rank key is numerical
