#BDD steps for the Common Impact Metrics (CIM) Top viewed media files monthly
@aqs_tests @aqs_tests.common_impact_metrics @aqs_tests.cim_top_viewed_media_files_monthly

Feature: Tests for Common Impact Metrics : Top Viewed Media Files Monthly
    # Business Need: Positive Scenarios

    # These are tests for Top Viewed Media Files Monthly

  Scenario Outline: Be able to run the CIM Top viewed media files monthly endpoint with varying category scope
        Given request is made to the Top viewed media files monthly endpoint with varying scope <scope>, 2023 and 11
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that response body with context key is of type dict
        And verify that the sub_key category in the context key response is equal to Connected_Heritage_project
        And verify that the sub_key category-scope in the context key response is equal to <scope>
        And verify that the sub_key wiki in the context key response is equal to all-wikis
        And verify that the sub_key year in the context key response is equal to 2023
        And verify that the sub_key month in the context key response is equal to 11
        Examples:
            | scope |
            |shallow|
            | deep|

    Scenario Outline: Be able to run the CIM Top viewed media files monthly endpoint with varying wiki
        Given request is made to the Top viewed media files monthly endpoint with varying wiki <wiki>, 2023 and 11
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that response body with context key is of type dict
        And verify that the sub_key category in the context key response is equal to Connected_Heritage_project
        And verify that the sub_key wiki in the context key response is equal to <wiki>
        And verify that the sub_key year in the context key response is equal to 2023
        And verify that the sub_key month in the context key response is equal to 11
        Examples:
            | wiki |
            |all-wikis|
            | en.wikipedia|
            | zh.wikipedia|
            | es.wikipedia|

    Scenario: Be able to run the CIM Top viewed media files monthly endpoint with invalid Route
        Given request is made to the Top viewed media files monthly endpoint with invalid route
        Then the request should be unsuccessful with status code of 404
        And verify that the content-type is application/problem+json
        And verify that title key contains Not Found
        And verify that the detail in response is equal to invalid route
        And verify that the method in response is equal to get

    Scenario: Be able to run the CIM Top viewed media files monthly endpoint with invalid category
        Given request is made to the Top viewed media files monthly endpoint with invalid category
        Then the request should be unsuccessful with status code of 404
        And verify that the content-type is application/problem+json
        And verify that title key contains Not Found
        And verify that the detail in response contains category you asked for is not loaded yet

   Scenario: Be able to run the CIM Top viewed media files monthly endpoint with invalid scope
        Given request is made to the Top viewed media files monthly endpoint with invalid scope
        Then the request should be unsuccessful with status code of 400
        And verify that the content-type is application/problem+json
        And verify that title key contains Bad Request
        And verify that the detail in response contains allowed values: [shallow,deep]

   Scenario: Be able to run the CIM Top viewed media files monthly endpoint with invalid year
        Given request is made to the Top viewed media files monthly endpoint with invalid year
        Then the request should be unsuccessful with status code of 400
        And verify that the content-type is application/problem+json
        And verify that title key contains bad request
        And verify that the detail in response contains given year/month is invalid date

   Scenario: Be able to run the CIM Top viewed media files monthly endpoint with invalid month
        Given request is made to the Top viewed media files monthly endpoint with invalid month
        Then the request should be unsuccessful with status code of 400
        And verify that the content-type is application/problem+json
        And verify that title key contains bad request
        And verify that the detail in response contains given year/month is invalid date

   Scenario Outline: Be able to run the CIM Top viewed media files monthly endpoint with invalid characters
        Given request is made to the CIM Top viewed media files monthly endpoint with invalid characters <wiki>
        Then the request should be successful with status code of 400
        And verify that the content-type is application/problem+json
        And verify that detail key contains the parameter `wiki` contains invalid characters

        Examples:
            | wiki |
            | `all-wikis |
            | all-wikis` |
            | `all-wikis` |
            | en.wiki*pedia |
            | es.wiki(pedia |
            | ^.wikipedia |
            | *.wikipedia |
            | !wiki*pedia |
            | $wikipedia* |

   # DATA TEST
    Scenario: Be able to run the CIM Top viewed media files monthly endpoint to validate category rank and edit count
        Given request is made to the Top viewed media files monthly endpoint to validate category rank and edit count
        Then the request should be unsuccessful with status code of 200
        And verify the value of the media-file key in the items object first json is equal to text Platform_1_at_Paddington.jpg
        And verify the value of the pageview-count key in the first json is equal to 35194
        And verify the value of the rank key in the first json is equal to 1
        And verify the value of the media-file key in the items object last json is equal to text GWR_War_Memorial_after_Remembrance_Day_2021_(centred).jpg
        And verify the value of the pageview-count key in the last json is equal to 15
        And verify the value of the rank key in the last json is equal to 22
        And verify that the sub_key year in the context key response is equal to 2023
        And verify that the sub_key month in the context key response is equal to 11
        And verify the order of the rank key is numerical