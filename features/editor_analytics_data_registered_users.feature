#BDD test steps for the editor registered users api scenarios

    # Editor Analytics data was formerly known as Editors Data

@aqs_tests @aqs_tests.editors_analytics_data @aqs_tests.editors_analytics_data_registered_users

Feature: Tests for Editor Analytics Data Registered Users endpoint

    # Business Need: Positive Scenarios

    # This is to test for Editor Analytics registered users endpoint with en.wikipedia, diff editor types, diff page types, diff activity levels, diff granularity,
    # 20210302 and 20210901
    # Success cases
    Scenario Outline: Be able to run the Editor analytics data registered users endpoint with projects,granularity, start dates and end dates
        Given request is made to the editor analytics registered users endpoint with <project>, <granularity>, <start_date> and <end_date>
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that items key contains project in list of arrays
        And verify that items key contains granularity in list of arrays
        And verify that items key with project in is equal to <response_project>
        And verify that items key with granularity in is equal to <granularity>
        And verify that items key with sub_key results sub_key contains new_registered_users of type int
        And verify that items key with sub_key results sub_key contains timestamp of type string
        And verify the value of new_registered_users key of the first json of the results array is equal to <first_registered_users>
        And verify the value of new_registered_users key of the last json of the results array is equal to <last_registered_users>

        Examples:
            | project              | response_project | granularity | start_date | end_date   | first_registered_users   | last_registered_users   |
            | sw.wikipedia         | sw.wikipedia     | daily       | 20210401   | 20210601   | 5                        | 3                       |
            | sw.wikipedia.org     | sw.wikipedia     | monthly     | 20210401   | 20210901   | 73                       | 67                      |
            | www.sw.wikipedia.org | sw.wikipedia     | daily       | 20210401   | 20210901   | 5                        | 4                       |
            | www.ab.wikipedia     | ab.wikipedia     | monthly     | 20190227   | 20211231   | 1                        | 2                       |
            | sw.wikipedia         | sw.wikipedia     | monthly     | 20210302   | 20210901   | 73                       | 67                      |
            | sw.wikipedia         | sw.wikipedia     | monthly     | 2021030200 | 20210901   | 73                       | 67                      |
            | ab.wikipedia.org     | ab.wikipedia     | monthly     | 20200302   | 2021090100 | 1                        | 2                       |


           
    # Business Need: Negative Scenario

    # Keep in mind that AQS 1.0 title error response for these cases is equal to "Invalid parameters".
    # Bad request when requesting invalid parameters (dates not included)
    Scenario Outline: Run the Editor analytics data registered users endpoints with sw.wikipedia, granularities, 20210302 and 20210901 using an invalid parameter for each case
        Given request is made to the editor analytics registered users endpoint with sw.wikipedia, <granularity>, 20210302 and 20210901
        Then the request should be unsuccessful with status code of 400
        And verify that the content-type is application/problem+json
        And verify that title key contains Bad Request
        And verify that method key contains get
        And verify that detail key contains <detail>

        Examples:
             | granularity          | detail                                                                                                                            |
             | yearly               | granularity should be equal to one of the allowed values: [daily, monthly]
             | invalid_granularity  | granularity should be equal to one of the allowed values: [daily, monthly]                                                               |

    # Keep in mind that AQS 1.0 title error response for these cases is equal to "Invalid parameters".
    # Bad request when requesting invalid dates
    Scenario Outline: Run the Editor analytics data registered users endpoints with sw.wikipedia,granularities, start dates and end dates using invalid date for each case
        Given request is made to the editor analytics registered users endpoint with sw.wikipedia, <granularity>, <start_date> and <end_date>
        Then the request should be unsuccessful with status code of 400
        And verify that the content-type is application/problem+json
        And verify that title key contains Bad Request
        And verify that method key contains get
        And verify that detail key contains <detail>

    Examples:
        | granularity   | start_date         | end_date         | detail                                                              |
        | daily         | 20210302333        | 20210901         | start timestamp is invalid, must be a valid date in YYYYMMDD format |
        | daily         | 20210302           | 20210901aaa      | end timestamp is invalid, must be a valid date in YYYYMMDD format   |
        | daily         | 20230902           | 20230301         | start timestamp should be before the end timestamp                  |
        | monthly       | 20220302           | 20220305         | no full months found in specified date range                        |

    # Invalid characters
    Scenario: Be able to run the Editor analytics data registered users endpoints with an invalid project, monthly, 20210302 and 20210901
        Given request is made to the editor analytics registered users endpoint with sw.wi*kipedia, monthly, 20210302 and 20210901
        Then the request should be unsuccessful with status code of 400
        And verify that the content-type is application/problem+json
        And verify that title key contains Bad request
        And verify that method key contains get
        And verify that detail key contains the parameter `project` contains invalid characters

    # Keep in mind that AQS 1.0 responds with a 200 status code and zero values when no data is found due to an invalid project
    # Not Found when requesting invalid project
    Scenario: Be able to run the Editor analytics data registeredd users endpoints with an invalid project, monthly, 20210302 and 20210901
    Given request is made to the editor analytics registered users endpoint with invalid_project, monthly, 20210302 and 20210901
    Then the request should be unsuccessful with status code of 404
    And verify that the content-type is application/problem+json
    And verify that title key contains Not Found
    And verify that method key contains get
    And verify that detail key contains The date(s) you used are valid, but we either do not have data for those date(s), or the project you asked for is not loaded yet. Please check documentation for more information

    # Invalid route
    Scenario: Be able to run the Editor analytics data registered users endpoints with sw.wikipedia, monthly, 20210302 and 20210901 using an invalid route
    Given request is made to the editors registered users endpoint using an invalid route
    Then the request should be unsuccessful with status code of 404
    And verify that the content-type is application/problem+json
    And verify that title key contains Not Found
    And verify that method key contains get
    And verify that detail key contains Invalid route