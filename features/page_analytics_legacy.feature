#BDD steps for the legacy service test scenarios

  # Page analytics Top was formerly called Pageviews Top

@aqs_tests @aqs_tests.page_analytics @aqs_tests.page_analytics_legacy

Feature: Tests for Page analytics legacy service endpoint

    # Business Need: Positive Scenarios
    Scenario Outline: Be able to run the AQS Page analytics legacy service endpoint with varying access
        Given request is made to the legacy endpoint with en.wikipedia, varying <access>, hourly, 2016010101 and 2016010123
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that items key with sub_key project is of type string
        And verify that items key with sub_key access-site is of type string
        And verify that items key with sub_key granularity is of type string
        And verify that items key with sub_key timestamp is of type string
        And verify that items key with sub_key count is of type int
        And verify that items key with access-site in is equal to <access>
        Examples:
            | access |
            | all-sites |
            | desktop-site |
            | mobile-site |

    Scenario Outline: Be able to run the Page analytics legacy service endpoint with varying granularity
        Given request is made to the legacy endpoint with en.wikipedia, access, varying <granularity>, 2016010101 and 2016010123
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that items key with sub_key project is of type string
        And verify that items key with sub_key access-site is of type string
        And verify that items key with sub_key granularity is of type string
        And verify that items key with sub_key timestamp is of type string
        And verify that items key with sub_key count is of type int
        And verify that items key with granularity in is equal to <granularity>
        Examples:
            | granularity |
            | hourly |
            | daily |

     Scenario Outline: Run the Page analytics legacy service endpoint with varying projects
        Given request is made to legacy service endpoint with valid data <projects>
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8

        Examples:
            | projects |
            | de.wikipedia.org |
            | et.wikipedia.org |
            | fr.wikipedia.org |
            | ng.wikipedia.org |
            | be.wikipedia.org |
            | ca.wikipedia.org |
            | es.wikisource.org |
            | et.wikisource.org |
            | de.wikisource.org |
            | fr.wikisource.org |
            | be.wikisource.org |
           | zh.wikisource.org |
           | sw.wikipedia.org |
           | bcl.wikipedia.org |
            | diq.wikipedia.org |
           | nl.wikibooks.org |
           | id.wikibooks.org |
           | ja.wikisource.org |
           | lmo.wikipedia.org |
           | ta.wiktionary.org |
           | tr.wikiquote.org |
           | pa.wikipedia.org |
           | he.wikiquote.org |
           | min.wikipedia.org |
           | pt.wikinews.org |
            | sv.wikibooks.org |
            | fr.wikipedia.org |
            | pl.wikipedia.org |

    Scenario Outline: Run the Page analytics legacy service endpoint with varying start and end dates
        Given request is made to legacy service endpoint with varying valid <start_date> and <end_date>
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8

        Examples:
            | start_date |  | end_date |
            | 2008010101 |  | 2008010120 |
            | 2009010101 |  | 2009010120 |
            | 2010010101 |  | 2010010120 |
            | 2011010101 |  | 2011010120 |
            | 2012010101 |  | 2012010120 |
            | 2013010101 |  | 2013010120 |
            | 2014010101 |  | 2014010120 |
            | 2015010101 |  | 2015010120 |
            | 2016010101 |  | 2016010120 |

    # Negative Scenarios

    Scenario: Be able to run the Page analytics legacy service endpoint with invalid route
        Given request is made to the legacy service endpoint with invalid route
        Then the request should be successful with status code of 404
        And verify that the content-type is application/problem+json
        And verify that method key contains get
        And verify that title key contains Not found

    Scenario: Be able to run the Page analytics legacy service endpoint with invalid project
        Given request is made to the legacy service endpoint with invalid project
        Then the request should be successful with status code of 404
        And verify that the content-type is application/problem+json
        And verify that method key contains get
        And verify that title key contains Not found
        And verify that detail key from the error response displays invalid project warning for legacy

    Scenario: Be able to run the Page analytics legacy service endpoint with invalid access site
        Given request is made to the legacy service endpoint with invalid access site
        Then the request should be successful with status code of 400
        And verify that the content-type is application/problem+json
        And verify that method key contains get
        And verify that title key contains bad request
        And verify that detail key from the error response displays invalid access-site warning for legacy

    Scenario: Be able to run the Page analytics legacy service endpoint with invalid granularity
        Given request is made to the legacy service endpoint with invalid granularity
        Then the request should be successful with status code of 400
        And verify that the content-type is application/problem+json
        And verify that method key contains get
        And verify that title key contains bad request
        And verify that detail key from the error response displays invalid granularity warning for legacy

    Scenario: Be able to run the Page analytics legacy service endpoint with invalid start time
        Given request is made to the legacy service endpoint with invalid start time
        Then the request should be successful with status code of 400
        And verify that the content-type is application/problem+json
        And verify that method key contains get
        And verify that detail key from the error response displays invalid start time warning for legacy

    Scenario: Be able to run the Page analytics legacy service endpoint with invalid end time
        Given request is made to the legacy service endpoint with invalid end time
        Then the request should be successful with status code of 400
        And verify that the content-type is application/problem+json
        And verify that method key contains get
        And verify that detail key from the error response displays invalid end time warning for legacy

   Scenario: Be able to run the Page analytics legacy service endpoint without full month range
        Given request is made to the legacy service endpoint without full month range
        Then the request should be successful with status code of 400
        And verify that the content-type is application/problem+json
        And verify that method key contains get
        And verify that detail key from the error response for no full month specified

   Scenario: Be able to run the Page analytics legacy service endpoint with 24 hour specified with hourly granularity
        Given request is made to the legacy service endpoint with 24 hour specified with hourly granularity
        Then the request should be successful with status code of 400
        And verify that the content-type is application/problem+json
        And verify that method key contains get
        And verify that detail key from the error response displays invalid end time warning for legacy

   Scenario: Be able to run the Page analytics legacy service endpoint with invalid start time format
        Given request is made to the legacy service endpoint with invalid start time format
        Then the request should be successful with status code of 400
        And verify that the content-type is application/problem+json
        And verify that method key contains get
        And verify that detail key from the error response displays invalid start time warning for legacy

   Scenario: Be able to run the Page analytics legacy service endpoint with invalid end time format
        Given request is made to the legacy service endpoint with invalid end time format
        Then the request should be successful with status code of 400
        And verify that the content-type is application/problem+json
        And verify that method key contains get
        And verify that detail key from the error response displays invalid end time warning for legacy

   Scenario: Be able to run the Page analytics legacy service endpoint with invalid start and end time format
        Given request is made to the legacy service endpoint with invalid start and end time format
        Then the request should be successful with status code of 400
        And verify that the content-type is application/problem+json
        And verify that method key contains get
        And verify that detail key from the error response displays invalid start and end time format warning for legacy