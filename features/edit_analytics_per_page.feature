#BDD test steps for the edits data per page api scenarios

@aqs_tests @aqs_tests.edit_analytics @aqs_tests.edit_analytics_data_per_page

Feature: Tests for Edit Data Per Page endpoint

    # Business Need: Positive Scenarios


    # This is to test for Edits data aggregate endpoint with en.wikipedia, page title, diff editor types, daily, 20220101 and 20221101
    Scenario Outline: Be able to run the Edit data per page endpoint with en.wikipedia, Ukraine, editor types, daily, 20220101 and 20221101
        Given request is made to the edit data per_page endpoint with en.wikipedia, Ukraine, <editor_type>, all-page-types, daily, 20200101 and 20201231
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that items key contains project in list of arrays
        And verify that items key contains editor-type in list of arrays
        And verify that items key contains page-title in list of arrays
        And verify that items key contains granularity in list of arrays
        And verify that items key with project in is equal to en.wikipedia
        And verify that items key with granularity in is equal to daily
        And verify that items key with editor-type in is equal to <editor_type>
        And verify that items key with sub_key results sub_key contains edits of type int
        And verify that items key with sub_key results sub_key contains timestamp of type string

      Examples:
            | editor_type |
            | all-editor-types |
#            | anonymous |
            | group-bot |
#            | name-bot |
            | user |

      # This is to test for Edits data aggregate endpoint with en.wikipedia, page title, diff editor types, monthly, 20220101 and 20221101
    Scenario Outline: Be able to run the Edit data per page endpoint with en.wikipedia, Ukraine, editor types, daily, 20220101 and 20221101
        Given request is made to the edit data per_page endpoint with en.wikipedia, Ukraine, <editor_type>, all-page-types, monthly, 20200101 and 20201231
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that items key contains project in list of arrays
        And verify that items key contains editor-type in list of arrays
        And verify that items key contains page-title in list of arrays
        And verify that items key contains granularity in list of arrays
        And verify that items key with project in is equal to en.wikipedia
        And verify that items key with granularity in is equal to monthly
        And verify that items key with editor-type in is equal to <editor_type>
        And verify that items key with sub_key results sub_key contains edits of type int
        And verify that items key with sub_key results sub_key contains timestamp of type string

      Examples:
            | editor_type |
            | all-editor-types |
#            | anonymous |
            | group-bot |
#            | name-bot |
            | user |


      # This is to test for Edits data aggregate endpoint with projects, page title, all-editor-types, daily, 20220101 and 20221101
    Scenario Outline: Be able to run the Edit data per page endpoint with projects, Ukraine, editor types, daily, 20220101 and 20221101
        Given request is made to the edit data per_page endpoint with <projects>, Ukraine, all-editor-types, daily, 20200101 and 20201231
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that items key contains project in list of arrays
        And verify that items key contains editor-type in list of arrays
        And verify that items key contains page-title in list of arrays
        And verify that items key contains granularity in list of arrays
        And verify that items key with project in is equal to <projects>
        And verify that items key with granularity in is equal to daily
        And verify that items key with page-title in is equal to Ukraine
        And verify that items key with editor-type in is equal to all-editor-types
        And verify that items key with sub_key results sub_key contains edits of type int
        And verify that items key with sub_key results sub_key contains timestamp of type string

      Examples:
            | projects |
            | de.wikipedia |
            | en.wikipedia |
            | fr.wikipedia |

      # This is to test for Edits data aggregate endpoint with mobile projects, page title, all-editor-types, daily, 20220101 and 20221101
#    Scenario Outline: Be able to run the Edit data per page endpoint with projects, Ukraine, editor types, daily, 20220101 and 20221101
#        Given request is made to the edit data per_page endpoint with mobile <projects>, Ukraine, editor, daily, 20200101 and 20201231
#        Then the request should be successful with status code of 200
#        And verify that the content-type is application/json; charset=utf-8
#        And verify that items key contains project in list of arrays
#        And verify that items key contains editor-type in list of arrays
#        And verify that items key contains page-title in list of arrays
#        And verify that items key contains granularity in list of arrays
#        And verify that items key with project in is equal to <projects>
#        And verify that items key with sub_key results sub_key contains edits and is equal to 0
#
#      Examples:
#            | projects |
#            | de.m.wikipedia |
#            | en.m.wikipedia |
#            | es.m.wikipedia |
#            | fr.m.wikipedia |
#            | it.m.wikipedia |


      # This is to test for Edits data aggregate endpoint with an invalid project, page title, all-editor-types, daily, 20220101 and 20221101
    Scenario: Be able to run the Edit data per page endpoint with invalid project, Ukraine, editor types, monthly, 20220101 and 20221101
        Given request is made to the edit data per_page endpoint with invalid project, Ukraine, all-editor-types, monthly, 20200101 and 20201231
        Then the request should be successful with status code of 404
        And verify that method key contains get
        And verify that title key from the error response displays title not found error

      # This is to test for Edits data aggregate endpoint with a project, page title, invalid editor, daily, 20220101 and 20221101
    Scenario: Be able to run the Edit data per page endpoint with invalid project, Ukraine, invalid editor, daily, 20220101 and 20221101
        Given request is made to the edit data per_page endpoint with a project, Ukraine, invalid editor, monthly, 20200101 and 20201231
        Then the request should be successful with status code of 400
        And verify that method key contains get
        And verify that title key contains Bad Request
        And verify that detail key from the error response displays the unknown selection error

      # This is to test for Edits data aggregate endpoint with a project, page title, editor, invalid granularity, 20220101 and 20221101
    Scenario: Be able to run the Edit data per page endpoint with invalid project, Ukraine, editor, daily, 20220101 and 20221101
        Given request is made to the edit data per_page endpoint with a project, Ukraine, editor, invalid granularity, 20200101 and 20201231
        Then the request should be successful with status code of 400
        And verify that method key contains get
        And verify that title key contains Bad Request
        And verify that detail key from the error response displays the invalid granularity selection error

      # This is to test for Edits data aggregate endpoint with a project, page title, editor, granularity, invalid date and 20221101
    Scenario: Be able to run the Edit data per page endpoint with invalid project, Ukraine, editor, daily, invalid start date and 20221101
        Given request is made to the edit data per_page endpoint with a project, Ukraine, editor, granularity, invalid start date and 20201231
        Then the request should be successful with status code of 400
        And verify that method key contains get
        And verify that detail key from the error response displays the invalid date timestamp error

      # This is to test for Edits data aggregate endpoint with a project, page title, editor, granularity, later start date than end date
    Scenario: Be able to run the Edit data per page endpoint with invalid project, Ukraine, editor, daily, later start date than end date
        Given request is made to the edit data per_page endpoint with a project, Ukraine, editor, granularity, later start date than end date
        Then the request should be successful with status code of 400
        And verify that method key contains get
        And verify that detail key from the error response displays the wrong logical date error

      # This is to test for Edits data aggregate endpoint with a project, page title, editor, granularity_monthly, incomplete full month
    Scenario: Be able to run the Edit data per page endpoint with invalid project, Ukraine, editor, monthly, incomplete full month
        Given request is made to the edit data per_page endpoint with a project, Ukraine, editor, monthly, incomplete full month
        Then the request should be successful with status code of 400
        And verify that method key contains get
        And verify that detail key from the error response displays the no full month error