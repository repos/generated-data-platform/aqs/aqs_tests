#BDD steps for the media analytics top endpoint test scenarios

@aqs_tests @aqs_tests.media_analytics @aqs_tests.media_analytics_top

Feature: Tests for media analytics top endpoint

    Scenario Outline: Be able to run the media analytics top endpoint with varying referer
        Given request is made to the media analytics top endpoint with varying <referer>
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that items key contains referer in list of arrays
        And verify that items key contains media_type in list of arrays
        And verify that items key contains year in list of arrays
        And verify that items key contains month in list of arrays
        And verify that items key contains day in list of arrays
        And verify that items key with sub_key files is of type list
        And verify that items key with sub_key year is of type string
        And verify that items key with sub_key month is of type string
        And verify that items key with sub_key day is of type string
        And verify that items key with sub_key files sub_key contains rank of type int
        And verify that items key with sub_key files sub_key contains requests of type int
        And verify that items key with sub_key files sub_key contains file_path with file extension period
        And verify that the <referer> used in request is same as the referer in response body

      Examples:
        | referer |
        | all-referers |
        | internal |
        | external |
        | search-engine |
        | unknown |
        | none |
        | en.wikipedia  |
        | de.wikipedia  |
        | fr.wikipedia  |
        | et.wikipedia  |
        | de.wikivoyage |
        | en.wikiquote  |
        | en.wikiversity  |


  Scenario Outline: Be able to run the media analytics top endpoint with image media type, month 01 and day 01
    Given request is made to the media analytics top endpoint with image media type and varying <referer>
    Then the request should be successful with status code of 200
    And verify that the content-type is application/json; charset=utf-8
    And verify that media_type sub_key in items key is equal to image
    And verify that year sub_key in items key is equal to 2020
    And verify that month sub_key in items key is equal to 01
    And verify that day sub_key in items key is equal to 01
    And verify that items key with sub_key files sub_key contains file_path with image file extensions

    Examples:
      | referer       |
      | all-referers  |
      | internal      |
      | external      |
      | search-engine |
      | unknown       |
      | none          |
      | en.wikipedia  |
      | de.wikipedia  |
      | fr.wikipedia  |
      | et.wikipedia  |
      | de.wikivoyage |
      | en.wikiquote  |
      | en.wikiversity  |

  Scenario Outline: Be able to run the media analytics top endpoint with video media type, month 01 and day 01
    Given request is made to the media analytics top endpoint with video media type and varying <referer>
    Then the request should be successful with status code of 200
    And verify that the content-type is application/json; charset=utf-8
    And verify that media_type sub_key in items key is equal to video
    And verify that items key with sub_key files sub_key contains file_path with video file extensions

    Examples:
      | referer       |
      | all-referers  |
      | internal      |
      | external      |
      | search-engine |
      | unknown       |
      | none          |
      | en.wikipedia  |
      | de.wikipedia  |
      | fr.wikipedia  |
      | et.wikipedia  |
      | en.wikiquote  |
      | en.wikiversity  |

    Scenario Outline: Be able to run the media analytics top endpoint with audio media type, month 01 and day 01
    Given request is made to the media analytics top endpoint with audio media type and varying <referer>
    Then the request should be successful with status code of 200
    And verify that the content-type is application/json; charset=utf-8
    And verify that media_type sub_key in items key is equal to audio
    And verify that referer sub_key in items key is equal to <referer>
    And verify that items key with sub_key files sub_key contains file_path with audio file extensions

    Examples:
      | referer       |
      | all-referers  |
      | internal      |
      | external      |
      | search-engine |
      | unknown       |
      | none          |
      | en.wikipedia  |
      | de.wikipedia  |
      | fr.wikipedia  |
      | et.wikipedia  |
      | de.wikivoyage |
      | en.wikiquote  |
      | en.wikiversity  |

  Scenario Outline: Be able to run the media analytics top endpoint with document media type, month 01 and day 01
    Given request is made to the media analytics top endpoint with document media type and varying <referer>
    Then the request should be successful with status code of 200
    And verify that the content-type is application/json; charset=utf-8
    And verify that media_type sub_key in items key is equal to document
    And verify that items key with sub_key files sub_key contains file_path with document file extensions

    Examples:
      | referer       |
      | all-referers  |
      | internal      |
      | external      |
      | search-engine |
      | unknown       |
      | none          |
      | en.wikipedia  |
      | de.wikipedia  |
      | fr.wikipedia  |
      | et.wikipedia  |
      | de.wikivoyage |
      | en.wikiquote  |
      | en.wikiversity  |

    Scenario Outline: Be able to run the media analytics top endpoint with other media type, month 01 and day 01
      Given request is made to the media analytics top endpoint with other media type and varying <referer>
      Then the request should be successful with status code of 200
      And verify that the content-type is application/json; charset=utf-8
      And verify that media_type sub_key in items key is equal to other
      And verify that year sub_key in items key is equal to 2020
      And verify that month sub_key in items key is equal to 01
      And verify that day sub_key in items key is equal to 01
      And verify that items key with sub_key files sub_key contains requests of type int
      And verify that items key with sub_key files sub_key contains rank of type int

      Examples:
        | referer       |
        | all-referers  |
        | internal      |
        | external      |
        | search-engine |
        | unknown       |
        | none          |
        | en.wikipedia  |
        | de.wikipedia  |
        | fr.wikipedia  |
        | et.wikipedia  |
        | en.wiktionary  |


 Scenario Outline: Be able to run the media analytics top endpoint with all media type and varying valid years
      Given request is made to the media analytics top endpoint with all media types and varying valid <years>
      Then the request should be successful with status code of 200
      And verify that the content-type is application/json; charset=utf-8
      And verify that media_type sub_key in items key is equal to all-media-types
      And verify that year sub_key in items key is equal to <years>
   Examples:
     | years |
     | 2015 |
     | 2016 |
     | 2017 |
     | 2018 |
     | 2019 |
     | 2020 |
     | 2021 |
     | 2022 |

   Scenario Outline: Be able to run the media analytics top endpoint with all media type and varying invalid years
      Given request is made to the media analytics top endpoint with all media types and varying invalid <years>
      Then the request should be successful with status code of 404
      And verify that the content-type is application/problem+json
      And verify that method key contains get
      And verify that title key contains Not found
      And verify that detail key from the error response for invalid date or project
   Examples:
     | years |
     | 2008 |
     | 2009 |
     | 2010 |
     | 2011 |
     | 2012 |
     | 2013 |
     | 2014 |

  Scenario: Be able to run the media analytics top endpoint with invalid referer
      Given request is made to the media analytics top endpoint with invalid referer
      Then the request should be successful with status code of 404
      And verify that the content-type is application/problem+json
      And verify that method key contains get
      And verify that title key contains Not found
      And verify that detail key from the error response for invalid date or project

  Scenario: Be able to run the media analytics top endpoint with invalid media type
      Given request is made to the media analytics top endpoint with invalid media type
      Then the request should be successful with status code of 400
      And verify that the content-type is application/problem+json
      And verify that method key contains get
      And verify that title key contains Bad Request
      And verify that detail key from the error response contains media type validation

  Scenario: Be able to run the media analytics top endpoint with invalid Year date
      Given request is made to the media analytics top endpoint with invalid Year date
      Then the request should be successful with status code of 400
      And verify that the content-type is application/problem+json
      And verify that method key contains get
      And verify that detail key from the error response contains year date validation

  Scenario: Be able to run the media analytics top endpoint with invalid month date
      Given request is made to the media analytics top endpoint with invalid month date
      Then the request should be successful with status code of 400
      And verify that the content-type is application/problem+json
      And verify that method key contains get
      And verify that detail key from the error response contains month date validation

  Scenario: Be able to run the media analytics top endpoint with invalid day date
      Given request is made to the media analytics top endpoint with invalid day date
      Then the request should be successful with status code of 400
      And verify that the content-type is application/problem+json
      And verify that method key contains get
      And verify that detail key from the error response contains day date validation

  Scenario: Run the AQS media analytics top endpoint to validate first and last data point using et.wikisource
      Given request is made to the media analytics top endpoint to validate first and last data point
      Then the request should be successful with status code of 200
      And verify that the content-type is application/json; charset=utf-8
      And verify the value of the requests key in the first json of the files key is equal to 223
      And verify the value of the requests key in the last json of the files key is equal to 171