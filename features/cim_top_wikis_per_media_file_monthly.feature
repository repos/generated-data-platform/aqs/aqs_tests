#BDD steps for the Common Impact Metrics (CIM) Top wikis per media file monthly
@aqs_tests @aqs_tests.common_impact_metrics @aqs_tests.cim_top_wikis_per_media_files_monthly

Feature: Tests for Common Impact Metrics : Top wikis per media files monthly
    # Business Need: Positive Scenarios

    # These are tests for Top wikis per media Monthly

  Scenario Outline: Be able to run the CIM Top wikis per media files monthly endpoint with varying media file scope
        Given request is made to the Top wikis per media files monthly endpoint with varying file <file>, 2023 and 11
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that response body with context key is of type dict
        And verify that the sub_key media-file in the context key response is equal to <file>
        And verify that the sub_key year in the context key response is equal to 2023
        And verify that the sub_key month in the context key response is equal to 11
        Examples:
            | file |
            |!-2014-wschowa-zamek-abri.jpg |
            | !-2009-lgin-palac-abri.jpg |
            |   !Nara.jpg           |
            |   !_Floriana_Lines_03.jpg |
            |   !_Valletta_3951_04.jpg      |
            |   !fotokolbin_IMG_0249_copy.jpg         |


  Scenario: Be able to run the CIM Top wikis per media files monthly endpoint with invalid Route
        Given request is made to the Top wikis per media files monthly endpoint with invalid route
        Then the request should be unsuccessful with status code of 404
        And verify that the content-type is application/problem+json
        And verify that title key contains Not Found
        And verify that the detail in response is equal to invalid route
        And verify that the method in response is equal to get


  Scenario: Be able to run the CIM Top wikis per media files monthly endpoint with invalid file
        Given request is made to the Top wikis per media files monthly endpoint with invalid file
        Then the request should be unsuccessful with status code of 404
        And verify that the content-type is application/problem+json
        And verify that title key contains Not Found
        And verify that the method in response is equal to get
        And verify that the detail in response contains media file you asked for is not loaded yet


  Scenario: Be able to run the CIM Top wikis per media files monthly endpoint with invalid year
        Given request is made to the Top wikis per media files monthly endpoint with invalid year
        Then the request should be unsuccessful with status code of 400
        And verify that the content-type is application/problem+json
        And verify that title key contains bad request
        And verify that the detail in response contains given year/month is invalid date

  Scenario: Be able to run the CIM Top wikis per media files monthly endpoint with invalid month
        Given request is made to the Top wikis per media files monthly endpoint with invalid month
        Then the request should be unsuccessful with status code of 400
        And verify that the content-type is application/problem+json
        And verify that title key contains bad request
        And verify that the detail in response contains given year/month is invalid date

  # DATA TEST
    Scenario: Be able to run the CIM Top wikis per media files monthly endpoint to validate category rank and edit count
        Given request is made to the Top wikis per media files monthly endpoint to validate category rank and edit count
        Then the request should be unsuccessful with status code of 200
        And verify that the sub_key media-file in the context key response is equal to !mohyla_N1_13.JPG
        And verify the value of the wiki key in the items object first json is equal to text uk.wikipedia
        And verify the value of the pageview-count key in the first json is equal to 24
        And verify the value of the rank key in the first json is equal to 1
        And verify the value of the pageview-count key in the last json is equal to 24
        And verify the value of the rank key in the last json is equal to 1
        And verify that the sub_key year in the context key response is equal to 2023
        And verify that the sub_key month in the context key response is equal to 11
        And verify the order of the rank key is numerical