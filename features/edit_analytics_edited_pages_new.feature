@aqs_tests @aqs_tests.edit_analytics @aqs_tests.edit_analytics_edited_pages_new

Feature: Tests for Edit analytics Edited Pages new


    # Business Need: Positive Scenarios

    Scenario Outline: Be able to run the AQS Edit analytics edited pages new with varying projects
        Given request is made to the AQS Edit analytics edited pages new endpoint with varying <projects>, editor_types, all-page-types, daily, 20200101, 20201201
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that items key with sub_key project is of type string
        And verify that items key with sub_key editor-type is of type string
        And verify that items key with sub_key page-type is of type string
        And verify that items key with sub_key granularity is of type string
        And verify that items key with sub_key results sub_key contains new_pages of type int
        And verify that items key with sub_key results sub_key contains timestamp of type string
        And verify that the project in the response is the same as the <projects> in the endpoint
        Examples:
            | projects |
            | de.wikipedia |
            | et.wikipedia |
            | fr.wikipedia |
            | be.wikipedia |
            | sw.wikipedia |
            | ca.wikipedia |
            | es.wikisource |
            | et.wikisource |
            | de.wikisource |
            | fr.wikisource|
            | be.wikisource |
           | zh.wikisource |
           | sw.wikipedia|
           | bcl.wikipedia|
            | diq.wikipedia|
           | nl.wikibooks|
           | id.wikibooks|
           | ja.wikisource |
           | lmo.wikipedia |
           | ta.wiktionary |
           | tr.wikiquote |
           | pa.wikipedia |
           | he.wikiquote |
           | min.wikipedia |
           | pt.wikinews|
            | sv.wikibooks|
            | fr.wikipedia |
            | pl.wikipedia |


    Scenario Outline: Be able to run the AQS Edit analytics edited pages new with varying editor types
        Given request is made to the Edit edited pages new endpoint with sw.wikipedia, varying <editor_types>, all-page-types, daily, 20180101, 20181231
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that items key with sub_key project is of type string
        And verify that items key with sub_key editor-type is of type string
        And verify that items key with sub_key page-type is of type string
        And verify that items key with sub_key granularity is of type string
        And verify that items key with sub_key results sub_key contains new_pages of type int
        And verify that items key with sub_key results sub_key contains timestamp of type string
        And verify that the editor_type in the response is same as the <editor_types> in the edited pages new endpoint
        Examples:
            | editor_types |
            | all-editor-types |
            | anonymous |
#            | group-bot |
#            | name-bot |
            | user |

      Scenario Outline: Be able to run the AQS Edit analytics Edits edited pages new endpoint with varying page-types
        Given request is made to the Edit edited pages new endpoint with sw.wikipedia, editor_types, varying <page_types>, daily, 20200101, 20201201
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that items key with sub_key project is of type string
        And verify that items key with sub_key editor-type is of type string
        And verify that items key with sub_key page-type is of type string
        And verify that items key with sub_key granularity is of type string
        And verify that items key with sub_key results sub_key contains new_pages of type int
        And verify that items key with sub_key results sub_key contains timestamp of type string
        And verify that the page_type in the response is the same as the <page_types> in the edited page new endpoint
        Examples:
            | page_types |
            | all-page-types |
            | content |
            | non-content |

    Scenario Outline: Be able to run the AQS Edit analytics Edits edited pages new endpoint with varying granularity
        Given request is made to the Edit edited pages new endpoint with sw.wikipedia, editor_types, page_types, varying <granularity>, 20200101, 20201201
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that items key with sub_key project is of type string
        And verify that items key with sub_key editor-type is of type string
        And verify that items key with sub_key page-type is of type string
        And verify that items key with sub_key granularity is of type string
        And verify that items key with project in is equal to sw.wikipedia
        And verify that items key with editor-type in is equal to all-editor-types
        And verify that items key with page-type in is equal to all-page-types
        And verify that items key with sub_key results sub_key contains new_pages of type int
        And verify that items key with sub_key results sub_key contains timestamp of type string
        And verify that the granularity in the response is the same as the <granularity> in the edited pages new endpoint
        Examples:
            | granularity |
            | daily |
            | monthly |

       Scenario: Be able to run the AQS Edit analytics edited pages new endpoint with invalid project
        Given request is made to the Edit analytics edited pages new endpoint with invalid project
        Then the request should be successful with status code of 404
        And verify that the content-type is application/problem+json
        And verify that method key contains get


  Scenario: Be able to run the Edit analytics edited pages new endpoint with invalid editor type
      Given request is made to the edited pages new endpoint with invalid editor type
      Then the request should be successful with status code of 400
      And verify that the content-type is application/problem+json
      And verify that method key contains get
      And verify that title key contains Bad Request
      And verify that detail key from the error response for invalid editor type for the edited pages new endpoint

  Scenario: Be able to run the Edit analytics edited pages new endpoint with invalid page type
      Given request is made to the edited pages new endpoint with invalid page type
      Then the request should be successful with status code of 400
      And verify that the content-type is application/problem+json
      And verify that method key contains get
      And verify that title key contains Bad Request
      And verify that detail key from the error response for invalid page type for edited pages new endpoint

  Scenario: Be able to run the Edit analytics edited pages new endpoint with invalid granularity
      Given request is made to the edited pages new endpoint with invalid granularity
      Then the request should be successful with status code of 400
      And verify that the content-type is application/problem+json
      And verify that method key contains get
      And verify that title key contains Bad Request
      And verify that detail key from the error response for invalid granularity for edited pages new endpoint

  Scenario: Be able to run the Edit analytics edited pages new endpoint with invalid start type
      Given request is made to the bytes difference edited pages new endpoint with invalid start time
      Then the request should be successful with status code of 400
      And verify that the content-type is application/problem+json
      And verify that method key contains get
      And verify that detail key from the error response for invalid start time for edited pages new endpoint

  Scenario: Be able to run the Edit analytics edited pages new endpoint with invalid end type
      Given request is made to the edited pages new endpoint with invalid end time
      Then the request should be successful with status code of 400
      And verify that the content-type is application/problem+json
      And verify that method key contains get
      And verify that detail key shows the error response for invalid end time for edited pages new endpoint