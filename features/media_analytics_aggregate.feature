#BDD steps for the media analytics aggregate endpoint test scenarios

@aqs_tests @aqs_tests.media_analytics @aqs_tests.media_analytics_aggregate

Feature: Tests for media analytics aggregate endpoint

    Scenario Outline: Be able to run the media analytics aggregate endpoint with varying referer and all media
        Given request is made to the media analytics aggregate endpoint with varying <referer>
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that media_type sub_key in items key is equal to all-media-types
        And verify that items key with sub_key referer is of type string
        And verify that items key with sub_key media_type is of type string
        And verify that items key with sub_key agent is of type string
        And verify that items key with sub_key granularity is of type string
        And verify that items key with sub_key timestamp is of type string
        And verify that items key with sub_key requests is of type integer
        And verify that the <referer> used in request is same as the referer in response body

      Examples:
        | referer |
        | all-referers |
        | internal |
        | external |
        | search-engine |
        | unknown |
        | none |
        | en.wikipedia  |
        | de.wikipedia  |
        | fr.wikipedia  |
        | et.wikipedia  |
        | de.wikivoyage |
        | en.wiktionary  |
        | en.wikibooks  |
        | en.wikinews  |
        | en.wikiquote  |
        | en.wikisource  |
        | en.wikiquote  |
        | en.wikiversity  |

    Scenario Outline: Be able to run the media analytics aggregate endpoint with image media type
        Given request is made to the media analytics aggregate endpoint with image media type and varying <referer>
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that media_type sub_key in items key is equal to image
        And verify that items key with sub_key referer is of type string
        And verify that items key with sub_key media_type is of type string
        And verify that items key with sub_key agent is of type string
        And verify that items key with sub_key granularity is of type string
        And verify that items key with sub_key timestamp is of type string
        And verify that items key with sub_key requests is of type integer
        And verify that the <referer> used in request is same as the referer in response body

      Examples:
        | referer       |
        | all-referers  |
        | internal      |
        | external      |
        | search-engine |
        | unknown       |
        | none          |
        | en.wikipedia  |
        | de.wikipedia  |
        | fr.wikipedia  |
        | et.wikipedia  |
        | de.wikivoyage |
        | en.wiktionary  |
        | en.wikibooks  |
        | en.wikinews  |
        | en.wikiquote  |
        | en.wikisource  |
        | en.wikiquote  |
        | en.wikiversity  |

      Scenario Outline: Be able to run the media analytics aggregate endpoint with video media type
        Given request is made to the media analytics aggregate endpoint with video type and varying <referer>
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that media_type sub_key in items key is equal to video
        And verify that agent sub_key in items key is equal to all-agents
        And verify that granularity sub_key in items key is equal to daily
        And verify that items key with sub_key referer is of type string
        And verify that items key with sub_key media_type is of type string
        And verify that items key with sub_key agent is of type string
        And verify that items key with sub_key granularity is of type string
        And verify that items key with sub_key timestamp is of type string
        And verify that items key with sub_key requests is of type integer
        And verify that the <referer> used in request is same as the referer in response body

      Examples:
        | referer       |
        | all-referers  |
        | internal      |
        | external      |
        | search-engine |
        | unknown       |
        | none          |
        | en.wikipedia  |
        | de.wikipedia  |
        | fr.wikipedia  |
        | et.wikipedia  |
        | de.wikivoyage |
        | en.wiktionary  |
        | en.wikibooks  |
        | en.wikinews  |
        | en.wikiquote  |
        | en.wikisource  |
        | en.wikiquote  |
        | en.wikiversity  |

    Scenario Outline: Be able to run the media analytics aggregate endpoint with audio media type
        Given request is made to the media analytics aggregate endpoint with audio type and varying <referer>
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that media_type sub_key in items key is equal to audio
        And verify that agent sub_key in items key is equal to all-agents
        And verify that granularity sub_key in items key is equal to daily
        And verify that items key with sub_key referer is of type string
        And verify that items key with sub_key media_type is of type string
        And verify that items key with sub_key agent is of type string
        And verify that items key with sub_key granularity is of type string
        And verify that items key with sub_key timestamp is of type string
        And verify that items key with sub_key requests is of type integer
        And verify that the <referer> used in request is same as the referer in response body

      Examples:
        | referer       |
        | all-referers  |
        | internal      |
        | external      |
        | search-engine |
        | unknown       |
        | none          |
        | en.wikipedia  |
        | de.wikipedia  |
        | fr.wikipedia  |
        | et.wikipedia  |
        | de.wikivoyage |
        | en.wiktionary  |
        | en.wikibooks  |
        | en.wikinews  |
        | en.wikiquote  |
        | en.wikisource  |
        | en.wikiquote  |
        | en.wikiversity  |

  Scenario Outline: Be able to run the media analytics aggregate endpoint with document media type
        Given request is made to the media analytics aggregate endpoint with document type and varying <referer>
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that media_type sub_key in items key is equal to document
        And verify that agent sub_key in items key is equal to all-agents
        And verify that granularity sub_key in items key is equal to daily
        And verify that items key with sub_key referer is of type string
        And verify that items key with sub_key media_type is of type string
        And verify that items key with sub_key agent is of type string
        And verify that items key with sub_key granularity is of type string
        And verify that items key with sub_key timestamp is of type string
        And verify that items key with sub_key requests is of type integer
        And verify that the <referer> used in request is same as the referer in response body

      Examples:
        | referer       |
        | all-referers  |
        | internal      |
        | external      |
        | search-engine |
        | unknown       |
        | none          |
        | en.wikipedia  |
        | de.wikipedia  |
        | fr.wikipedia  |
        | et.wikipedia  |
        | de.wikivoyage |
        | en.wiktionary  |
        | en.wikibooks  |
        | en.wikinews  |
        | en.wikisource  |
        | en.wikiquote  |
        | en.wikiversity  |

  Scenario Outline: Be able to run the media analytics aggregate endpoint with other media type
        Given request is made to the media analytics aggregate endpoint with other type and varying <referer>
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that media_type sub_key in items key is equal to other
        And verify that agent sub_key in items key is equal to all-agents
        And verify that granularity sub_key in items key is equal to daily
        And verify that items key with sub_key referer is of type string
        And verify that items key with sub_key media_type is of type string
        And verify that items key with sub_key agent is of type string
        And verify that items key with sub_key granularity is of type string
        And verify that items key with sub_key timestamp is of type string
        And verify that items key with sub_key requests is of type integer
        And verify that the <referer> used in request is same as the referer in response body

      Examples:
        | referer       |
        | all-referers  |
        | internal      |
        | external      |
        | search-engine |
        | unknown       |
        | none          |
        | en.wikipedia  |
        | de.wikipedia  |
        | fr.wikipedia  |
        | et.wikipedia  |
        | en.wiktionary  |

    Scenario Outline: Run the media analytics aggregate endpoint with other media type with invalid projects
        Given request is made to the media analytics aggregate endpoint with other type and varying <referer>
        Then the request should be successful with status code of 404
        And verify that the content-type is application/problem+json
        And verify that method key contains get
        And verify that title key contains Not found
        And verify that detail key contains the project you asked for is not loaded yet

      Examples:
        | referer |
        | ng.wikisource  |
        | ng.wikisource  |
        | et.wikisource  |
        | gh.wikisource  |
        | ng.wikibooks  |
        | et.wikibooks  |
        | et.wikinews  |
        | ng.wikinews  |
        | gh.wikinews  |
        | et.wikiquote  |
        | ng.wikiquote  |
        | gh.wikiquote  |
        | gh.wikisource |
        | et.wikisource  |
        | ng.wikisource |
        | et.wikiversity |
        | ng.wikiversity |
        | gh.wikiversity |

  Scenario Outline: Be able to run the media analytics aggregate endpoint with varying agent type
        Given request is made to the media analytics aggregate endpoint with all media type and varying <agent>
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that media_type sub_key in items key is equal to all-media-types
        And verify that granularity sub_key in items key is equal to daily
        And verify that items key with sub_key referer is of type string
        And verify that items key with sub_key media_type is of type string
        And verify that items key with sub_key agent is of type string
        And verify that items key with sub_key granularity is of type string
        And verify that items key with sub_key timestamp is of type string
        And verify that items key with sub_key requests is of type integer
        And verify that the <agent> used in request is same as the agent in response body

      Examples:
        | agent       |
        | all-agents |
        | user      |
        | spider      |

  Scenario Outline: Run the media analytics aggregate endpoint with varying referer and all media and monthly grat
        Given request is made to media analytics aggregate endpoint with varying <referer> and monthly granularity
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that media_type sub_key in items key is equal to all-media-types
        And verify that granularity sub_key in items key is equal to monthly
        And verify that items key with sub_key referer is of type string
        And verify that items key with sub_key media_type is of type string
        And verify that items key with sub_key agent is of type string
        And verify that items key with sub_key granularity is of type string
        And verify that items key with sub_key timestamp is of type string
        And verify that items key with sub_key requests is of type integer
        And verify that the <referer> used in request is same as the referer in response body

      Examples:
        | referer |
        | all-referers |
        | internal |
        | external |
        | search-engine |
        | unknown |
        | none |
        | en.wikipedia  |
        | de.wikipedia  |
        | fr.wikipedia  |
        | et.wikipedia  |
        | de.wikivoyage |
        | en.wiktionary  |
        | en.wikibooks  |
        | en.wikinews  |
        | en.wikiquote  |
        | en.wikisource  |
        | en.wikiquote  |
        | en.wikiversity  |

  Scenario Outline: Be able to run the media analytics aggregate endpoint with varying referer 2018-2021
        Given request is made to the media analytics aggregate endpoint from 2018-21 with varying <referer>
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that media_type sub_key in items key is equal to all-media-types
        And verify that the <referer> used in request is same as the referer in response body

      Examples:
        | referer |
        | all-referers |
        | internal |
        | external |
        | search-engine |
        | unknown |
        | none |
        | en.wikipedia  |

  Scenario: Be able to run the media analytics aggregate endpoint with invalid referrer
      Given request is made to the media analytics aggregate endpoint with invalid referrer
      Then the request should be successful with status code of 404
      And verify that the content-type is application/problem+json
      And verify that method key contains get
      And verify that title key contains Not found
      And verify that detail key contains the project you asked for is not loaded yet

  Scenario: Be able to run the media analytics aggregate endpoint with invalid media type
      Given request is made to the media analytics aggregate endpoint with invalid media type
      Then the request should be successful with status code of 400
      And verify that the content-type is application/problem+json
      And verify that method key contains get
      And verify that detail key from the error response contains media type validation

  Scenario: Be able to run the media analytics aggregate endpoint with invalid agent
      Given request is made to the media analytics aggregate endpoint with invalid agent
      Then the request should be successful with status code of 400
      And verify that the content-type is application/problem+json
      And verify that method key contains get
      And verify that title key contains Bad Request
      And verify that detail key from the error response contains agent type validation

  Scenario: Be able to run the media analytics aggregate endpoint with invalid granularity
      Given request is made to the media analytics aggregate endpoint with invalid granularity
      Then the request should be successful with status code of 400
      And verify that the content-type is application/problem+json
      And verify that method key contains get
      And verify that title key contains Bad Request
      And verify that detail key from the error response contains granularity validation

  Scenario: Be able to run the media analytics aggregate endpoint with invalid start date
      Given request is made to the media analytics aggregate endpoint with invalid start date
      Then the request should be successful with status code of 400
      And verify that the content-type is application/problem+json
      And verify that method key contains get
      And verify that detail key from the error response contains start date validation

  Scenario: Be able to run the media analytics aggregate endpoint with invalid end date
      Given request is made to the media analytics aggregate endpoint with invalid end date
      Then the request should be successful with status code of 400
      And verify that the content-type is application/problem+json
      And verify that method key contains get
      And verify that detail key from the error response contains end date validation

  Scenario: Run the AQS media analytics aggregate endpoint to validate first and last data point using et.wikisource
      Given request is made to the media analytics aggregate endpoint to validate first and last data point
      Then the request should be successful with status code of 200
      And verify that the content-type is application/json; charset=utf-8
      And verify the value of the requests key in the first json is equal to 223
      And verify the value of the requests key in the last json is equal to 171