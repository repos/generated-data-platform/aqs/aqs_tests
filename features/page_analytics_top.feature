#BDD steps for the Page analytics Top endpoint test scenarios

  # Page analytics Top was formerly called Pageviews Top

@aqs_tests @aqs_tests.page_analytics @aqs_tests.page_analytics_top

Feature: Tests for page_analytics endpoint

    Scenario Outline: Be able to run the AQS page analytics top endpoint with valid parameters
        Given request made to the page analytics top endpoint with project, <access>, year, valid month, day
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that items key contains project in list of arrays
        And verify that items key contains year in list of arrays
        And verify that items key contains month in list of arrays
        And verify that items key contains day in list of arrays
        And verify that items key contains access in list of arrays
        And verify that items key with sub_key articles contains article in the list
        And verify that items key with sub_key articles contains views in the list
        And verify that items key with sub_key articles contains rank in the list
        And verify that the ranking is in increasing order

      Examples:
            | access |
            | all-access |
            | desktop |
            | mobile-app |
            | mobile-web |


   Scenario Outline: Be able to run the AQS page analytics top endpoint with varying years
        Given request is made to the page analytics top endpoint with project, valid access, <years>, month, valid day
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that items key contains project in list of arrays
        And verify that items key contains year in list of arrays
        And verify that items key contains month in list of arrays
        And verify that items key contains day in list of arrays
        And verify that items key contains access in list of arrays
        And verify that items key with sub_key articles contains article in the list
        And verify that items key with sub_key articles contains views in the list
        And verify that items key with sub_key articles contains rank in the list
        And verify that the ranking is in increasing order

     Examples:
       | years |
       | 2016 |
       | 2017 |
       | 2018 |
       | 2019 |
       | 2020 |
       | 2021 |
       | 2022 |
       | 2023 |


   Scenario: Be able to run the AQS page analytics top endpoint with invalid project
        Given request is made to the page analytics top endpoint with invalid project, access, year, month, day
        Then the request should be successful with status code of 404
        And verify that the content-type is application/problem+json
        And verify that title key contains Not found
        And verify that detail key from the error response displays invalid project warning for pageviews

  Scenario: Be able to run the AQS page analytics top endpoint with invalid access
        Given request is made to the page analytics top endpoint with project and invalid access
        Then the request should be successful with status code of 400
        And verify that the content-type is application/problem+json
        And verify that method key contains get
        And verify that title key contains Bad Request
        And verify that detail key from the error response displays invalid access warning for pageviews

  Scenario: Be able to run the AQS page analytics top endpoint with invalid year
        Given request is made to the page analytics top endpoint with project, invalid year entry
        Then the request should be successful with status code of 400
        And verify that the content-type is application/problem+json
        And verify that method key contains get
        And verify that detail key from the error response displays invalid year warning for pageviews

  Scenario: Be able to run the AQS page analytics top endpoint with invalid month
        Given request is made to the page analytics top endpoint with project, invalid month entry
        Then the request should be successful with status code of 400
        And verify that the content-type is application/problem+json
        And verify that method key contains get
        And verify that detail key from the error response displays invalid month warning for pageviews

  Scenario: Be able to run the AQS page analytics top endpoint with invalid day
        Given request is made to the page analytics top endpoint with project, invalid day entry
        Then the request should be successful with status code of 400
        And verify that the content-type is application/problem+json
        And verify that method key contains get
        And verify that detail key from the error response displays invalid day warning for pageviews

  Scenario Outline: Be able to run the AQS page analytics top endpoint with missing data in for years
        Given request is made to the page analytics top endpoint with project, <missing_data_years>
        Then the request should be successful with status code of 404
        And verify that the content-type is application/problem+json
        And verify that method key contains get
        And verify that title key contains Not found
#        And verify that detail key from the error response displays invalid data warning for pageviews
    Examples:
      | missing_data_years  |
      | 2009  |
      | 2010  |
      | 2011  |
      | 2012  |
      | 2013  |
      | 2014  |
      | 2015  |

  Scenario Outline: Be able to run the AQS page analytics top endpoint with special characters
        Given request is made to the page analytics top endpoint with <special_characters>
        Then the request should be successful with status code of 404
        And verify that the content-type is application/problem+json
        And verify that method key contains get
        And verify that title key contains Not found
#        And verify that detail key from the error response displays invalid character warning for pageviews
    Examples:
      | special_characters  |
      | @  |
      | ~ |
      | %  |
      | !  |

 # The below test scenario has been commented out due to defect reported in : https://phabricator.wikimedia.org/T356797
#  Scenario: Be able to run the invalid AQS page analytics top endpoint route
#        Given request is made to an invalid page analytics top endpoint route with project, invalid day entry
#        Then the request should be successful with status code of 404
#        And verify that the content-type is application/problem+json
#        And verify that title key contains Not found
#        And verify that method key contains get

  Scenario: Validate the first and last in articles data for year 2020 using en.wikipedia project
        Given request is made to page analytics top endpoint with en.wikipedia project, year 2018, month 01 and day 01
        Then the request should be successful with status code of 200
        And verify the value of the views key in the first json of the articles key is equal to 18300927
        And verify the value of the rank key in the first json of the articles key is equal to 1
        And verify the value of the views key in the last json of the articles key is equal to 11497
        And verify the value of the rank key in the first json of the articles key is equal to 1000

  Scenario: Validate the first and last in articles data for year 2020 using en.wikipedia project
        Given request is made to page analytics top endpoint with en.wikipedia project, year 2019, month 01 and day 01
        Then the request should be successful with status code of 200
        And verify the value of the views key in the first json of the articles key is equal to 16865930
        And verify the value of the rank key in the first json of the articles key is equal to 1
        And verify the value of the views key in the last json of the articles key is equal to 11798
        And verify the value of the rank key in the first json of the articles key is equal to 1000

  Scenario: Validate the first and last in articles data for year 2020 using en.wikipedia project
        Given request is made to page analytics top endpoint with en.wikipedia project, year 2020, month 01 and day 01
        Then the request should be successful with status code of 200
        And verify the value of the views key in the first json of the articles key is equal to 11673899
        And verify the value of the rank key in the first json of the articles key is equal to 1
        And verify the value of the views key in the last json of the articles key is equal to 11526
        And verify the value of the rank key in the first json of the articles key is equal to 1000

Scenario: Validate the first and last in articles data for year 2020 using de.wikipedia project
        Given request is made to page analytics top endpoint with de.wikipedia project, year 2018, month 01 and day 01
        Then the request should be successful with status code of 200
        And verify the value of the views key in the first json of the articles key is equal to 1410541
        And verify the value of the rank key in the first json of the articles key is equal to 1
        And verify the value of the views key in the last json of the articles key is equal to 2413
        And verify the value of the rank key in the first json of the articles key is equal to 1000

Scenario: Validate the first and last in articles data for year 2020 using et.wikipedia project
        Given request is made to page analytics top endpoint with et.wikipedia project, year 2018, month 01 and day 01
        Then the request should be successful with status code of 200
        And verify the value of the views key in the first json of the articles key is equal to 9628
        And verify the value of the rank key in the first json of the articles key is equal to 1
        And verify the value of the views key in the last json of the articles key is equal to 895
        And verify the value of the rank key in the first json of the articles key is equal to 1000

     Scenario Outline: Verify valid data projects for page analytics top
        Given request is made to page analytics top endpoint with valid data <projects>
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8

        Examples:
            | projects |
            | de.wikipedia.org |
            | et.wikipedia.org |
            | fr.wikipedia.org |
            | ng.wikipedia.org |
            | be.wikipedia.org |
            | ca.wikipedia.org |
            | es.wikisource.org |
            | et.wikisource.org |
            | de.wikisource.org |
            | fr.wikisource.org |
            | be.wikisource.org |
           | zh.wikisource.org |
           | sw.wikipedia.org |
           | bcl.wikipedia.org |
            | diq.wikipedia.org |
           | nl.wikibooks.org |
           | id.wikibooks.org |
           | ja.wikisource.org |
           | lmo.wikipedia.org |
           | ta.wiktionary.org |
           | tr.wikiquote.org |
           | pa.wikipedia.org |
           | he.wikiquote.org |
           | min.wikipedia.org |
           | pt.wikinews.org |
            | sv.wikibooks.org |
            | fr.wikipedia.org |
            | pl.wikipedia.org |


    Scenario Outline: Verify invalid data projects for page analytics top
        Given request is made to page analytics top endpoint with invalid data <projects>
        Then the request should be successful with status code of 404
        And verify that the content-type is application/problem+json

        Examples:
            | projects |
            | jp.wikipedia.org |
            | ke.wikipedia.org |
            | td.wikipedia.org |
            | tz.wikipedia.org |
            | jp.wikisource.org |
            | ke.wikisource.org |
            | ng.wikisource.org |
            | cd.wikisource.org |
            | td.wikisource.org |
            | bd.wikipedia |
            | bb.wikipedia |
            | by.wikipedia |
            | in.wikipedia |