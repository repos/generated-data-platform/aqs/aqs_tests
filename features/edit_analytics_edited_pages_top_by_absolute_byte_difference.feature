@aqs_tests @aqs_tests.edit_analytics @aqs_tests.edit_analytics_edited_pages_top_by_absolute_bytes_difference

Feature: Tests for Edits analytics Edited Pages top by absolute bytes difference


    # Business Need: Positive Scenarios

    Scenario Outline: Be able to run the AQS Edit analytics Edited Pages top by absolute bytes difference with varying projects
        Given request is made to the Edit analytics Edited Pages top by absolute bytes difference endpoint with varying <projects>, editor_types, all-page-types, 2018, 01
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that items key with sub_key project is of type string
        And verify that items key with sub_key editor-type is of type string
        And verify that items key with sub_key page-type is of type string
        And verify that items key with sub_key granularity is of type string
        And verify that items key with sub_key results sub_key contains timestamp of type string
        And verify the page_title key in the top sub key
        #And verify the abs_bytes_diff key in the top sub key
        And verify the rank key in the top sub key
        And verify that the project in the response is the same as the <projects> in the endpoint
        Examples:
            | projects |
            | de.wikipedia |
            | et.wikipedia |
            | fr.wikipedia |
            | sw.wikipedia |
            | ca.wikipedia |
            | es.wikisource |
            | et.wikisource |
            | de.wikisource |
            | fr.wikisource|
           | zh.wikisource |
           | sw.wikipedia|
           | bcl.wikipedia|
            | diq.wikipedia|
           | nl.wikibooks|
           | id.wikibooks|
           | ja.wikisource |
           | lmo.wikipedia |
           | ta.wiktionary |
           | tr.wikiquote |
           | pa.wikipedia |
           | he.wikiquote |
           | min.wikipedia |
           | pt.wikinews|
            | fr.wikipedia |
            | pl.wikipedia |

    Scenario Outline: Be able to run the AQS Edit analytics Edited Pages top by absolute bytes difference with varying editor types
        Given request is made to the Edit analytics Edited Pages top by absolute bytes difference endpoint with sw.wikipedia, varying <editor_types>, all-page-types, daily, 20180101, 20181231
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that items key with sub_key project is of type string
        And verify that items key with sub_key editor-type is of type string
        And verify that items key with sub_key page-type is of type string
        And verify that items key with sub_key granularity is of type string
        And verify that items key with sub_key results sub_key contains timestamp of type string
        And verify the page_title key in the top sub key
        #And verify the edits key in the top sub key
        #And verify the abs_bytes_diff key in the top sub key
        And verify that the editor_type in the response is same as the <editor_types> in the edited pages new endpoint
        Examples:
            | editor_types |
            | all-editor-types |
            | anonymous |
#            | group-bot |
#            | name-bot |
            | user |

    Scenario Outline: Be able to run the AQS Edit analytics Edited Pages top by absolute bytes difference endpoint with varying page-types
        Given request is made to the Edit analytics Edited Pages top by absolute bytes difference endpoint with sw.wikipedia, editor_types, varying <page_types>, daily, 20200101, 20201201
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that items key with sub_key project is of type string
        And verify that items key with sub_key editor-type is of type string
        And verify that items key with sub_key page-type is of type string
        And verify that items key with sub_key granularity is of type string
        And verify that items key with sub_key results sub_key contains timestamp of type string
        And verify the page_title key in the top sub key
        And verify the rank key in the top sub key
        #And verify the abs_bytes_diff key in the top sub key
        And verify that the page_type in the response is the same as the <page_types> in the edited page new endpoint
        Examples:
            | page_types |
            | all-page-types |
            | content |
            | non-content |

   Scenario: Be able to run the AQS Edit analytics Edited Pages top by absolute bytes difference endpoint with invalid year
      Given request is made to the AQS Edit analytics Edited Pages top by absolute bytes difference endpoint with invalid year
      Then the request should be successful with status code of 400
      And verify that the content-type is application/problem+json
      And verify that method key contains get
      And verify that title key contains Bad Request
      And verify that detail key contains Given year/month/day is invalid date

  Scenario: Be able to run the AQS Edit analytics edited pages top by edits endpoint with invalid month
      Given request is made to the AQS Edit analytics Edited Pages top by absolute bytes difference endpoint with invalid month
      Then the request should be successful with status code of 400
      And verify that the content-type is application/problem+json
      And verify that method key contains get
      And verify that title key contains Bad Request
      And verify that detail key contains Given year/month/day is invalid date

  Scenario: Be able to run the AQS Edit analytics Edited Pages top by absolute bytes difference endpoint with invalid day
      Given request is made to the AQS Edit analytics Edited Pages top by absolute bytes difference endpoint with invalid day
      Then the request should be successful with status code of 400
      And verify that the content-type is application/problem+json
      And verify that method key contains get
      And verify that title key contains Bad Request
      And verify that detail key contains Given year/month/day is invalid date
