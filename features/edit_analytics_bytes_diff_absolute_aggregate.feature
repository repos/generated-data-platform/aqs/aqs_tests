@aqs_tests @aqs_tests.edit_analytics @aqs_tests.edit_analytics_bytes_diff_absolute_aggregate

Feature: Tests for Edit analytics Bytes Difference absolute aggregate


    # Business Need: Positive Scenarios
  Scenario Outline: Be able to run the AQS Edit analytics bytes difference absolute aggregate with varying projects
        Given request is made to the bytes difference absolute aggregate endpoint with varying <projects>, editor_types, all-page-types, daily, 20180101, 20181231
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that items key with sub_key project is of type string
        And verify that items key with sub_key editor-type is of type string
        And verify that items key with sub_key page-type is of type string
        And verify that items key with sub_key granularity is of type string
        And verify that items key with sub_key results sub_key contains abs_bytes_diff of type int
        And verify that items key with sub_key results sub_key contains timestamp of type string
        And verify that the project in the response is the same as the <projects> in the endpoint
        Examples:
            | projects |
            | de.wikipedia |
            | et.wikipedia |
            | fr.wikipedia |
         #   | ng.wikipedia |
            | be.wikipedia |
            | sw.wikipedia |
         #   | ke.wikipedia |
            | ca.wikipedia |
            | pl.wikipedia |
             | bcl.wikipedia|
            | diq.wikipedia|
            | sw.wikipedia|
            | lmo.wikipedia |
            | fr.wikipedia |

 Scenario Outline: Be able to run the AQS Edit analytics bytes difference absolute difference with varying editor types
        Given request is made to the bytes difference absolute aggregate endpoint with project, varying <editor_types>, all-page-types, daily, 20180101, 20181231
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that items key with sub_key project is of type string
        And verify that items key with sub_key editor-type is of type string
        And verify that items key with sub_key page-type is of type string
        And verify that items key with sub_key granularity is of type string
        And verify that items key with sub_key results sub_key contains abs_bytes_diff of type int
        And verify that items key with sub_key results sub_key contains timestamp of type string
        And verify that the editor-type in the response is the same as the <editor_types> in the endpoint
        Examples:
            | editor_types |
            | all-editor-types |
            | anonymous |
            | group-bot |
            | name-bot |
            | user |

   Scenario Outline: Be able to run the AQS Edit analytics bytes difference absolute difference with varying granularity
        Given request is made to the bytes difference absolute aggregate endpoint with project, editor_types, all-page-types, varying <granularity>, 20200101, 20201201
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that items key with sub_key project is of type string
        And verify that items key with sub_key editor-type is of type string
        And verify that items key with sub_key page-type is of type string
        And verify that items key with sub_key granularity is of type string
        And verify that items key with sub_key results sub_key contains abs_bytes_diff of type int
        And verify that items key with sub_key results sub_key contains timestamp of type string
        And verify that the granularity in the response is the same as the <granularity> in the absolute diff endpoint
        Examples:
            | granularity |
            | daily |
            | monthly |

     Scenario: Be able to run the AQS Edit analytics bytes difference absolute difference with invalid project
        Given request is made to the Edit analytics bytes difference absolute aggregate with invalid project
        Then the request should be successful with status code of 404
        And verify that the content-type is application/problem+json
        And verify that method key contains get

     Scenario: Be able to run the AQS Edit analytics bytes difference absolute aggregate with special character in project
        Given request is made to the Edit analytics bytes difference absolute aggregate with special character in the project
        Then the request should be successful with status code of 400
        And verify that the content-type is application/problem+json
        And verify that method key contains get
        And verify that detail key from the error response displaying the special character warning for absolute difference

    Scenario: Be able to run the Edit analytics bytes difference absolute aggregate with invalid editor type
      Given request is made to the bytes difference absolute aggregate with invalid editor type
      Then the request should be successful with status code of 400
      And verify that the content-type is application/problem+json
      And verify that method key contains get
      And verify that title key contains Bad Request
      And verify that detail key from the error response for invalid editor type for byte diff absolute aggregate

   Scenario: Be able to run the Edit analytics bytes difference absolute aggregate with invalid granularity
      Given request is made to the bytes difference absolute aggregate with invalid granularity
      Then the request should be successful with status code of 400
      And verify that the content-type is application/problem+json
      And verify that method key contains get
      And verify that title key contains Bad Request
      And verify that detail key from the error response for invalid granularity for byte diff absolute aggregate

   Scenario: Be able to run the Edit analytics bytes difference absolute aggregate with invalid start date
      Given request is made to the bytes difference absolute aggregate with invalid start date
      Then the request should be successful with status code of 400
      And verify that the content-type is application/problem+json
      And verify that method key contains get
      And verify that title key contains Bad Request
      And verify that detail key from the error response for invalid start date for byte diff absolute aggregate

   Scenario: Be able to run the Edit analytics bytes difference absolute aggregate with invalid end date
      Given request is made to the bytes difference absolute aggregate with invalid end date
      Then the request should be successful with status code of 400
      And verify that the content-type is application/problem+json
      And verify that method key contains get
      And verify that title key contains Bad Request
      And verify that detail key from the error response shows invalid end date for byte diff absolute aggregate

   Scenario: Be able to run the Edit analytics bytes difference absolute aggregate with end date before start date
      Given request is made to the bytes difference absolute aggregate with end date before start date
      Then the request should be successful with status code of 400
      And verify that the content-type is application/problem+json
      And verify that method key contains get
      And verify that title key contains Bad Request
      And verify that detail key from the error response for end date before start date for byte diff absolute aggregate

   Scenario: Be able to run the Edit analytics bytes difference absolute aggregate with no full months specified
      Given request is made to the bytes difference absolute aggregate with no full months specified
      Then the request should be successful with status code of 400
      And verify that the content-type is application/problem+json
      And verify that method key contains get
      And verify that title key contains Bad Request
      And verify that detail key from the error response for with no full months specified for byte diff absolute aggregate