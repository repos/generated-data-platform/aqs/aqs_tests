#BDD steps for the page_analytics_top_by_country endpoint test scenarios

  # Page analytics Top was formerly called Pageviews Top

@aqs_tests @aqs_tests.page_analytics @aqs_tests.page_analytics_top_by_country

Feature: Tests for page analytics top by country endpoint

    Scenario Outline: Be able to run the AQS page analytics top by country endpoint with valid parameters
        Given request is made to the page analytics top by country endpoint with project, <access>, year, month
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that items key contains project in list of arrays
        And verify that items key contains year in list of arrays
        And verify that items key contains month in list of arrays
        And verify that items key with access in is equal to <access>
        And verify that items key with sub_key countries contains country in the list
        And verify that items key with sub_key countries sub_key contains country of type string
        And verify that items key with sub_key countries sub_key contains country has upper case values
        And verify that items key with sub_key countries sub_key contains country has length of two(2)
        And verify that items key with sub_key countries contains views in the list
        And verify that items key with sub_key countries sub_key contains views of type string
        And verify that items key with sub_key countries sub_key contains views with character '-'
        And verify that items key with sub_key countries contains rank in the list
        And verify that items key with sub_key countries sub_key contains rank of type int
        And verify that items key with sub_key countries contains views_ceil in the list
        And verify that items key with sub_key countries sub_key contains views_ceil of type int
      Examples:
        | access |
        | all-access |
        | desktop |
        | mobile-app |
        | mobile-web |

  Scenario Outline: Be able to run the AQS page analytics top by country endpoint with valid data and varying year
        Given request is made to the page analytics top by country endpoint with varying <year>
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that items key contains project in list of arrays
        And verify that items key contains year in list of arrays
        And verify that items key contains month in list of arrays
        And verify that items key with access in is equal to all-access
        And verify that items key with sub_key countries contains country in the list
        And verify that items key with sub_key countries sub_key contains country of type string
        And verify that items key with sub_key countries sub_key contains country has upper case values
        And verify that items key with sub_key countries sub_key contains country has length of two(2)
        And verify that items key with sub_key countries contains views in the list
        And verify that items key with sub_key countries sub_key contains views of type string
        And verify that items key with sub_key countries sub_key contains views with character '-'
        And verify that items key with sub_key countries contains rank in the list
        And verify that items key with sub_key countries sub_key contains rank of type int
        And verify that items key with sub_key countries contains views_ceil in the list
        And verify that items key with sub_key countries sub_key contains views_ceil of type int
      Examples:
        | year |
        | 2016 |
        | 2017 |
        | 2018 |
        | 2019 |
        | 2020 |
        | 2021 |
        | 2022 |


  Scenario Outline: Be able to run the AQS page analytics top by country endpoint with different projects
        Given request is made to the page analytics top by country endpoint with <project>, access, year, month
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that items key contains project in list of arrays
        And verify that items key with project in is equal to <project>
        And verify that items key with sub_key countries sub_key contains views with character '-'
        And verify the year in the request matches year in the response

    Examples:
      | project |
      | es.wikipedia |
      | en.wikipedia |
      | fr.wikipedia |
      | zh.wikipedia |
      | uk.wikipedia |
      | ng.wikipedia |
      | lo.wikipedia |

  Scenario: Be able to run the AQS page analytics top by country endpoint with invalid project
        Given request is made to the page analytics top by country endpoint with invalid project
        Then the request should be successful with status code of 404
        And verify that title key contains Not found
        And verify that method key contains get
        And verify that detail key from the error response displays invalid project warning for pageviews_top_by_country

  Scenario: Be able to run the AQS page analytics top by country endpoint with invalid access
        Given request is made to the page analytics top by country endpoint with invalid access
        Then the request should be successful with status code of 400
        And verify that title key contains Bad Request
        And verify that method key contains get
        And verify that detail key from the error response displays invalid access warning for pageviews_top_by_country

  Scenario: Be able to run the AQS page analytics top by country endpoint with invalid year
        Given request is made to the page analytics top by country endpoint with invalid year
        Then the request should be successful with status code of 400
        And verify that method key contains get
        And verify that detail key from the error response displays invalid year warning for pageviews_top_by_country

  Scenario: Be able to run the AQS page analytics top by country endpoint with invalid month
        Given request is made to the page analytics top by country endpoint with invalid month
        Then the request should be successful with status code of 400
        And verify that method key contains get
        And verify that detail key from the error response displays invalid month warning for pageviews_top_by_country

  Scenario Outline: Be able to run the AQS page analytics top by country endpoint with invalid character in the project
        Given request is made to the page analytics top by country endpoint with invalid character <projects>
        Then the request should be successful with status code of 400
        And verify that method key contains get
        And verify that detail key from the error response displays invalid character

    Examples:
      | projects |
      | lo.wikipedia* |
      | lo.wikipedia& |
      | lo.wikipedia@ |
      | lo.wikipedia^ |

  Scenario: Be able to run the AQS page analytics top by country endpoint with invalid route
        Given request is made to the page analytics top by country endpoint with invalid route
        Then the request should be successful with status code of 404
        And verify that title key contains Not found
        And verify that method key contains get

  Scenario Outline: Be able to run the AQS page analytics top by country endpoint with years with no data
        Given request is made to the page analytics top by country endpoint with <year> with no data
        Then the request should be successful with status code of 404
        And verify that title key contains Not found
        And verify that method key contains get
        And verify that detail key from the error response displays invalid project warning for pageviews_top_by_country

    Examples:
      | year |
      | 2008 |
      | 2009 |
      | 2010 |
      | 2011 |
      | 2012 |
      | 2013 |
      | 2014 |

  Scenario: Validate the first and last data in page analytics top by country for year 2018 using en.wikipedia project
        Given request is made to page analytics top by country endpoint with en.wikipedia project, year 2018, month 01
        Then the request should be successful with status code of 200
        And verify the value of the views_ceil key in the first json of the countries key is equal to 3552068000
        And verify the value of the rank key in the first json of the countries key is equal to 1
        And verify the value of the views_ceil key in the last json of the countries key is equal to 1000
        And verify the value of the rank key in the first json of the countries key is equal to 246

    Scenario: Validate the first and last data in page analytics top by country for year 2019 using en.wikipedia project
        Given request is made to page analytics top by country endpoint with en.wikipedia project, year 2019, month 01
        Then the request should be successful with status code of 200
        And verify the value of the views_ceil key in the first json of the countries key is equal to 3305556000
        And verify the value of the rank key in the first json of the countries key is equal to 1
        And verify the value of the views_ceil key in the last json of the countries key is equal to 1000
        And verify the value of the rank key in the first json of the countries key is equal to 244

    Scenario: Validate the first and last data in page analytics top by country for year 2020 using en.wikipedia project
        Given request is made to page analytics top by country endpoint with en.wikipedia project, year 2020, month 01
        Then the request should be successful with status code of 200
        And verify the value of the views_ceil key in the first json of the countries key is equal to 3546667000
        And verify the value of the rank key in the first json of the countries key is equal to 1
        And verify the value of the views_ceil key in the last json of the countries key is equal to 1000
        And verify the value of the rank key in the first json of the countries key is equal to 245


     Scenario Outline: Verify valid data projects for page analytics top by country
        Given request is made to page analytics top by country endpoint with valid data <projects>
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8

        Examples:
            | projects |
            | de.wikipedia.org |
            | et.wikipedia.org |
            | fr.wikipedia.org |
            | ng.wikipedia.org |
            | be.wikipedia.org |
            | ca.wikipedia.org |
            | es.wikisource.org |
            | et.wikisource.org |
            | de.wikisource.org |
            | fr.wikisource.org |
            | be.wikisource.org |
           | zh.wikisource.org |
           | sw.wikipedia.org |
           | bcl.wikipedia.org |
            | diq.wikipedia.org |
           | nl.wikibooks.org |
           | id.wikibooks.org |
           | ja.wikisource.org |
           | lmo.wikipedia.org |
           | ta.wiktionary.org |
           | tr.wikiquote.org |
           | pa.wikipedia.org |
           | he.wikiquote.org |
           | min.wikipedia.org |
           | pt.wikinews.org |
            | sv.wikibooks.org |
            | fr.wikipedia.org |
            | pl.wikipedia.org |


    Scenario Outline: Verify invalid data projects for page analytics top by country
        Given request is made to page analytics top by country endpoint with invalid data <projects>
        Then the request should be successful with status code of 404
        And verify that the content-type is application/problem+json

        Examples:
            | projects |
            | jp.wikipedia.org |
            | ke.wikipedia.org |
            | td.wikipedia.org |
            | tz.wikipedia.org |
            | jp.wikisource.org |
            | ke.wikisource.org |
            | ng.wikisource.org |
            | cd.wikisource.org |
            | td.wikisource.org |
            | bd.wikipedia |
            | bb.wikipedia |
            | by.wikipedia |
            | in.wikipedia |
