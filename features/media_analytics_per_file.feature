#BDD steps for the media analytics per file endpoint test scenarios

@aqs_tests @aqs_tests.media_analytics @aqs_tests.media_analytics_per_file

Feature: Tests for media analytics per file endpoint

    Scenario Outline: Be able to run the AQS media analytics per file endpoint with varying referer
        Given request is made to the media analytics per file endpoint with varying <referer>
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that items key contains referer in list of arrays
        And verify that items key contains file_path in list of arrays
        And verify that items key contains granularity in list of arrays
        And verify that items key contains timestamp in list of arrays
        And verify that items key contains agent in list of arrays
        And verify that items key contains requests in list of arrays
        And verify that items key with sub_key requests is of type integer
        And verify that items key with sub_key file_path is of type string
        And verify that items key with sub_key granularity is of type string
        And verify that items key with sub_key timestamp is of type string
        And verify that items key with sub_key agent is of type string
        And verify that items key with sub_key requests is of type integer
        And verify that granularity sub_key in items key is equal to daily
        And verify that the <referer> used in request is same as the referer in response body

      Examples:
        | referer |
        | all-referers |
        | internal |
        | external |
        | search-engine |
        | unknown |
        | none |
        | en.wikipedia  |


  Scenario Outline: Be able to run the AQS media analytics per file endpoint with varying agents
        Given request for media analytics per file endpoint is made with varying <agents>
        Then the request should be successful with status code of 200
        And verify that items key contains referer in list of arrays
        And verify that items key contains file_path in list of arrays
        And verify that items key contains granularity in list of arrays
        And verify that items key contains timestamp in list of arrays
        And verify that items key contains agent in list of arrays
        And verify that items key contains requests in list of arrays
        And verify that items key with sub_key requests is of type integer
        And verify that items key with sub_key file_path is of type string
        And verify that items key with sub_key granularity is of type string
        And verify that items key with sub_key timestamp is of type string
        And verify that items key with sub_key agent is of type string
        And verify that items key with sub_key requests is of type integer
        And verify that granularity sub_key in items key is equal to daily
        And verify that referer sub_key in items key is equal to all-referers
        And verify that the <agents> used in request is same as the agents in response body

    Examples:
      | agents |
      | all-agents |
      | user |
      | spider |

   Scenario Outline: Run the AQS media analytics per file endpoint with varying referer and monthly granularity
        Given request is made to the media analytics per file endpoint with monthly granularity and varying <referer>
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that items key contains referer in list of arrays
        And verify that items key contains file_path in list of arrays
        And verify that items key contains granularity in list of arrays
        And verify that items key contains timestamp in list of arrays
        And verify that items key contains agent in list of arrays
        And verify that items key contains requests in list of arrays
        And verify that items key with sub_key requests is of type integer
        And verify that items key with sub_key file_path is of type string
        And verify that items key with sub_key granularity is of type string
        And verify that items key with sub_key timestamp is of type string
        And verify that items key with sub_key agent is of type string
        And verify that items key with sub_key requests is of type integer
        And verify that granularity sub_key in items key is equal to monthly
        And verify that agent sub_key in items key is equal to all-agents
        And verify that the <referer> used in request is same as the referer in response body

      Examples:
        | referer |
        | all-referers |
        | internal |
        | external |
        | search-engine |
        | unknown |
        | none |
        | en.wikipedia  |

   Scenario Outline: Run the AQS media analytics per file endpoint with varying agents and monthly granularity
        Given request for media analytics per file endpoint is made using monthly granularity and varying <agents>
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that items key contains referer in list of arrays
        And verify that items key contains file_path in list of arrays
        And verify that items key contains granularity in list of arrays
        And verify that items key contains timestamp in list of arrays
        And verify that items key contains agent in list of arrays
        And verify that items key contains requests in list of arrays
        And verify that items key with sub_key requests is of type integer
        And verify that items key with sub_key file_path is of type string
        And verify that items key with sub_key granularity is of type string
        And verify that items key with sub_key timestamp is of type string
        And verify that items key with sub_key agent is of type string
        And verify that items key with sub_key requests is of type integer
        And verify that granularity sub_key in items key is equal to monthly
        And verify that referer sub_key in items key is equal to all-referers
        And verify that the <agents> used in request is same as the agents in response body

    Examples:
      | agents |
      | all-agents |
      | user |
      | spider |

  Scenario Outline: Run the AQS media analytics per file endpoint with varying agents and monthly granularity
        Given request for media analytics per file endpoint with monthly granularity, hourly date and varying <agents>
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that items key contains referer in list of arrays
        And verify that items key contains file_path in list of arrays
        And verify that items key contains granularity in list of arrays
        And verify that items key contains timestamp in list of arrays
        And verify that items key contains agent in list of arrays
        And verify that items key contains requests in list of arrays
        And verify that items key with sub_key requests is of type integer
        And verify that items key with sub_key file_path is of type string
        And verify that items key with sub_key granularity is of type string
        And verify that items key with sub_key timestamp is of type string
        And verify that items key with sub_key agent is of type string
        And verify that items key with sub_key requests is of type integer
        And verify that granularity sub_key in items key is equal to monthly
        And verify that referer sub_key in items key is equal to all-referers
        And verify that the <agents> used in request is same as the agents in response body

    Examples:
      | agents |
      | all-agents |
      | user |
      | spider |

  Scenario: Be able to run the AQS media analytics per file endpoint with invalid project
      Given request is made to the media analytics per file endpoint with invalid project
      Then the request should be successful with status code of 404
      And verify that the content-type is application/problem+json
      And verify that method key contains get
      And verify that title key contains Not found
      And verify that detail key from the error response for invalid date or project

  Scenario: Be able to run the AQS media analytics per file endpoint with invalid agent
      Given request is made to the media analytics per file endpoint with invalid agent
      Then the request should be successful with status code of 400
      And verify that the content-type is application/problem+json
      And verify that method key contains get
      And verify that title key contains Bad Request
      And verify that detail key from the error response displays invalid agent validation

  Scenario: Be able to run the AQS media analytics per file endpoint with invalid file path
      Given request is made to the media analytics per file endpoint with invalid file path
      Then the request should be successful with status code of 404
      And verify that the content-type is application/problem+json
      And verify that method key contains get
      And verify that title key contains Not found
      And verify that detail key from the error response for invalid date or project

  Scenario: Be able to run the AQS media analytics per file endpoint with invalid granularity
      Given request is made to the media analytics per file endpoint with invalid granularity
      Then the request should be successful with status code of 400
      And verify that the content-type is application/problem+json
      And verify that method key contains get
      And verify that title key contains Bad Request
      And verify that detail key from the error response displays invalid granularity validation

  Scenario: Be able to run the AQS media analytics per file endpoint with invalid start time
      Given request is made to the media analytics per file endpoint with invalid start time
      Then the request should be successful with status code of 400
      And verify that the content-type is application/problem+json
      And verify that method key contains get
      And verify that detail key from the error response displays invalid start time validation

  Scenario: Be able to run the AQS media analytics per file endpoint with invalid end time
      Given request is made to the media analytics per file endpoint with invalid end time
      Then the request should be successful with status code of 400
      And verify that the content-type is application/problem+json
      And verify that method key contains get
      And verify that detail key from the error response displays invalid end time validation

    Scenario: Be able to run the AQS media analytics per file endpoint with incomplete month range
      Given request is made to the media analytics per file endpoint with incomplete month range
      Then the request should be successful with status code of 400
      And verify that the content-type is application/problem+json
      And verify that method key contains get
      And verify that detail key from the error response displays incomplete month range validation

   Scenario Outline: Be able to run the AQS media analytics per file endpoint with invalid domain referer
        Given request is made to the media analytics per file endpoint with invalid domain <referer>
        Then the request should be successful with status code of 404
        And verify that method key contains get
        And verify that title key contains Not found
        And verify that detail key from the error response for invalid date or project

      Examples:
        | referer |
        | de.wikipedia |
        | et.wikipedia |
       | ke.wikipedia |
       | ng.wikipedia |
       | fr.wikipedia |
       | tz.wikipedia |

   Scenario: Verify the first and last data with daily granularity
      Given request is made to the media analytics per file endpoint with 20200101 and 20200110 start and end times
      Then the request should be successful with status code of 200
      And verify that the content-type is application/json; charset=utf-8
      And verify the value of the requests key in the first json is equal to 416559
      And verify the value of the requests key in the last json is equal to 440708

   Scenario: Verify the first and last data with monthly granularity
      Given request is made to the media analytics per file endpoint with 20200101 and 20200331 start and end times
      Then the request should be successful with status code of 200
      And verify that the content-type is application/json; charset=utf-8
      And verify the value of the requests key in the first json is equal to 15604205
      And verify the value of the requests key in the last json is equal to 13078137

     Scenario Outline: Validate specific media analytics per file with special characters
       Given request is made to the media analytics per file endpoint with <special_file>, daily, 20220101 and 20220131
       Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that items key contains referer in list of arrays
        And verify that items key contains file_path in list of arrays
        And verify that items key contains granularity in list of arrays
        And verify that items key contains timestamp in list of arrays
        And verify that items key contains agent in list of arrays
        And verify that items key contains requests in list of arrays
        And verify that items key with sub_key requests is of type integer
        And verify that items key with sub_key file_path is of type string
        And verify that items key with sub_key granularity is of type string
        And verify that items key with sub_key timestamp is of type string
        And verify that items key with sub_key agent is of type string
        And verify that items key with sub_key requests is of type integer


       Examples:
         |  special_file |
         | %2Fwikipedia%2Fcommons%2Fb%2Fbd%2FTitan_(moon).ogg                                                                                                              |
          | %2Fwikipedia%2Fcommons%2F0%2F0e%2FAngkor_-_Zentrum_des_K%C3%B6nigreichs_der_Khmer_(CC_BY-SA_4.0).webm |
          | %2Fwikipedia%2Fcommons%2Fb%2Fbd%2FTitan_%28moon%29.ogg |
          | %2Fwiktionary%2Fte%2F4%2F40%2Fpeacocks.JPG             |





  Scenario Outline: Validate specific media analytics per file with special characters ( part 2)
       Given request is made to the media analytics per file endpoint with <special_file>, daily, 2023080100 and 2023090100
       Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that items key contains referer in list of arrays
        And verify that items key contains file_path in list of arrays
        And verify that items key contains granularity in list of arrays
        And verify that items key contains timestamp in list of arrays
        And verify that items key contains agent in list of arrays
        And verify that items key contains requests in list of arrays
        And verify that items key with sub_key requests is of type integer

    Examples:
         |  special_file |
         |   %2Fwikipedia%2Fcommons%2Fe%2Fef%2FAB_Tacksfabriken_och_konf-fabr._AB_Viking._Trollh%25C3%25A4ttan_FiBs_serien_-_Nordiska_museet_-_NMAx.0001508.tif          |

