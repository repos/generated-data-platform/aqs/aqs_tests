#BDD steps for the Common Impact Metrics (CIM) Edits per user monthly
@aqs_tests @aqs_tests.common_impact_metrics @aqs_tests.cim_edits_per_user_monthly

Feature: Tests for Common Impact Metrics : Edits per user Monthly
    # Business Need: Positive Scenarios

    # These are tests for Edits per user Monthly

   Scenario Outline: Be able to run the CIM Edits per user monthly endpoint with varying category
        Given request is made to the Edits per user monthly endpoint with varying users <user>, 20231101 and 20240601
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that response body with context key is of type dict
        And verify that the sub_key user-name in the context key response is equal to <user>
        And verify that the sub_key start in the context key response is equal to 2023110100
        And verify that the sub_key end in the context key response is equal to 2024060100
        Examples:
            | user |
            | 13243546A |
            | 15ferate |
            | 1989mohemd |
            | APneunzehn74  |


   Scenario Outline: Be able to run the CIM Edits per user monthly endpoint with varying edit type
        Given request made to Edits per user monthly endpoint with varying edit type <edit_type>, 20231101 and 20240601
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that response body with context key is of type dict
        And verify that the sub_key edit-type in the context key response is equal to <edit_type>
        And verify that the sub_key start in the context key response is equal to 2023110100
        And verify that the sub_key end in the context key response is equal to 2024060100
        Examples:
            | edit_type |
            | create |
            | update |
            | all-edit-types |


   Scenario: Be able to run the CIM Edits per user monthly endpoint with invalid Route
        Given request is made to the Edits per user monthly endpoint with invalid route
        Then the request should be unsuccessful with status code of 404
        And verify that the content-type is application/problem+json
        And verify that title key contains Not Found
        And verify that the detail in response is equal to invalid route
        And verify that the method in response is equal to get


   Scenario: Be able to run the CIM Edits per user monthly endpoint with invalid username
        Given request is made to the Edits per user monthly endpoint with invalid username
        Then the request should be unsuccessful with status code of 404
        And verify that the content-type is application/problem+json
        And verify that title key contains Not Found
        And verify that the method in response is equal to get
        And verify that the detail in response contains the user name you asked for is not loaded yet


  Scenario: Be able to run the CIM Edits per user monthly endpoint with invalid edit type
        Given request is made to the Edits per user monthly endpoint with invalid edit type
        Then the request should be unsuccessful with status code of 400
        And verify that the content-type is application/problem+json
        And verify that title key contains Bad Request
        And verify that the detail in response contains allowed values: [create,update,all-edit-types]

  Scenario: Be able to run the CIM Edits per user monthly endpoint with invalid start time
        Given request is made to the Edits per user monthly endpoint with invalid start time
        Then the request should be unsuccessful with status code of 400
        And verify that the content-type is application/problem+json
        And verify that title key contains Bad Request
        And verify that the detail in response contains start timestamp is invalid, must be a valid date in YYYYMMDD format


  Scenario: Be able to run the CIM Edits per user monthly endpoint with invalid end time
        Given request is made to the Edits per user monthly endpoint with invalid end time
        Then the request should be unsuccessful with status code of 400
        And verify that the content-type is application/problem+json
        And verify that title key contains Bad Request
        And verify that the detail in response contains end timestamp is invalid, must be a valid date in YYYYMMDD format


  # DATA TEST
  Scenario: Be able to run the CIM Edits per user monthly endpoint to validate category rank and pageview count
        Given request is made to the Edits per user monthly endpoint to validate timestamp and edit count
        Then the request should be unsuccessful with status code of 200
        And verify that the sub_key edit-type in the context key response is equal to all-edit-types
        And verify that the sub_key user-name in the context key response is equal to 13243546A
        And verify the value of the timestamp key in the items object first json is equal to text 2023110100
        And verify the value of the edit-count key in the first json is equal to 22
        And verify the value of the timestamp key in the items object last json is equal to text 2024010100
        And verify the value of the edit-count key in the last json is equal to 41
