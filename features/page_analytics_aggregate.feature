#BDD steps for the pageviews_aggregate endpoint test scenarios

  # Page analytics Top was formerly called Pageviews Top

@aqs_tests @aqs_tests.page_analytics @aqs_tests.page_analytics_aggregate

Feature: Tests for page analytics aggregate endpoint

    Scenario Outline: Be able to run the AQS page analytics aggregate endpoint with varying access
        Given request is made to the page analytics aggregate endpoint with varying <access>
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that items key contains project in list of arrays
        And verify that items key contains granularity in list of arrays
        And verify that items key contains agent in list of arrays
        And verify that items key contains timestamp in list of arrays
        And verify that items key contains access in list of arrays
        And verify that items key contains views in list of arrays
        And verify that items key with sub_key project is of type string
        And verify that items key with sub_key granularity is of type string
        And verify that items key with sub_key agent is of type string
        And verify that items key with sub_key timestamp is of type string
        And verify that items key with sub_key access is of type string
        And verify that items key with sub_key views is of type integer
        And verify that access sub_key in items key is equal to <access>

      Examples:
        | access |
        | all-access |
        | desktop |
        | mobile-app |
        | mobile-web |


   Scenario Outline: Be able to run the AQS page analytics aggregate endpoint with varying agents
        Given request is made to the page analytics aggregate endpoint with valid parameters and varying <agents>
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that items key contains project in list of arrays
        And verify that items key contains granularity in list of arrays
        And verify that items key contains agent in list of arrays
        And verify that items key contains timestamp in list of arrays
        And verify that items key contains access in list of arrays
        And verify that items key contains views in list of arrays
        And verify that items key with sub_key project is of type string
        And verify that items key with sub_key granularity is of type string
        And verify that items key with sub_key agent is of type string
        And verify that items key with sub_key timestamp is of type string
        And verify that items key with sub_key access is of type string
        And verify that items key with sub_key views is of type integer
        And verify that agent sub_key in items key is equal to <agents>

      Examples:
        | agents |
        | all-agents |
        | user |
        | spider |

  Scenario: Be able to run the AQS page analytics aggregate endpoint with automated agent
        Given request is made to the page analytics aggregate endpoint with automated agent
        Then the request should be successful with status code of 404
        And verify that method key contains get
        And verify that title key contains Not found
        And verify that detail key from the error response displays the error warning

  Scenario Outline: Be able to run the AQS page analytics aggregate endpoint with varying granularity
        Given request is made to the page analytics aggregate endpoint to test varying <granularity>
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that items key contains project in list of arrays
        And verify that items key contains granularity in list of arrays
        And verify that items key contains agent in list of arrays
        And verify that items key contains timestamp in list of arrays
        And verify that items key contains access in list of arrays
        And verify that items key contains views in list of arrays
        And verify that items key with sub_key project is of type string
        And verify that items key with sub_key granularity is of type string
        And verify that items key with sub_key agent is of type string
        And verify that items key with sub_key timestamp is of type string
        And verify that items key with sub_key access is of type string
        And verify that items key with sub_key views is of type integer
        And verify that granularity sub_key in items key is equal to <granularity>

      Examples:
        | granularity |
        | hourly |
        | daily |
        | monthly |

  Scenario Outline: Be able to run the AQS page analytics aggregate endpoint with varying granularity
        Given request is made to the page analytics aggregate endpoint for varying <projects>
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that project sub_key in items key is same as the <projects> in the request

    Examples:
      | projects |
      | fr.wikipedia |
      | es.wikipedia |
      | ng.wikipedia |
      | de.wikipedia |
      | it.wikipedia |
      | en.wikipedia |
      | de.wikipedia |
      | et.wikipedia |
      | fr.wikipedia |
      | ng.wikipedia |
      | be.wikipedia |
      | ca.wikipedia |
      | es.wikisource |
      | et.wikisource |
      | de.wikisource |
      | fr.wikisource |
      | be.wikisource |


  Scenario: Be able to run the AQS page analytics aggregate endpoint with invalid project
        Given request is made to the page analytics aggregate endpoint with invalid project
        Then the request should be successful with status code of 404
        And verify that title key contains Not found
        And verify that method key contains get
        And verify that detail key from the error response displays the error warning

  Scenario: Be able to run the AQS page analytics aggregate endpoint with invalid access
        Given request is made to the page analytics aggregate endpoint with invalid access
        Then the request should be successful with status code of 400
        And verify that title key contains Bad Request
        And verify that method key contains get
        And verify that detail key from the error response displays the access warning

  Scenario: Be able to run the AQS page analytics aggregate endpoint with invalid agent
        Given request is made to the page analytics aggregate endpoint with invalid agent
        Then the request should be successful with status code of 400
        And verify that title key contains Bad Request
        And verify that method key contains get
        And verify that detail key from the error response displays the agent warning

  Scenario: Be able to run the AQS page analytics aggregate endpoint with invalid granularity
        Given request is made to the page analytics aggregate endpoint with invalid granularity
        Then the request should be successful with status code of 400
        And verify that title key contains Bad Request
        And verify that method key contains get
        And verify that detail key from the error response displays the granularity warning

  Scenario: Be able to run the AQS page analytics aggregate endpoint with invalid start date
        Given request is made to the page analytics aggregate endpoint with invalid start date
        Then the request should be successful with status code of 400
        And verify that method key contains get
        And verify that detail key from the error response displays the start date warning

  Scenario: Be able to run the AQS page analytics aggregate endpoint with invalid end date
        Given request is made to the page analytics aggregate endpoint with invalid end date
        Then the request should be successful with status code of 400
        And verify that method key contains get
        And verify that detail key from the error response displays the end date warning

  Scenario: Validate the first and last data in page analytics aggregate for year 2018 using en.wikipedia project
        Given request is made to page analytics aggregate endpoint with daily, year 2018, month 01 and day 01-31
        Then the request should be successful with status code of 200
        And verify the value of the views key in the first json is equal to 302828330
        And verify the value of the views key in the last json is equal to 320642885

    Scenario: Validate the first and last data in page analytics aggregate for year 2019 using en.wikipedia project
        Given request is made to page analytics aggregate endpoint with daily, year 2019, month 01 and day 01-31
        Then the request should be successful with status code of 200
        And verify the value of the views key in the first json is equal to 312601914
        And verify the value of the views key in the last json is equal to 298057463

    Scenario: Validate the first and last data in page analytics aggregate for year 2020 using en.wikipedia project
        Given request is made to page analytics aggregate endpoint with daily, year 2020, month 01 and day 01-31
        Then the request should be successful with status code of 200
        And verify the value of the views key in the first json is equal to 301607319
        And verify the value of the views key in the last json is equal to 297766535

    Scenario Outline: Validate invalid data projects for page analytics aggregate on Mobile phone article
        Given request is made to page analytics aggregate endpoint with invalid data <projects>
        Then the request should be successful with status code of 404
        And verify that the content-type is application/problem+json
        And verify that method key contains get

        Examples:
            | projects |
            | jp.wikipedia.org |
            | ke.wikipedia.org |
            | td.wikipedia.org |
            | tz.wikipedia.org |
            | jp.wikisource.org  |
            | ke.wikisource.org   |
            | ng.wikisource.org    |
            | cd.wikisource.org    |
            | td.wikisource.org     |