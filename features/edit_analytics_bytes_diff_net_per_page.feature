@aqs_tests @aqs_tests.edit_analytics @aqs_tests.edit_analytics_bytes_diff_net_per_page

Feature: Tests for Edit analytics Bytes Difference Net per page


    # Business Need: Positive Scenarios
  Scenario Outline: Be able to run the AQS Edit analytics bytes difference net per page with varying projects
        Given request is made to the bytes difference net per page endpoint with varying <projects>, page_title, editor_types, all-page-types, daily, 20200101, 20201201
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that items key with sub_key project is of type string
        And verify that items key with sub_key editor-type is of type string
        And verify that items key with sub_key page-title is of type string
        And verify that items key with sub_key granularity is of type string
        And verify that items key with sub_key results sub_key contains net_bytes_diff of type int
        And verify that items key with sub_key results sub_key contains timestamp of type string
        And verify that the project in the response is the same as the <projects> in the endpoint
        Examples:
            | projects |
            | de.wikipedia |
            | en.wikipedia |
#            | et.wikipedia |
#            | fr.wikipedia |
#            | ng.wikipedia |
#            | be.wikipedia |
#            | sw.wikipedia |
#            | ke.wikipedia |
#            | ng.wikipedia |
#            | ca.wikipedia |
#            | pl.wikipedia |
#             | bcl.wikipedia|
#            | diq.wikipedia|
#            | sw.wikipedia|
#            | lmo.wikipedia |
#            | fr.wikipedia |


  Scenario Outline: Be able to run the AQS Edit analytics bytes difference net per page with varying editor types
        Given request is made to the bytes difference net per page endpoint with project, page_title, varying <editor_types>, all-page-types, daily, 20200101, 20201201
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that items key with sub_key project is of type string
        And verify that items key with sub_key editor-type is of type string
        And verify that items key with sub_key page-title is of type string
        And verify that items key with sub_key granularity is of type string
        And verify that items key with sub_key results sub_key contains net_bytes_diff of type int
        And verify that items key with sub_key results sub_key contains timestamp of type string
        And verify that the editor-type in the response is the same as the <editor_types> in the endpoint
        Examples:
            | editor_types |
            | all-editor-types |
            | anonymous |
            | group-bot |
#            | name-bot |
            | user |

  Scenario Outline: Be able to run the AQS Edit analytics bytes difference net per page with varying granularity
        Given request is made to the bytes difference net per page endpoint with project, page_title, editor_types, all-page-types, varying <granularity>, 20200101, 20201201
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that items key with sub_key project is of type string
        And verify that items key with sub_key editor-type is of type string
        And verify that items key with sub_key page-title is of type string
        And verify that items key with sub_key granularity is of type string
        And verify that items key with sub_key results sub_key contains net_bytes_diff of type int
        And verify that items key with sub_key results sub_key contains timestamp of type string
        And verify that the granularity in the response is the same as the <granularity> in the per-file endpoint
        Examples:
            | granularity |
            | daily |
            | monthly |

  Scenario: Be able to run the AQS Edit analytics bytes difference per file with invalid project
        Given request is made to the Edits analytics bytes difference net per file with invalid project
        Then the request should be successful with status code of 404
        And verify that the content-type is application/problem+json
        And verify that method key contains get

  Scenario: Be able to run the AQS Edit analytics bytes difference per file with special character in project
        Given request is made to the Edits analytics bytes difference net per file with special character in the project
        Then the request should be successful with status code of 400
        And verify that the content-type is application/problem+json
        And verify that method key contains get
        And verify that detail key from the error response displaying the special character warning for per page

  Scenario: Be able to run the AQS Edit analytics bytes difference per file with invalid page title
        Given request is made to the Edits analytics bytes difference net per file with invalid page title
        Then the request should be successful with status code of 404
        And verify that the content-type is application/problem+json
        And verify that method key contains get

  Scenario: Be able to run the Edit analytics bytes difference net per file with invalid editor type
      Given request is made to the bytes difference net per file with invalid editor type
      Then the request should be successful with status code of 400
      And verify that the content-type is application/problem+json
      And verify that method key contains get
      And verify that title key contains Bad Request
      And verify that detail key from the error response for invalid editor type for byte diff net per file

  Scenario: Be able to run the Edit analytics bytes difference net per file with invalid granularity
      Given request is made to the bytes difference net per file with invalid granularity
      Then the request should be successful with status code of 400
      And verify that the content-type is application/problem+json
      And verify that method key contains get
      And verify that title key contains Bad Request
      And verify that detail key from the error response for invalid granularity for byte diff net per file

  Scenario: Be able to run the Edit analytics bytes difference net per file with invalid start date
      Given request is made to the bytes difference net per file with invalid start date
      Then the request should be successful with status code of 400
      And verify that the content-type is application/problem+json
      And verify that method key contains get
      And verify that title key contains Bad Request
      And verify that detail key from the error response for invalid start date for byte diff net per file

  Scenario: Be able to run the Edit analytics bytes difference net per file with dates with no data
      Given request is made to the bytes difference net per file with dates with no data
      Then the request should be successful with status code of 404
      And verify that the content-type is application/problem+json
      And verify that method key contains get
      And verify that detail key from the error response for invalid date with no data for byte diff net per file

  Scenario: Be able to run the Edit analytics bytes difference net per file with invalid end date
      Given request is made to the bytes difference net per file with invalid end date
      Then the request should be successful with status code of 400
      And verify that the content-type is application/problem+json
      And verify that method key contains get
      And verify that title key contains Bad Request
      And verify that detail key from the error response for invalid end date for byte diff net per file

  Scenario: Be able to run the Edit analytics bytes difference net per file with end date before start date
      Given request is made to the bytes difference net per file with end date before start date
      Then the request should be successful with status code of 400
      And verify that the content-type is application/problem+json
      And verify that method key contains get
      And verify that title key contains Bad Request
      And verify that detail key from the error response for end date before start date for byte diff net per file

    Scenario: Be able to run the Edit analytics bytes difference net per file with no full months specified
      Given request is made to the bytes difference net per file with no full months specified
      Then the request should be successful with status code of 400
      And verify that the content-type is application/problem+json
      And verify that method key contains get
      And verify that title key contains Bad Request
      And verify that detail key from the error response for with no full months specified for byte diff net per file