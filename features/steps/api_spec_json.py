import requests
from behave import *
from utilities.header import header
from resources.all_resources.resources import *
from utilities.base_uri import uri


@given("request to the api spec json for editor analytics")
def step_impl(context):
    context.response = requests.get(
        uri + editor_analytics_schema_resource + "/" + context.data_config['api_spec_json']['api_spec_json'],
        headers=header)


@given("request to the api spec json for editor by country with invalid route")
def step_impl(context):
    context.response = requests.get(
        uri + editor_analytics_schema_resource + "/" + context.data_config['api_spec_json']['api_spec_json_invalid'],
        headers=header)


@given("request to the api spec json for media analytics")
def step_impl(context):
    context.response = requests.get(
        uri + media_analytics_schema_resource + "/" + context.data_config['api_spec_json']['api_spec_json'],
        headers=header)


@given("request to the api spec json for media analytics with invalid route")
def step_impl(context):
    context.response = requests.get(
        uri + media_analytics_schema_resource + "/" + context.data_config['api_spec_json']['api_spec_json_invalid'],
        headers=header)


@given("request to the api spec json for page analytics")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_schema_resource + "/" + context.data_config['api_spec_json']['api_spec_json'],
        headers=header)


@given("request to the api spec json for page analytics with invalid route")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_schema_resource + "/" + context.data_config['api_spec_json']['api_spec_json_invalid'],
        headers=header)


@given("request to the api spec json for edit analytics")
def step_impl(context):
    context.response = requests.get(
        uri + edit_analytics_schema_resource + "/" + context.data_config['api_spec_json']['api_spec_json'],
        headers=header)


@given("request to the api spec json for edit analytics with invalid route")
def step_impl(context):
    context.response = requests.get(
        uri + edit_analytics_schema_resource + "/" + context.data_config['api_spec_json']['api_spec_json_invalid'],
        headers=header)


@given("request to the api spec json for geo analytics")
def step_impl(context):
    context.response = requests.get(
        uri + geo_analytics_schema_resource + "/" + context.data_config['api_spec_json']['api_spec_json'],
        headers=header)


@given("request to the api spec json for Geo analytics with invalid route")
def step_impl(context):
    context.response = requests.get(
        uri + geo_analytics_schema_resource + "/" + context.data_config['api_spec_json']['api_spec_json_invalid'],
        headers=header)


@given("request to the api spec json for device analytics")
def step_impl(context):
    context.response = requests.get(
        uri + device_analytics_schema_resource + "/" + context.data_config['api_spec_json']['api_spec_json'],
        headers=header)


@given("request to the api spec json for device analytics with invalid route")
def step_impl(context):
    context.response = requests.get(
        uri + device_analytics_schema_resource + "/" + context.data_config['api_spec_json']['api_spec_json_invalid'],
        headers=header)


@given("request to the api spec json for Common Impact Metrics")
def step_impl(context):
    context.response = requests.get(
        uri + cim_schema_resource + "/" + context.data_config['api_spec_json']['api_spec_json'],
        headers=header)
