from behave import *
from resources.all_resources.resources import media_analytics_per_file
from utilities.base_uri import uri
import requests
import assertpy
from utilities.header import header


@given("request is made to the media analytics per file endpoint with varying {referer}")
def step_impl(context, referer):
    context.response = requests.get(
        uri + media_analytics_per_file + "/" + referer
        + "/" + context.data_config['media_analytics_per_file']['agent_all']
        + "/" + context.data_config['media_analytics_per_file']['file_path']
        + "/" + context.data_config['media_analytics_per_file']['granularity_daily']
        + "/" + context.data_config['media_analytics_per_file']['start_date']
        + "/" + context.data_config['media_analytics_per_file']['end_date'],
        headers=header)


@given("request for media analytics per file endpoint is made with varying {agents}")
def step_impl(context, agents):
    context.response = requests.get(
        uri + media_analytics_per_file + "/" + context.data_config['media_analytics_per_file']['referer']
        + "/" + agents
        + "/" + context.data_config['media_analytics_per_file']['file_path_video']
        + "/" + context.data_config['media_analytics_per_file']['granularity_daily']
        + "/" + context.data_config['media_analytics_per_file']['start_date']
        + "/" + context.data_config['media_analytics_per_file']['end_date'],
        headers=header)


@step("verify that the {agents} used in request is same as the agents in response body")
def step_impl(context, agents):
    for array in context.response.json()["items"]:
        assertpy.assert_that(array["agent"]).is_equal_to(agents)


@given("request is made to the media analytics per file endpoint with monthly granularity and varying {referer}")
def step_impl(context, referer):
    context.response = requests.get(
        uri + media_analytics_per_file + "/" + referer
        + "/" + context.data_config['media_analytics_per_file']['agent_all']
        + "/" + context.data_config['media_analytics_per_file']['file_path']
        + "/" + context.data_config['media_analytics_per_file']['granularity_monthly']
        + "/" + context.data_config['media_analytics_per_file']['start_date']
        + "/" + context.data_config['media_analytics_per_file']['end_date_monthly'],
        headers=header)


@given("request for media analytics per file endpoint is made using monthly granularity and varying {agents}")
def step_impl(context, agents):
    context.response = requests.get(
        uri + media_analytics_per_file + "/" + context.data_config['media_analytics_per_file']['referer']
        + "/" + agents
        + "/" + context.data_config['media_analytics_per_file']['file_path_image']
        + "/" + context.data_config['media_analytics_per_file']['granularity_monthly']
        + "/" + context.data_config['media_analytics_per_file']['start_date']
        + "/" + context.data_config['media_analytics_per_file']['end_date_monthly'],
        headers=header)


@given("request for media analytics per file endpoint with monthly granularity, hourly date and varying {agents}")
def step_impl(context, agents):
    context.response = requests.get(
        uri + media_analytics_per_file + "/" + context.data_config['media_analytics_per_file']['referer']
        + "/" + agents
        + "/" + context.data_config['media_analytics_per_file']['file_path_image']
        + "/" + context.data_config['media_analytics_per_file']['granularity_monthly']
        + "/" + context.data_config['media_analytics_per_file']['start_date_hourly']
        + "/" + context.data_config['media_analytics_per_file']['end_date_hourly'],
        headers=header)


@given("request is made to the media analytics per file endpoint with invalid project")
def step_impl(context):
    context.response = requests.get(
        uri + media_analytics_per_file + "/" + context.data_config['media_analytics_per_file']['referer_invalid']
        + "/" + context.data_config['media_analytics_per_file']['agent_all']
        + "/" + context.data_config['media_analytics_per_file']['file_path_image']
        + "/" + context.data_config['media_analytics_per_file']['granularity_monthly']
        + "/" + context.data_config['media_analytics_per_file']['start_date_hourly']
        + "/" + context.data_config['media_analytics_per_file']['end_date_hourly'],
        headers=header)


@given("request is made to the media analytics per file endpoint with invalid agent")
def step_impl(context):
    context.response = requests.get(
        uri + media_analytics_per_file + "/" + context.data_config['media_analytics_per_file']['referer']
        + "/" + context.data_config['media_analytics_per_file']['agent_invalid']
        + "/" + context.data_config['media_analytics_per_file']['file_path_image']
        + "/" + context.data_config['media_analytics_per_file']['granularity_monthly']
        + "/" + context.data_config['media_analytics_per_file']['start_date_hourly']
        + "/" + context.data_config['media_analytics_per_file']['end_date_hourly'],
        headers=header)


@step("verify that detail key from the error response displays invalid agent validation")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).contains(
        "agent should be equal to one of the allowed values: [all-agents, user, spider]")


@given("request is made to the media analytics per file endpoint with invalid file path")
def step_impl(context):
    context.response = requests.get(
        uri + media_analytics_per_file + "/" + context.data_config['media_analytics_per_file']['referer']
        + "/" + context.data_config['media_analytics_per_file']['agent_all']
        + "/" + context.data_config['media_analytics_per_file']['file_path_invalid']
        + "/" + context.data_config['media_analytics_per_file']['granularity_monthly']
        + "/" + context.data_config['media_analytics_per_file']['start_date_hourly']
        + "/" + context.data_config['media_analytics_per_file']['end_date_hourly'],
        headers=header)


@given("request is made to the media analytics per file endpoint with invalid granularity")
def step_impl(context):
    context.response = requests.get(
        uri + media_analytics_per_file + "/" + context.data_config['media_analytics_per_file']['referer']
        + "/" + context.data_config['media_analytics_per_file']['agent_all']
        + "/" + context.data_config['media_analytics_per_file']['file_path']
        + "/" + context.data_config['media_analytics_per_file']['granularity_invalid']
        + "/" + context.data_config['media_analytics_per_file']['start_date_hourly']
        + "/" + context.data_config['media_analytics_per_file']['end_date_hourly'],
        headers=header)


@step("verify that detail key from the error response displays invalid granularity validation")
def step_impl(context):
    context.response = requests.get(
        uri + media_analytics_per_file + "/" + context.data_config['media_analytics_per_file']['referer']
        + "/" + context.data_config['media_analytics_per_file']['agent_all']
        + "/" + context.data_config['media_analytics_per_file']['file_path']
        + "/" + context.data_config['media_analytics_per_file']['granularity_invalid']
        + "/" + context.data_config['media_analytics_per_file']['start_date_hourly']
        + "/" + context.data_config['media_analytics_per_file']['end_date_hourly'],
        headers=header)


@given("request is made to the media analytics per file endpoint with invalid start time")
def step_impl(context):
    context.response = requests.get(
        uri + media_analytics_per_file + "/" + context.data_config['media_analytics_per_file']['referer']
        + "/" + context.data_config['media_analytics_per_file']['agent_all']
        + "/" + context.data_config['media_analytics_per_file']['file_path']
        + "/" + context.data_config['media_analytics_per_file']['granularity_daily']
        + "/" + context.data_config['media_analytics_per_file']['start_date_invalid']
        + "/" + context.data_config['media_analytics_per_file']['end_date_hourly'],
        headers=header)


@step("verify that detail key from the error response displays invalid start time validation")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).contains(
        "start timestamp is invalid, must be a valid date in YYYYMMDD format")


@given("request is made to the media analytics per file endpoint with invalid end time")
def step_impl(context):
    context.response = requests.get(
        uri + media_analytics_per_file + "/" + context.data_config['media_analytics_per_file']['referer']
        + "/" + context.data_config['media_analytics_per_file']['agent_all']
        + "/" + context.data_config['media_analytics_per_file']['file_path']
        + "/" + context.data_config['media_analytics_per_file']['granularity_daily']
        + "/" + context.data_config['media_analytics_per_file']['start_date']
        + "/" + context.data_config['media_analytics_per_file']['end_date_invalid'],
        headers=header)


@step("verify that detail key from the error response displays invalid end time validation")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).contains(
        "end timestamp is invalid, must be a valid date in YYYYMMDD format")


@given("request is made to the media analytics per file endpoint with invalid domain {referer}")
def step_impl(context, referer):
    context.response = requests.get(
        uri + media_analytics_per_file + "/" + referer
        + "/" + context.data_config['media_analytics_per_file']['agent_all']
        + "/" + context.data_config['media_analytics_per_file']['file_path']
        + "/" + context.data_config['media_analytics_per_file']['granularity_daily']
        + "/" + context.data_config['media_analytics_per_file']['start_date']
        + "/" + context.data_config['media_analytics_per_file']['end_date'],
        headers=header)


@given("request is made to the media analytics per file endpoint with incomplete month range")
def step_impl(context):
    context.response = requests.get(
        uri + media_analytics_per_file + "/" + context.data_config['media_analytics_per_file']['referer']
        + "/" + context.data_config['media_analytics_per_file']['agent_all']
        + "/" + context.data_config['media_analytics_per_file']['file_path']
        + "/" + context.data_config['media_analytics_per_file']['granularity_monthly']
        + "/" + context.data_config['media_analytics_per_file']['start_date']
        + "/" + context.data_config['media_analytics_per_file']['end_date'],
        headers=header)


@step("verify that detail key from the error response displays incomplete month range validation")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).contains(
        "no full months found in specified date range")


@given("request is made to the media analytics per file endpoint with 20200101 and 20200110 start and end times")
def step_impl(context):
    context.response = requests.get(
        uri + media_analytics_per_file + "/" + context.data_config['media_analytics_per_file']['referer']
        + "/" + context.data_config['media_analytics_per_file']['agent_all']
        + "/" + context.data_config['media_analytics_per_file']['file_path']
        + "/" + context.data_config['media_analytics_per_file']['granularity_daily']
        + "/" + context.data_config['media_analytics_per_file']['start_date']
        + "/" + context.data_config['media_analytics_per_file']['end_date'],
        headers=header)


@given("request is made to the media analytics per file endpoint with 20200101 and 20200331 start and end times")
def step_impl(context):
    context.response = requests.get(
        uri + media_analytics_per_file + "/" + context.data_config['media_analytics_per_file']['referer']
        + "/" + context.data_config['media_analytics_per_file']['agent_all']
        + "/" + context.data_config['media_analytics_per_file']['file_path']
        + "/" + context.data_config['media_analytics_per_file']['granularity_monthly']
        + "/" + context.data_config['media_analytics_per_file']['start_date']
        + "/" + context.data_config['media_analytics_per_file']['end_date_monthly'],
        headers=header)


@given("request is made to the media analytics per file endpoint with {special_file}, daily, 20220101 and 20220131")
def step_impl(context, special_file):
    context.response = requests.get(
        uri + media_analytics_per_file + "/" + context.data_config['media_analytics_per_file']['referer']
        + "/" + context.data_config['media_analytics_per_file']['agent_all']
        + "/" + special_file
        + "/" + context.data_config['media_analytics_per_file']['granularity_daily']
        + "/" + context.data_config['media_analytics_per_file']['start_date_special_char']
        + "/" + context.data_config['media_analytics_per_file']['start_date_special_char'],
        headers=header)


@given("request is made to the media analytics per file endpoint with {special_file}, daily, 2023080100 and 2023090100")
def step_impl(context, special_file):
    context.response = requests.get(
        uri + media_analytics_per_file + "/" + context.data_config['media_analytics_per_file']['referer']
        + "/" + context.data_config['media_analytics_per_file']['agent_all']
        + "/" + special_file
        + "/" + context.data_config['media_analytics_per_file']['granularity_daily']
        + "/" + context.data_config['media_analytics_per_file']['start_date_hr_special_char']
        + "/" + context.data_config['media_analytics_per_file']['start_date_hr_special_char'],
        headers=header)