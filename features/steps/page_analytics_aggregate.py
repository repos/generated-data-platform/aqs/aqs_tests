import requests
import assertpy
from behave import *
from resources.all_resources.resources import *
from utilities.base_uri import *
from utilities.header import header


@given("request is made to the page analytics aggregate endpoint with varying {access}")
def step_impl(context, access):
    context.response = requests.get(
        uri + page_analytics_aggregate + "/" + context.data_config['page_analytics_aggregate']['project'] + "/" + access
        + "/" + context.data_config['page_analytics_aggregate']['agent']
        + "/" + context.data_config['page_analytics_aggregate']['granularity']
        + "/" + context.data_config['page_analytics_aggregate']['start']
        + "/" + context.data_config['page_analytics_aggregate']['end'],
        headers=header)


@given("request is made to the page analytics aggregate endpoint with valid parameters and varying {agents}")
def step_impl(context, agents):
    context.response = requests.get(
        uri + page_analytics_aggregate + "/" + context.data_config['page_analytics_aggregate']['project']
        + "/" + context.data_config['page_analytics_aggregate']['access']
        + "/" + agents
        + "/" + context.data_config['page_analytics_aggregate']['granularity']
        + "/" + context.data_config['page_analytics_aggregate']['start']
        + "/" + context.data_config['page_analytics_aggregate']['end'],
        headers=header)


@given("request is made to the page analytics aggregate endpoint with automated agent")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_aggregate + "/" + context.data_config['page_analytics_aggregate']['project']
        + "/" + context.data_config['page_analytics_aggregate']['access']
        + "/" + context.data_config['page_analytics_aggregate']['agent_automated']
        + "/" + context.data_config['page_analytics_aggregate']['granularity']
        + "/" + context.data_config['page_analytics_aggregate']['start']
        + "/" + context.data_config['page_analytics_aggregate']['end'],
        headers=header)


@step("verify that detail key from the error response displays the error warning")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).contains(
        context.data_config['pageviews_aggregate_validations']
        ['agent_automated_validation'])


@given("request is made to the page analytics aggregate endpoint to test varying {granularity}")
def step_impl(context, granularity):
    context.response = requests.get(
        uri + page_analytics_aggregate + "/" + context.data_config['page_analytics_aggregate']['project']
        + "/" + context.data_config['page_analytics_aggregate']['access']
        + "/" + context.data_config['page_analytics_aggregate']['agent']
        + "/" + granularity
        + "/" + context.data_config['page_analytics_aggregate']['start']
        + "/" + context.data_config['page_analytics_aggregate']['end'],
        headers=header)


@given("request is made to the page analytics aggregate endpoint for varying {projects}")
def step_impl(context, projects):
    context.response = requests.get(
        uri + page_analytics_aggregate + "/" + projects
        + "/" + context.data_config['page_analytics_aggregate']['access']
        + "/" + context.data_config['page_analytics_aggregate']['agent']
        + "/" + context.data_config['page_analytics_aggregate']['granularity']
        + "/" + context.data_config['page_analytics_aggregate']['start']
        + "/" + context.data_config['page_analytics_aggregate']['end'],
        headers=header)


@step("verify that project sub_key in items key is same as the {project} in the request")
def step_impl(context, project):
    for array in context.response.json()['items']:
        assertpy.assert_that(array["project"]).is_equal_to(project)


@given("request is made to the page analytics aggregate endpoint with invalid project")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_aggregate + "/" + context.data_config['page_analytics_aggregate']['project_invalid']
        + "/" + context.data_config['page_analytics_aggregate']['access']
        + "/" + context.data_config['page_analytics_aggregate']['agent']
        + "/" + context.data_config['page_analytics_aggregate']['granularity']
        + "/" + context.data_config['page_analytics_aggregate']['start']
        + "/" + context.data_config['page_analytics_aggregate']['end'],
        headers=header)


@given("request is made to the page analytics aggregate endpoint with invalid access")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_aggregate + "/" + context.data_config['page_analytics_aggregate']['project']
        + "/" + context.data_config['page_analytics_aggregate']['access_invalid']
        + "/" + context.data_config['page_analytics_aggregate']['agent']
        + "/" + context.data_config['page_analytics_aggregate']['granularity']
        + "/" + context.data_config['page_analytics_aggregate']['start']
        + "/" + context.data_config['page_analytics_aggregate']['end'],
        headers=header)


@step("verify that detail key from the error response displays the access warning")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).contains(
        context.data_config['pageviews_aggregate_validations']
        ['access_validation'])


@given("request is made to the page analytics aggregate endpoint with invalid agent")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_aggregate + "/" + context.data_config['page_analytics_aggregate']['project']
        + "/" + context.data_config['page_analytics_aggregate']['access']
        + "/" + context.data_config['page_analytics_aggregate']['agent_invalid']
        + "/" + context.data_config['page_analytics_aggregate']['granularity']
        + "/" + context.data_config['page_analytics_aggregate']['start']
        + "/" + context.data_config['page_analytics_aggregate']['end'],
        headers=header)


@step("verify that detail key from the error response displays the agent warning")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).contains(
        context.data_config['pageviews_aggregate_validations']
        ['agent_validation'])


@given("request is made to the page analytics aggregate endpoint with invalid granularity")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_aggregate + "/" + context.data_config['page_analytics_aggregate']['project']
        + "/" + context.data_config['page_analytics_aggregate']['access']
        + "/" + context.data_config['page_analytics_aggregate']['agent']
        + "/" + context.data_config['page_analytics_aggregate']['granularity_invalid']
        + "/" + context.data_config['page_analytics_aggregate']['start']
        + "/" + context.data_config['page_analytics_aggregate']['end'],
        headers=header)


@step("verify that detail key from the error response displays the granularity warning")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).contains(
        context.data_config['pageviews_aggregate_validations']
        ['granularity_validation'])


@given("request is made to the page analytics aggregate endpoint with invalid start date")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_aggregate + "/" + context.data_config['page_analytics_aggregate']['project']
        + "/" + context.data_config['page_analytics_aggregate']['access']
        + "/" + context.data_config['page_analytics_aggregate']['agent']
        + "/" + context.data_config['page_analytics_aggregate']['granularity']
        + "/" + context.data_config['page_analytics_aggregate']['start_invalid']
        + "/" + context.data_config['page_analytics_aggregate']['end'],
        headers=header)


@step("verify that detail key from the error response displays the start date warning")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).is_equal_to(
        context.data_config['pageviews_aggregate_validations']
        ['start_validation'])


@given("request is made to the page analytics aggregate endpoint with invalid end date")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_aggregate + "/" + context.data_config['page_analytics_aggregate']['project']
        + "/" + context.data_config['page_analytics_aggregate']['access']
        + "/" + context.data_config['page_analytics_aggregate']['agent']
        + "/" + context.data_config['page_analytics_aggregate']['granularity']
        + "/" + context.data_config['page_analytics_aggregate']['start']
        + "/" + context.data_config['page_analytics_aggregate']['end_invalid'],
        headers=header)


@step("verify that detail key from the error response displays the end date warning")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).is_equal_to(
        context.data_config['pageviews_aggregate_validations']
        ['end_validation'])


@given("request is made to page analytics aggregate endpoint with daily, year 2018, month 01 and day 01-31")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_aggregate + "/" + context.data_config['page_analytics_aggregate']['project']
        + "/" + context.data_config['page_analytics_aggregate']['access']
        + "/" + context.data_config['page_analytics_aggregate']['agent']
        + "/" + context.data_config['page_analytics_aggregate']['granularity_daily']
        + "/" + context.data_config['page_analytics_aggregate']['start_date_2018']
        + "/" + context.data_config['page_analytics_aggregate']['end_date_2018'],
        headers=header)


@given("request is made to page analytics aggregate endpoint with daily, year 2019, month 01 and day 01-31")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_aggregate + "/" + context.data_config['page_analytics_aggregate']['project']
        + "/" + context.data_config['page_analytics_aggregate']['access']
        + "/" + context.data_config['page_analytics_aggregate']['agent']
        + "/" + context.data_config['page_analytics_aggregate']['granularity_daily']
        + "/" + context.data_config['page_analytics_aggregate']['start_date_2019']
        + "/" + context.data_config['page_analytics_aggregate']['end_date_2019'],
        headers=header)


@given("request is made to page analytics aggregate endpoint with daily, year 2020, month 01 and day 01-31")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_aggregate + "/" + context.data_config['page_analytics_aggregate']['project']
        + "/" + context.data_config['page_analytics_aggregate']['access']
        + "/" + context.data_config['page_analytics_aggregate']['agent']
        + "/" + context.data_config['page_analytics_aggregate']['granularity_daily']
        + "/" + context.data_config['page_analytics_aggregate']['start_date_2020']
        + "/" + context.data_config['page_analytics_aggregate']['end_date_2020'],
        headers=header)


@given("request is made to page analytics aggregate endpoint with invalid data {projects}")
def step_impl(context, projects):
    context.response = requests.get(
        uri + page_analytics_aggregate + "/" + projects
        + "/" + context.data_config['page_analytics_aggregate']['access']
        + "/" + context.data_config['page_analytics_aggregate']['agent']
        + "/" + context.data_config['page_analytics_aggregate']['granularity_daily']
        + "/" + context.data_config['page_analytics_aggregate']['start_date_2020']
        + "/" + context.data_config['page_analytics_aggregate']['end_date_2020'],
        headers=header)
