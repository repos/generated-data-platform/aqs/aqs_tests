from behave import *
import assertpy
import requests
from utilities.header import header
from resources.all_resources.resources import edit_data_aggregate
from utilities.base_uri import uri


@given(
    "request is made to the edit data aggregate endpoint with en.wikipedia, {editor_type}, all-page-types, daily, "
    "20200101 and 20201231")
def step_impl(context, editor_type):
    context.response = requests.get(
        uri + edit_data_aggregate + "/" + context.data_config['edit_data_aggregate']['project'] + "/" +
        editor_type + "/" + context.data_config['edit_data_aggregate']['page_type_all']
        + "/" + context.data_config['edit_data_aggregate']['granularity'] + "/" +
        context.data_config['edit_data_aggregate']['start_date'] + "/" + context.data_config['edit_data_aggregate'][
            'end_date'], headers=header)


@given(
    "request is made to the edit data aggregate endpoint using en.wikipedia, all_editor_types, {page_type}, daily, "
    "20200101 & 20201231")
def step_impl(context, page_type):
    context.response = requests.get(
        uri + edit_data_aggregate + "/" + context.data_config['edit_data_aggregate']['project'] + "/" +
        context.data_config['edit_data_aggregate']['editor_type_all'] + "/" + page_type
        + "/" + context.data_config['edit_data_aggregate']['granularity'] + "/" +
        context.data_config['edit_data_aggregate']['start_date'] + "/" + context.data_config['edit_data_aggregate'][
            'end_date'], headers=header)


@given(
    "request is made to the edit data aggregate endpoint with en.wikipedia, all_editor_types, all-page-types, "
    "using diff. {granularity}, 20200101 and 20201231")
def step_impl(context, granularity):
    context.response = requests.get(
        uri + edit_data_aggregate + "/" + context.data_config['edit_data_aggregate']['project'] + "/" +
        context.data_config['edit_data_aggregate']['editor_type_all']
        + "/" + context.data_config['edit_data_aggregate']['page_type_all'] + "/" + granularity + "/" +
        context.data_config['edit_data_aggregate']['start_date'] + "/" + context.data_config['edit_data_aggregate'][
            'end_date'], headers=header)


@given(
    "request is made to the edit data aggregate endpoint using en.wikipedia, all_editor_types, all-page-types, daily, "
    "20200101 and 20201231")
def step_impl(context):
    context.response = requests.get(
        uri + edit_data_aggregate + "/" + context.data_config['edit_data_aggregate']['project'] + "/" +
        context.data_config['edit_data_aggregate']['editor_type_all'] + "/" +
        context.data_config['edit_data_aggregate']['page_type_all']
        + "/" + context.data_config['edit_data_aggregate']['granularity'] + "/" +
        context.data_config['edit_data_aggregate']['start_date'] + "/" + context.data_config['edit_data_aggregate'][
            'end_date'], headers=header)


@given(
    "request is made to the edit data aggregate endpoint using en.wikipedia, all_editor_types, all-page-types, "
    "monthly, 20200101 and 20200101")
def step_impl(context):
    context.response = requests.get(
        uri + edit_data_aggregate + "/" + context.data_config['edit_data_aggregate']['project'] + "/" +
        context.data_config['edit_data_aggregate']['editor_type_all'] + "/" +
        context.data_config['edit_data_aggregate']['page_type_all']
        + "/" + context.data_config['edit_data_aggregate']['granularity_monthly'] + "/" +
        context.data_config['edit_data_aggregate']['start_date'] + "/" + context.data_config['edit_data_aggregate'][
            'start_date'], headers=header)


@step("verify that detail key from the error response displays full month warning")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).is_equal_to(
        "no full months found in specified date range")


@given(
    "request is made to the edit data aggregate endpoint using en.wikipedia, all_editor_types, "
    "all-page-types, daily, 20201231 and 20200101")
def step_impl(context):
    context.response = requests.get(
        uri + edit_data_aggregate + "/" + context.data_config['edit_data_aggregate']['project'] + "/" +
        context.data_config['edit_data_aggregate']['editor_type_all'] + "/" +
        context.data_config['edit_data_aggregate']['page_type_all']
        + "/" + context.data_config['edit_data_aggregate']['granularity'] + "/" +
        context.data_config['edit_data_aggregate']['end_date'] + "/" + context.data_config['edit_data_aggregate'][
            'start_date'], headers=header)


@step("verify that detail key from the error response displays start timestamp after end timestamp error")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).is_equal_to(
        "start timestamp should be before the end timestamp")


@given(
    "request is made to the edit data aggregate endpoint using en.wikipedia, all_editor_types, all-page-types, "
    "monthly, 20201231 and 20200101")
def step_impl(context):
    context.response = requests.get(
        uri + edit_data_aggregate + "/" + context.data_config['edit_data_aggregate']['project'] + "/" +
        context.data_config['edit_data_aggregate']['editor_type_all'] + "/" +
        context.data_config['edit_data_aggregate']['page_type_all']
        + "/" + context.data_config['edit_data_aggregate']['granularity_monthly'] + "/" +
        context.data_config['edit_data_aggregate']['end_date'] + "/" + context.data_config['edit_data_aggregate'][
            'start_date'], headers=header)


@step(
    "verify that detail key from the error response displays start timestamp after end timestamp and full month errors")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).is_equal_to(
        "start timestamp should be before the end timestamp")

@given(
    "request is made to the edit data aggregate endpoint with invalideditsdataaggregate.org, all_editor_types, "
    "all-page-types, daily, 20200101 and 20201231")
def step_impl(context):
    context.response = requests.get(
        uri + edit_data_aggregate + "/" + context.data_config['edit_data_aggregate']['project_invalid'] + "/" +
        context.data_config['edit_data_aggregate']['editor_type_all'] + "/" +
        context.data_config['edit_data_aggregate']['page_type_all']
        + "/" + context.data_config['edit_data_aggregate']['granularity'] + "/" +
        context.data_config['edit_data_aggregate']['start_date'] + "/" + context.data_config['edit_data_aggregate'][
            'end_date'], headers=header)


@given(
    "request is made to the edit data aggregate endpoint with en.wikipedia, using an invalid "
    "editor type editor_type_invalid")
def step_impl(context):
    context.response = requests.get(
        uri + edit_data_aggregate + "/" + context.data_config['edit_data_aggregate']['project'] + "/" +
        context.data_config['edit_data_aggregate']['editor_type_invalid'] + "/" +
        context.data_config['edit_data_aggregate']['page_type_all']
        + "/" + context.data_config['edit_data_aggregate']['granularity'] + "/" +
        context.data_config['edit_data_aggregate']['start_date'] + "/" + context.data_config['edit_data_aggregate'][
            'end_date'], headers=header)


@step("verify that detail key from the error response displays editor type errors")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).is_equal_to(
        "editor-type should be equal to one of the allowed values: [all-editor-types, anonymous, group-bot, name-bot, "
        "user]")


@given(
    "request is made to the edit data aggregate endpoint with en.wikipedia, using an invalid page type "
    "page_type_invalid")
def step_impl(context):
    context.response = requests.get(
        uri + edit_data_aggregate + "/" + context.data_config['edit_data_aggregate']['project'] + "/" +
        context.data_config['edit_data_aggregate']['editor_type_all'] + "/" +
        context.data_config['edit_data_aggregate']['page_type_invalid']
        + "/" + context.data_config['edit_data_aggregate']['granularity'] + "/" +
        context.data_config['edit_data_aggregate']['start_date'] + "/" + context.data_config['edit_data_aggregate'][
            'end_date'], headers=header)


@step("verify that detail key from the error response displays page type errors")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).is_equal_to("page-type should be equal to one of the "
                                                                        "allowed values: [all-page-types, content, "
                                                                        "non-content]")


@given(
    "request is made to the edit data aggregate endpoint with en.wikipedia, using an invalid granularity type "
    "granularity_invalid")
def step_impl(context):
    context.response = requests.get(
        uri + edit_data_aggregate + "/" + context.data_config['edit_data_aggregate']['project'] + "/" +
        context.data_config['edit_data_aggregate']['editor_type_all'] + "/" +
        context.data_config['edit_data_aggregate']['page_type_all']
        + "/" + context.data_config['edit_data_aggregate']['granularity_invalid'] + "/" +
        context.data_config['edit_data_aggregate']['start_date'] + "/" + context.data_config['edit_data_aggregate'][
            'end_date'], headers=header)


@step("verify that detail key from the error response displays granularity type errors")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).is_equal_to(
        "granularity should be equal to one of the allowed values: [daily, monthly]")


@given("request is made to the edit data aggregate endpoint with en.wikipedia, using a non leap year date")
def step_impl(context):
    context.response = requests.get(
        uri + edit_data_aggregate + "/" + context.data_config['edit_data_aggregate']['project'] + "/" +
        context.data_config['edit_data_aggregate']['editor_type_all'] + "/" +
        context.data_config['edit_data_aggregate']['page_type_all']
        + "/" + context.data_config['edit_data_aggregate']['granularity'] + "/" +
        context.data_config['edit_data_aggregate']['non_leap_year_date'] + "/" +
        context.data_config['edit_data_aggregate'][
            'non_leap_year_date'], headers=header)


@step("verify that detail key from the error response displays start timestamp and end timestamp errors")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).is_equal_to(
        "start timestamp is invalid, must be a valid date in YYYYMMDD format")



@given("request is made to the edit data aggregate endpoint with en.wikipedia, using a leap year date")
def step_impl(context):
    context.response = requests.get(
        uri + edit_data_aggregate + "/" + context.data_config['edit_data_aggregate']['project'] + "/" +
        context.data_config['edit_data_aggregate']['editor_type_all'] + "/" +
        context.data_config['edit_data_aggregate']['page_type_all']
        + "/" + context.data_config['edit_data_aggregate']['granularity'] + "/" +
        context.data_config['edit_data_aggregate']['leap_year_date'] + "/" +
        context.data_config['edit_data_aggregate'][
            'leap_year_date'], headers=header)
