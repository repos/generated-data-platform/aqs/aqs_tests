from behave import *
import requests
from utilities.header import header
from resources.all_resources.resources import cim_top_viewed_categories_monthly_resource
from utilities.base_uri import *


@given("request is made to the Top viewed categories monthly endpoint with varying scope {scope}, 2023 and 11")
def step_impl(context, scope):
    context.response = requests.get(
        uri + cim_top_viewed_categories_monthly_resource
        + "/" + scope
        + "/" + context.data_config['cim_top_viewed_categories_monthly']['wiki']
        + "/" + context.data_config['cim_top_viewed_categories_monthly']['year']
        + "/" + context.data_config['cim_top_viewed_categories_monthly']['month'],
        headers=header)


@given("request is made to the Top viewed categories monthly endpoint with varying wiki {wiki}, 2023 and 11")
def step_impl(context, wiki):
    context.response = requests.get(
        uri + cim_top_viewed_categories_monthly_resource
        + "/" + context.data_config['cim_top_viewed_categories_monthly']['category_scope']
        + "/" + wiki
        + "/" + context.data_config['cim_top_viewed_categories_monthly']['year']
        + "/" + context.data_config['cim_top_viewed_categories_monthly']['month'],
        headers=header)


@given("request is made to the Top viewed categories monthly endpoint with invalid route")
def step_impl(context):
    context.response = requests.get(
        uri + cim_top_viewed_categories_monthly_resource
        + "/" + "/" + context.data_config['cim_top_viewed_categories_monthly']['category_scope']
        + "/" + context.data_config['cim_top_viewed_categories_monthly']['wiki']
        + "/" + context.data_config['cim_top_viewed_categories_monthly']['year']
        + "/" + context.data_config['cim_top_viewed_categories_monthly']['month'],
        headers=header)


@given("request is made to the Top viewed categories monthly endpoint with invalid scope")
def step_impl(context):
    context.response = requests.get(
        uri + cim_top_viewed_categories_monthly_resource
        + "/" + context.data_config['cim_top_viewed_categories_monthly']['category_scope_invalid']
        + "/" + context.data_config['cim_top_viewed_categories_monthly']['wiki']
        + "/" + context.data_config['cim_top_viewed_categories_monthly']['year']
        + "/" + context.data_config['cim_top_viewed_categories_monthly']['month'],
        headers=header)


@given("request is made to the Top viewed categories monthly endpoint with invalid wiki")
def step_impl(context):
    context.response = requests.get(
        uri + cim_top_viewed_categories_monthly_resource
        + "/" + context.data_config['cim_top_viewed_categories_monthly']['category_scope']
        + "/" + context.data_config['cim_top_viewed_categories_monthly']['wiki_invalid']
        + "/" + context.data_config['cim_top_viewed_categories_monthly']['year']
        + "/" + context.data_config['cim_top_viewed_categories_monthly']['month'],
        headers=header)


@given("request is made to the Top viewed categories monthly endpoint with invalid year")
def step_impl(context):
    context.response = requests.get(
        uri + cim_top_viewed_categories_monthly_resource
        + "/" + context.data_config['cim_top_viewed_categories_monthly']['category_scope']
        + "/" + context.data_config['cim_top_viewed_categories_monthly']['wiki']
        + "/" + context.data_config['cim_top_viewed_categories_monthly']['year_invalid']
        + "/" + context.data_config['cim_top_viewed_categories_monthly']['month'],
        headers=header)


@given("request is made to the Top viewed categories monthly endpoint with invalid month")
def step_impl(context):
    context.response = requests.get(
        uri + cim_top_viewed_categories_monthly_resource
        + "/" + context.data_config['cim_top_viewed_categories_monthly']['category_scope']
        + "/" + context.data_config['cim_top_viewed_categories_monthly']['wiki']
        + "/" + context.data_config['cim_top_viewed_categories_monthly']['year']
        + "/" + context.data_config['cim_top_viewed_categories_monthly']['month_invalid'],
        headers=header)


@given("request is made to the Top viewed categories monthly endpoint to validate category rank and pageview count")
def step_impl(context):
    context.response = requests.get(
        uri + cim_top_viewed_categories_monthly_resource
        + "/" + context.data_config['cim_top_viewed_categories_monthly']['category_scope']
        + "/" + context.data_config['cim_top_viewed_categories_monthly']['wiki']
        + "/" + context.data_config['cim_top_viewed_categories_monthly']['year']
        + "/" + context.data_config['cim_top_viewed_categories_monthly']['month'],
        headers=header)


@given("request is made to the CIM Top viewed categories monthly endpoint with invalid characters {wiki}")
def step_impl(context, wiki):
    context.response = requests.get(
        uri + cim_top_viewed_categories_monthly_resource
        + "/" + context.data_config['cim_top_viewed_categories_monthly']['category_scope']
        + "/" + wiki
        + "/" + context.data_config['cim_top_viewed_categories_monthly']['year']
        + "/" + context.data_config['cim_top_viewed_categories_monthly']['month'],
        headers=header)