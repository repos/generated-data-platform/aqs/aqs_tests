from behave import *
import requests
from utilities.header import header
from resources.all_resources.resources import editor_analytics_data_top_by_absolute_bytes_difference
from utilities.base_uri import uri


@given(
    "request is made to the editors data top-by-absolute-bytes-difference endpoint with {project}, {editor_type}, "
    "{page_type}, {year}, {month} and {day}")
def step_impl(context, project, editor_type, page_type, year, month, day):
    context.response = requests.get(
        uri + editor_analytics_data_top_by_absolute_bytes_difference + "/" + project + "/" +
        editor_type + "/" + page_type + "/" + year + "/" +
        month + "/" + day, headers=header)


@given(
    "request is made to the editors data top-by-absolute-bytes-difference endpoint using an invalid route")
def step_impl(context):
    context.response = requests.get(
        uri + editor_analytics_data_top_by_absolute_bytes_difference
        + context.data_config['editor_analytics_data_top_by_absolute_bytes_difference']['invalid_route'])
