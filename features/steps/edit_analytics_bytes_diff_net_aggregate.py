from behave import *
import assertpy
import requests
from utilities.header import header
from resources.all_resources.resources import *
from utilities.base_uri import uri


@given(
    "request is made to the bytes difference endpoint with sw.wikipedia, varying {editor_types}, all-page-types, daily, 20200101, 20201201")
def step_impl(context, editor_types):
    context.response = requests.get(
        uri + edit_bytes_diff_net_aggregate + "/" + context.data_config['edit_bytes_difference_net_aggregate'][
            'project']
        + "/" + editor_types + "/" + context.data_config['edit_bytes_difference_net_aggregate']['page_type_all']
        + "/" + context.data_config['edit_bytes_difference_net_aggregate']['granularity'] + "/" +
        context.data_config['edit_bytes_difference_net_aggregate']['start_date'] + "/"
        + context.data_config['edit_bytes_difference_net_aggregate']['end_date'], headers=header)


@step("verify that the editor_type in the response is the same as the {editor_types} in the endpoint")
def step_impl(context, editor_types):
    for obj in context.response.json()["items"]:
        assertpy.assert_that(obj["editor-type"]).is_equal_to(editor_types)


@given(
    "request is made to the bytes difference endpoint with sw.wikipedia, editor_types, varying {page_types}, daily, 20200101, 20201201")
def step_impl(context, page_types):
    context.response = requests.get(
        uri + edit_bytes_diff_net_aggregate + "/" + context.data_config['edit_bytes_difference_net_aggregate'][
            'project']
        + "/" + context.data_config['edit_bytes_difference_net_aggregate']['editor_type'] + "/" + page_types
        + "/" + context.data_config['edit_bytes_difference_net_aggregate']['granularity'] + "/" +
        context.data_config['edit_bytes_difference_net_aggregate']['start_date'] + "/"
        + context.data_config['edit_bytes_difference_net_aggregate']['end_date'], headers=header)


@step("verify that the page_type in the response is the same as the {page_types} in the endpoint")
def step_impl(context, page_types):
    for obj in context.response.json()["items"]:
        assertpy.assert_that(obj["page-type"]).is_equal_to(page_types)


@given(
    "request is made to the bytes difference endpoint with sw.wikipedia, editor_types, page_types, varying {granularity}, 20200101, 20201201")
def step_impl(context, granularity):
    context.response = requests.get(
        uri + edit_bytes_diff_net_aggregate + "/" + context.data_config['edit_bytes_difference_net_aggregate'][
            'project']
        + "/" + context.data_config['edit_bytes_difference_net_aggregate']['editor_type']
        + "/" + context.data_config['edit_bytes_difference_net_aggregate']['page_type_all'] + "/" + granularity
        + "/" + context.data_config['edit_bytes_difference_net_aggregate']['start_date'] + "/"
        + context.data_config['edit_bytes_difference_net_aggregate']['end_date'], headers=header)


@step("verify that the granularity in the response is the same as the {granularity} in the endpoint")
def step_impl(context, granularity):
    for obj in context.response.json()["items"]:
        assertpy.assert_that(obj["granularity"]).is_equal_to(granularity)


@given("request is made to the Edit analytics bytes difference net aggregate with invalid project")
def step_impl(context):
    context.response = requests.get(
        uri + edit_bytes_diff_net_aggregate + "/" + context.data_config['edit_bytes_difference_net_aggregate'][
            'project_invalid']
        + "/" + context.data_config['edit_bytes_difference_net_aggregate']['editor_type']
        + "/" + context.data_config['edit_bytes_difference_net_aggregate']['page_type_all']
        + "/" + context.data_config['edit_bytes_difference_net_aggregate']['granularity']
        + "/" + context.data_config['edit_bytes_difference_net_aggregate']['start_date'] + "/"
        + context.data_config['edit_bytes_difference_net_aggregate']['end_date'], headers=header)


@given("request is made to the bytes difference net aggregate with invalid editor type")
def step_impl(context):
    context.response = requests.get(
        uri + edit_bytes_diff_net_aggregate + "/" + context.data_config['edit_bytes_difference_net_aggregate'][
            'project']
        + "/" + context.data_config['edit_bytes_difference_net_aggregate']['editor_type_invalid']
        + "/" + context.data_config['edit_bytes_difference_net_aggregate']['page_type_all']
        + "/" + context.data_config['edit_bytes_difference_net_aggregate']['granularity']
        + "/" + context.data_config['edit_bytes_difference_net_aggregate']['start_date'] + "/"
        + context.data_config['edit_bytes_difference_net_aggregate']['end_date'], headers=header)


@step("verify that detail key from the error response for invalid editor type for byte diff")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).contains(
        "should be equal to one of the allowed values: [all-editor-types, anonymous, group-bot, name-bot, user]")


@given("request is made to the bytes difference net aggregate with invalid page type")
def step_impl(context):
    context.response = requests.get(
        uri + edit_bytes_diff_net_aggregate + "/" + context.data_config['edit_bytes_difference_net_aggregate'][
            'project']
        + "/" + context.data_config['edit_bytes_difference_net_aggregate']['editor_type']
        + "/" + context.data_config['edit_bytes_difference_net_aggregate']['page_type_invalid']
        + "/" + context.data_config['edit_bytes_difference_net_aggregate']['granularity']
        + "/" + context.data_config['edit_bytes_difference_net_aggregate']['start_date'] + "/"
        + context.data_config['edit_bytes_difference_net_aggregate']['end_date'], headers=header)


@step("verify that detail key from the error response for invalid page type for byte diff")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).contains(
        "should be equal to one of the allowed values: [all-page-types, content, non-content]")


@given("request is made to the bytes difference net aggregate with invalid granularity")
def step_impl(context):
    context.response = requests.get(
        uri + edit_bytes_diff_net_aggregate + "/" + context.data_config['edit_bytes_difference_net_aggregate'][
            'project']
        + "/" + context.data_config['edit_bytes_difference_net_aggregate']['editor_type']
        + "/" + context.data_config['edit_bytes_difference_net_aggregate']['page_type_all']
        + "/" + context.data_config['edit_bytes_difference_net_aggregate']['granularity_invalid']
        + "/" + context.data_config['edit_bytes_difference_net_aggregate']['start_date'] + "/"
        + context.data_config['edit_bytes_difference_net_aggregate']['end_date'], headers=header)


@step("verify that detail key from the error response for invalid granularity for byte diff")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).contains(
        "should be equal to one of the allowed values: [daily, monthly]")


@given("request is made to the bytes difference net aggregate with invalid start time")
def step_impl(context):
    context.response = requests.get(
        uri + edit_bytes_diff_net_aggregate + "/" + context.data_config['edit_bytes_difference_net_aggregate'][
            'project']
        + "/" + context.data_config['edit_bytes_difference_net_aggregate']['editor_type']
        + "/" + context.data_config['edit_bytes_difference_net_aggregate']['page_type_all']
        + "/" + context.data_config['edit_bytes_difference_net_aggregate']['granularity']
        + "/" + context.data_config['edit_bytes_difference_net_aggregate']['start_date_invalid'] + "/"
        + context.data_config['edit_bytes_difference_net_aggregate']['end_date'], headers=header)


@step("verify that detail key from the error response for invalid start time for byte diff")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).contains(
        "start timestamp is invalid, must be a valid date in YYYYMMDD format")


@given("request is made to the bytes difference net aggregate with invalid end time")
def step_impl(context):
    context.response = requests.get(
        uri + edit_bytes_diff_net_aggregate + "/" + context.data_config['edit_bytes_difference_net_aggregate'][
            'project']
        + "/" + context.data_config['edit_bytes_difference_net_aggregate']['editor_type']
        + "/" + context.data_config['edit_bytes_difference_net_aggregate']['page_type_all']
        + "/" + context.data_config['edit_bytes_difference_net_aggregate']['granularity']
        + "/" + context.data_config['edit_bytes_difference_net_aggregate']['start_date'] + "/"
        + context.data_config['edit_bytes_difference_net_aggregate']['end_date_invalid'], headers=header)


@step("verify that detail key from the error response for invalid end time for byte diff")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).contains(
        "end timestamp is invalid, must be a valid date in YYYYMMDD format")


@given("request is made to the bytes difference net aggregate with years with no data")
def step_impl(context):
    context.response = requests.get(
        uri + edit_bytes_diff_net_aggregate + "/" + context.data_config['edit_bytes_difference_net_aggregate'][
            'project']
        + "/" + context.data_config['edit_bytes_difference_net_aggregate']['editor_type']
        + "/" + context.data_config['edit_bytes_difference_net_aggregate']['page_type_all']
        + "/" + context.data_config['edit_bytes_difference_net_aggregate']['granularity']
        + "/" + context.data_config['edit_bytes_difference_net_aggregate']['start_date_no_data'] + "/"
        + context.data_config['edit_bytes_difference_net_aggregate']['end_date_no_data'], headers=header)


@step("verify that results sub key in the items key is an empty list")
def step_impl(context):
    assertpy.assert_that(context.response.json()["items"][0]["results"]).is_empty()


@given(
    "request is made to the bytes difference endpoint with varying {projects}, editor_types, all-page-types, daily, 20200101, 20201201")
def step_impl(context, projects):
    context.response = requests.get(
        uri + edit_bytes_diff_net_aggregate + "/" + projects
        + "/" + context.data_config['edit_bytes_difference_net_aggregate']['editor_type']
        + "/" + context.data_config['edit_bytes_difference_net_aggregate']['page_type_all']
        + "/" + context.data_config['edit_bytes_difference_net_aggregate']['granularity'] + "/" +
        context.data_config['edit_bytes_difference_net_aggregate']['start_date'] + "/"
        + context.data_config['edit_bytes_difference_net_aggregate']['end_date'], headers=header)


@step("verify that the project in the response is the same as the {projects} in the endpoint")
def step_impl(context, projects):
    for obj in context.response.json()["items"]:
        assertpy.assert_that(obj["project"]).is_equal_to(projects)