from behave import *
import requests
from utilities.header import header
from resources.all_resources.resources import editor_analytics_data_aggregate
from utilities.base_uri import uri


@given(
    "request is made to the editor analytics data aggregate endpoint with {project}, {editor_type}, {page_type}, "
    "{activity_level}, {granularity}, {start_date} and {end_date}")
def step_impl(context, project, editor_type, page_type, activity_level, granularity, start_date, end_date):
    context.response = requests.get(
        uri + editor_analytics_data_aggregate + "/" + project + "/" +
        editor_type + "/" + page_type + "/" + activity_level + "/" + granularity + "/" +
        start_date + "/" + end_date, headers=header)


@given(
    "request is made to the editors data aggregate endpoint using an invalid route")
def step_impl(context):
    context.response = requests.get(
        uri + editor_analytics_data_aggregate + context.data_config['editor_analytics_data_aggregate']['invalid_route'])
