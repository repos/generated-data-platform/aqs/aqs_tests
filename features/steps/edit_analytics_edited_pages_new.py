import requests
from behave import *
from utilities.header import header
from resources.all_resources.resources import *
from utilities.base_uri import uri
import assertpy


@given(
    "request is made to the Edit edited pages new endpoint with sw.wikipedia, varying {editor_types}, all-page-types, daily, 20180101, 20181231")
def step_impl(context, editor_types):
    context.response = requests.get(
        uri + edit_edited_pages_new + "/" + context.data_config['edit_edited_pages_new']['project']
        + "/" + editor_types + "/" + context.data_config['edit_edited_pages_new']['page_type_all']
        + "/" + context.data_config['edit_edited_pages_new']['granularity'] + "/" +
        context.data_config['edit_edited_pages_new']['start_date'] + "/"
        + context.data_config['edit_edited_pages_new']['end_date'], headers=header)


@step("verify that the editor_type in the response is same as the {editor_types} in the edited pages new endpoint")
def step_impl(context, editor_types):
    for obj in context.response.json()["items"]:
        assertpy.assert_that(obj["editor-type"]).is_equal_to(editor_types)


@given(
    "request is made to the Edit edited pages new endpoint with sw.wikipedia, editor_types, varying {page_types}, daily, 20200101, 20201201")
def step_impl(context, page_types):
    context.response = requests.get(
        uri + edit_edited_pages_new + "/" + context.data_config['edit_edited_pages_new']['project']
        + "/" + context.data_config['edit_edited_pages_new']['editor_type']
        + "/" + page_types
        + "/" + context.data_config['edit_edited_pages_new']['granularity']
        + "/" + context.data_config['edit_edited_pages_new']['start_date']
        + "/" + context.data_config['edit_edited_pages_new']['end_date'], headers=header)


@step("verify that the page_type in the response is the same as the {page_types} in the edited page new endpoint")
def step_impl(context, page_types):
    for obj in context.response.json()["items"]:
        assertpy.assert_that(obj["page-type"]).is_equal_to(page_types)


@given(
    "request is made to the Edit edited pages new endpoint with sw.wikipedia, editor_types, page_types, varying {granularity}, 20200101, 20201201")
def step_impl(context, granularity):
    context.response = requests.get(
        uri + edit_edited_pages_new + "/" + context.data_config['edit_edited_pages_new']['project']
        + "/" + context.data_config['edit_edited_pages_new']['editor_type']
        + "/" + context.data_config['edit_edited_pages_new']['page_type_all']
        + "/" + granularity
        + "/" + context.data_config['edit_edited_pages_new']['start_date']
        + "/" + context.data_config['edit_edited_pages_new']['end_date'], headers=header)


@step("verify that the granularity in the response is the same as the {granularity} in the edited pages new endpoint")
def step_impl(context, granularity):
    for obj in context.response.json()["items"]:
        assertpy.assert_that(obj["granularity"]).is_equal_to(granularity)


@given("request is made to the Edit analytics edited pages new endpoint with invalid project")
def step_impl(context):
    context.response = requests.get(
        uri + edit_edited_pages_new + "/" + context.data_config['edit_edited_pages_new']['project_invalid']
        + "/" + context.data_config['edit_edited_pages_new']['editor_type']
        + "/" + context.data_config['edit_edited_pages_new']['page_type_all']
        + "/" + context.data_config['edit_edited_pages_new']['granularity']
        + "/" + context.data_config['edit_edited_pages_new']['start_date']
        + "/" + context.data_config['edit_edited_pages_new']['end_date'], headers=header)


@given("request is made to the edited pages new endpoint with invalid editor type")
def step_impl(context):
    context.response = requests.get(
        uri + edit_edited_pages_new + "/" + context.data_config['edit_edited_pages_new']['project']
        + "/" + context.data_config['edit_edited_pages_new']['editor_type_invalid']
        + "/" + context.data_config['edit_edited_pages_new']['page_type_all']
        + "/" + context.data_config['edit_edited_pages_new']['granularity']
        + "/" + context.data_config['edit_edited_pages_new']['start_date']
        + "/" + context.data_config['edit_edited_pages_new']['end_date'], headers=header)


@step("verify that detail key from the error response for invalid editor type for the edited pages new endpoint")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).contains(
        "should be equal to one of the allowed values: [all-editor-types, anonymous, group-bot, name-bot, user]")


@given("request is made to the edited pages new endpoint with invalid page type")
def step_impl(context):
    context.response = requests.get(
        uri + edit_edited_pages_new + "/" + context.data_config['edit_edited_pages_new']['project']
        + "/" + context.data_config['edit_edited_pages_new']['editor_type']
        + "/" + context.data_config['edit_edited_pages_new']['page_type_invalid']
        + "/" + context.data_config['edit_edited_pages_new']['granularity']
        + "/" + context.data_config['edit_edited_pages_new']['start_date']
        + "/" + context.data_config['edit_edited_pages_new']['end_date'], headers=header)


@step("verify that detail key from the error response for invalid page type for edited pages new endpoint")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).contains(
        "should be equal to one of the allowed values: [all-page-types, content, non-content]")


@given("request is made to the edited pages new endpoint with invalid granularity")
def step_impl(context):
    context.response = requests.get(
        uri + edit_edited_pages_new + "/" + context.data_config['edit_edited_pages_new']['project']
        + "/" + context.data_config['edit_edited_pages_new']['editor_type']
        + "/" + context.data_config['edit_edited_pages_new']['page_type_all']
        + "/" + context.data_config['edit_edited_pages_new']['granularity_invalid']
        + "/" + context.data_config['edit_edited_pages_new']['start_date']
        + "/" + context.data_config['edit_edited_pages_new']['end_date'], headers=header)


@step("verify that detail key from the error response for invalid granularity for edited pages new endpoint")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).contains(
        "should be equal to one of the allowed values: [daily, monthly]")

@given("request is made to the bytes difference edited pages new endpoint with invalid start time")
def step_impl(context):
    context.response = requests.get(
        uri + edit_edited_pages_new + "/" + context.data_config['edit_edited_pages_new']['project']
        + "/" + context.data_config['edit_edited_pages_new']['editor_type']
        + "/" + context.data_config['edit_edited_pages_new']['page_type_all']
        + "/" + context.data_config['edit_edited_pages_new']['granularity']
        + "/" + context.data_config['edit_edited_pages_new']['start_date_invalid']
        + "/" + context.data_config['edit_edited_pages_new']['end_date'], headers=header)

@step("verify that detail key from the error response for invalid start time for edited pages new endpoint")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).contains(
        "start timestamp is invalid, must be a valid date in YYYYMMDD format")


@given("request is made to the edited pages new endpoint with invalid end time")
def step_impl(context):
    context.response = requests.get(
        uri + edit_edited_pages_new + "/" + context.data_config['edit_edited_pages_new']['project']
        + "/" + context.data_config['edit_edited_pages_new']['editor_type']
        + "/" + context.data_config['edit_edited_pages_new']['page_type_all']
        + "/" + context.data_config['edit_edited_pages_new']['granularity']
        + "/" + context.data_config['edit_edited_pages_new']['start_date']
        + "/" + context.data_config['edit_edited_pages_new']['end_date_invalid'], headers=header)


@step("verify that detail key shows the error response for invalid end time for edited pages new endpoint")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).contains(
        "end timestamp is invalid, must be a valid date in YYYYMMDD format")


@given(
    "request is made to the AQS Edit analytics edited pages new endpoint with varying {projects}, editor_types, all-page-types, daily, 20200101, 20201201")
def step_impl(context, projects):
    context.response = requests.get(
        uri + edit_edited_pages_new + "/" + projects
        + "/" + context.data_config['edit_edited_pages_new']['editor_type']
        + "/" + context.data_config['edit_edited_pages_new']['page_type_all']
        + "/" + context.data_config['edit_edited_pages_new']['granularity'] + "/" +
        context.data_config['edit_edited_pages_new']['start_date'] + "/"
        + context.data_config['edit_edited_pages_new']['end_date'], headers=header)