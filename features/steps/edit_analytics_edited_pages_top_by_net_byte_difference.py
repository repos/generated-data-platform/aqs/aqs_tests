import requests
from behave import *
from utilities.header import header
from resources.all_resources.resources import edit_edited_pages_top_by_net_byte_difference
from utilities.base_uri import uri
import assertpy


@given(
    "request is made to the Edit analytics Edited Pages top by net byte difference endpoint with varying {projects}, editor_types, all-page-types, daily, 20200101, 20201201")
def step_impl(context, projects):
    context.response = requests.get(
        uri + edit_edited_pages_top_by_net_byte_difference + "/" + projects
        + "/" + context.data_config['edit_edited_pages_top_by_net_byte_difference']['editor_type']
        + "/" + context.data_config['edit_edited_pages_top_by_net_byte_difference']['page_type_all']
        + "/" + context.data_config['edit_edited_pages_top_by_net_byte_difference']['year_2018']
        + "/" + context.data_config['edit_edited_pages_top_by_net_byte_difference']['month_01'] + "/" +
        context.data_config['edit_edited_pages_top_by_net_byte_difference']['day_01'], headers=header)


@step("verify the net_bytes_diff key in the top sub key")
def step_impl(context):
    for item in context.response.json()['items'][0]['results'][0]['top']:
        assertpy.assert_that(item).contains_key("net_bytes_diff")


@given(
    "request is made to the Edits analytics Edited Pages top by net byte difference endpoint with sw.wikipedia, varying {editor_types}, all-page-types, daily, 20180101, 20181231")
def step_impl(context, editor_types):
    context.response = requests.get(
        uri + edit_edited_pages_top_by_net_byte_difference + "/" +
        context.data_config['edit_edited_pages_top_by_net_byte_difference']['project']
        + "/" + editor_types
        + "/" + context.data_config['edit_edited_pages_top_by_net_byte_difference']['page_type_all']
        + "/" + context.data_config['edit_edited_pages_top_by_net_byte_difference']['year_2018']
        + "/" + context.data_config['edit_edited_pages_top_by_net_byte_difference']['month_01'] + "/" +
        context.data_config['edit_edited_pages_top_by_net_byte_difference']['day_01'], headers=header)


@given(
    "request is made to the Edits analytics Edited Pages top by net byte difference endpoint with sw.wikipedia, editor_types, varying {page_types}, daily, 20200101, 20201201")
def step_impl(context, page_types):
    context.response = requests.get(
        uri + edit_edited_pages_top_by_net_byte_difference + "/" +
        context.data_config['edit_edited_pages_top_by_net_byte_difference']['project']
        + "/" + context.data_config['edit_edited_pages_top_by_net_byte_difference']['editor_type']
        + "/" + page_types
        + "/" + context.data_config['edit_edited_pages_top_by_net_byte_difference']['year_2018']
        + "/" + context.data_config['edit_edited_pages_top_by_net_byte_difference']['month_01'] + "/" +
        context.data_config['edit_edited_pages_top_by_net_byte_difference']['day_01'], headers=header)


@given("request is made to the Edits analytics Edited Pages top by net byte difference endpoint with invalid project")
def step_impl(context):
    context.response = requests.get(
        uri + edit_edited_pages_top_by_net_byte_difference + "/" +
        context.data_config['edit_edited_pages_top_by_net_byte_difference']['project_invalid']
        + "/" + context.data_config['edit_edited_pages_top_by_net_byte_difference']['editor_type']
        + "/" + context.data_config['edit_edited_pages_top_by_net_byte_difference']['page_type_all']
        + "/" + context.data_config['edit_edited_pages_top_by_net_byte_difference']['year_2018']
        + "/" + context.data_config['edit_edited_pages_top_by_net_byte_difference']['month_01'] + "/" +
        context.data_config['edit_edited_pages_top_by_net_byte_difference']['day_01'], headers=header)


@given(
    "request is made to the Edits analytics Edited Pages top by net byte difference endpoint with invalid editor type")
def step_impl(context):
    context.response = requests.get(
        uri + edit_edited_pages_top_by_net_byte_difference + "/" +
        context.data_config['edit_edited_pages_top_by_net_byte_difference']['project']
        + "/" + context.data_config['edit_edited_pages_top_by_net_byte_difference']['editor_type_invalid']
        + "/" + context.data_config['edit_edited_pages_top_by_net_byte_difference']['page_type_all']
        + "/" + context.data_config['edit_edited_pages_top_by_net_byte_difference']['year_2018']
        + "/" + context.data_config['edit_edited_pages_top_by_net_byte_difference']['month_01'] + "/" +
        context.data_config['edit_edited_pages_top_by_net_byte_difference']['day_01'], headers=header)


@given("request is made to the Edited Pages top by net byte difference endpoint with invalid page type")
def step_impl(context):
    context.response = requests.get(
        uri + edit_edited_pages_top_by_net_byte_difference + "/" +
        context.data_config['edit_edited_pages_top_by_net_byte_difference']['project']
        + "/" + context.data_config['edit_edited_pages_top_by_net_byte_difference']['editor_type']
        + "/" + context.data_config['edit_edited_pages_top_by_net_byte_difference']['page_type_invalid']
        + "/" + context.data_config['edit_edited_pages_top_by_net_byte_difference']['year_2018']
        + "/" + context.data_config['edit_edited_pages_top_by_net_byte_difference']['month_01'] + "/" +
        context.data_config['edit_edited_pages_top_by_net_byte_difference']['day_01'], headers=header)


@given("request is made to the AQS Edited Pages top by net byte difference endpoint with invalid year")
def step_impl(context):
    context.response = requests.get(
        uri + edit_edited_pages_top_by_net_byte_difference + "/" +
        context.data_config['edit_edited_pages_top_by_net_byte_difference']['project']
        + "/" + context.data_config['edit_edited_pages_top_by_net_byte_difference']['editor_type']
        + "/" + context.data_config['edit_edited_pages_top_by_net_byte_difference']['page_type_all']
        + "/" + context.data_config['edit_edited_pages_top_by_net_byte_difference']['year_invalid']
        + "/" + context.data_config['edit_edited_pages_top_by_net_byte_difference']['month_01'] + "/" +
        context.data_config['edit_edited_pages_top_by_net_byte_difference']['day_01'], headers=header)


@given("request is made to the AQS Edited Pages top by net byte difference endpoint with invalid month")
def step_impl(context):
    context.response = requests.get(
        uri + edit_edited_pages_top_by_net_byte_difference + "/" +
        context.data_config['edit_edited_pages_top_by_net_byte_difference']['project']
        + "/" + context.data_config['edit_edited_pages_top_by_net_byte_difference']['editor_type']
        + "/" + context.data_config['edit_edited_pages_top_by_net_byte_difference']['page_type_all']
        + "/" + context.data_config['edit_edited_pages_top_by_net_byte_difference']['year_2018']
        + "/" + context.data_config['edit_edited_pages_top_by_net_byte_difference']['month_invalid'] + "/" +
        context.data_config['edit_edited_pages_top_by_net_byte_difference']['day_01'], headers=header)


@given("request is made to the AQS Edited Pages top by net byte difference endpoint with invalid day")
def step_impl(context):
    context.response = requests.get(
        uri + edit_edited_pages_top_by_net_byte_difference + "/" +
        context.data_config['edit_edited_pages_top_by_net_byte_difference']['project']
        + "/" + context.data_config['edit_edited_pages_top_by_net_byte_difference']['editor_type']
        + "/" + context.data_config['edit_edited_pages_top_by_net_byte_difference']['page_type_all']
        + "/" + context.data_config['edit_edited_pages_top_by_net_byte_difference']['year_2018']
        + "/" + context.data_config['edit_edited_pages_top_by_net_byte_difference']['month_01'] + "/" +
        context.data_config['edit_edited_pages_top_by_net_byte_difference']['day_invalid'], headers=header)