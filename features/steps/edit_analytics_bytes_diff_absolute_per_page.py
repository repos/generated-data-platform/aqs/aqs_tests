from behave import *
import requests
from resources.all_resources.resources import edit_bytes_diff_absolute_per_page
from utilities.base_uri import uri
from utilities.header import header
import assertpy

@given(
    "request is made to the bytes difference absolute per page endpoint with varying {projects}, editor_types, all-page-types, daily, 20180101, 20181231")
def step_impl(context, projects):
    context.response = requests.get(
        uri + edit_bytes_diff_absolute_per_page + "/" + projects
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['page_title']
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['editor_type']
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['granularity']
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['start_date']
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['end_date'], headers=header)


@given(
    "request is made to the bytes difference absolute per page endpoint with project, page_title, varying {editor_types}, all-page-types, daily, 20200101, 20201201")
def step_impl(context, editor_types):
    context.response = requests.get(
        uri + edit_bytes_diff_absolute_per_page + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['project']
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['page_title']
        + "/" + editor_types
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['granularity']
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['start_date']
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['end_date'], headers=header)


@given(
    "request is made to the bytes difference absolute per page endpoint with project, page_title, editor_types, all-page-types, varying {granularity}, 20200101, 20201201")
def step_impl(context, granularity):
    context.response = requests.get(
        uri + edit_bytes_diff_absolute_per_page + "/" +
        context.data_config['edit_bytes_difference_absolute_per_page']['project']
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['page_title']
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['editor_type']
        + "/" + granularity
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['start_date']
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['end_date'], headers=header)


@step("verify that the granularity in the response is the same as the {granularity} in the absolute per page endpoint")
def step_impl(context, granularity):
    for obj in context.response.json()["items"]:
        assertpy.assert_that(obj["granularity"]).is_equal_to(granularity)


@given("request is made to the Edit analytics bytes difference absolute per page with invalid project")
def step_impl(context):
    context.response = requests.get(
        uri + edit_bytes_diff_absolute_per_page + "/" +
        context.data_config['edit_bytes_difference_absolute_per_page']['project_invalid']
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['page_title']
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['editor_type']
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['granularity']
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['start_date']
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['end_date'], headers=header)


@given(
    "request is made to the Edit analytics bytes difference absolute per page with special character in the project")
def step_impl(context):
    context.response = requests.get(
        uri + edit_bytes_diff_absolute_per_page + "/" +
        context.data_config['edit_bytes_difference_absolute_per_page']['project_special_character']
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['page_title']
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['editor_type']
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['granularity']
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['start_date']
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['end_date'], headers=header)


@step("verify that detail key from the error response displaying the special character warning for absolute per page")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).contains(
        "the parameter `project` contains invalid characters")


@given("request is made to the Edit analytics bytes difference absolute per page with invalid page title")
def step_impl(context):
    context.response = requests.get(
        uri + edit_bytes_diff_absolute_per_page + "/" +
        context.data_config['edit_bytes_difference_absolute_per_page']['project']
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['page_title_invalid']
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['editor_type']
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['granularity']
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['start_date']
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['end_date'], headers=header)


@given("request is made to the bytes difference absolute per page with invalid editor type")
def step_impl(context):
    context.response = requests.get(
        uri + edit_bytes_diff_absolute_per_page + "/" +
        context.data_config['edit_bytes_difference_absolute_per_page']['project']
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['page_title']
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['editor_type_invalid']
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['granularity']
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['start_date']
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['end_date'], headers=header)


@step("verify that detail key from the error response for invalid editor type for byte diff absolute per page")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).contains(
        "should be equal to one of the allowed values: [all-editor-types, anonymous, group-bot, name-bot, user]")


@given("request is made to the bytes difference absolute per page with invalid granularity")
def step_impl(context):
    context.response = requests.get(
        uri + edit_bytes_diff_absolute_per_page + "/" +
        context.data_config['edit_bytes_difference_absolute_per_page']['project']
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['page_title']
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['editor_type']
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['granularity_invalid']
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['start_date']
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['end_date'], headers=header)


@step("verify that detail key from the error response for invalid granularity for byte diff absolute per page")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).contains(
        "should be equal to one of the allowed values: [daily, monthly]")


@given("request is made to the bytes difference absolute per page with invalid start date")
def step_impl(context):
    context.response = requests.get(
        uri + edit_bytes_diff_absolute_per_page + "/" +
        context.data_config['edit_bytes_difference_absolute_per_page']['project']
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['page_title']
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['editor_type']
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['granularity']
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['start_date_invalid']
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['end_date'], headers=header)


@step("verify that detail key from the error response for invalid start date for byte diff absolute per page")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).contains(
        "start timestamp is invalid, must be a valid date in YYYYMMDD format")


@given("request is made to the bytes difference absolute per page with invalid end date")
def step_impl(context):
    context.response = requests.get(
        uri + edit_bytes_diff_absolute_per_page + "/" +
        context.data_config['edit_bytes_difference_absolute_per_page']['project']
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['page_title']
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['editor_type']
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['granularity']
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['start_date']
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['end_date_invalid'], headers=header)

@step("verify that detail key from the error response shows invalid end date for byte diff absolute per page")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).contains(
        "end timestamp is invalid, must be a valid date in YYYYMMDD format")


@given("request is made to the bytes difference absolute per page with end date before start date")
def step_impl(context):
    context.response = requests.get(
        uri + edit_bytes_diff_absolute_per_page + "/" +
        context.data_config['edit_bytes_difference_absolute_per_page']['project']
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['page_title']
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['editor_type']
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['granularity']
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['end_date']
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['start_date'], headers=header)


@step("verify that detail key from the error response for end date before start date for byte diff absolute per page")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).contains(
        "start timestamp should be before the end timestamp")


@given("request is made to the bytes difference absolute per page with no full months specified")
def step_impl(context):
    context.response = requests.get(
        uri + edit_bytes_diff_absolute_per_page + "/" +
        context.data_config['edit_bytes_difference_absolute_per_page']['project']
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['page_title']
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['editor_type']
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['granularity_monthly']
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['start_date']
        + "/" + context.data_config['edit_bytes_difference_absolute_per_page']['start_date'], headers=header)


@step(
    "verify that detail key from the error response for with no full months specified for byte diff absolute per page")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).contains(
        "no full months found in specified date range")
