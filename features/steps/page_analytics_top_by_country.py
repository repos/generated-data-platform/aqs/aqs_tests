import requests
import assertpy
from behave import *
from resources.all_resources.resources import *
from utilities.base_uri import *
from utilities.header import header


@given("request is made to the page analytics top by country endpoint with project, {access}, year, month")
def step_impl(context, access):
    context.response = requests.get(
        uri + page_analytics_top_by_country + "/" + context.data_config['page_analytics_top_by_country']['project'] + "/" + access
        + "/" + context.data_config['page_analytics_top_by_country']['year']
        + "/" + context.data_config['page_analytics_top_by_country']['month'],
        headers=header)


@given("request is made to the page analytics top by country endpoint with {project}, access, year, month")
def step_impl(context, project):
    context.response = requests.get(
        uri + page_analytics_top_by_country + "/" + project + "/" + context.data_config['page_analytics_top_by_country']['access']
        + "/" + context.data_config['page_analytics_top_by_country']['year']
        + "/" + context.data_config['page_analytics_top_by_country']['month'],
        headers=header)


@step("verify the year in the request matches year in the response")
def step_impl(context):
    assertpy.assert_that(context.response.json()["items"][0]["year"]).is_equal_to(
        context.data_config['page_analytics_top_by_country']['year'])


@given("request is made to the page analytics top by country endpoint with invalid project")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_top_by_country + "/" + context.data_config['page_analytics_top_by_country']['project_invalid'] + "/"
        + context.data_config['page_analytics_top_by_country']['access']
        + "/" + context.data_config['page_analytics_top_by_country']['year']
        + "/" + context.data_config['page_analytics_top_by_country']['month'],
        headers=header)


@step("verify that detail key from the error response displays invalid project warning for pageviews_top_by_country")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).contains(
        context.data_config['pageviews_top_by_country_validations']['invalid_project_validation'])


@given("request is made to the page analytics top by country endpoint with invalid access")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_top_by_country + "/" + context.data_config['page_analytics_top_by_country']['project'] + "/"
        + context.data_config['page_analytics_top_by_country']['access_invalid']
        + "/" + context.data_config['page_analytics_top_by_country']['year']
        + "/" + context.data_config['page_analytics_top_by_country']['month'],
        headers=header)


@step("verify that detail key from the error response displays invalid access warning for pageviews_top_by_country")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).contains(
        context.data_config['pageviews_top_by_country_validations']['invalid_access_validation'])


@given("request is made to the page analytics top by country endpoint with invalid year")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_top_by_country + "/" + context.data_config['page_analytics_top_by_country']['project'] + "/"
        + context.data_config['page_analytics_top_by_country']['access']
        + "/" + context.data_config['page_analytics_top_by_country']['year_invalid']
        + "/" + context.data_config['page_analytics_top_by_country'][
            'month'],
        headers=header)


@step("verify that detail key from the error response displays invalid year warning for pageviews_top_by_country")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).is_equal_to(
        context.data_config['pageviews_top_by_country_validations']['invalid_year_month'])


@given("request is made to the page analytics top by country endpoint with invalid month")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_top_by_country + "/" + context.data_config['page_analytics_top_by_country']['project'] + "/"
        + context.data_config['page_analytics_top_by_country']['access']
        + "/" + context.data_config['page_analytics_top_by_country']['year']
        + "/" + context.data_config['page_analytics_top_by_country'][
            'month_invalid'],
        headers=header)


@step("verify that detail key from the error response displays invalid month warning for pageviews_top_by_country")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).is_equal_to(
        context.data_config['pageviews_top_by_country_validations']['invalid_year_month'])


@given("request is made to the page analytics top by country endpoint with invalid character {project}")
def step_impl(context, project):
    context.response = requests.get(
        uri + page_analytics_top_by_country + "/" + project + "/"
        + context.data_config['page_analytics_top_by_country']['access']
        + "/" + context.data_config['page_analytics_top_by_country']['year']
        + "/" + context.data_config['page_analytics_top_by_country'][
            'month'],
        headers=header)


@step("verify that detail key from the error response displays invalid character")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).contains(
        context.data_config['pageviews_top_by_country_validations']['special_character_validation'])


@given("request is made to the page analytics top by country endpoint with invalid route")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_top_by_country + "/" + context.data_config['page_analytics_top_by_country']['project'] + "//"
        + context.data_config['page_analytics_top_by_country']['access']
        + "/" + context.data_config['page_analytics_top_by_country']['year']
        + "/" + context.data_config['page_analytics_top_by_country']['month'], headers=header)


@given("request is made to the page analytics top by country endpoint with {year} with no data")
def step_impl(context, year):
    context.response = requests.get(
        uri + page_analytics_top_by_country + "/" + context.data_config['page_analytics_top_by_country']['project']
        + "/" + context.data_config['page_analytics_top_by_country']['access']
        + "/" + year + "/" + context.data_config['page_analytics_top_by_country']['month'], headers=header)


@given("request is made to the page analytics top by country endpoint with varying {year}")
def step_impl(context, year):
    context.response = requests.get(
        uri + page_analytics_top_by_country + "/" + context.data_config['page_analytics_top_by_country']['project']
        + "/" + context.data_config['page_analytics_top_by_country']['access']
        + "/" + year + "/" + context.data_config['page_analytics_top_by_country']['month'], headers=header)


@given("request is made to page analytics top by country endpoint with en.wikipedia project, year 2018, month 01")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_top_by_country + "/" + context.data_config['page_analytics_top_by_country']['project'] + "/"
        + context.data_config['page_analytics_top_by_country']['access']
        + "/" + context.data_config['page_analytics_top_by_country']['year_2018'] + "/" +
        context.data_config['page_analytics_top_by_country']['month'], headers=header)


@given("request is made to page analytics top by country endpoint with en.wikipedia project, year 2019, month 01")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_top_by_country + "/" + context.data_config['page_analytics_top_by_country']['project']
        + "/" + context.data_config['page_analytics_top_by_country']['access']
        + "/" + context.data_config['page_analytics_top_by_country']['year_2019']
        + "/" + context.data_config['page_analytics_top_by_country']['month'], headers=header)


@given("request is made to page analytics top by country endpoint with en.wikipedia project, year 2020, month 01")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_top_by_country + "/" + context.data_config['page_analytics_top_by_country']['project'] + "/"
        + context.data_config['page_analytics_top_by_country']['access']
        + "/" + context.data_config['page_analytics_top_by_country']['year_2020'] + "/" +
        context.data_config['page_analytics_top_by_country']['month'], headers=header)


@given("request is made to page analytics top by country endpoint with valid data {projects}")
def step_impl(context, projects):
    context.response = requests.get(
        uri + page_analytics_top_by_country + "/" + projects + "/"
        + context.data_config['page_analytics_top_by_country']['access']
        + "/" + context.data_config['page_analytics_top_by_country']['year_2020']
        + "/" + context.data_config['page_analytics_top_by_country']['month'], headers=header)


@given("request is made to page analytics top by country endpoint with invalid data {projects}")
def step_impl(context, projects):
    context.response = requests.get(
        uri + page_analytics_top_by_country + "/" + projects + "/"
        + context.data_config['page_analytics_top_by_country']['access']
        + "/" + context.data_config['page_analytics_top_by_country']['year_2020']
        + "/" + context.data_config['page_analytics_top_by_country'][
            'month'], headers=header)