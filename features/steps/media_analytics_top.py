from behave import *
import requests
import assertpy
from resources.all_resources.resources import media_analytics_top
from utilities.base_uri import uri
from utilities.header import header


@given("request is made to the media analytics top endpoint with varying {referer}")
def step_impl(context, referer):
    context.response = requests.get(
        uri + media_analytics_top + "/" + referer
        + "/" + context.data_config['media_analytics_top']['media_type']
        + "/" + context.data_config['media_analytics_top']['year']
        + "/" + context.data_config['media_analytics_top']['month']
        + "/" + context.data_config['media_analytics_top']['day'],
        headers=header)


@step("verify that the {referer} used in request is same as the referer in response body")
def step_impl(context, referer):
    for array in context.response.json()["items"]:
        assertpy.assert_that(array["referer"]).is_equal_to(referer)


@given("request is made to the media analytics top endpoint with image media type and varying {referer}")
def step_impl(context, referer):
    context.response = requests.get(
        uri + media_analytics_top + "/" + referer
        + "/" + context.data_config['media_analytics_top']['media_type_image']
        + "/" + context.data_config['media_analytics_top']['year']
        + "/" + context.data_config['media_analytics_top']['month']
        + "/" + context.data_config['media_analytics_top']['day'],
        headers=header)


@given("request is made to the media analytics top endpoint with video media type and varying {referer}")
def step_impl(context, referer):
    context.response = requests.get(
        uri + media_analytics_top + "/" + referer
        + "/" + context.data_config['media_analytics_top']['media_type_video']
        + "/" + context.data_config['media_analytics_top']['year']
        + "/" + context.data_config['media_analytics_top']['month']
        + "/" + context.data_config['media_analytics_top']['day'],
        headers=header)


@given("request is made to the media analytics top endpoint with audio media type and varying {referer}")
def step_impl(context, referer):
    context.response = requests.get(
        uri + media_analytics_top + "/" + referer
        + "/" + context.data_config['media_analytics_top']['media_type_audio']
        + "/" + context.data_config['media_analytics_top']['year']
        + "/" + context.data_config['media_analytics_top']['month']
        + "/" + context.data_config['media_analytics_top']['day'],
        headers=header)


@given("request is made to the media analytics top endpoint with document media type and varying {referer}")
def step_impl(context, referer):
    context.response = requests.get(
        uri + media_analytics_top + "/" + referer
        + "/" + context.data_config['media_analytics_top']['media_type_document']
        + "/" + context.data_config['media_analytics_top']['year']
        + "/" + context.data_config['media_analytics_top']['month']
        + "/" + context.data_config['media_analytics_top']['day'],
        headers=header)


@given("request is made to the media analytics top endpoint with other media type and varying {referer}")
def step_impl(context, referer):
    context.response = requests.get(
        uri + media_analytics_top + "/" + referer
        + "/" + context.data_config['media_analytics_top']['media_type_other']
        + "/" + context.data_config['media_analytics_top']['year']
        + "/" + context.data_config['media_analytics_top']['month']
        + "/" + context.data_config['media_analytics_top']['day'],
        headers=header)


@step("verify that {key} key with sub_key {sub_key} sub_key contains {obj} with {file} file extensions")
def step_impl(context, key, sub_key, obj, file):
    def assert_media_file(file_type):
        for array in context.response.json()[key]:
            for item in array[sub_key]:
                if item[obj].split(".")[1] in file_type:
                    assertpy.assert_that(file_type).contains(str(item[obj].split(".")[1]).lower())
                else:
                    assertpy.assert_that(str(item[obj].split(".")[0]).lower()).contains('wiki')

    if file == "image":
        arr = ["svg", "v", "png", "gif", "jpg", "logo", "jpeg", "30", "5x", "tiff", "pdf", "djvu", "ani", "9", "agr",
               "wp-orange-source", "79",
               "svg_aj_ashton_01", "_louis_cardinals_logo", "_hoffa_nywts", "_bir_ankara_polisiyesi_afi%c5%9f", "s",
               "_fox_2012_(cropped)_(2)", "thott", "1_lc", "022-8)", "ht_logo", "06", "12", "_liga_logo_(2014)",
               "_kennedy%2c_white_house_color_photo_portrait", "1_(green)", "com-logo", "gov_crop", "_fulgentius", "09",
               "malmesbury", "m", "_img_022", "tif", "i", "a",
               "_2016_cjcs_uso_holiday_tour_-_incirlik_air_base_161205-d-pb383-044_(31430825446)_(cropped)_(cropped)",
               "_babasaheb_ambedkar_(film)", "_logo_2009", "utd_1905-06_dailygraph", "%d0%91", "", "_1900"
                                                                                                   "_camel_profile%2c_near_silverton%2c_nsw%2c_07"]
        assert_media_file(arr)
    elif file == "video":
        arr = ["ogv", "webm", "mov", "avi", "wmv", "mkv", "mp4", "vob", "mp2", "qt", "flv", "swf", "mpe", "mpv", "mpeg",
               "ogg", "mpg", "mp2", "mpe", "mp4", "m4p", "m4v", "_5", "theora", "_louis_bank_robbery_(1959)",
               "_strangelove_(1964)_-_trailer", "04", "0072879", "s", "0004391", "_strangelove_(1964)_-_trailer",
               "_%d0%95%d0%ba%d1%81%d0%bf%d0%bb%d0%be%d0%b7%d0%b8%d0%b2%d0%bd%d0%b8_%d1%81%d0%b2%d0%be%d1%98%d1%81%d1%"
               "82%d0%b2%d0%b0_%d0%bd%d0%b0_%d1%82%d1%80%d0%b8_%d1%80%d0%b0%d0%b7%d0%bb%d0%b8%d1%87%d0%bd%d0%b8_%d1%82%"
               "d0%b8%d0%bf%d0%be%d0%b2%d0%b8_%d0%b5%d0%ba%d1%81%d0%bf%d0%bb%d0%be%d0%b7%d0%b8%d0%b2%d0%b8",
               "_kennedy_and_jacqueline_kennedy_vote_on_election_day", "archives", "_wolfgang_moroder", "_(1924)",
               "_%d0%95%d0%ba%d1%81%d0%bf%d0%bb%d0%be%d0%b7%d0%b8%d0%b2%d0%bd%d0%b8_%d1%81%d0%b2%d0%be%d1%98%d1%81%d1%8"
               "2%d0%b2%d0%b0_%d0%bd%d0%b0_%d1%82%d1%80%d0%b8_%d1%80%d0%b0%d0%b7%d0%bb%d0%b8%d1%87%d0%bd%d0%b8_%d1%82%d"
               "0%b8%d0%bf%d0%be%d0%b2%d0%b8_%d0%b5%d0%ba%d1%81%d0%bf%d0%bb%d0%be%d0%b7%d0%b8%d0%b2%d0%b8",
               "_%d0%a0%d0%b5%d0%b0%d0%ba%d1%86%d0%b8%d1%98%d0%b0_%d0%bc%d0%b5%d1%93%d1%83_%d1%81%d0%b8%d0%bb%d0%bd%d0%"
               "be_%d0%be%d0%ba%d1%81%d0%b8%d0%b4%d0%b0%d1%86%d0%b8%d0%be%d0%bd%d0%be_%d0%b8_%d1%80%d0%b5%d0%b4%d1%83%d0"
               "%ba%d1%86%d0%b8%d0%be%d0%bd%d0%be_%d1%81%d1%80%d0%b5%d0%b4%d1%81%d1%82%d0%b2%d0%be", "com)", "webmhd",
               "f1", "_%d0%9e%d1%81%d0%bb%d0%be%d0%b1%d0%be%d0%b4%d1%83%d0%b2%d0%b0%d1%9a%d0%b5_%d0%bd%d0%b0_%d0%b3%d0"
                     "%be%d0%bb%d0%b5%d0%bc%d0%be_%d0%ba%d0%be%d0%bb%d0%b8%d1%87%d0%b5%d1%81%d1%82%d0%b2%d0%be_%d0%b5%d"
                     "0%bd%d0%b5%d1%80%d0%b3%d0%b8%d1%98%d0%b0_%d0%bf%d1%80%d0%b8_%d1%81%d0%be%d0%b3%d0%be%d1%80%d0%be%"
                     "d0%b2%d1%83%d0%b0%d1%9a%d0%b5_%d0%b5%d1%82%d0%b0%d0%bd%d0%be%d0%bb",
               "_%d0%a0%d0%b5%d0%b0%d0%ba%d1%86%d0%b8%d1%98%d0%b0_%d0%bc%d0%b5%d1%93%d1%83_%d1%81%d0%b8%d0%bb%d0%bd%d0%"
               "be_%d0%be%d0%ba%d1%81%d0%b8%d0%b4%d0%b0%d1%86%d0%b8%d0%be%d0%bd%d0%be_%d0%b8_%d1%80%d0%b5%d0%b4%d1%83%d"
               "0%ba%d1%86%d0%b8%d0%be%d0%bd%d0%be_%d1%81%d1%80%d0%b5%d0%b4%d1%81%d1%82%d0%b2%d0%be", "r",
               "_lin-manuel_miranda", "_edison", "_abbot_bitt_at_convent", "_c", "ntis", "0050901", "_edison",
               "oggtheora", "0127367", "com_-_viennese_apartment",
               "_%d0%a1%d0%bb%d0%be%d0%bd%d0%be%d0%b2%d1%81%d0%ba%d0%b0_%d0%bf%d0%b0%d1%81%d1%82%d0%b0_%d0%b7%d0%b"
               "0_%d0%b7%d0%b0%d0%b1%d0%b8", "_johnson_speech_(september_29%2c_1967)",
               "_iii)_at_the_white_house_(trimmed)", "0054629", "_science_museum_painless_exhibition_series"
                                                                "_%d0%a0%d0%be%d1%82%d0%b0%d1%86%d0%b8%d0%be%d0%bd%d0%b5%d0%bd_%d1%81%d1%82%d0%be%d0%bb",
               "0149107"]
        assert_media_file(arr)

    elif file == "audio":
        arr = ["efs", "abc", "flp", "pcm", "wav", "aiff", "mp3", "aac", "ogg", "wma", "flac", "alac", "wav", "mp4",
               "m4a", "ec3", "wrproj", "ust", "sf2", "toc", "asd", "aup", "rip", "mid", "oga", "p", "s",
               "_3_in_g_minor_-_2", "_yaprak_asimov)", "k-france", "_allegro_con_brio",
               "_stephan%2c_vienna_-_october_26%2c_2013", "k-gen%c3%a8ve", "%e0%ae%aa%e0%af%8a", "_molto_allegro",
               "0158546", "_9_in_e_minor_'from_the_new_world'%2c_op", "creator", "_goode",
               "_bourgeois%2c_director_%c2%b7_john_philip_sousa_%c2%b7_united_states_marine_band"
               "_truman's_farewell_address_1953", "k-charlotte_gainsbourg", "", "vorb", "a", "0000892",
               "_bourgeois%2c_director_%c2%b7_john_philip_sousa_%c2%b7_united_states_marine_band", "nlw-llandudno", "r",
               "_14_in_c_sharp_minor_'moonlight'%2c_op", "_truman's_farewell_address_1953", "hymnus",
               "364_wo_die_citronen_bl%c3%bch'n!", "_m%c3%bcller-ableismus", "_iii)_at_the_white_house_(trimmed)",
               "_ou_instructions_sur_les_principales_v%c3%a9rit%c3%a9s_de_la_religion_-_1758", "0_cn_license", "_nat",
               "_38", "1862", "alikhlas-misharyrashedalafasy"]
        assert_media_file(arr)

    elif file == "document":
        arr = ["djvu", "pdf", "1862",
               "_starr_in_conformity_with_the_requriement_of_title_28%2c_united_states_code%2c_section_595(c)",
               "_e", "_racz", "154_loreley-rhein-kl%c3%a4nge", "_stephan%2c_vienna_-_october_26%2c_2013",
               "_trump%2c_president_of_the_united_states_%e2%80%94_report_of_the_committee_on_the_judiciary%2c_house_of_representatives",
               "_31", "05", "forwiki", "_etymologicum_magnum_romaniae_-_dic%c8%9b", "9%2c_op", "_trump",
               "_thom%c3%a6_phillipps%2c_bart", "364_wo_die_citronen_bl%c3%bch'n!",
               "_2)%2c_1821", "_burt%2c_1904)", "_1", "_borneo%2c_sarawak_and_singapore", "s", "_38",
               "_thom%c3%a6_phillipps%2c_bart", "_res", "_vindman", "_2", "_2)%2c_1821", "_poe",
               "_men_the_complete_collection_(2010)%2c_bottom_cover", "_i%2c_1929_%e2%80%93_beic_1845433", "_a",
               "_ou_instructions_sur_les_principales_v%c3%a9rit%c3%a9s_de_la_religion_-_1758", "0_cn_license", "_nat"
               ]
        assert_media_file(arr)

    elif file == "other":
        arr = ["stl", "opus", "ico"]
        assert_media_file(arr)


@given("request is made to the media analytics top endpoint with all media types and varying valid {years}")
def step_impl(context, years):
    context.response = requests.get(
        uri + media_analytics_top + "/" + context.data_config['media_analytics_top']['referer']
        + "/" + context.data_config['media_analytics_top']['media_type']
        + "/" + years + "/" + context.data_config['media_analytics_top']['month']
        + "/" + context.data_config['media_analytics_top']['day'],
        headers=header)


@given("request is made to the media analytics top endpoint with all media types and varying invalid {years}")
def step_impl(context, years):
    context.response = requests.get(
        uri + media_analytics_top + "/" + context.data_config['media_analytics_top']['referer']
        + "/" + context.data_config['media_analytics_top']['media_type']
        + "/" + years + "/" + context.data_config['media_analytics_top']['month']
        + "/" + context.data_config['media_analytics_top']['day'],
        headers=header)


@step("verify that detail key from the error response for invalid date or project")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).contains(
        "project you asked for is not loaded yet")


@given("request is made to the media analytics top endpoint with invalid referer")
def step_impl(context):
    context.response = requests.get(
        uri + media_analytics_top + "/" + context.data_config['media_analytics_top']['referer_invalid']
        + "/" + context.data_config['media_analytics_top']['media_type']
        + "/" + context.data_config['media_analytics_top']['year']
        + "/" + context.data_config['media_analytics_top']['month']
        + "/" + context.data_config['media_analytics_top']['day'],
        headers=header)


@given("request is made to the media analytics top endpoint with invalid media type")
def step_impl(context):
    context.response = requests.get(
        uri + media_analytics_top + "/" + context.data_config['media_analytics_top']['referer']
        + "/" + context.data_config['media_analytics_top']['media_type_invalid']
        + "/" + context.data_config['media_analytics_top']['year']
        + "/" + context.data_config['media_analytics_top']['month']
        + "/" + context.data_config['media_analytics_top']['day'],
        headers=header)


@step("verify that detail key from the error response contains media type validation")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).is_equal_to("media-type should be equal to one of the "
                                                                        "allowed values: [all-media-types, image, "
                                                                        "video, audio, document, other]")


@given("request is made to the media analytics top endpoint with invalid Year date")
def step_impl(context):
    context.response = requests.get(
        uri + media_analytics_top + "/" + context.data_config['media_analytics_top']['referer']
        + "/" + context.data_config['media_analytics_top']['media_type']
        + "/" + context.data_config['media_analytics_top']['year_invalid']
        + "/" + context.data_config['media_analytics_top']['month']
        + "/" + context.data_config['media_analytics_top']['day'],
        headers=header)


@step("verify that detail key from the error response contains year date validation")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"].lower()).contains("Given year/month/day is invalid date".lower())


@given("request is made to the media analytics top endpoint with invalid month date")
def step_impl(context):
    context.response = requests.get(
        uri + media_analytics_top + "/" + context.data_config['media_analytics_top']['referer']
        + "/" + context.data_config['media_analytics_top']['media_type']
        + "/" + context.data_config['media_analytics_top']['year']
        + "/" + context.data_config['media_analytics_top']['month_invalid']
        + "/" + context.data_config['media_analytics_top']['day'],
        headers=header)


@step("verify that detail key from the error response contains month date validation")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"].lower()).contains("Given year/month/day is invalid date".lower())


@given("request is made to the media analytics top endpoint with invalid day date")
def step_impl(context):
    context.response = requests.get(
        uri + media_analytics_top + "/" + context.data_config['media_analytics_top']['referer']
        + "/" + context.data_config['media_analytics_top']['media_type']
        + "/" + context.data_config['media_analytics_top']['year']
        + "/" + context.data_config['media_analytics_top']['month']
        + "/" + context.data_config['media_analytics_top']['day_invalid'],
        headers=header)


@step("verify that detail key from the error response contains day date validation")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"].lower()).contains("Given year/month/day is invalid date".lower())


@given("request is made to the media analytics top endpoint to validate first and last data point")
def step_impl(context):
    context.response = requests.get(
        uri + media_analytics_top + "/" + context.data_config['media_analytics_top']['referer_first_last']
        + "/" + context.data_config['media_analytics_top']['media_type']
        + "/" + context.data_config['media_analytics_top']['year']
        + "/" + context.data_config['media_analytics_top']['month']
        + "/" + context.data_config['media_analytics_top']['day'],
        headers=header)
