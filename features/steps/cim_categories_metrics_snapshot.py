from behave import *
import requests
from resources.all_resources.resources import *
from utilities.base_uri import uri
from utilities.header import header


@given("request is made to the Categories Metrics Snapshot endpoint with varying category {category} 20231101 and 20240601 times")
def step_impl(context, category):
    context.response = requests.get(
        uri + cim_category_metrics_snapshot_resource
        + "/" + category
        + "/" + context.data_config['cim_category_metrics_snapshot']['start_date']
        + "/" + context.data_config['cim_category_metrics_snapshot']['end_date'],
        headers=header)


@given("request is made to the Categories Metrics Snapshot endpoint with and invalid route")
def step_impl(context):
    context.response = requests.get(
        uri + cim_category_metrics_snapshot_resource_invalid
        + "/" + context.data_config['cim_category_metrics_snapshot']['category']
        + "/" + context.data_config['cim_category_metrics_snapshot']['start_date']
        + "/" + context.data_config['cim_category_metrics_snapshot']['end_date'],
        headers=header)


@given("request is made to the Categories Metrics Snapshot endpoint with and invalid category")
def step_impl(context):
    context.response = requests.get(
        uri + cim_category_metrics_snapshot_resource
        + "/" + context.data_config['cim_category_metrics_snapshot']['category_invalid']
        + "/" + context.data_config['cim_category_metrics_snapshot']['start_date']
        + "/" + context.data_config['cim_category_metrics_snapshot']['end_date'],
        headers=header)


@given("request is made to the Categories Metrics Snapshot endpoint with and invalid start time")
def step_impl(context):
    context.response = requests.get(
        uri + cim_category_metrics_snapshot_resource
        + "/" + context.data_config['cim_category_metrics_snapshot']['category']
        + "/" + context.data_config['cim_category_metrics_snapshot']['start_date_invalid']
        + "/" + context.data_config['cim_category_metrics_snapshot']['end_date'],
        headers=header)


@given("request is made to the Categories Metrics Snapshot endpoint with and invalid end time")
def step_impl(context):
    context.response = requests.get(
        uri + cim_category_metrics_snapshot_resource
        + "/" + context.data_config['cim_category_metrics_snapshot']['category']
        + "/" + context.data_config['cim_category_metrics_snapshot']['start_date']
        + "/" + context.data_config['cim_category_metrics_snapshot']['end_date_invalid'],
        headers=header)


@given("request is made to the Categories Metrics Snapshot endpoint to validate counts and deep")
def step_impl(context):
    context.response = requests.get(
        uri + cim_category_metrics_snapshot_resource
        + "/" + context.data_config['cim_category_metrics_snapshot']['category']
        + "/" + context.data_config['cim_category_metrics_snapshot']['start_date']
        + "/" + context.data_config['cim_category_metrics_snapshot']['end_date'],
        headers=header)