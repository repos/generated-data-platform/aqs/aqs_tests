from behave import *
import requests
from utilities.header import header
from resources.all_resources.resources import cim_edits_per_user_monthly_resource
from utilities.base_uri import *


@given("request is made to the Edits per user monthly endpoint with varying users {user}, 20231101 and 20240601")
def step_impl(context, user):
    context.response = requests.get(
        uri + cim_edits_per_user_monthly_resource
        + "/" + user
        + "/" + context.data_config['cim_edits_per_user_monthly']['edit_type']
        + "/" + context.data_config['cim_edits_per_user_monthly']['start_time']
        + "/" + context.data_config['cim_edits_per_user_monthly']['end_time'],
        headers=header)


@given("request made to Edits per user monthly endpoint with varying edit type {edit_type}, 20231101 and 20240601")
def step_impl(context, edit_type):
    context.response = requests.get(
        uri + cim_edits_per_user_monthly_resource
        + "/" + context.data_config['cim_edits_per_user_monthly']['user']
        + "/" + edit_type
        + "/" + context.data_config['cim_edits_per_user_monthly']['start_time']
        + "/" + context.data_config['cim_edits_per_user_monthly']['end_time'],
        headers=header)


@given("request is made to the Edits per user monthly endpoint with invalid route")
def step_impl(context):
    context.response = requests.get(
        uri + cim_edits_per_user_monthly_resource
        + "/" + "/" + context.data_config['cim_edits_per_user_monthly']['user']
        + "/" + context.data_config['cim_edits_per_user_monthly']['edit_type']
        + "/" + context.data_config['cim_edits_per_user_monthly']['start_time']
        + "/" + context.data_config['cim_edits_per_user_monthly']['end_time'],
        headers=header)


@given("request is made to the Edits per user monthly endpoint with invalid username")
def step_impl(context):
    context.response = requests.get(
        uri + cim_edits_per_user_monthly_resource
        + "/" + context.data_config['cim_edits_per_user_monthly']['user_invalid']
        + "/" + context.data_config['cim_edits_per_user_monthly']['edit_type']
        + "/" + context.data_config['cim_edits_per_user_monthly']['start_time']
        + "/" + context.data_config['cim_edits_per_user_monthly']['end_time'],
        headers=header)


@given("request is made to the Edits per user monthly endpoint with invalid edit type")
def step_impl(context):
    context.response = requests.get(
        uri + cim_edits_per_user_monthly_resource
        + "/" + context.data_config['cim_edits_per_user_monthly']['user']
        + "/" + context.data_config['cim_edits_per_user_monthly']['edit_type_invalid']
        + "/" + context.data_config['cim_edits_per_user_monthly']['start_time']
        + "/" + context.data_config['cim_edits_per_user_monthly']['end_time'],
        headers=header)


@given("request is made to the Edits per user monthly endpoint with invalid start time")
def step_impl(context):
    context.response = requests.get(
        uri + cim_edits_per_user_monthly_resource
        + "/" + context.data_config['cim_edits_per_user_monthly']['user']
        + "/" + context.data_config['cim_edits_per_user_monthly']['edit_type']
        + "/" + context.data_config['cim_edits_per_user_monthly']['start_time_invalid']
        + "/" + context.data_config['cim_edits_per_user_monthly']['end_time'],
        headers=header)


@given("request is made to the Edits per user monthly endpoint with invalid end time")
def step_impl(context):
    context.response = requests.get(
        uri + cim_edits_per_user_monthly_resource
        + "/" + context.data_config['cim_edits_per_user_monthly']['user']
        + "/" + context.data_config['cim_edits_per_user_monthly']['edit_type']
        + "/" + context.data_config['cim_edits_per_user_monthly']['start_time']
        + "/" + context.data_config['cim_edits_per_user_monthly']['end_time_invalid'],
        headers=header)


@given("request is made to the Edits per user monthly endpoint to validate timestamp and edit count")
def step_impl(context):
    context.response = requests.get(
        uri + cim_edits_per_user_monthly_resource
        + "/" + context.data_config['cim_edits_per_user_monthly']['user']
        + "/" + context.data_config['cim_edits_per_user_monthly']['edit_type']
        + "/" + context.data_config['cim_edits_per_user_monthly']['start_time']
        + "/" + context.data_config['cim_edits_per_user_monthly']['end_time'],
        headers=header)