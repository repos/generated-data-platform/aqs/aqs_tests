from behave import *
import requests
from utilities.header import header
from resources.all_resources.resources import cim_edits_per_category_monthly_resource
from utilities.base_uri import *


@given(
    "request is made to the Edits per category monthly endpoint with varying category {category}, 20231101 and 20240601")
def step_impl(context, category):
    context.response = requests.get(
        uri + cim_edits_per_category_monthly_resource
        + "/" + category
        + "/" + context.data_config['cim_edits_per_category_monthly']['category_scope']
        + "/" + context.data_config['cim_edits_per_category_monthly']['edit_type']
        + "/" + context.data_config['cim_edits_per_category_monthly']['start_time']
        + "/" + context.data_config['cim_edits_per_category_monthly']['end_time'],
        headers=header)


@given("request is made to the Edits per category monthly endpoint with varying scope {scope}, 20231101 and 20240601")
def step_impl(context, scope):
    context.response = requests.get(
        uri + cim_edits_per_category_monthly_resource
        + "/" + context.data_config['cim_edits_per_category_monthly']['category']
        + "/" + scope
        + "/" + context.data_config['cim_edits_per_category_monthly']['edit_type']
        + "/" + context.data_config['cim_edits_per_category_monthly']['start_time']
        + "/" + context.data_config['cim_edits_per_category_monthly']['end_time'],
        headers=header)


@given(
    "request is made to the Edits per category monthly endpoint with varying edit_type {edit_type}, 20231101 and 20240601")
def step_impl(context, edit_type):
    context.response = requests.get(
        uri + cim_edits_per_category_monthly_resource
        + "/" + context.data_config['cim_edits_per_category_monthly']['category']
        + "/" + context.data_config['cim_edits_per_category_monthly']['category_scope']
        + "/" + edit_type
        + "/" + context.data_config['cim_edits_per_category_monthly']['start_time']
        + "/" + context.data_config['cim_edits_per_category_monthly']['end_time'],
        headers=header)


@given("request is made to the Edits per category monthly endpoint with invalid route")
def step_impl(context):
    context.response = requests.get(
        uri + cim_edits_per_category_monthly_resource
        + "/" + "/" + context.data_config['cim_edits_per_category_monthly']['category']
        + "/" + context.data_config['cim_edits_per_category_monthly']['category_scope']
        + "/" + context.data_config['cim_edits_per_category_monthly']['edit_type']
        + "/" + context.data_config['cim_edits_per_category_monthly']['start_time']
        + "/" + context.data_config['cim_edits_per_category_monthly']['end_time'],
        headers=header)


@given("request is made to the Edits per category monthly endpoint with invalid category")
def step_impl(context):
    context.response = requests.get(
        uri + cim_edits_per_category_monthly_resource
        + "/" + context.data_config['cim_edits_per_category_monthly']['category_invalid']
        + "/" + context.data_config['cim_edits_per_category_monthly']['category_scope']
        + "/" + context.data_config['cim_edits_per_category_monthly']['edit_type']
        + "/" + context.data_config['cim_edits_per_category_monthly']['start_time']
        + "/" + context.data_config['cim_edits_per_category_monthly']['end_time'],
        headers=header)


@given("request is made to the Edits per category monthly endpoint with invalid scope")
def step_impl(context):
    context.response = requests.get(
        uri + cim_edits_per_category_monthly_resource
        + "/" + context.data_config['cim_edits_per_category_monthly']['category']
        + "/" + context.data_config['cim_edits_per_category_monthly']['category_scope_invalid']
        + "/" + context.data_config['cim_edits_per_category_monthly']['edit_type']
        + "/" + context.data_config['cim_edits_per_category_monthly']['start_time']
        + "/" + context.data_config['cim_edits_per_category_monthly']['end_time'],
        headers=header)


@given("request is made to the Edits per category monthly endpoint with invalid edit type")
def step_impl(context):
    context.response = requests.get(
        uri + cim_edits_per_category_monthly_resource
        + "/" + context.data_config['cim_edits_per_category_monthly']['category']
        + "/" + context.data_config['cim_edits_per_category_monthly']['category_scope']
        + "/" + context.data_config['cim_edits_per_category_monthly']['edit_type_invalid']
        + "/" + context.data_config['cim_edits_per_category_monthly']['start_time']
        + "/" + context.data_config['cim_edits_per_category_monthly']['end_time'],
        headers=header)


@given("request is made to the Edits per category monthly endpoint with invalid start time")
def step_impl(context):
    context.response = requests.get(
        uri + cim_edits_per_category_monthly_resource
        + "/" + context.data_config['cim_edits_per_category_monthly']['category']
        + "/" + context.data_config['cim_edits_per_category_monthly']['category_scope']
        + "/" + context.data_config['cim_edits_per_category_monthly']['edit_type']
        + "/" + context.data_config['cim_edits_per_category_monthly']['start_time_invalid']
        + "/" + context.data_config['cim_edits_per_category_monthly']['end_time'],
        headers=header)


@given("request is made to the Edits per category monthly endpoint with invalid end time")
def step_impl(context):
    context.response = requests.get(
        uri + cim_edits_per_category_monthly_resource
        + "/" + context.data_config['cim_edits_per_category_monthly']['category']
        + "/" + context.data_config['cim_edits_per_category_monthly']['category_scope']
        + "/" + context.data_config['cim_edits_per_category_monthly']['edit_type']
        + "/" + context.data_config['cim_edits_per_category_monthly']['start_time']
        + "/" + context.data_config['cim_edits_per_category_monthly']['end_time_invalid'],
        headers=header)


@given("request is made to the Edits per category monthly endpoint to validate timestamp and edit count")
def step_impl(context):
    context.response = requests.get(
        uri + cim_edits_per_category_monthly_resource
        + "/" + context.data_config['cim_edits_per_category_monthly']['category']
        + "/" + context.data_config['cim_edits_per_category_monthly']['category_scope']
        + "/" + context.data_config['cim_edits_per_category_monthly']['edit_type']
        + "/" + context.data_config['cim_edits_per_category_monthly']['start_time']
        + "/" + context.data_config['cim_edits_per_category_monthly']['end_time'],
        headers=header)