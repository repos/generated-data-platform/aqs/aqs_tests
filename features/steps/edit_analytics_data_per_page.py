from behave import *
import assertpy
import requests
from utilities.header import header
from resources.all_resources.resources import edit_data_per_page
from utilities.base_uri import uri


@given(
    "request is made to the edit data per_page endpoint with en.wikipedia, Ukraine, {editor_type}, all-page-types, "
    "daily, 20200101 and 20201231")
def step_impl(context, editor_type):
    context.response = requests.get(
        uri + edit_data_per_page + "/" + context.data_config['edit_data_per_page']['project']
        + "/" + context.data_config['edit_data_per_page']['page_title'] + "/" +
        editor_type + "/" + context.data_config['edit_data_per_page']['granularity'] + "/" +
        context.data_config['edit_data_per_page']['start_date'] + "/" + context.data_config['edit_data_per_page'][
            'end_date'], headers=header)


@given(
    "request is made to the edit data per_page endpoint with en.wikipedia, Ukraine, {editor_type}, all-page-types, "
    "monthly, 20200101 and 20201231")
def step_impl(context, editor_type):
    context.response = requests.get(
        uri + edit_data_per_page + "/" + context.data_config['edit_data_per_page']['project']
        + "/" + context.data_config['edit_data_per_page']['page_title'] + "/" +
        editor_type + "/" + context.data_config['edit_data_per_page']['granularity_monthly'] + "/" +
        context.data_config['edit_data_per_page']['start_date'] + "/" + context.data_config['edit_data_per_page'][
            'end_date'], headers=header)


@given(
    "request is made to the edit data per_page endpoint with {projects}, Ukraine, "
    "all-editor-types, daily, 20200101 and 20201231")
def step_impl(context, projects):
    context.response = requests.get(
        uri + edit_data_per_page + "/" + projects
        + "/" + context.data_config['edit_data_per_page']['page_title'] + "/" +
        context.data_config['edit_data_per_page']['editor_type_all']
        + "/" + context.data_config['edit_data_per_page']['granularity'] + "/" +
        context.data_config['edit_data_per_page']['start_date']
        + "/" + context.data_config['edit_data_per_page'][
            'end_date'], headers=header)

@given(
    "request is made to the edit data per_page endpoint with mobile {projects}, Ukraine, editor, daily, 20200101 and 20201231")
def step_impl(context, projects):
    context.response = requests.get(
        uri + edit_data_per_page + "/" + projects
        + "/" + context.data_config['edit_data_per_page']['page_title'] + "/" +
        context.data_config['edit_data_per_page']['editor_type_all']
        + "/" + context.data_config['edit_data_per_page']['granularity'] + "/" +
        context.data_config['edit_data_per_page']['start_date']
        + "/" + context.data_config['edit_data_per_page'][
            'end_date'], headers=header)

@given(
    "request is made to the edit data per_page endpoint with invalid project, Ukraine, all-editor-types, monthly, 20200101 and 20201231")
def step_impl(context):
    context.response = requests.get(
        uri + edit_data_per_page + "/" + context.data_config['edit_data_per_page']['project_invalid']
        + "/" + context.data_config['edit_data_per_page']['page_title'] + "/" +
        context.data_config['edit_data_per_page']['editor_type_all']
        + "/" + context.data_config['edit_data_per_page']['granularity_monthly'] + "/" +
        context.data_config['edit_data_per_page']['start_date']
        + "/" + context.data_config['edit_data_per_page']['end_date'], headers=header)


@step("verify that title key from the error response displays title not found error")
def step_impl(context):
    assertpy.assert_that(context.response.json()["title"].lower()).is_equal_to(
        "Not found".lower())


@given(
    "request is made to the edit data per_page endpoint with a project, Ukraine, invalid editor, monthly, 20200101 and 20201231")
def step_impl(context):
    context.response = requests.get(
        uri + edit_data_per_page + "/" + context.data_config['edit_data_per_page']['project']
        + "/" + context.data_config['edit_data_per_page']['page_title'] + "/" +
        context.data_config['edit_data_per_page']['editor_type_invalid']
        + "/" + context.data_config['edit_data_per_page']['granularity_monthly'] + "/" +
        context.data_config['edit_data_per_page']['start_date']
        + "/" + context.data_config['edit_data_per_page']['end_date'], headers=header)


@step("verify that detail key from the error response displays the unknown selection error")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).contains(
        "editor-type should be equal to one of the allowed values: [all-editor-types, anonymous, group-bot, name-bot, "
        "user]")


@given(
    "request is made to the edit data per_page endpoint with a project, Ukraine, editor, invalid granularity, 20200101 and 20201231")
def step_impl(context):
    context.response = requests.get(
        uri + edit_data_per_page + "/" + context.data_config['edit_data_per_page']['project']
        + "/" + context.data_config['edit_data_per_page']['page_title'] + "/" +
        context.data_config['edit_data_per_page']['editor_type_all']
        + "/" + context.data_config['edit_data_per_page']['granularity_invalid'] + "/" +
        context.data_config['edit_data_per_page']['start_date']
        + "/" + context.data_config['edit_data_per_page']['end_date'], headers=header)


@step("verify that detail key from the error response displays the invalid granularity selection error")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).contains(
        "granularity should be equal to one of the allowed values: [daily, monthly]")


@given(
    "request is made to the edit data per_page endpoint with a project, Ukraine, editor, granularity, invalid start date and 20201231")
def step_impl(context):
    context.response = requests.get(
        uri + edit_data_per_page + "/" + context.data_config['edit_data_per_page']['project']
        + "/" + context.data_config['edit_data_per_page']['page_title'] + "/" +
        context.data_config['edit_data_per_page']['editor_type_all']
        + "/" + context.data_config['edit_data_per_page']['granularity'] + "/" +
        context.data_config['edit_data_per_page']['start_date_invalid']
        + "/" + context.data_config['edit_data_per_page']['end_date'], headers=header)


@step("verify that detail key from the error response displays the invalid date timestamp error")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).is_equal_to(
        "start timestamp is invalid, must be a valid date in YYYYMMDD format")



@given(
    "request is made to the edit data per_page endpoint with a project, Ukraine, editor, granularity, later start date than end date")
def step_impl(context):
    context.response = requests.get(
        uri + edit_data_per_page + "/" + context.data_config['edit_data_per_page']['project']
        + "/" + context.data_config['edit_data_per_page']['page_title'] + "/" +
        context.data_config['edit_data_per_page']['editor_type_all']
        + "/" + context.data_config['edit_data_per_page']['granularity'] + "/" +
        context.data_config['edit_data_per_page']['start_date_later']
        + "/" + context.data_config['edit_data_per_page']['end_date'], headers=header)


@step("verify that detail key from the error response displays the wrong logical date error")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).is_equal_to(
        "start timestamp should be before the end timestamp")


@given(
    "request is made to the edit data per_page endpoint with a project, Ukraine, editor, monthly, incomplete full month")
def step_impl(context):
    context.response = requests.get(
        uri + edit_data_per_page + "/" + context.data_config['edit_data_per_page']['project']
        + "/" + context.data_config['edit_data_per_page']['page_title'] + "/" +
        context.data_config['edit_data_per_page']['editor_type_all']
        + "/" + context.data_config['edit_data_per_page']['granularity_monthly'] + "/" +
        context.data_config['edit_data_per_page']['start_date']
        + "/" + context.data_config['edit_data_per_page']['end_date_incomplete_month'], headers=header)


@step("verify that detail key from the error response displays the no full month error")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).is_equal_to(
        "no full months found in specified date range")
