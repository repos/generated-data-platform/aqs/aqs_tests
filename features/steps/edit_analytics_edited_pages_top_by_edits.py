import assertpy
import requests
from behave import *
from resources.all_resources.resources import edit_edited_pages_top_by_edits
from utilities.base_uri import uri
from utilities.header import header


@given(
    "request is made to the Edit analytics edited pages top by edits endpoint with varying {projects}, editor_types, all-page-types, daily, 20200101, 20201201")
def step_impl(context, projects):
    context.response = requests.get(
        uri + edit_edited_pages_top_by_edits + "/" + projects
        + "/" + context.data_config['edit_edited_pages_top_by_edits']['editor_type']
        + "/" + context.data_config['edit_edited_pages_top_by_edits']['page_type_all']
        + "/" + context.data_config['edit_edited_pages_top_by_edits']['year_2018']
        + "/" + context.data_config['edit_edited_pages_top_by_edits']['month_01'] + "/" +
        context.data_config['edit_edited_pages_top_by_edits']['day_01'], headers=header)


@step("verify the page_title key in the top sub key")
def step_impl(context):
    for item in context.response.json()['items'][0]['results'][0]['top']:
        assertpy.assert_that(item).contains_key("page_title")


@step("verify the edits key in the top sub key")
def step_impl(context):
    for item in context.response.json()['items'][0]['results'][0]['top']:
        assertpy.assert_that(item).contains_key("edits")


@step("verify the rank key in the top sub key")
def step_impl(context):
    for item in context.response.json()['items'][0]['results'][0]['top']:
        assertpy.assert_that(item).contains_key("rank")


@given(
    "request is made to the Edits analytics edited pages top by edits endpoint with en.wikipedia, varying {editor_types}, all-page-types, daily, 20180101, 20181231")
def step_impl(context, editor_types):
    context.response = requests.get(
        uri + edit_edited_pages_top_by_edits + "/" + context.data_config['edit_edited_pages_top_by_edits']['project']
        + "/" + editor_types
        + "/" + context.data_config['edit_edited_pages_top_by_edits']['page_type_all']
        + "/" + context.data_config['edit_edited_pages_top_by_edits']['year_2018']
        + "/" + context.data_config['edit_edited_pages_top_by_edits']['month_01'] + "/" +
        context.data_config['edit_edited_pages_top_by_edits']['day_01'], headers=header)


@given(
    "request is made to the Edits analytics edited pages top by edits endpoint with en.wikipedia, editor_types, varying {page_types}, daily, 20200101, 20201201")
def step_impl(context, page_types):
    context.response = requests.get(
        uri + edit_edited_pages_top_by_edits + "/" + context.data_config['edit_edited_pages_top_by_edits']['project']
        + "/" + context.data_config['edit_edited_pages_top_by_edits']['editor_type']
        + "/" + page_types
        + "/" + context.data_config['edit_edited_pages_top_by_edits']['year_2018']
        + "/" + context.data_config['edit_edited_pages_top_by_edits']['month_01'] + "/" +
        context.data_config['edit_edited_pages_top_by_edits']['day_01'], headers=header)


@given("request is made to the Edits analytics edited pages top by edits endpoint with invalid editor type")
def step_impl(context):
    context.response = requests.get(
        uri + edit_edited_pages_top_by_edits + "/" + context.data_config['edit_edited_pages_top_by_edits'][
            'project_invalid']
        + "/" + context.data_config['edit_edited_pages_top_by_edits']['editor_type_invalid']
        + "/" + context.data_config['edit_edited_pages_top_by_edits']['page_type_all']
        + "/" + context.data_config['edit_edited_pages_top_by_edits']['year_2018']
        + "/" + context.data_config['edit_edited_pages_top_by_edits']['month_01'] + "/" +
        context.data_config['edit_edited_pages_top_by_edits']['day_01'], headers=header)


@given("request is made to the AQS Edits analytics edited pages top by edits endpoint with invalid page type")
def step_impl(context):
    context.response = requests.get(
        uri + edit_edited_pages_top_by_edits + "/" + context.data_config['edit_edited_pages_top_by_edits'][
            'project']
        + "/" + context.data_config['edit_edited_pages_top_by_edits']['editor_type']
        + "/" + context.data_config['edit_edited_pages_top_by_edits']['page_type_invalid']
        + "/" + context.data_config['edit_edited_pages_top_by_edits']['year_2018']
        + "/" + context.data_config['edit_edited_pages_top_by_edits']['month_01'] + "/" +
        context.data_config['edit_edited_pages_top_by_edits']['day_01'], headers=header)


@given("request is made to the AQS Edits analytics edited pages top by edits endpoint with invalid year")
def step_impl(context):
    context.response = requests.get(
        uri + edit_edited_pages_top_by_edits + "/" + context.data_config['edit_edited_pages_top_by_edits'][
            'project']
        + "/" + context.data_config['edit_edited_pages_top_by_edits']['editor_type']
        + "/" + context.data_config['edit_edited_pages_top_by_edits']['page_type_all']
        + "/" + context.data_config['edit_edited_pages_top_by_edits']['year_invalid']
        + "/" + context.data_config['edit_edited_pages_top_by_edits']['month_01'] + "/" +
        context.data_config['edit_edited_pages_top_by_edits']['day_01'], headers=header)


@given("request is made to the AQS Edits analytics edited pages top by edits endpoint with invalid month")
def step_impl(context):
    context.response = requests.get(
        uri + edit_edited_pages_top_by_edits + "/" + context.data_config['edit_edited_pages_top_by_edits'][
            'project']
        + "/" + context.data_config['edit_edited_pages_top_by_edits']['editor_type']
        + "/" + context.data_config['edit_edited_pages_top_by_edits']['page_type_all']
        + "/" + context.data_config['edit_edited_pages_top_by_edits']['year_2018']
        + "/" + context.data_config['edit_edited_pages_top_by_edits']['month_invalid'] + "/" +
        context.data_config['edit_edited_pages_top_by_edits']['day_01'], headers=header)


@given("request is made to the AQS Edits analytics edited pages top by edits endpoint with invalid day")
def step_impl(context):
    context.response = requests.get(
        uri + edit_edited_pages_top_by_edits + "/" + context.data_config['edit_edited_pages_top_by_edits'][
            'project']
        + "/" + context.data_config['edit_edited_pages_top_by_edits']['editor_type']
        + "/" + context.data_config['edit_edited_pages_top_by_edits']['page_type_all']
        + "/" + context.data_config['edit_edited_pages_top_by_edits']['year_2018']
        + "/" + context.data_config['edit_edited_pages_top_by_edits']['month_01'] + "/" +
        context.data_config['edit_edited_pages_top_by_edits']['day_invalid'], headers=header)