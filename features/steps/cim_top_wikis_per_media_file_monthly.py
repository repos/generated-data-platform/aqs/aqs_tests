from behave import *
import requests
from utilities.header import header

from resources.all_resources.resources import cim_top_wikis_per_media_file_monthly_resource
from utilities.base_uri import *


@given("request is made to the Top wikis per media files monthly endpoint with varying file {file}, 2023 and 11")
def step_impl(context, file):
    context.response = requests.get(
        uri + cim_top_wikis_per_media_file_monthly_resource
        + "/" + file
        + "/" + context.data_config['top_wikis_per_media_file_monthly']['year']
        + "/" + context.data_config['top_wikis_per_media_file_monthly']['month'],
        headers=header)


@given("request is made to the Top wikis per media files monthly endpoint with invalid route")
def step_impl(context):
    context.response = requests.get(
        uri + cim_top_wikis_per_media_file_monthly_resource
        + "/" + "/" + context.data_config['top_wikis_per_media_file_monthly']['media_file']
        + "/" + context.data_config['top_wikis_per_media_file_monthly']['year']
        + "/" + context.data_config['top_wikis_per_media_file_monthly']['month'],
        headers=header)


@given("request is made to the Top wikis per media files monthly endpoint with invalid file")
def step_impl(context):
    context.response = requests.get(
        uri + cim_top_wikis_per_media_file_monthly_resource
        + "/" + context.data_config['top_wikis_per_media_file_monthly']['media_file_invalid']
        + "/" + context.data_config['top_wikis_per_media_file_monthly']['year']
        + "/" + context.data_config['top_wikis_per_media_file_monthly']['month'],
        headers=header)


@given("request is made to the Top wikis per media files monthly endpoint with invalid year")
def step_impl(context):
    context.response = requests.get(
        uri + cim_top_wikis_per_media_file_monthly_resource
        + "/" + context.data_config['top_wikis_per_media_file_monthly']['media_file']
        + "/" + context.data_config['top_wikis_per_media_file_monthly']['year_invalid']
        + "/" + context.data_config['top_wikis_per_media_file_monthly']['month'],
        headers=header)


@given("request is made to the Top wikis per media files monthly endpoint with invalid month")
def step_impl(context):
    context.response = requests.get(
        uri + cim_top_wikis_per_media_file_monthly_resource
        + "/" + context.data_config['top_wikis_per_media_file_monthly']['media_file']
        + "/" + context.data_config['top_wikis_per_media_file_monthly']['year']
        + "/" + context.data_config['top_wikis_per_media_file_monthly']['month_invalid'],
        headers=header)


@given("request is made to the Top wikis per media files monthly endpoint to validate category rank and edit count")
def step_impl(context):
    context.response = requests.get(
        uri + cim_top_wikis_per_media_file_monthly_resource
        + "/" + context.data_config['top_wikis_per_media_file_monthly']['media_file']
        + "/" + context.data_config['top_wikis_per_media_file_monthly']['year']
        + "/" + context.data_config['top_wikis_per_media_file_monthly']['month'],
        headers=header)


@given("request is made to the Top pages per media file monthly endpoint with invalid wiki")
def step_impl(context):
    context.response = requests.get(
        uri + cim_top_wikis_per_media_file_monthly_resource
        + "/" + context.data_config['top_wikis_per_media_file_monthly']['media_file_invalid']
        + "/" + context.data_config['top_wikis_per_media_file_monthly']['year']
        + "/" + context.data_config['top_wikis_per_media_file_monthly']['month'],
        headers=header)