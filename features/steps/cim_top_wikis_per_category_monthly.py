from behave import *
import requests
from utilities.header import header
from resources.all_resources.resources import cim_top_wikis_per_category_monthly_resource
from utilities.base_uri import *


@given(
    "request is made to the Top wikis per category monthly endpoint with varying category {category}, 2023 and 11")
def step_impl(context, category):
    context.response = requests.get(
        uri + cim_top_wikis_per_category_monthly_resource
        + "/" + category
        + "/" + context.data_config['cim_top_wikis_per_category_monthly']['category_scope']
        + "/" + context.data_config['cim_top_wikis_per_category_monthly']['year']
        + "/" + context.data_config['cim_top_wikis_per_category_monthly']['month'],
        headers=header)


@given("request is made to the Top wikis per category monthly endpoint with varying scope {scope}, 2023 and 11")
def step_impl(context, scope):
    context.response = requests.get(
        uri + cim_top_wikis_per_category_monthly_resource
        + "/" + context.data_config['cim_top_wikis_per_category_monthly']['category']
        + "/" + scope
        + "/" + context.data_config['cim_top_wikis_per_category_monthly']['year']
        + "/" + context.data_config['cim_top_wikis_per_category_monthly']['month'],
        headers=header)


@given("request is made to the Top wikis per category monthly endpoint with invalid route")
def step_impl(context):
    context.response = requests.get(
        uri + cim_top_wikis_per_category_monthly_resource
        + "/" + "/" + context.data_config['cim_top_wikis_per_category_monthly']['category']
        + "/" + context.data_config['cim_top_wikis_per_category_monthly']['category_scope']
        + "/" + context.data_config['cim_top_wikis_per_category_monthly']['year']
        + "/" + context.data_config['cim_top_wikis_per_category_monthly']['month'],
        headers=header)


@given("request is made to the Top wikis per category monthly endpoint with invalid category")
def step_impl(context):
    context.response = requests.get(
        uri + cim_top_wikis_per_category_monthly_resource
        + "/" + context.data_config['cim_top_wikis_per_category_monthly']['category_invalid']
        + "/" + context.data_config['cim_top_wikis_per_category_monthly']['category_scope']
        + "/" + context.data_config['cim_top_wikis_per_category_monthly']['year']
        + "/" + context.data_config['cim_top_wikis_per_category_monthly']['month'],
        headers=header)


@given("request is made to the Top wikis per category monthly endpoint with invalid scope")
def step_impl(context):
    context.response = requests.get(
        uri + cim_top_wikis_per_category_monthly_resource
        + "/" + context.data_config['cim_top_wikis_per_category_monthly']['category']
        + "/" + context.data_config['cim_top_wikis_per_category_monthly']['category_scope_invalid']
        + "/" + context.data_config['cim_top_wikis_per_category_monthly']['year']
        + "/" + context.data_config['cim_top_wikis_per_category_monthly']['month'],
        headers=header)


@given("request is made to the Top wikis per category monthly endpoint with invalid year")
def step_impl(context):
    context.response = requests.get(
        uri + cim_top_wikis_per_category_monthly_resource
        + "/" + context.data_config['cim_top_wikis_per_category_monthly']['category']
        + "/" + context.data_config['cim_top_wikis_per_category_monthly']['category_scope']
        + "/" + context.data_config['cim_top_wikis_per_category_monthly']['year_invalid']
        + "/" + context.data_config['cim_top_wikis_per_category_monthly']['month'],
        headers=header)


@given("request is made to the Top wikis per category monthly endpoint with invalid month")
def step_impl(context):
    context.response = requests.get(
        uri + cim_top_wikis_per_category_monthly_resource
        + "/" + context.data_config['cim_top_wikis_per_category_monthly']['category']
        + "/" + context.data_config['cim_top_wikis_per_category_monthly']['category_scope']
        + "/" + context.data_config['cim_top_wikis_per_category_monthly']['year']
        + "/" + context.data_config['cim_top_wikis_per_category_monthly']['month_invalid'],
        headers=header)


@given("request is made to the Top wikis per category monthly endpoint to validate category rank and pageview count")
def step_impl(context):
    context.response = requests.get(
        uri + cim_top_wikis_per_category_monthly_resource
        + "/" + context.data_config['cim_top_wikis_per_category_monthly']['category']
        + "/" + context.data_config['cim_top_wikis_per_category_monthly']['category_scope']
        + "/" + context.data_config['cim_top_wikis_per_category_monthly']['year']
        + "/" + context.data_config['cim_top_wikis_per_category_monthly']['month'],
        headers=header)