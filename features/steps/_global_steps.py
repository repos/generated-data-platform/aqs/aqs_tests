""" These are steps that are reusable across the test framework"""

import assertpy
from behave import *
from jsonschema import validate
from schemas.commons_impact_metrics_schema import schema_commons_impact_metrics
from schemas.edit_analytics_schema import schema_edit_analytics
from schemas.editor_analytics_schema import schema_editor_analytics
from schemas.geo_analytics_schema import schema_geo_analytics
from schemas.media_analytics_schema import schema_media_analytics
from schemas.page_analytics_schema import schema_page_analytics
from schemas.device_analytics_schema import schema_device_analytics


@then("the request should be successful with status code of {status_code:d}")
def step_impl(context, status_code):
    assertpy.assert_that(context.response.status_code).is_equal_to(status_code)


# @then("the request should be successful with status code of {status_code:d}")
# def step_impl(context, status_code):
#     assertpy.assert_that(context.response.status_code).is_equal_to(status_code)


@then("the request should be unsuccessful with status code of {status_code:d}")
def step_impl(context, status_code):
    assertpy.assert_that(context.response.status_code).is_equal_to(status_code)


@step("verify that the content-type is {content_type}")
def step_impl(context, content_type):
    assertpy.assert_that(context.response.headers["Content-Type"]).is_equal_to(content_type)


@step("verify that the header contains the {key}")
def step_impl(context, key):
    assertpy.assert_that(context.response.headers).contains(key)


@step("verify that the server contains {server}")
def step_impl(context, server):
    assertpy.assert_that(context.response.headers["server"]).contains(server)


@step("verify that {key} key contains {sub_key} in list of arrays")
def step_impl(context, key, sub_key):
    for array in context.response.json()[key]:
        assertpy.assert_that(array).contains_key(sub_key)


@step("verify that {key} key contains {sub_key} sub_key have upper case values")
def step_impl(context, key, sub_key):
    for array in context.response.json()[key]:
        assertpy.assert_that(array[sub_key]).is_upper()


@step("verify that {key} key contains {sub_key} sub_key has a length of {length:d}")
def step_impl(context, key, sub_key, length):
    for array in context.response.json()[key]:
        assertpy.assert_that(array[sub_key]).is_length(length)


@step("verify that {key} key with {sub_key} in is equal to {sub_key_value}")
def step_impl(context, key, sub_key, sub_key_value):
    for array in context.response.json()[key]:
        assertpy.assert_that(array[sub_key]).is_equal_to(sub_key_value)


@step(
    "verify that {key} key with sub_key {sub_key} sub_key contains {obj} where first data is equal to integer number {number:d}")
def step_impl(context, key, sub_key, obj, number):
    for array in context.response.json()[key]:
        assertpy.assert_that(array[sub_key][0][obj]).is_equal_to(number)


@step(
    "verify that {key} key with sub_key {sub_key} sub_key contains {obj} where last data is equal to integer number {number:d}")
def step_impl(context, key, sub_key, obj, number):
    for array in context.response.json()[key]:
        assertpy.assert_that(array[sub_key][-1][obj]).is_equal_to(number)


@step("verify that {key} key with sub_key {sub_key} is of type string")
def step_impl(context, key, sub_key):
    for array in context.response.json()[key]:
        assertpy.assert_that(array[sub_key]).is_type_of(str)


@step("verify that {key} key with sub_key {sub_key} is of type list")
def step_impl(context, key, sub_key):
    for array in context.response.json()[key]:
        assertpy.assert_that(array[sub_key]).is_type_of(list)


@step("verify that {key} key with sub_key {sub_key} is of type int")
def step_impl(context, key, sub_key):
    for array in context.response.json()[key]:
        assertpy.assert_that(array[sub_key]).is_type_of(int)


@step("verify that {key} key with sub_key {sub_key} is an empty list")
def step_impl(context, key, sub_key):
    for array in context.response.json()[key]:
        assertpy.assert_that(array[sub_key]).is_empty()


@step("verify that {key} key with sub_key {sub_key} is of type integer")
def step_impl(context, key, sub_key):
    for array in context.response.json()[key]:
        assertpy.assert_that(array[sub_key]).is_type_of(int)


@step("verify that {sub_key} sub_key in {key} key is equal to {sub_key_value}")
def step_impl(context, sub_key, key, sub_key_value):
    for array in context.response.json()[key]:
        assertpy.assert_that(array[sub_key]).is_equal_to(sub_key_value)


@step("verify that {sub_key} sub_key with .org domain in {key} key is equal to {sub_key_value}")
def step_impl(context, sub_key, key, sub_key_value):
    for array in context.response.json()[key]:
        assertpy.assert_that(array[sub_key] + ".org").is_equal_to(sub_key_value)


@step("verify that {key} key contains {value_key}")
def step_impl(context, key, value_key):
    assert value_key.lower() in context.response.json()[key].lower()


@step("verify that {key} key with sub_key {sub_key} sub_key contains {obj} of type int")
def step_impl(context, key, sub_key, obj):
    for array in context.response.json()[key]:
        for item in array[sub_key]:
            assertpy.assert_that(item[obj]).is_type_of(int)


@step("verify that {key} key with sub_key {sub_key} sub_key contains {obj} with file extension period")
def step_impl(context, key, sub_key, obj):
    for array in context.response.json()[key]:
        for item in array[sub_key]:
            assertpy.assert_that(item[obj]).contains(".")


@step("verify that {key} key with sub_key {sub_key} sub_key contains {obj} and is equal to {obj_value:d}")
def step_impl(context, key, sub_key, obj, obj_value):
    for array in context.response.json()[key]:
        for item in array[sub_key]:
            assertpy.assert_that(item[obj]).is_equal_to(obj_value)


@step("verify that {key} key with sub_key {sub_key} contains {obj} in the list")
def step_impl(context, key, sub_key, obj):
    for array in context.response.json()[key]:
        for item in array[sub_key]:
            assertpy.assert_that(item).contains_key(obj)


@step("verify that {key} key with sub_key {sub_key} sub_key contains {obj} of type string")
def step_impl(context, key, sub_key, obj):
    for array in context.response.json()[key]:
        for item in array[sub_key]:
            assertpy.assert_that(item[obj]).is_type_of(str)


@step("verify that {key} key with sub_key {sub_key} sub_key contains {obj} with character '-'")
def step_impl(context, key, sub_key, obj):
    for array in context.response.json()[key]:
        for item in array[sub_key]:
            assertpy.assert_that(item[obj]).contains('-')


@step("verify that {key} key with sub_key {sub_key} sub_key contains {obj} has upper case values")
def step_impl(context, key, sub_key, obj):
    for array in context.response.json()[key]:
        for item in array[sub_key]:
            assertpy.assert_that(item[obj]).is_upper()


@step("verify that {key} key with sub_key {sub_key} sub_key contains {obj} has length of two(2)")
def step_impl(context, key, sub_key, obj):
    for array in context.response.json()[key]:
        for item in array[sub_key]:
            assertpy.assert_that(item[obj]).is_length(2)


@step("verify that {key} key with sub_key {sub_key} sub_key contains {obj} that is divisible by 10")
def step_impl(context, key, sub_key, obj):
    for array in context.response.json()[key]:
        for item in array[sub_key]:
            assert item[obj] % 10 == 0


@step("verify that response does not contain key {key}")
def step_impl(context, key):
    assertpy.assert_that(context.response.json()).does_not_contain(key)


@step("verify the value of the {first} key in the first json is equal to {number:d}")
def step_impl(context, first, number):
    for array in context.response.json()["items"]:
        if array == context.response.json()["items"][0]:
            assertpy.assert_that(array[first]).is_equal_to(number)


@step("verify the value of the {last} key in the last json is equal to {number:d}")
def step_impl(context, last, number):
    for array in context.response.json()["items"]:
        if array == context.response.json()["items"][-1]:
            assertpy.assert_that(array[last]).is_equal_to(number)


@step("verify the value of the {first} key in the items object first json is equal to text {string}")
def step_impl(context, first, string):
    for array in context.response.json()["items"]:
        if array == context.response.json()["items"][0]:
            assertpy.assert_that(array[first]).is_equal_to(string)


@step("verify the value of the {last} key in the items object last json is equal to text {string}")
def step_impl(context, last, string):
    for array in context.response.json()["items"]:
        if array == context.response.json()["items"][-1]:
            assertpy.assert_that(array[last]).is_equal_to(string)


@step("verify the value of the {first} key in the first json of the {sub_key} key is equal to {number:d}")
def step_impl(context, first, sub_key, number):
    for array in context.response.json()["items"]:
        if array[sub_key] == context.response.json()["items"][0]:
            assertpy.assert_that(array[sub_key]).is_equal_to(number)


@step("verify the value of the {last} key in the last json of the {sub_key} key is equal to {number:d}")
def step_impl(context, last, sub_key, number):
    for array in context.response.json()["items"]:
        if array[sub_key] == context.response.json()["items"][-1]:
            assertpy.assert_that(array[sub_key]).is_equal_to(number)


@step("verify that top sub_key contains {obj} of type string")
def step_impl(context, obj):
    for array in context.response.json()["items"]:
        for item in array["results"][0]["top"]:
            assertpy.assert_that(item[obj]).is_type_of(str)


@step("verify that top sub_key contains {obj} of type string or none type")
def step_impl(context, obj):
    for array in context.response.json()["items"]:
        for item in array["results"][0]["top"]:
            if type(item[obj]) == str:
                assertpy.assert_that(item[obj]).is_type_of(str)
            elif item[obj] is None:
                assertpy.assert_that(item[obj]).is_equal_to(None)
            else:
                raise TypeError('This data type should not be returned')


@step("verify that top sub_key contains {obj} of type int")
def step_impl(context, obj):
    for array in context.response.json()["items"]:
        for item in array["results"][0]["top"]:
            assertpy.assert_that(item[obj]).is_type_of(int)


@step(
    "verify the value of {sub_key} key of the first json of the top results array is a string and is equal to {string}")
def step_impl(context, sub_key, string):
    first_top_result = context.response.json()["items"][0]["results"][0]["top"][0]
    assertpy.assert_that(first_top_result[sub_key]).is_type_of(str)
    assertpy.assert_that(first_top_result[sub_key]).is_equal_to(string)


@step(
    "verify the value of {sub_key} key of the last json of the top results array is a string and is equal to {string}")
def step_impl(context, sub_key, string):
    first_top_result = context.response.json()["items"][0]["results"][0]["top"][-1]
    assertpy.assert_that(first_top_result[sub_key]).is_type_of(str)
    assertpy.assert_that(first_top_result[sub_key]).is_equal_to(string)


@step(
    "verify the value of {sub_key} key of the first json of the top results array is an integer and is equal to {number:g}")
def step_impl(context, sub_key, number):
    first_top_result = context.response.json()["items"][0]["results"][0]["top"][0]
    assertpy.assert_that(first_top_result[sub_key]).is_type_of(int)
    assertpy.assert_that(first_top_result[sub_key]).is_equal_to(number)


@step(
    "verify the value of {sub_key} key of the last json of the top results array is an integer and is equal to {number:g}")
def step_impl(context, sub_key, number):
    first_top_result = context.response.json()["items"][0]["results"][0]["top"][-1]
    assertpy.assert_that(first_top_result[sub_key]).is_type_of(int)
    assertpy.assert_that(first_top_result[sub_key]).is_equal_to(number)


@step("verify the value of {sub_key} key of the first json of the results array is equal to {number:d}")
def step_impl(context, sub_key, number):
    data = context.response.json()["items"][0]
    first_results_json = data["results"][0]
    assertpy.assert_that(first_results_json[sub_key]).is_equal_to(number)


@step("verify the value of {sub_key} key of the last json of the results array is equal to {number:d}")
def step_impl(context, sub_key, number):
    data = context.response.json()["items"][0]
    first_results_json = data["results"][-1]
    assertpy.assert_that(first_results_json[sub_key]).is_equal_to(number)


@step("validate that the {schema} schema is valid")
def step_impl(context, schema):
    def validate_schema(instance_to_validate, schema_to_test):
        validate(instance=instance_to_validate, schema=schema_to_test)

    if schema == "editor_analytics":
        validate_schema(context.response.json(), schema_editor_analytics)

    elif schema == "media_analytics":
        validate_schema(context.response.json(), schema_media_analytics)

    elif schema == "page_analytics":
        validate_schema(context.response.json(), schema_page_analytics)

    elif schema == "edit_analytics":
        validate_schema(context.response.json(), schema_edit_analytics)

    elif schema == "geo_analytics":
        validate_schema(context.response.json(), schema_geo_analytics)

    elif schema == "device_analytics":
        validate_schema(context.response.json(), schema_device_analytics)

    elif schema == "commons_impact_metrics":
        validate_schema(context.response.json(), schema_commons_impact_metrics)
    else:
        raise Exception("Use the right service name")


@step("verify that the {key} in response is equal to {value}")
def step_impl(context, key, value):
    assertpy.assert_that(context.response.json()[key].lower()).is_equal_to(value.lower())


@step("verify that the {key} in response contains {value}")
def step_impl(context, key, value):
    assertpy.assert_that(context.response.json()[key].lower()).contains(value.lower())


@step("verify that response body with {key} key is of type dict")
def step_impl(context, key):
    assertpy.assert_that(context.response.json()[key]).is_type_of(dict)


@step("verify that the sub_key {sub_key} in the {key} key response is equal to {value}")
def step_impl(context, sub_key, key, value):
    assertpy.assert_that(context.response.json()[key][sub_key]).is_equal_to(value)


@step("verify the order of the {rank} key is numerical")
def step_impl(context, rank):
    context.previous_rank = None
    context.target_rank = 1
    context.repetition = 0

    for item in context.response.json()["items"]:
        if context.previous_rank is not None and item["rank"] == context.previous_rank:
            context.repetition += 1
        else:
            assertpy.assert_that(item["rank"]).is_equal_to(context.target_rank + context.repetition)
            context.repetition = 0
        context.previous_rank = item["rank"]
        context.target_rank = context.previous_rank + 1
