from behave import *
import requests
from utilities.header import header
from resources.all_resources.resources import editor_analytics_data_registered_users
from utilities.base_uri import uri


@given(
    "request is made to the editor analytics registered users endpoint with {project}, "
    "{granularity}, {start_date} and {end_date}")
def step_impl(context, project, granularity, start_date, end_date):
    context.response = requests.get(uri + editor_analytics_data_registered_users + "/" + project + "/"
                                    + granularity + "/" + start_date + "/" + end_date, headers=header)


@given(
    "request is made to the editors registered users endpoint using an invalid route")
def step_impl(context):
    context.response = requests.get(uri + editor_analytics_data_registered_users
                                    + context.data_config['editor_analytics_data_registered_users']['invalid_route'])
