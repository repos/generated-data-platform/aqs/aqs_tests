""" Test code for the pageviews endpoints with the BDD steps"""

import requests
import assertpy
from behave import *
from resources.all_resources.resources import *
from utilities.base_uri import *
from utilities.header import header


# Define the given section of the request call
@given(
    'request is made to the page analytics per_article endpoint with en.wikipedia.org, '
    '{access}, all-agents, St. Louis City SC, daily, 20220701 and 20220929')
def step_impl(context, access):
    context.response = requests.get(
        uri + page_analytics_per_article_resource + "/" + context.data_config['page_analytics_per_file']['project'] + "/" + access
        + "/" + context.data_config['page_analytics_per_file']['agents']
        + "/" + context.data_config['page_analytics_per_file']['article']
        + "/" + context.data_config['page_analytics_per_file']['granularity']
        + "/" + context.data_config['page_analytics_per_file']['start_date']
        + "/" + context.data_config['page_analytics_per_file']['end_date'],
        headers=header)


@given(
    "request is made to the page analytics per_article endpoint with en.wikipedia.org, access, varying {agents}, St. Louis City SC, "
    "daily, 20220701 and 20220929")
def step_impl(context, agents):
    context.response = requests.get(
        uri + page_analytics_per_article_resource + "/" + context.data_config['page_analytics_per_file']['project'] +
        "/" + context.data_config['page_analytics_per_file']['access']
        + "/" + agents + "/" + context.data_config['page_analytics_per_file']['article']
        + "/" + context.data_config['page_analytics_per_file']['granularity']
        + "/" + context.data_config['page_analytics_per_file']['start_date']
        + "/" + context.data_config['page_analytics_per_file']['end_date'],
        headers=header)


@given(
    "request is made to the page analytics per_article endpoint with en.wikipedia.org, {access}, all-agents, St. Louis City SC, "
    "monthly, 20220701 and 20220929")
def step_impl(context, access):
    context.response = requests.get(
        uri + page_analytics_per_article_resource + "/" + context.data_config['page_analytics_per_file'][
            'project'] + "/" + access
        + "/" + context.data_config['page_analytics_per_file']['agents']
        + "/" + context.data_config['page_analytics_per_file']['article']
        + "/" + context.data_config['page_analytics_per_file']['granularity_monthly']
        + "/" + context.data_config['page_analytics_per_file']['start_date']
        + "/" + context.data_config['page_analytics_per_file']['end_date'],
        headers=header)


@given(
    "request is made to the page analytics per_article endpoint with special character, access, all-agents, St. Louis City SC, monthly,"
    " 20220701 and 20220929")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_per_article_resource + "/" + context.data_config['page_analytics_per_file'][
            'project_special_character']
        + "/" + context.data_config['page_analytics_per_file']['access']
        + "/" + context.data_config['page_analytics_per_file']['agents']
        + "/" + context.data_config['page_analytics_per_file']['article']
        + "/" + context.data_config['page_analytics_per_file']['granularity_monthly']
        + "/" + context.data_config['page_analytics_per_file']['start_date']
        + "/" + context.data_config['page_analytics_per_file']['end_date'],
        headers=header)


@step("verify that detail key from the error response displays the invalid character error")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).is_equal_to(
        "the parameter `project` contains invalid characters")


@given(
    "request is made to the page analytics per_article endpoint with project, invalid access, all-agents, St. Louis City SC, monthly, 20220701 and 20220929")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_per_article_resource + "/" + context.data_config['page_analytics_per_file']['project']
        + "/" + context.data_config['page_analytics_per_file']['access_invalid']
        + "/" + context.data_config['page_analytics_per_file']['agents']
        + "/" + context.data_config['page_analytics_per_file']['article']
        + "/" + context.data_config['page_analytics_per_file']['granularity_monthly']
        + "/" + context.data_config['page_analytics_per_file']['start_date']
        + "/" + context.data_config['page_analytics_per_file']['end_date'],
        headers=header)


@step("verify that detail key from the error response displays the invalid access error")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).is_equal_to("access should be equal to one of the allowed "
                                                                        "values: [all-access, desktop, mobile-app, "
                                                                        "mobile-web]")


@given(
    "request is made to the page analytics per_article endpoint with project, access, invalid agent, St. Louis City SC, monthly, 20220701 and 20220929")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_per_article_resource + "/" + context.data_config['page_analytics_per_file']['project']
        + "/" + context.data_config['page_analytics_per_file']['access']
        + "/" + context.data_config['page_analytics_per_file']['agent_invalid']
        + "/" + context.data_config['page_analytics_per_file']['article']
        + "/" + context.data_config['page_analytics_per_file']['granularity_monthly']
        + "/" + context.data_config['page_analytics_per_file']['start_date']
        + "/" + context.data_config['page_analytics_per_file']['end_date'],
        headers=header)


@step("verify that detail key from the error response displays the invalid agent error")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).is_equal_to("agent should be equal to one of the allowed "
                                                                        "values: [all-agents, automated, spider, "
                                                                        "user]")


@given(
    "request is made to the page analytics per_article endpoint with project, access, agent, invalid article, monthly, 20220701 and 20220929")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_per_article_resource + "/" + context.data_config['page_analytics_per_file']['project']
        + "/" + context.data_config['page_analytics_per_file']['access']
        + "/" + context.data_config['page_analytics_per_file']['agents']
        + "/" + context.data_config['page_analytics_per_file']['article_invalid']
        + "/" + context.data_config['page_analytics_per_file']['granularity_monthly']
        + "/" + context.data_config['page_analytics_per_file']['start_date']
        + "/" + context.data_config['page_analytics_per_file']['end_date'],
        headers=header)


@step("verify that detail key from the error response displays the invalid article error")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).contains("The date(s) you used are valid, but we "
                                                                     "either do not have data for those date(s), "
                                                                     "or the project you asked for is not loaded "
                                                                     "yet.")


@given(
    "request is made to the page analytics per_article endpoint with project, access, agent, article, invalid granularity, 20220701 and 20220929")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_per_article_resource + "/" + context.data_config['page_analytics_per_file']['project']
        + "/" + context.data_config['page_analytics_per_file']['access']
        + "/" + context.data_config['page_analytics_per_file']['agents']
        + "/" + context.data_config['page_analytics_per_file']['article']
        + "/" + context.data_config['page_analytics_per_file']['granularity_invalid']
        + "/" + context.data_config['page_analytics_per_file']['start_date']
        + "/" + context.data_config['page_analytics_per_file']['end_date'],
        headers=header)


@step("verify that detail key from the error response displays the invalid granularity error")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).is_equal_to("granularity should be equal to one of the "
                                                                        "allowed values: [daily, monthly]")


@given(
    "request is made to the page analytics per_article endpoint with project, access, agent, article, granularity, later date before early date")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_per_article_resource + "/" + context.data_config['page_analytics_per_file']['project']
        + "/" + context.data_config['page_analytics_per_file']['access']
        + "/" + context.data_config['page_analytics_per_file']['agents']
        + "/" + context.data_config['page_analytics_per_file']['article']
        + "/" + context.data_config['page_analytics_per_file']['granularity_monthly']
        + "/" + context.data_config['page_analytics_per_file']['end_date']
        + "/" + context.data_config['page_analytics_per_file']['start_date'],
        headers=header)


@step("verify that detail key from the error response displays the invalid start and end date timestamp error")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).is_equal_to("start timestamp should be before the end"
                                                                        " timestamp")


@given(
    "request is made to the page analytics per_article endpoint with project, access, agent, article, daily granularity, later date before early date")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_per_article_resource + "/" + context.data_config['page_analytics_per_file']['project']
        + "/" + context.data_config['page_analytics_per_file']['access']
        + "/" + context.data_config['page_analytics_per_file']['agents']
        + "/" + context.data_config['page_analytics_per_file']['article']
        + "/" + context.data_config['page_analytics_per_file']['granularity']
        + "/" + context.data_config['page_analytics_per_file']['end_date']
        + "/" + context.data_config['page_analytics_per_file']['start_date'],
        headers=header)


@step(
    "verify that detail key from the error response displays the invalid start and end date timestamp error for daily")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).is_equal_to("start timestamp should be before the end"
                                                                        " timestamp")


@given(
    "request is made to the page analytics per_article endpoint with invalid project, access, all-agents, St. Louis City SC, daily, 20220701 and 20220929")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_per_article_resource + "/" + context.data_config['page_analytics_per_file']['project_invalid']
        + "/" + context.data_config['page_analytics_per_file']['access']
        + "/" + context.data_config['page_analytics_per_file']['agents']
        + "/" + context.data_config['page_analytics_per_file']['article']
        + "/" + context.data_config['page_analytics_per_file']['granularity']
        + "/" + context.data_config['page_analytics_per_file']['start_date']
        + "/" + context.data_config['page_analytics_per_file']['end_date'],
        headers=header)


@step("verify that detail key from the error response displays the invalid project error")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).is_equal_to("The date(s) you used are valid, but we "
                                                                        "either do not have data for those date(s), "
                                                                        "or the project you asked for is not loaded "
                                                                        "yet. Please check documentation for more "
                                                                        "information")


@given("request is made to page analytics per_article endpoint with en.wikipedia project, year 2018, month 01 and day 01")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_per_article_resource + "/" + context.data_config['page_analytics_per_file']['project']
        + "/" + context.data_config['page_analytics_per_file']['access']
        + "/" + context.data_config['page_analytics_per_file']['agents']
        + "/" + context.data_config['page_analytics_per_file']['article_cell_phone']
        + "/" + context.data_config['page_analytics_per_file']['granularity']
        + "/" + context.data_config['page_analytics_per_file']['start_date_cell_phone']
        + "/" + context.data_config['page_analytics_per_file']['end_date_cell_phone'],
        headers=header)


@given("request is made to page analytics per_article endpoint with en.wikipedia project, year 2019, month 01 and day 01")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_per_article_resource + "/" + context.data_config['page_analytics_per_file']['project']
        + "/" + context.data_config['page_analytics_per_file']['access']
        + "/" + context.data_config['page_analytics_per_file']['agents']
        + "/" + context.data_config['page_analytics_per_file']['article_cell_phone']
        + "/" + context.data_config['page_analytics_per_file']['granularity']
        + "/" + context.data_config['page_analytics_per_file']['start_date_cell_phone_2019']
        + "/" + context.data_config['page_analytics_per_file']['end_date_cell_phone_2019'],
        headers=header)


@given("request is made to page analytics per_article endpoint with en.wikipedia project, year 2020, month 01 and day 01")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_per_article_resource + "/" + context.data_config['page_analytics_per_file']['project']
        + "/" + context.data_config['page_analytics_per_file']['access']
        + "/" + context.data_config['page_analytics_per_file']['agents']
        + "/" + context.data_config['page_analytics_per_file']['article_cell_phone']
        + "/" + context.data_config['page_analytics_per_file']['granularity']
        + "/" + context.data_config['page_analytics_per_file']['start_date_cell_phone_2020']
        + "/" + context.data_config['page_analytics_per_file']['end_date_cell_phone_2020'],
        headers=header)


@given("request is made to page analytics per_article endpoint with invalid data {projects}")
def step_impl(context, projects):
    context.response = requests.get(
        uri + page_analytics_per_article_resource + "/" + projects
        + "/" + context.data_config['page_analytics_per_file']['access']
        + "/" + context.data_config['page_analytics_per_file']['agents']
        + "/" + context.data_config['page_analytics_per_file']['article_cell_phone']
        + "/" + context.data_config['page_analytics_per_file']['granularity']
        + "/" + context.data_config['page_analytics_per_file']['start_date_cell_phone_2020']
        + "/" + context.data_config['page_analytics_per_file']['end_date_cell_phone_2020'],
        headers=header)


@given("request is made to page analytics per_article endpoint with valid data {projects}")
def step_impl(context, projects):
    context.response = requests.get(
        uri + page_analytics_per_article_resource + "/" + projects
        + "/" + context.data_config['page_analytics_per_file']['access']
        + "/" + context.data_config['page_analytics_per_file']['agents']
        + "/" + context.data_config['page_analytics_per_file']['article_cell_phone']
        + "/" + context.data_config['page_analytics_per_file']['granularity']
        + "/" + context.data_config['page_analytics_per_file']['start_date_cell_phone_2020']
        + "/" + context.data_config['page_analytics_per_file']['end_date_cell_phone_2020'],
        headers=header)
