from behave import *
import requests
import assertpy
from resources.all_resources.resources import edits_bytes_diff_absolute_aggregate
from utilities.base_uri import uri
from utilities.header import header


@given(
    "request is made to the bytes difference absolute aggregate endpoint with varying {projects}, editor_types, all-page-types, daily, 20180101, 20181231")
def step_impl(context, projects):
    context.response = requests.get(
        uri + edits_bytes_diff_absolute_aggregate + "/" + projects
        + "/" + context.data_config['edit_bytes_difference_absolute_aggregate']['editor_type']
        + "/" + context.data_config['edit_bytes_difference_absolute_aggregate']['page_type_all']
        + "/" + context.data_config['edit_bytes_difference_absolute_aggregate']['granularity']
        + "/" + context.data_config['edit_bytes_difference_absolute_aggregate']['start_date']
        + "/" + context.data_config['edit_bytes_difference_absolute_aggregate']['end_date'], headers=header)


@given(
    "request is made to the bytes difference absolute aggregate endpoint with project, varying {editor_types}, all-page-types, daily, 20180101, 20181231")
def step_impl(context, editor_types):
    context.response = requests.get(
        uri + edits_bytes_diff_absolute_aggregate + "/" +
        context.data_config['edit_bytes_difference_absolute_aggregate']['project']
        + "/" + editor_types
        + "/" + context.data_config['edit_bytes_difference_absolute_aggregate']['page_type_all']
        + "/" + context.data_config['edit_bytes_difference_absolute_aggregate']['granularity']
        + "/" + context.data_config['edit_bytes_difference_absolute_aggregate']['start_date']
        + "/" + context.data_config['edit_bytes_difference_absolute_aggregate']['end_date'], headers=header)


@given(
    "request is made to the bytes difference absolute aggregate endpoint with project, editor_types, all-page-types, varying {granularity}, 20200101, 20201201")
def step_impl(context, granularity):
    context.response = requests.get(
        uri + edits_bytes_diff_absolute_aggregate + "/" +
        context.data_config['edit_bytes_difference_absolute_aggregate']['project']
        + "/" + context.data_config['edit_bytes_difference_absolute_aggregate']['editor_type']
        + "/" + context.data_config['edit_bytes_difference_absolute_aggregate']['page_type_all']
        + "/" + granularity
        + "/" + context.data_config['edit_bytes_difference_absolute_aggregate']['start_date']
        + "/" + context.data_config['edit_bytes_difference_absolute_aggregate']['end_date'], headers=header)


@step("verify that the granularity in the response is the same as the {granularity} in the absolute diff endpoint")
def step_impl(context, granularity):
    for obj in context.response.json()["items"]:
        assertpy.assert_that(obj["granularity"]).is_equal_to(granularity)


@given("request is made to the Edit analytics bytes difference absolute aggregate with invalid project")
def step_impl(context):
    context.response = requests.get(
        uri + edits_bytes_diff_absolute_aggregate + "/" +
        context.data_config['edit_bytes_difference_absolute_aggregate']['project_invalid']
        + "/" + context.data_config['edit_bytes_difference_absolute_aggregate']['editor_type']
        + "/" + context.data_config['edit_bytes_difference_absolute_aggregate']['page_type_all']
        + "/" + context.data_config['edit_bytes_difference_absolute_aggregate']['granularity']
        + "/" + context.data_config['edit_bytes_difference_absolute_aggregate']['start_date']
        + "/" + context.data_config['edit_bytes_difference_absolute_aggregate']['end_date'], headers=header)


@given(
    "request is made to the Edit analytics bytes difference absolute aggregate with special character in the project")
def step_impl(context):
    context.response = requests.get(
        uri + edits_bytes_diff_absolute_aggregate + "/" +
        context.data_config['edit_bytes_difference_absolute_aggregate']['project_special_character']
        + "/" + context.data_config['edit_bytes_difference_absolute_aggregate']['editor_type']
        + "/" + context.data_config['edit_bytes_difference_absolute_aggregate']['page_type_all']
        + "/" + context.data_config['edit_bytes_difference_absolute_aggregate']['granularity']
        + "/" + context.data_config['edit_bytes_difference_absolute_aggregate']['start_date']
        + "/" + context.data_config['edit_bytes_difference_absolute_aggregate']['end_date'], headers=header)


@step("verify that detail key from the error response displaying the special character warning for absolute difference")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).contains(
        "the parameter `project` contains invalid characters")


@given("request is made to the bytes difference absolute aggregate with invalid editor type")
def step_impl(context):
    context.response = requests.get(
        uri + edits_bytes_diff_absolute_aggregate + "/" +
        context.data_config['edit_bytes_difference_absolute_aggregate']['project']
        + "/" + context.data_config['edit_bytes_difference_absolute_aggregate']['editor_type_invalid']
        + "/" + context.data_config['edit_bytes_difference_absolute_aggregate']['page_type_all']
        + "/" + context.data_config['edit_bytes_difference_absolute_aggregate']['granularity']
        + "/" + context.data_config['edit_bytes_difference_absolute_aggregate']['start_date']
        + "/" + context.data_config['edit_bytes_difference_absolute_aggregate']['end_date'], headers=header)


@step("verify that detail key from the error response for invalid editor type for byte diff absolute aggregate")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).contains(
        "should be equal to one of the allowed values: [all-editor-types, anonymous, group-bot, name-bot, user]")


@given("request is made to the bytes difference absolute aggregate with invalid granularity")
def step_impl(context):
    context.response = requests.get(
        uri + edits_bytes_diff_absolute_aggregate + "/" +
        context.data_config['edit_bytes_difference_absolute_aggregate']['project']
        + "/" + context.data_config['edit_bytes_difference_absolute_aggregate']['editor_type']
        + "/" + context.data_config['edit_bytes_difference_absolute_aggregate']['page_type_all']
        + "/" + context.data_config['edit_bytes_difference_absolute_aggregate']['granularity_invalid']
        + "/" + context.data_config['edit_bytes_difference_absolute_aggregate']['start_date']
        + "/" + context.data_config['edit_bytes_difference_absolute_aggregate']['end_date'], headers=header)


@step("verify that detail key from the error response for invalid granularity for byte diff absolute aggregate")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).contains(
        "should be equal to one of the allowed values: [daily, monthly]")


@given("request is made to the bytes difference absolute aggregate with invalid start date")
def step_impl(context):
    context.response = requests.get(
        uri + edits_bytes_diff_absolute_aggregate + "/" +
        context.data_config['edit_bytes_difference_absolute_aggregate']['project']
        + "/" + context.data_config['edit_bytes_difference_absolute_aggregate']['editor_type']
        + "/" + context.data_config['edit_bytes_difference_absolute_aggregate']['page_type_all']
        + "/" + context.data_config['edit_bytes_difference_absolute_aggregate']['granularity']
        + "/" + context.data_config['edit_bytes_difference_absolute_aggregate']['start_date_invalid']
        + "/" + context.data_config['edit_bytes_difference_absolute_aggregate']['end_date'], headers=header)


@step("verify that detail key from the error response for invalid start date for byte diff absolute aggregate")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).contains(
        "start timestamp is invalid, must be a valid date in YYYYMMDD format")


@given("request is made to the bytes difference absolute aggregate with invalid end date")
def step_impl(context):
    context.response = requests.get(
        uri + edits_bytes_diff_absolute_aggregate + "/" +
        context.data_config['edit_bytes_difference_absolute_aggregate']['project']
        + "/" + context.data_config['edit_bytes_difference_absolute_aggregate']['editor_type']
        + "/" + context.data_config['edit_bytes_difference_absolute_aggregate']['page_type_all']
        + "/" + context.data_config['edit_bytes_difference_absolute_aggregate']['granularity']
        + "/" + context.data_config['edit_bytes_difference_absolute_aggregate']['start_date']
        + "/" + context.data_config['edit_bytes_difference_absolute_aggregate']['end_date_invalid'], headers=header)


@step("verify that detail key from the error response shows invalid end date for byte diff absolute aggregate")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).contains(
        "end timestamp is invalid, must be a valid date in YYYYMMDD format")


@given("request is made to the bytes difference absolute aggregate with end date before start date")
def step_impl(context):
    context.response = requests.get(
        uri + edits_bytes_diff_absolute_aggregate + "/" +
        context.data_config['edit_bytes_difference_absolute_aggregate']['project']
        + "/" + context.data_config['edit_bytes_difference_absolute_aggregate']['editor_type']
        + "/" + context.data_config['edit_bytes_difference_absolute_aggregate']['page_type_all']
        + "/" + context.data_config['edit_bytes_difference_absolute_aggregate']['granularity']
        + "/" + context.data_config['edit_bytes_difference_absolute_aggregate']['end_date']
        + "/" + context.data_config['edit_bytes_difference_absolute_aggregate']['start_date'], headers=header)


@step("verify that detail key from the error response for end date before start date for byte diff absolute aggregate")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).contains(
        "start timestamp should be before the end timestamp")


@given("request is made to the bytes difference absolute aggregate with no full months specified")
def step_impl(context):
    context.response = requests.get(
        uri + edits_bytes_diff_absolute_aggregate + "/" +
        context.data_config['edit_bytes_difference_absolute_aggregate']['project']
        + "/" + context.data_config['edit_bytes_difference_absolute_aggregate']['editor_type']
        + "/" + context.data_config['edit_bytes_difference_absolute_aggregate']['page_type_all']
        + "/" + context.data_config['edit_bytes_difference_absolute_aggregate']['granularity_monthly']
        + "/" + context.data_config['edit_bytes_difference_absolute_aggregate']['start_date']
        + "/" + context.data_config['edit_bytes_difference_absolute_aggregate']['start_date'], headers=header)


@step(
    "verify that detail key from the error response for with no full months specified for byte diff absolute aggregate")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).contains(
        "no full months found in specified date range")