from behave import *
import requests
import assertpy
from resources.all_resources.resources import page_analytics_top_per_country
from utilities.base_uri import uri
from utilities.header import header


@given("request is made to the page analytics top per country endpoint with project, {country}, access, year, month, day")
def step_impl(context, country):
    context.response = requests.get(
        uri + page_analytics_top_per_country + "/" + country + "/"
        + context.data_config['page_analytics_top_per_country']['access'] + "/"
        + context.data_config['page_analytics_top_per_country']['year']
        + "/" + context.data_config['page_analytics_top_per_country']['month'] + "/" +
        context.data_config['page_analytics_top_per_country']['day'],
        headers=header)


@step("verify that the country in the response is the same as the {country} in the endpoint")
def step_impl(context, country):
    for obj in context.response.json()["items"]:
        assertpy.assert_that(obj["country"]).is_equal_to(country)


@step("verify that the year in the response is the same as the year in the endpoint")
def step_impl(context):
    for obj in context.response.json()["items"]:
        assertpy.assert_that(obj["year"]).is_equal_to(context.data_config['page_analytics_top_per_country']['year'])


@step("verify that the month in the response is the same as the month in the endpoint")
def step_impl(context):
    for obj in context.response.json()["items"]:
        assertpy.assert_that(obj["year"]).is_equal_to(context.data_config['page_analytics_top_per_country']['year'])


@step("verify that the day in the response is the same as the day in the endpoint")
def step_impl(context):
    for obj in context.response.json()["items"]:
        assertpy.assert_that(obj["day"]).is_equal_to(context.data_config['page_analytics_top_per_country']['day'])


@given("request is made to the page analytics top per country endpoint with varying {access}")
def step_impl(context, access):
    context.response = requests.get(
        uri + page_analytics_top_per_country + "/" + context.data_config['page_analytics_top_per_country']['country_2']
        + "/" + access + "/" + context.data_config['page_analytics_top_per_country']['year']
        + "/" + context.data_config['page_analytics_top_per_country']['month'] + "/" +
        context.data_config['page_analytics_top_per_country']['day'],
        headers=header)


@step("verify {access} in the request is the same as the access in the response")
def step_impl(context, access):
    for obj in context.response.json()["items"]:
        assertpy.assert_that(obj["access"]).is_equal_to(access)


@given("request is made to the page analytics top per country endpoint with valid varying {year}")
def step_impl(context, year):
    context.response = requests.get(
        uri + page_analytics_top_per_country + "/" + context.data_config['page_analytics_top_per_country']['country_2'] + "/"
        + context.data_config['page_analytics_top_per_country']['access'] + "/" + year
        + "/" + context.data_config['page_analytics_top_per_country']['month'] + "/" +
        context.data_config['page_analytics_top_per_country']['day'],
        headers=header)


@given("request is made to the page analytics top per country endpoint with invalid varying {year_invalid}")
def step_impl(context, year_invalid):
    context.response = requests.get(
        uri + page_analytics_top_per_country + "/" + context.data_config['page_analytics_top_per_country']['country'] + "/"
        + context.data_config['page_analytics_top_per_country']['access'] + "/" + year_invalid
        + "/" + context.data_config['page_analytics_top_per_country']['month']
        + "/" + context.data_config['page_analytics_top_per_country']['day'],
        headers=header)


@step("verify that detail key from the error response displays error for the pageviews top per country project")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).contains(
        context.data_config['pageviews_top_per_country_validations']
        ['invalid_project_validation'])


@given("request is made to the page analytics top per country endpoint with invalid country")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_top_per_country + "/" + context.data_config['page_analytics_top_per_country']['country_invalid']
        + "/" + context.data_config['page_analytics_top_per_country']['access']
        + "/" + context.data_config['page_analytics_top_per_country']['year']
        + "/" + context.data_config['page_analytics_top_per_country']['month'] + "/" +
        context.data_config['page_analytics_top_per_country']['day'], headers=header)


@given("request is made to the page analytics top per country endpoint with invalid access")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_top_per_country + "/" + context.data_config['page_analytics_top_per_country']['country']
        + "/" + context.data_config['page_analytics_top_per_country']['access_invalid']
        + "/" + context.data_config['page_analytics_top_per_country']['year']
        + "/" + context.data_config['page_analytics_top_per_country']['month'] + "/" +
        context.data_config['page_analytics_top_per_country']['day'], headers=header)


@step("verify that detail key from the error response displays error for the pageviews top per country access")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).contains(
        context.data_config['pageviews_top_per_country_validations']
        ['invalid_access_validation'])


@given("request is made to the page analytics top per country endpoint with invalid year")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_top_per_country + "/" + context.data_config['page_analytics_top_per_country']['country']
        + "/" + context.data_config['page_analytics_top_per_country']['access']
        + "/" + context.data_config['page_analytics_top_per_country']['year_invalid']
        + "/" + context.data_config['page_analytics_top_per_country']['month'] + "/" +
        context.data_config['page_analytics_top_per_country']['day'], headers=header)


@given("request is made to the page analytics top per country endpoint with invalid month")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_top_per_country + "/" + context.data_config['page_analytics_top_per_country']['country']
        + "/" + context.data_config['page_analytics_top_per_country']['access']
        + "/" + context.data_config['page_analytics_top_per_country']['year']
        + "/" + context.data_config['page_analytics_top_per_country']['month_invalid'] + "/" +
        context.data_config['page_analytics_top_per_country']['day'], headers=header)


@step("verify that detail key from the error response displays error for the pageviews top per country {year}")
def step_impl(context, year):
    assertpy.assert_that(context.response.json()["detail"]).is_equal_to(
        context.data_config['pageviews_top_per_country_validations']
        ['invalid_year_month_date_validation'])


@given("request is made to the page analytics top per country endpoint with invalid day")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_top_per_country + "/" + context.data_config['page_analytics_top_per_country']['country']
        + "/" + context.data_config['page_analytics_top_per_country']['access']
        + "/" + context.data_config['page_analytics_top_per_country']['year']
        + "/" + context.data_config['page_analytics_top_per_country']['month'] + "/" +
        context.data_config['page_analytics_top_per_country']['day_invalid'], headers=header)


@given("request is made to the page analytics top per country endpoint with invalid route")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_top_per_country + "//" + context.data_config['page_analytics_top_per_country']['country']
        + "/" + context.data_config['page_analytics_top_per_country']['access']
        + "/" + context.data_config['page_analytics_top_per_country']['year']
        + "/" + context.data_config['page_analytics_top_per_country']['month'] + "/" +
        context.data_config['page_analytics_top_per_country']['day'], headers=header)


@given("request is made to the page analytics top per country endpoint with excluded {country}")
def step_impl(context, country):
    context.response = requests.get(
        uri + page_analytics_top_per_country + "/" + country + "/"
        + context.data_config['page_analytics_top_per_country']['access_2'] + "/"
        + context.data_config['page_analytics_top_per_country']['year']
        + "/" + context.data_config['page_analytics_top_per_country']['month'] + "/" +
        context.data_config['page_analytics_top_per_country']['day'],
        headers=header)


@given("request is made to page analytics top per country for year 2021 using US")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_top_per_country + "/" + context.data_config['page_analytics_top_per_country']['country_US']
        + "/" + context.data_config['page_analytics_top_per_country']['access']
        + "/" + context.data_config['page_analytics_top_per_country']['year']
        + "/" + context.data_config['page_analytics_top_per_country']['month'] + "/" +
        context.data_config['page_analytics_top_per_country']['day'], headers=header)


@given("request is made to page analytics top per country for year 2022 using US")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_top_per_country + "/" + context.data_config['page_analytics_top_per_country']['country_US']
        + "/" + context.data_config['page_analytics_top_per_country']['access']
        + "/" + context.data_config['page_analytics_top_per_country']['year_2022']
        + "/" + context.data_config['page_analytics_top_per_country']['month'] + "/" +
        context.data_config['page_analytics_top_per_country']['day'], headers=header)


@given("request is made to page analytics top per country for year 2023 using US")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_top_per_country + "/" + context.data_config['page_analytics_top_per_country']['country_US']
        + "/" + context.data_config['page_analytics_top_per_country']['access']
        + "/" + context.data_config['page_analytics_top_per_country']['year_2023']
        + "/" + context.data_config['page_analytics_top_per_country']['month'] + "/" +
        context.data_config['page_analytics_top_per_country']['day'], headers=header)


@given("request is made to page analytics top per country endpoint with invalid data {countries}")
def step_impl(context, countries):
    context.response = requests.get(
        uri + page_analytics_top_per_country + "/" + countries
        + "/" + context.data_config['page_analytics_top_per_country']['access']
        + "/" + context.data_config['page_analytics_top_per_country']['year']
        + "/" + context.data_config['page_analytics_top_per_country']['month'] + "/" +
        context.data_config['page_analytics_top_per_country']['day'], headers=header)