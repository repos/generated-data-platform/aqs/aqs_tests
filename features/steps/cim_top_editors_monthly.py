from behave import *
import requests
from utilities.header import header
from resources.all_resources.resources import cim_top_editors_monthly_resource
from utilities.base_uri import *


@given("request is made to the Top Editors monthly endpoint with varying scope {scope}, 2023 and 11")
def step_impl(context, scope):
    context.response = requests.get(
        uri + cim_top_editors_monthly_resource
        + "/" + context.data_config['cim_top_editors_monthly']['category']
        + "/" + scope
        + "/" + context.data_config['cim_top_editors_monthly']['edit_type']
        + "/" + context.data_config['cim_top_editors_monthly']['year']
        + "/" + context.data_config['cim_top_editors_monthly']['month'],
        headers=header)


@given("request is made to the Top Editors monthly endpoint with varying edit_type {edit_type}, 2023 and 11")
def step_impl(context, edit_type):
    context.response = requests.get(
        uri + cim_top_editors_monthly_resource
        + "/" + context.data_config['cim_top_editors_monthly']['category']
        + "/" + context.data_config['cim_top_editors_monthly']['category_scope']
        + "/" + edit_type
        + "/" + context.data_config['cim_top_editors_monthly']['year']
        + "/" + context.data_config['cim_top_editors_monthly']['month'],
        headers=header)


@given("request is made to the Top Editors Monthly endpoint with invalid route")
def step_impl(context):
    context.response = requests.get(
        uri + cim_top_editors_monthly_resource
        + "/" + "/" + context.data_config['cim_top_editors_monthly']['category']
        + "/" + context.data_config['cim_top_editors_monthly']['category_scope']
        + "/" + context.data_config['cim_top_editors_monthly']['edit_type']
        + "/" + context.data_config['cim_top_editors_monthly']['year']
        + "/" + context.data_config['cim_top_editors_monthly']['month'],
        headers=header)


@given("request is made to the CIM Top Editors Monthly endpoint with invalid category")
def step_impl(context):
    context.response = requests.get(
        uri + cim_top_editors_monthly_resource
        + "/" + context.data_config['cim_top_editors_monthly']['category_invalid']
        + "/" + context.data_config['cim_top_editors_monthly']['category_scope']
        + "/" + context.data_config['cim_top_editors_monthly']['edit_type']
        + "/" + context.data_config['cim_top_editors_monthly']['year']
        + "/" + context.data_config['cim_top_editors_monthly']['month'],
        headers=header)


@given("request is made to the Top Editors Monthly endpoint with invalid year")
def step_impl(context):
    context.response = requests.get(
        uri + cim_top_editors_monthly_resource
        + "/" + context.data_config['cim_top_editors_monthly']['category']
        + "/" + context.data_config['cim_top_editors_monthly']['category_scope']
        + "/" + context.data_config['cim_top_editors_monthly']['edit_type']
        + "/" + context.data_config['cim_top_editors_monthly']['year_invalid']
        + "/" + context.data_config['cim_top_editors_monthly']['month'],
        headers=header)


@given("request is made to the Top Editors Monthly endpoint with invalid month")
def step_impl(context):
    context.response = requests.get(
        uri + cim_top_editors_monthly_resource
        + "/" + context.data_config['cim_top_editors_monthly']['category']
        + "/" + context.data_config['cim_top_editors_monthly']['category_scope']
        + "/" + context.data_config['cim_top_editors_monthly']['edit_type']
        + "/" + context.data_config['cim_top_editors_monthly']['year']
        + "/" + context.data_config['cim_top_editors_monthly']['month_invalid'],
        headers=header)


@given("request is made to the Top Editors Monthly endpoint to validate username and edit count")
def step_impl(context):
    context.response = requests.get(
        uri + cim_top_editors_monthly_resource
        + "/" + context.data_config['cim_top_editors_monthly']['category']
        + "/" + context.data_config['cim_top_editors_monthly']['category_scope']
        + "/" + context.data_config['cim_top_editors_monthly']['edit_type']
        + "/" + context.data_config['cim_top_editors_monthly']['year']
        + "/" + context.data_config['cim_top_editors_monthly']['month'],
        headers=header)


@given("request is made to the CIM Top Editors monthly endpoint with invalid scope")
def step_impl(context):
    context.response = requests.get(
        uri + cim_top_editors_monthly_resource
        + "/" + context.data_config['cim_top_editors_monthly']['category']
        + "/" + context.data_config['cim_top_editors_monthly']['category_scope_invalid']
        + "/" + context.data_config['cim_top_editors_monthly']['edit_type']
        + "/" + context.data_config['cim_top_editors_monthly']['year']
        + "/" + context.data_config['cim_top_editors_monthly']['month'],
        headers=header)