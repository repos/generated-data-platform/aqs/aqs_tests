from behave import *
import requests
from utilities.header import header
from resources.all_resources.resources import cim_media_file_metrics_snapshot_resource
from utilities.base_uri import *


@given(
    "request is made to the Media file metrics snapshot endpoint with varying media file {media_file}, 20231101 and 20240601")
def step_impl(context, media_file):
    context.response = requests.get(
        uri + cim_media_file_metrics_snapshot_resource
        + "/" + media_file
        + "/" + context.data_config['cim_media_file_metrics_snapshot']['start_time']
        + "/" + context.data_config['cim_media_file_metrics_snapshot']['end_time'],
        headers=header)


@given("request is made to the Media file metrics snapshot endpoint with invalid route")
def step_impl(context):
    context.response = requests.get(
        uri + cim_media_file_metrics_snapshot_resource
        + "/" + "/" + context.data_config['cim_media_file_metrics_snapshot']['media_file']
        + "/" + context.data_config['cim_media_file_metrics_snapshot']['start_time']
        + "/" + context.data_config['cim_media_file_metrics_snapshot']['end_time'],
        headers=header)


@given("request is made to the Media file metrics snapshot endpoint with invalid file")
def step_impl(context):
    context.response = requests.get(
        uri + cim_media_file_metrics_snapshot_resource
        + "/" + context.data_config['cim_media_file_metrics_snapshot']['media_file_invalid']
        + "/" + context.data_config['cim_media_file_metrics_snapshot']['start_time']
        + "/" + context.data_config['cim_media_file_metrics_snapshot']['end_time'],
        headers=header)


@given("request is made to the Media file metrics snapshot endpoint with invalid start time")
def step_impl(context):
    context.response = requests.get(
        uri + cim_media_file_metrics_snapshot_resource
        + "/" + context.data_config['cim_media_file_metrics_snapshot']['media_file']
        + "/" + context.data_config['cim_media_file_metrics_snapshot']['start_time_invalid']
        + "/" + context.data_config['cim_media_file_metrics_snapshot']['end_time'],
        headers=header)


@given("request is made to the Media file metrics snapshot endpoint with invalid end time")
def step_impl(context):
    context.response = requests.get(
        uri + cim_media_file_metrics_snapshot_resource
        + "/" + context.data_config['cim_media_file_metrics_snapshot']['media_file']
        + "/" + context.data_config['cim_media_file_metrics_snapshot']['start_time']
        + "/" + context.data_config['cim_media_file_metrics_snapshot']['end_time_invalid'],
        headers=header)


@given("request is made to the Media file metrics snapshot endpoint to validate timestamp and edit count")
def step_impl(context):
    context.response = requests.get(
        uri + cim_media_file_metrics_snapshot_resource
        + "/" + context.data_config['cim_media_file_metrics_snapshot']['media_file']
        + "/" + context.data_config['cim_media_file_metrics_snapshot']['start_time']
        + "/" + context.data_config['cim_media_file_metrics_snapshot']['end_time'],
        headers=header)