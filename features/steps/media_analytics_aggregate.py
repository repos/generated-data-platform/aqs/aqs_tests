from behave import *
from resources.all_resources.resources import media_analytics_aggregate
from utilities.base_uri import uri
import requests
import assertpy
from utilities.header import header


@given("request is made to the media analytics aggregate endpoint with varying {referer}")
def step_impl(context, referer):
    context.response = requests.get(
        uri + media_analytics_aggregate + "/" + referer + "/" + context.data_config['media_analytics_aggregate'][
            'media_type']
        + "/" + context.data_config['media_analytics_aggregate']['agent_all']
        + "/" + context.data_config['media_analytics_aggregate']['granularity_daily']
        + "/" + context.data_config['media_analytics_aggregate']['start_date']
        + "/" + context.data_config['media_analytics_aggregate']['end_date'],
        headers=header)


@given("request is made to the media analytics aggregate endpoint with image media type and varying {referer}")
def step_impl(context, referer):
    context.response = requests.get(
        uri + media_analytics_aggregate + "/" + referer + "/" + context.data_config['media_analytics_aggregate'][
            'media_type_image']
        + "/" + context.data_config['media_analytics_aggregate']['agent_all']
        + "/" + context.data_config['media_analytics_aggregate']['granularity_daily']
        + "/" + context.data_config['media_analytics_aggregate']['start_date']
        + "/" + context.data_config['media_analytics_aggregate']['end_date'],
        headers=header)


@given("request is made to the media analytics aggregate endpoint with video type and varying {referer}")
def step_impl(context, referer):
    context.response = requests.get(
        uri + media_analytics_aggregate + "/" + referer + "/" + context.data_config['media_analytics_aggregate'][
            'media_type_video']
        + "/" + context.data_config['media_analytics_aggregate']['agent_all']
        + "/" + context.data_config['media_analytics_aggregate']['granularity_daily']
        + "/" + context.data_config['media_analytics_aggregate']['start_date']
        + "/" + context.data_config['media_analytics_aggregate']['end_date'],
        headers=header)


@given("request is made to the media analytics aggregate endpoint with audio type and varying {referer}")
def step_impl(context, referer):
    context.response = requests.get(
        uri + media_analytics_aggregate + "/" + referer + "/" + context.data_config['media_analytics_aggregate'][
            'media_type_audio']
        + "/" + context.data_config['media_analytics_aggregate']['agent_all']
        + "/" + context.data_config['media_analytics_aggregate']['granularity_daily']
        + "/" + context.data_config['media_analytics_aggregate']['start_date']
        + "/" + context.data_config['media_analytics_aggregate']['end_date'],
        headers=header)


@given("request is made to the media analytics aggregate endpoint with document type and varying {referer}")
def step_impl(context, referer):
    context.response = requests.get(
        uri + media_analytics_aggregate + "/" + referer + "/" + context.data_config['media_analytics_aggregate'][
            'media_type_document']
        + "/" + context.data_config['media_analytics_aggregate']['agent_all']
        + "/" + context.data_config['media_analytics_aggregate']['granularity_daily']
        + "/" + context.data_config['media_analytics_aggregate']['start_date']
        + "/" + context.data_config['media_analytics_aggregate']['end_date'],
        headers=header)


@given("request is made to the media analytics aggregate endpoint with other type and varying {referer}")
def step_impl(context, referer):
    context.response = requests.get(
        uri + media_analytics_aggregate + "/" + referer + "/" + context.data_config['media_analytics_aggregate'][
            'media_type_other']
        + "/" + context.data_config['media_analytics_aggregate']['agent_all']
        + "/" + context.data_config['media_analytics_aggregate']['granularity_daily']
        + "/" + context.data_config['media_analytics_aggregate']['start_date']
        + "/" + context.data_config['media_analytics_aggregate']['end_date'],
        headers=header)


@given("request is made to the media analytics aggregate endpoint with all media type and varying {agent}")
def step_impl(context, agent):
    context.response = requests.get(
        uri + media_analytics_aggregate + "/" + context.data_config['media_analytics_aggregate'][
            'referer'] + "/" + context.data_config['media_analytics_aggregate'][
            'media_type'] + "/" + agent
        + "/" + context.data_config['media_analytics_aggregate']['granularity_daily']
        + "/" + context.data_config['media_analytics_aggregate']['start_date']
        + "/" + context.data_config['media_analytics_aggregate']['end_date'],
        headers=header)


@step("verify that the {agent} used in request is same as the agent in response body")
def step_impl(context, agent):
    for array in context.response.json()["items"]:
        assertpy.assert_that(array["agent"]).is_equal_to(agent)


@given("request is made to media analytics aggregate endpoint with varying {referer} and monthly granularity")
def step_impl(context, referer):
    context.response = requests.get(
        uri + media_analytics_aggregate + "/" + referer + "/" + context.data_config['media_analytics_aggregate'][
            'media_type']
        + "/" + context.data_config['media_analytics_aggregate']['agent_all']
        + "/" + context.data_config['media_analytics_aggregate']['granularity_monthly']
        + "/" + context.data_config['media_analytics_aggregate']['start_date']
        + "/" + context.data_config['media_analytics_aggregate']['end_date_monthly'],
        headers=header)


@given("request is made to the media analytics aggregate endpoint from 2018-21 with varying {referer}")
def step_impl(context, referer):
    context.response = requests.get(
        uri + media_analytics_aggregate + "/" + referer + "/" + context.data_config['media_analytics_aggregate'][
            'media_type']
        + "/" + context.data_config['media_analytics_aggregate']['agent_all']
        + "/" + context.data_config['media_analytics_aggregate']['granularity_daily']
        + "/" + context.data_config['media_analytics_aggregate']['start_date_2018']
        + "/" + context.data_config['media_analytics_aggregate']['end_date_2021'],
        headers=header)


@given("request is made to the media analytics aggregate endpoint with invalid referrer")
def step_impl(context):
    context.response = requests.get(
        uri + media_analytics_aggregate + "/" + context.data_config['media_analytics_aggregate'][
            'referer_invalid'] + "/" + context.data_config['media_analytics_aggregate'][
            'media_type']
        + "/" + context.data_config['media_analytics_aggregate']['agent_all']
        + "/" + context.data_config['media_analytics_aggregate']['granularity_daily']
        + "/" + context.data_config['media_analytics_aggregate']['start_date_2018']
        + "/" + context.data_config['media_analytics_aggregate']['end_date_2021'],
        headers=header)


@given("request is made to the media analytics aggregate endpoint with invalid media type")
def step_impl(context):
    context.response = requests.get(
        uri + media_analytics_aggregate + "/" + context.data_config['media_analytics_aggregate'][
            'referer'] + "/" + context.data_config['media_analytics_aggregate'][
            'media_type_invalid']
        + "/" + context.data_config['media_analytics_aggregate']['agent_all']
        + "/" + context.data_config['media_analytics_aggregate']['granularity_daily']
        + "/" + context.data_config['media_analytics_aggregate']['start_date_2018']
        + "/" + context.data_config['media_analytics_aggregate']['end_date_2021'],
        headers=header)


@given("request is made to the media analytics aggregate endpoint with invalid agent")
def step_impl(context):
    context.response = requests.get(
        uri + media_analytics_aggregate + "/" + context.data_config['media_analytics_aggregate'][
            'referer'] + "/" + context.data_config['media_analytics_aggregate'][
            'media_type']
        + "/" + context.data_config['media_analytics_aggregate']['agent_invalid']
        + "/" + context.data_config['media_analytics_aggregate']['granularity_daily']
        + "/" + context.data_config['media_analytics_aggregate']['start_date_2018']
        + "/" + context.data_config['media_analytics_aggregate']['end_date_2021'],
        headers=header)


@step("verify that detail key from the error response contains agent type validation")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).is_equal_to("agent should be equal to one of the allowed "
                                                                        "values: [all-agents, user, spider]")


@given("request is made to the media analytics aggregate endpoint with invalid granularity")
def step_impl(context):
    context.response = requests.get(
        uri + media_analytics_aggregate + "/" + context.data_config['media_analytics_aggregate'][
            'referer'] + "/" + context.data_config['media_analytics_aggregate'][
            'media_type']
        + "/" + context.data_config['media_analytics_aggregate']['agent_all']
        + "/" + context.data_config['media_analytics_aggregate']['granularity_invalid']
        + "/" + context.data_config['media_analytics_aggregate']['start_date_2018']
        + "/" + context.data_config['media_analytics_aggregate']['end_date_2021'],
        headers=header)


@step("verify that detail key from the error response contains granularity validation")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).is_equal_to("granularity should be equal to one of the "
                                                                        "allowed values: [daily, monthly]")


@given("request is made to the media analytics aggregate endpoint with invalid start date")
def step_impl(context):
    context.response = requests.get(
        uri + media_analytics_aggregate + "/" + context.data_config['media_analytics_aggregate'][
            'referer'] + "/" + context.data_config['media_analytics_aggregate'][
            'media_type']
        + "/" + context.data_config['media_analytics_aggregate']['agent_all']
        + "/" + context.data_config['media_analytics_aggregate']['granularity_daily']
        + "/" + context.data_config['media_analytics_aggregate']['start_date_invalid']
        + "/" + context.data_config['media_analytics_aggregate']['end_date_2021'],
        headers=header)


@step("verify that detail key from the error response contains start date validation")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).is_equal_to("start timestamp is invalid, must be a "
                                                                           "valid date in YYYYMMDD format")


@given("request is made to the media analytics aggregate endpoint with invalid end date")
def step_impl(context):
    context.response = requests.get(
        uri + media_analytics_aggregate + "/" + context.data_config['media_analytics_aggregate'][
            'referer'] + "/" + context.data_config['media_analytics_aggregate'][
            'media_type']
        + "/" + context.data_config['media_analytics_aggregate']['agent_all']
        + "/" + context.data_config['media_analytics_aggregate']['granularity_daily']
        + "/" + context.data_config['media_analytics_aggregate']['start_date']
        + "/" + context.data_config['media_analytics_aggregate']['end_date_invalid'],
        headers=header)


@step("verify that detail key from the error response contains end date validation")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).is_equal_to("end timestamp is invalid, must be a "
                                                                           "valid date in YYYYMMDD format")


@given("request is made to the media analytics aggregate endpoint to validate first and last data point")
def step_impl(context):
    context.response = requests.get(
        uri + media_analytics_aggregate + "/" + context.data_config['media_analytics_aggregate'][
            'referer_first_last'] + "/" + context.data_config['media_analytics_aggregate'][
            'media_type']
        + "/" + context.data_config['media_analytics_aggregate']['agent_all']
        + "/" + context.data_config['media_analytics_aggregate']['granularity_daily']
        + "/" + context.data_config['media_analytics_aggregate']['start_date']
        + "/" + context.data_config['media_analytics_aggregate']['end_date'],
        headers=header)