import requests
import assertpy
from behave import *
from resources.all_resources.resources import *
from utilities.base_uri import *
from utilities.header import header

""" Page analytics Top was formerly called Pageviews Top """

@given("request made to the page analytics top endpoint with project, {access}, year, valid month, day")
def step_impl(context, access):
    context.response = requests.get(
        uri + page_analytics_top + "/" + context.data_config['page_analytics_top']['project'] + "/" + access
        + "/" + context.data_config['page_analytics_top']['year'] + "/" + context.data_config['page_analytics_top']['month']
        + "/" + context.data_config['page_analytics_top']['day'],
        headers=header)


@step("verify that the ranking is in increasing order")
def step_impl(context):
    for key in context.response.json()["items"][0]["articles"]:
        if key["rank"] - context.response.json()["items"][0]["articles"].index(key) == 1:
            assertpy.assert_that(
                key["rank"] - context.response.json()["items"][0]["articles"].index(key)).is_equal_to(1)
        elif key["rank"] - context.response.json()["items"][0]["articles"].index(key) > 1:
            assertpy.assert_that(
                key["rank"] - context.response.json()["items"][0]["articles"].index(key)).is_negative()


@given("request is made to the page analytics top endpoint with project, valid access, {years}, month, valid day")
def step_impl(context, years):
    context.response = requests.get(
        uri + page_analytics_top + "/" + context.data_config['page_analytics_top']['project']
        + "/" + context.data_config['page_analytics_top']['access'] + "/" + years
        + "/" + context.data_config['page_analytics_top']['month']
        + "/" + context.data_config['page_analytics_top']['day'],
        headers=header)


@given("request is made to the page analytics top endpoint with invalid project, access, year, month, day")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_top + "/" + context.data_config['page_analytics_top']['project_invalid'] + "/"
        + context.data_config['page_analytics_top']['access'] + "/" + context.data_config['page_analytics_top']['year']
        + "/" + context.data_config['page_analytics_top']['month'] + "/" + context.data_config['page_analytics_top']['day'],
        headers=header)


@step("verify that detail key from the error response displays invalid project warning for pageviews")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).contains(
        context.data_config['pageviews_top_validations']['invalid_project_validation'])


@given("request is made to the page analytics top endpoint with project and invalid access")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_top + "/" + context.data_config['page_analytics_top']['project'] + "/"
        + context.data_config['page_analytics_top']['access_invalid'] + "/" + context.data_config['page_analytics_top']['year']
        + "/" + context.data_config['page_analytics_top']['month'] + "/" + context.data_config['page_analytics_top']['day'],
        headers=header)


@step("verify that detail key from the error response displays invalid access warning for pageviews")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).is_equal_to(context.data_config['pageviews_top_validations']
                                                                        ['invalid_access_validation'])


@given("request is made to the page analytics top endpoint with project, invalid year entry")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_top + "/" + context.data_config['page_analytics_top']['project']
        + "/" + context.data_config['page_analytics_top']['access']
        + "/" + context.data_config['page_analytics_top']['year_invalid']
        + "/" + context.data_config['page_analytics_top']['month']
        + "/" + context.data_config['page_analytics_top']['day'],
        headers=header)


@step("verify that detail key from the error response displays invalid year warning for pageviews")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).is_equal_to(
        context.data_config['pageviews_top_validations']
        ['invalid_year_validation'])


@given("request is made to the page analytics top endpoint with project, invalid month entry")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_top + "/" + context.data_config['page_analytics_top']['project'] + "/"
        + context.data_config['page_analytics_top']['access']
        + "/" + context.data_config['page_analytics_top']['year']
        + "/" + context.data_config['page_analytics_top']['month_invalid']
        + "/" + context.data_config['page_analytics_top']['day'],
        headers=header)


@step("verify that detail key from the error response displays invalid month warning for pageviews")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).is_equal_to(
        context.data_config['pageviews_top_validations']
        ['invalid_year_validation'])


@given("request is made to the page analytics top endpoint with project, invalid day entry")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_top + "/" + context.data_config['page_analytics_top']['project']
        + "/" + context.data_config['page_analytics_top']['access']
        + "/" + context.data_config['page_analytics_top']['year']
        + "/" + context.data_config['page_analytics_top']['month']
        + "/" + context.data_config['page_analytics_top']['day_invalid'],
        headers=header)


@step("verify that detail key from the error response displays invalid day warning for pageviews")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).is_equal_to(
        context.data_config['pageviews_top_validations']
        ['invalid_year_validation'])


@given("request is made to the page analytics top endpoint with project, {missing_data_years}")
def step_impl(context, missing_data_years):
    context.response = requests.get(
        uri + page_analytics_top + "/" + context.data_config['page_analytics_top']['project']
        + context.data_config['page_analytics_top']['access']
        + "/" + missing_data_years + "/" + context.data_config['page_analytics_top']['month'] + "/"
        + context.data_config['page_analytics_top']['day'],
        headers=header)


@step("verify that detail key from the error response displays invalid data warning for pageviews")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).contains(
        context.data_config['pageviews_top_validations']['invalid_project_validation'])


@given("request is made to the page analytics top endpoint with {special_characters}")
def step_impl(context, special_characters):
    context.response = requests.get(
        uri + page_analytics_top + "/" + special_characters +
        context.data_config['page_analytics_top']['access'] + "/"
        + context.data_config['page_analytics_top'][
            'year'] + "/" + context.data_config['page_analytics_top']['month'] + "/"
        + context.data_config['page_analytics_top']['day'],
        headers=header)


@step("verify that detail key from the error response displays invalid character warning for pageviews")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).is_equal_to(
        'The parameter `project` contains invalid charaters.')


@given("request is made to an invalid page analytics top endpoint route with project, invalid day entry")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_top_invalid_route + "/" + context.data_config['page_analytics_top']['project'] + "/"
        + context.data_config['page_analytics_top']['access']
        + "/" + context.data_config['page_analytics_top']['year']
        + "/" + context.data_config['page_analytics_top']['month']
        + "/" + context.data_config['page_analytics_top']['day'],
        headers=header)


@given("request is made to page analytics top endpoint with en.wikipedia project, year 2020, month 01 and day 01")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_top + "/" + context.data_config['page_analytics_top']['project'] + "/"
        + context.data_config['page_analytics_top']['access']
        + "/" + context.data_config['page_analytics_top']['year']
        + "/" + context.data_config['page_analytics_top']['month']
        + "/" + context.data_config['page_analytics_top']['day'],
        headers=header)


@given("request is made to page analytics top endpoint with en.wikipedia project, year 2018, month 01 and day 01")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_top + "/" + context.data_config['page_analytics_top']['project']
        + "/" + context.data_config['page_analytics_top']['access']
        + "/" + context.data_config['page_analytics_top']['year_2018']
        + "/" + context.data_config['page_analytics_top']['month']
        + "/" + context.data_config['page_analytics_top']['day'],
        headers=header)


@given("request is made to page analytics top endpoint with en.wikipedia project, year 2019, month 01 and day 01")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_top + "/" + context.data_config['page_analytics_top']['project']
        + "/" + context.data_config['page_analytics_top']['access']
        + "/" + context.data_config['page_analytics_top']['year_2019']
        + "/" + context.data_config['page_analytics_top']['month']
        + "/" + context.data_config['page_analytics_top']['day'],
        headers=header)


@given("request is made to page analytics top endpoint with de.wikipedia project, year 2018, month 01 and day 01")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_top + "/" + context.data_config['page_analytics_top']['project_de']
        + "/" + context.data_config['page_analytics_top']['access']
        + "/" + context.data_config['page_analytics_top']['year_2018']
        + "/" + context.data_config['page_analytics_top']['month']
        + "/" + context.data_config['page_analytics_top']['day'],
        headers=header)


@given("request is made to page analytics top endpoint with et.wikipedia project, year 2018, month 01 and day 01")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_top + "/" + context.data_config['page_analytics_top']['project_et']
        + "/" + context.data_config['page_analytics_top']['access']
        + "/" + context.data_config['page_analytics_top']['year_2018']
        + "/" + context.data_config['page_analytics_top']['month']
        + "/" + context.data_config['page_analytics_top']['day'],
        headers=header)


@given("request is made to page analytics top endpoint with valid data {projects}")
def step_impl(context, projects):
    context.response = requests.get(
        uri + page_analytics_top + "/" + projects + "/"
        + context.data_config['page_analytics_top']['access']
        + "/" + context.data_config['page_analytics_top']['year_2018']
        + "/" + context.data_config['page_analytics_top']['month']
        + "/" +context.data_config['page_analytics_top']['day'],
        headers=header)


@given("request is made to page analytics top endpoint with invalid data {projects}")
def step_impl(context, projects):
    context.response = requests.get(
        uri + page_analytics_top + "/" + projects
        + "/" + context.data_config['page_analytics_top']['access']
        + "/" + context.data_config['page_analytics_top']['year_2018']
        + "/" + context.data_config['page_analytics_top']['month'] + "/" +
        context.data_config['page_analytics_top']['day'],
        headers=header)