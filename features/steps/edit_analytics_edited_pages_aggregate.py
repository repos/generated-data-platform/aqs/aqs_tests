from behave import *
import requests
import assertpy
from resources.all_resources.resources import edit_edited_pages_aggregate
from utilities.base_uri import uri
from utilities.header import header


@given(
    "request is made to the Edit analytics edited pages aggregate endpoint with varying {projects}, editor_types, all-page-types, daily, 20200101, 20201201")
def step_impl(context, projects):
    context.response = requests.get(
        uri + edit_edited_pages_aggregate + "/" + projects
        + "/" + context.data_config['edit_edited_pages_aggregate']['editor_type']
        + "/" + context.data_config['edit_edited_pages_aggregate']['page_type_all']
        + "/" + context.data_config['edit_edited_pages_aggregate']['activity_level']
        + "/" + context.data_config['edit_edited_pages_aggregate']['granularity'] + "/" +
        context.data_config['edit_edited_pages_aggregate']['start_date'] + "/"
        + context.data_config['edit_edited_pages_aggregate']['end_date'], headers=header)


@given(
    "request is made to the Edit analytics edited pages aggregate endpoint with en.wikipedia, varying {editor_types}, all-page-types, daily, 20180101, 20181231")
def step_impl(context, editor_types):
    context.response = requests.get(
        uri + edit_edited_pages_aggregate + "/" + context.data_config['edit_edited_pages_aggregate']['project']
        + "/" + editor_types + "/" + context.data_config['edit_edited_pages_aggregate']['page_type_all']
        + "/" + context.data_config['edit_edited_pages_aggregate']['activity_level']
        + "/" + context.data_config['edit_edited_pages_aggregate']['granularity']
        + "/" + context.data_config['edit_edited_pages_aggregate']['start_date']
        + "/" + context.data_config['edit_edited_pages_aggregate']['end_date'], headers=header)


@given(
    "request is made to the Edit analytics edited pages aggregate endpoint with en.wikipedia, editor_types, varying {page_types}, daily, 20200101, 20201201")
def step_impl(context, page_types):
    context.response = requests.get(
        uri + edit_edited_pages_aggregate + "/" + context.data_config['edit_edited_pages_aggregate']['project']
        + "/" + context.data_config['edit_edited_pages_aggregate']['editor_type'] + "/" + page_types
        + "/" + context.data_config['edit_edited_pages_aggregate']['activity_level']
        + "/" + context.data_config['edit_edited_pages_aggregate']['granularity']
        + "/" + context.data_config['edit_edited_pages_aggregate']['start_date']
        + "/" + context.data_config['edit_edited_pages_aggregate']['end_date'], headers=header)


@given(
    "request is made to the AQS Edit analytics edited pages aggregate endpoint with en.wikipedia, editor_types, page_types, varying {granularity}, 20200101, 20201201")
def step_impl(context, granularity):
    context.response = requests.get(
        uri + edit_edited_pages_aggregate + "/" + context.data_config['edit_edited_pages_aggregate']['project']
        + "/" + context.data_config['edit_edited_pages_aggregate']['editor_type']
        + "/" + context.data_config['edit_edited_pages_aggregate']['page_type_all']
        + "/" + context.data_config['edit_edited_pages_aggregate']['activity_level']
        + "/" + granularity + "/" + context.data_config['edit_edited_pages_aggregate']['start_date']
        + "/" + context.data_config['edit_edited_pages_aggregate']['end_date'], headers=header)


@given("request is made to the Edit analytics edited pages aggregate endpoint with invalid project")
def step_impl(context):
    context.response = requests.get(
        uri + edit_edited_pages_aggregate + "/" + context.data_config['edit_edited_pages_aggregate'][
            'project_invalid']
        + "/" + context.data_config['edit_edited_pages_aggregate']['editor_type']
        + "/" + context.data_config['edit_edited_pages_aggregate']['page_type_all']
        + "/" + context.data_config['edit_edited_pages_aggregate']['activity_level']
        + "/" + context.data_config['edit_edited_pages_aggregate']['granularity']
        + "/" + context.data_config['edit_edited_pages_aggregate']['start_date']
        + "/" + context.data_config['edit_edited_pages_aggregate']['end_date'], headers=header)


@given("request is made to the Edit analytics edited pages aggregate endpoint with invalid editor type")
def step_impl(context):
    context.response = requests.get(
        uri + edit_edited_pages_aggregate + "/" + context.data_config['edit_edited_pages_aggregate'][
            'project']
        + "/" + context.data_config['edit_edited_pages_aggregate']['editor_type_invalid']
        + "/" + context.data_config['edit_edited_pages_aggregate']['page_type_all']
        + "/" + context.data_config['edit_edited_pages_aggregate']['activity_level']
        + "/" + context.data_config['edit_edited_pages_aggregate']['granularity']
        + "/" + context.data_config['edit_edited_pages_aggregate']['start_date']
        + "/" + context.data_config['edit_edited_pages_aggregate']['end_date'], headers=header)


@given("request is made to the Edit analytics edited pages aggregate endpoint with invalid page type")
def step_impl(context):
    context.response = requests.get(
        uri + edit_edited_pages_aggregate + "/" + context.data_config['edit_edited_pages_aggregate'][
            'project']
        + "/" + context.data_config['edit_edited_pages_aggregate']['editor_type']
        + "/" + context.data_config['edit_edited_pages_aggregate']['page_type_invalid']
        + "/" + context.data_config['edit_edited_pages_aggregate']['activity_level']
        + "/" + context.data_config['edit_edited_pages_aggregate']['granularity']
        + "/" + context.data_config['edit_edited_pages_aggregate']['start_date']
        + "/" + context.data_config['edit_edited_pages_aggregate']['end_date'], headers=header)


@given("request is made to the Edit analytics edited pages aggregate endpoint with invalid granularity")
def step_impl(context):
    context.response = requests.get(
        uri + edit_edited_pages_aggregate + "/" + context.data_config['edit_edited_pages_aggregate'][
            'project']
        + "/" + context.data_config['edit_edited_pages_aggregate']['editor_type']
        + "/" + context.data_config['edit_edited_pages_aggregate']['page_type_all']
        + "/" + context.data_config['edit_edited_pages_aggregate']['activity_level']
        + "/" + context.data_config['edit_edited_pages_aggregate']['granularity_invalid']
        + "/" + context.data_config['edit_edited_pages_aggregate']['start_date']
        + "/" + context.data_config['edit_edited_pages_aggregate']['end_date'], headers=header)


@given("request is made to the Edit analytics edited pages aggregate endpoint with invalid activity level")
def step_impl(context):
    context.response = requests.get(
        uri + edit_edited_pages_aggregate + "/" + context.data_config['edit_edited_pages_aggregate'][
            'project']
        + "/" + context.data_config['edit_edited_pages_aggregate']['editor_type']
        + "/" + context.data_config['edit_edited_pages_aggregate']['page_type_all']
        + "/" + context.data_config['edit_edited_pages_aggregate']['activity_level_invalid']
        + "/" + context.data_config['edit_edited_pages_aggregate']['granularity']
        + "/" + context.data_config['edit_edited_pages_aggregate']['start_date']
        + "/" + context.data_config['edit_edited_pages_aggregate']['end_date'], headers=header)


@step("verify that detail key from the error response for invalid activity level for edited pages aggregate endpoint")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).contains(
        "should be equal to one of the allowed values: [all-activity-levels, 1..4-edits, 5..24-edits, 25..99-edits, 100..-edits]")


@given("request is made to the Edit analytics edited pages aggregate endpoint with invalid start time")
def step_impl(context):
    context.response = requests.get(
        uri + edit_edited_pages_aggregate + "/" + context.data_config['edit_edited_pages_aggregate'][
            'project']
        + "/" + context.data_config['edit_edited_pages_aggregate']['editor_type']
        + "/" + context.data_config['edit_edited_pages_aggregate']['page_type_all']
        + "/" + context.data_config['edit_edited_pages_aggregate']['activity_level']
        + "/" + context.data_config['edit_edited_pages_aggregate']['granularity']
        + "/" + context.data_config['edit_edited_pages_aggregate']['start_date_invalid']
        + "/" + context.data_config['edit_edited_pages_aggregate']['end_date'], headers=header)


@given("request is made to the Edit analytics edited pages aggregate endpoint with invalid end time")
def step_impl(context):
    context.response = requests.get(
        uri + edit_edited_pages_aggregate + "/" + context.data_config['edit_edited_pages_aggregate'][
            'project']
        + "/" + context.data_config['edit_edited_pages_aggregate']['editor_type']
        + "/" + context.data_config['edit_edited_pages_aggregate']['page_type_all']
        + "/" + context.data_config['edit_edited_pages_aggregate']['activity_level']
        + "/" + context.data_config['edit_edited_pages_aggregate']['granularity']
        + "/" + context.data_config['edit_edited_pages_aggregate']['start_date']
        + "/" + context.data_config['edit_edited_pages_aggregate']['end_date_invalid'], headers=header)


@given(
    "request is made to the Edit analytics edited pages aggregate endpoint with {project}, {editor_type}, "
    "{page_type}, {activity_level}, {granularity}, {start_date}, {end_date}")
def step_impl(context, project, editor_type, page_type, activity_level, granularity, start_date, end_date):
    context.response = requests.get(
        uri + edit_edited_pages_aggregate
        + "/" + project
        + "/" + editor_type
        + "/" + page_type
        + "/" + activity_level
        + "/" + granularity
        + "/" + start_date
        + "/" + end_date, headers=header)


@when("the response status is different from 404")
def step_impl(context):
    if context.response.status_code == 404:
        context.scenario.skip(reason='there is no data for this request')
