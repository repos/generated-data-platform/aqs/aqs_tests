""" Test code for the unique devices endpoints with the BDD steps"""

from behave import *
import assertpy
import requests
from utilities.header import header
from resources.all_resources.resources import device_analytics_resource
from utilities.base_uri import *


@given("request to the unique_devices endpoint with en.wikipedia.org, all-sites, daily, 20220701 and 20220929")
def step_impl(context):
    context.response = requests.get(
        uri + device_analytics_resource + "/" + context.data_config['device_analytics']['project'] + "/" +
        context.data_config['device_analytics']['access_site']
        + "/" + context.data_config['device_analytics']['granularity'] + "/" + context.data_config['device_analytics'][
            'start_date'] + "/" + context.data_config['device_analytics']['end_date'],
        headers=header)


@given(
    "request is made to the unique_devices endpoint with en.wikipedia.org, desktop-site, daily, 20220701 and 20220929")
def step_impl(context):
    context.response = requests.get(
        uri + device_analytics_resource + "/" + context.data_config['device_analytics']['project'] + "/" +
        context.data_config['device_analytics']['access_site_desktop']
        + "/" + context.data_config['device_analytics']['granularity'] + "/" + context.data_config['device_analytics'][
            'start_date'] + "/" + context.data_config['device_analytics']['end_date'],
        headers=header)


@given(
    "request is made to the unique_devices endpoint with en.wikipedia.org, mobile-site, daily, 20220701 and 20220929")
def step_impl(context):
    context.response = requests.get(
        uri + device_analytics_resource + "/" + context.data_config['device_analytics']['project'] + "/" +
        context.data_config['device_analytics']['access_site_mobile']
        + "/" + context.data_config['device_analytics']['granularity'] + "/" + context.data_config['device_analytics'][
            'start_date'] + "/" + context.data_config['device_analytics']['end_date'],
        headers=header)


@given(
    "request is made to the unique_devices endpoint with en.wikipedia.org, all-sites, monthly, 20220701 and 20220929")
def step_impl(context):
    context.response = requests.get(
        uri + device_analytics_resource + "/" + context.data_config['device_analytics']['project'] + "/" +
        context.data_config['device_analytics']['access_site']
        + "/" + context.data_config['device_analytics']['granularity_monthly'] + "/" +
        context.data_config['device_analytics'][
            'start_date'] + "/" + context.data_config['device_analytics']['end_date'],
        headers=header)


@given("request is made to the unique_devices endpoint with {projects}, all-sites, daily, 20220701 and 20220929")
def step_impl(context, projects):
    context.response = requests.get(
        uri + device_analytics_resource + "/" + projects + "/" +
        context.data_config['device_analytics']['access_site']
        + "/" + context.data_config['device_analytics']['granularity'] + "/" + context.data_config['device_analytics'][
            'start_date'] + "/" + context.data_config['device_analytics']['end_date'],
        headers=header)


@given(
    "request is made to the unique_devices endpoint with invalid_project, "
    "all-sites, monthly, 20220701 and 20220929")
def step_impl(context):
    context.response = requests.get(
        uri + device_analytics_resource + "/" + context.data_config['device_analytics']['project_wrong_site'] + "/" +
        context.data_config['device_analytics']['access_site']
        + "/" + context.data_config['device_analytics']['granularity'] + "/" + context.data_config['device_analytics'][
            'start_date'] + "/" + context.data_config['device_analytics']['end_date'],
        headers=header)


@step("verify that the used project is in the returned uri")
def step_impl(context):
    assertpy.assert_that(context.response.json()["uri"]).contains(
        context.data_config['device_analytics']['project_wrong_site'])


@given(
    "request is made to the unique_devices endpoint with www.mediawiki.org, all-sites, wrongdate, 20220701 and 20220929")
def step_impl(context):
    context.response = requests.get(
        uri + device_analytics_resource + "/" + context.data_config['device_analytics']['project_mediawiki'] + "/" +
        context.data_config['device_analytics']['access_site']
        + "/" + context.data_config['device_analytics']['granularity'] + "/" + context.data_config['device_analytics'][
            'wrong_date'] + "/" + context.data_config['device_analytics']['end_date'],
        headers=header)


@given("request is made to the unique_devices endpoint with www.mediawiki.org, all-sites, daily, 20220929 and 20220701")
def step_impl(context):
    context.response = requests.get(
        uri + device_analytics_resource + "/" + context.data_config['device_analytics']['project_mediawiki'] + "/" +
        context.data_config['device_analytics']['access_site']
        + "/" + context.data_config['device_analytics']['granularity'] + "/" + context.data_config['device_analytics'][
            'end_date'] + "/" + context.data_config['device_analytics']['start_date'],
        headers=header)


@step("verify that detail key from the error response displays valid date warning")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).is_equal_to("start timestamp is invalid, "
                                                                           "must be a valid date in YYYYMMDD format")


@step("verify that detail key from the error response displays start timestamp warning")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).is_equal_to("start timestamp should be "
                                                                           "before the end timestamp")


@given(
    "request is made to the unique_devices endpoint with wrongwebsitefortesting.org, all-sites, monthly, "
    "20220701 and 20220929")
def step_impl(context):
    context.response = requests.get(
        uri + device_analytics_resource + "/" + context.data_config['device_analytics']['project_wrong_site2'] + "/" +
        context.data_config['device_analytics']['access_site']
        + "/" + context.data_config['device_analytics']['granularity'] + "/" + context.data_config['device_analytics'][
            'start_date'] + "/" + context.data_config['device_analytics']['end_date'],
        headers=header)


@given("request is made to the unique_devices endpoint with all-sites, daily and with a day interval")
def step_impl(context):
    context.response = requests.get(
        uri + device_analytics_resource + "/" + context.data_config['device_analytics']['project'] + "/" +
        context.data_config['device_analytics']['access_site']
        + "/" + context.data_config['device_analytics']['granularity'] + "/" + context.data_config['device_analytics'][
            'start_date_daily'] + "/" + context.data_config['device_analytics']['end_date_daily'],
        headers=header)


@step("verify the first object in the items key response using the start date timestamp")
def step_impl(context):
    assertpy.assert_that(context.response.json()["items"][0]["timestamp"]).is_equal_to(context.data_config['device_analytics'][
            'start_date_daily'])


@step("verify the last object in the items key response using the end date timestamp")
def step_impl(context):
    assertpy.assert_that(context.response.json()["items"][-1]["timestamp"]).is_equal_to(context.data_config['device_analytics']
                                                                           ['end_date_daily'])


@given("request is made to the unique_devices endpoint with all-sites, monthly and with a 30 day interval")
def step_impl(context):
    context.response = requests.get(
        uri + device_analytics_resource + "/" + context.data_config['device_analytics']['project'] + "/" +
        context.data_config['device_analytics']['access_site']
        + "/" + context.data_config['device_analytics']['granularity_monthly'] + "/" + context.data_config['device_analytics'][
            'start_date_30_day'] + "/" + context.data_config['device_analytics']['end_date_30_day'],
        headers=header)


@step("verify the first object in the items key response for monthly granularity")
def step_impl(context):
    assertpy.assert_that(context.response.json()["items"][0]["timestamp"]).is_equal_to(
        context.data_config['device_analytics']
        ['start_date_30_day'])


@step("verify the last object in the items key response for monthly granularity")
def step_impl(context):
    assertpy.assert_that(context.response.json()["items"][-1]["timestamp"]).is_equal_to(
        context.data_config['device_analytics']
        ['end_date_30_day'])


@given(
    "request is made to the unique_devices endpoint with en.wikipedia.org, all-sites, daily, 20200101 and 20200110")
def step_impl(context):
    context.response = requests.get(
        uri + device_analytics_resource + "/" + context.data_config['device_analytics']['project'] + "/" +
        context.data_config['device_analytics']['access_site']
        + "/" + context.data_config['device_analytics']['granularity'] + "/" +
        context.data_config['device_analytics'][
            'start_date_first'] + "/" + context.data_config['device_analytics']['end_date_last'],
        headers=header)