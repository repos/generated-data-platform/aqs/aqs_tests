from behave import *
import assertpy
import requests
from utilities.header import header
from resources.all_resources.resources import geo_analytics
from utilities.base_uri import *


# this is the test the month of jan for year 2018 - 2021  with 100..-edits activity level
@given("request is made to the geo analytics by country endpoint with en.wikipedia.org, 100..-edits, {year}, 01")
def step_impl(context, year):
    context.response = requests.get(
        uri + geo_analytics + "/" + context.data_config['geo_analytics']['project']
        + "/" + context.data_config['geo_analytics']['activity_level_100_edits']
        + "/" + year + "/" + context.data_config['geo_analytics']['month_jan'], headers=header)


# this is the test the month of dec for year 2018 - 2021  with 100..-edits activity level
@given("request is made to the geo analytics by country endpoint with en.wikipedia.org, 100..-edits, {year}, 12")
def step_impl(context, year):
    context.response = requests.get(
        uri + geo_analytics + "/" + context.data_config['geo_analytics']['project']
        + "/" + context.data_config['geo_analytics']['activity_level_100_edits']
        + "/" + year + "/" + context.data_config['geo_analytics'][
            'month_dec'], headers=header)


# this is the test the month of jan for years 2018 - 2021  with 5.99..-edits activity level
@given("request is made to the geo analytics by country endpoint with en.wikipedia.org, 5.99..-edits, {year}, 01")
def step_impl(context, year):
    context.response = requests.get(
        uri + geo_analytics + "/" + context.data_config['geo_analytics']['project']
        + "/" + context.data_config['geo_analytics']['activity_level_5_edits']
        + "/" + year + "/" + context.data_config['geo_analytics']['month_jan'], headers=header)


# this is the test the month of dec for year 2018 - 2021  with 5.99..-edits activity level
@given("request is made to the geo analytics by country endpoint with en.wikipedia.org, 5.99..-edits, {year}, 12")
def step_impl(context, year):
    context.response = requests.get(
        uri + geo_analytics + "/" + context.data_config['geo_analytics']['project']
        + "/" + context.data_config['geo_analytics']['activity_level_5_edits']
        + "/" + year + "/" + context.data_config['geo_analytics']['month_dec'], headers=header)


# This is to test for the month of jan for year 2018 with 100..-edits activity level with different countries
@given("request is made to the geo analytics by country endpoint with {projects}, 100..-edits, 2018, 01")
def step_impl(context, projects):
    context.response = requests.get(
        uri + geo_analytics + "/" + projects + "/" +
        context.data_config['geo_analytics']['activity_level_100_edits']
        + "/" + context.data_config['geo_analytics']['year_2018'] + "/" +
        context.data_config['geo_analytics'][
            'month_jan'], headers=header)


@step("verify that the {project} used in request is same as project in response body")
def step_impl(context, project):
    for array in context.response.json()["items"]:
        assertpy.assert_that(array["project"]).is_equal_to(project)


@given("request is made to the editors by country endpoint with invalid_project, 100..-edits, 2018, 01")
def step_impl(context):
    context.response = requests.get(
        uri + geo_analytics + "/" + context.data_config['geo_analytics']['project_invalid'] + "/" +
        context.data_config['geo_analytics']['activity_level_100_edits']
        + "/" + context.data_config['geo_analytics']['year_2018'] + "/" +
        context.data_config['geo_analytics']['month_jan'], headers=header)


@step("verify that detail key from the error response displays valid project or data warning")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).contains("project you asked for is not loaded yet")


@given("request is made to the geo analytics by country endpoint with valid project, invalid activity level, 2018, 01")
def step_impl(context):
    context.response = requests.get(
        uri + geo_analytics + "/" + context.data_config['geo_analytics']['project'] + "/" +
        context.data_config['geo_analytics']['activity_level_invalid']
        + "/" + context.data_config['geo_analytics']['year_2018'] + "/" +
        context.data_config['geo_analytics']['month_jan'], headers=header)


@step("verify that detail key from the error response displays activity level validation")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"].lower()).is_equal_to("Activity-level should be equal to one of the "
                                                                        "allowed values: [5..99-edits, 100..-edits]".lower())


@given("request is made to the geo analytics by country endpoint with en.wikipedia.org, 5.99..-edits, {year}, 06")
def step_impl(context, year):
    context.response = requests.get(
        uri + geo_analytics + "/" + context.data_config['geo_analytics']['project'] + "/" +
        context.data_config['geo_analytics']['activity_level_5_edits']
        + "/" + year + "/" + context.data_config['geo_analytics'][
            'month_jun'], headers=header)


@step("verify that detail key from the error response displays validation for the date(year)")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).contains("The date(s) you used are valid, but we "
                                                                     "either do not have data for those date(s), "
                                                                     "or the project you asked for is not loaded "
                                                                     "yet")


@step("verify the entered {year} is in the uri key in the error response")
def step_impl(context, year):
    assertpy.assert_that(context.response.json()["uri"]).contains(year)


@given("request is made to the geo analytics by country endpoint with project, 100..-edits, year, invalid_month")
def step_impl(context):
    context.response = requests.get(
        uri + geo_analytics + "/" + context.data_config['geo_analytics']['project'] + "/" +
        context.data_config['geo_analytics']['activity_level_100_edits']
        + "/" + context.data_config['geo_analytics']['year_2018'] + "/" + context.data_config['geo_analytics'][
            'month_invalid'], headers=header)


@step("verify that detail key from the error response displays invalid year/month warning")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"].lower()).contains("Given year/month is invalid date".lower())


@given("request is made to the geo analytics by country endpoint with project, 100..-edits, invalid_year, month")
def step_impl(context):
    context.response = requests.get(
        uri + geo_analytics + "/" + context.data_config['geo_analytics']['project'] + "/" +
        context.data_config['geo_analytics']['activity_level_100_edits']
        + "/" + context.data_config['geo_analytics']['year_invalid'] + "/" +
        context.data_config['geo_analytics']['month_jan'], headers=header)


@given("request is made to the geo analytics by country endpoint with project, 5..99-edits, year, month")
def step_impl(context):
    context.response = requests.get(
        uri + geo_analytics + "/" + context.data_config['geo_analytics']['project'] + "/" +
        context.data_config['geo_analytics']['activity_level_5_edits']
        + "/" + context.data_config['geo_analytics']['year_2020'] + "/" +
        context.data_config['geo_analytics']['month_jan'], headers=header)

@given("request is made to the geo analytics by country endpoint with <project>, 5..99-edits, year, month")
def step_impl(context, project):
    context.response = requests.get(
        uri + geo_analytics + "/" + project + "/" +
        context.data_config['geo_analytics']['activity_level_5_edits']
        + "/" + context.data_config['geo_analytics']['year_2020'] + "/" +
        context.data_config['geo_analytics']['month_jan'], headers=header)
