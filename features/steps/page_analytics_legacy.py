import requests
from behave import *
import assertpy
from resources.all_resources.resources import page_analytics_legacy_resource
from utilities.base_uri import uri
from utilities.header import header


@given("request is made to the legacy endpoint with en.wikipedia, varying {access}, hourly, 2016010101 and 2016010123")
def step_impl(context, access):
    context.response = requests.get(
        uri + page_analytics_legacy_resource + "/" + context.data_config['page_analytics_legacy']['project'] + "/"
        + access + "/" + context.data_config['page_analytics_legacy']['granularity']
        + "/" + context.data_config['page_analytics_legacy']['start_time'] + "/" +
        context.data_config['page_analytics_legacy']['end_time'],
        headers=header)


@given(
    "request is made to the legacy endpoint with en.wikipedia, access, varying {granularity}, 2016010101 and 2016010123")
def step_impl(context, granularity):
    context.response = requests.get(
        uri + page_analytics_legacy_resource + "/" + context.data_config['page_analytics_legacy']['project']
        + "/" + context.data_config['page_analytics_legacy']['access'] + "/" + granularity
        + "/" + context.data_config['page_analytics_legacy']['start_time'] + "/" +
        context.data_config['page_analytics_legacy']['end_time'],
        headers=header)


@given("request is made to legacy service endpoint with valid data {projects}")
def step_impl(context, projects):
    context.response = requests.get(
        uri + page_analytics_legacy_resource + "/" + projects + "/"
        + context.data_config['page_analytics_legacy']['access']
        + "/" + context.data_config['page_analytics_legacy']['granularity']
        + "/" + context.data_config['page_analytics_legacy']['start_time'] + "/" +
        context.data_config['page_analytics_legacy']['end_time'],
        headers=header)


@given("request is made to legacy service endpoint with varying valid {start_date} and {end_date}")
def step_impl(context, start_date, end_date):
    context.response = requests.get(
        uri + page_analytics_legacy_resource + "/" + context.data_config['page_analytics_legacy']['project']
        + "/" + context.data_config['page_analytics_legacy']['access']
        + "/" + context.data_config['page_analytics_legacy']['granularity']
        + "/" + start_date + "/" + end_date,
        headers=header)


@given("request is made to the legacy service endpoint with invalid route")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_legacy_resource + "/" + "/" + context.data_config['page_analytics_legacy']['project']
        + "/" + context.data_config['page_analytics_legacy']['access']
        + "/" + context.data_config['page_analytics_legacy']['granularity']
        + "/" + context.data_config['page_analytics_legacy']['start_time'] + "/" +
        context.data_config['page_analytics_legacy']['end_time'],
        headers=header)


@given("request is made to the legacy service endpoint with invalid project")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_legacy_resource + "/" + context.data_config['page_analytics_legacy']['project_invalid']
        + "/" + context.data_config['page_analytics_legacy']['access']
        + "/" + context.data_config['page_analytics_legacy']['granularity']
        + "/" + context.data_config['page_analytics_legacy']['start_time'] + "/" +
        context.data_config['page_analytics_legacy']['end_time'],
        headers=header)


@step("verify that detail key from the error response displays invalid project warning for legacy")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).contains(
        context.data_config['page_analytics_legacy']['invalid_project_warning'])


@given("request is made to the legacy service endpoint with invalid access site")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_legacy_resource + "/" + context.data_config['page_analytics_legacy']['project'] + "/"
        + context.data_config['page_analytics_legacy']['access_invalid']
        + "/" + context.data_config['page_analytics_legacy']['granularity']
        + "/" + context.data_config['page_analytics_legacy']['start_time']
        + "/" + context.data_config['page_analytics_legacy']['end_time'],
        headers=header)


@step("verify that detail key from the error response displays invalid access-site warning for legacy")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).contains(
        context.data_config['page_analytics_legacy']['invalid_access_site_warning'])


@given("request is made to the legacy service endpoint with invalid granularity")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_legacy_resource + "/" + context.data_config['page_analytics_legacy']['project']
        + "/" + context.data_config['page_analytics_legacy']['access']
        + "/" + context.data_config['page_analytics_legacy']['granularity_invalid']
        + "/" + context.data_config['page_analytics_legacy']['start_time'] + "/" +
        context.data_config['page_analytics_legacy']['end_time'],
        headers=header)


@step("verify that detail key from the error response displays invalid granularity warning for legacy")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).contains(
        context.data_config['page_analytics_legacy']['invalid_granularity_warning'])


@given("request is made to the legacy service endpoint with invalid start time")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_legacy_resource + "/" + context.data_config['page_analytics_legacy']['project'] + "/"
        + context.data_config['page_analytics_legacy']['access']
        + "/" + context.data_config['page_analytics_legacy']['granularity']
        + "/" + context.data_config['page_analytics_legacy']['start_time_invalid'] + "/" +
        context.data_config['page_analytics_legacy']['end_time'],
        headers=header)


@step("verify that detail key from the error response displays invalid start time warning for legacy")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).contains(
        context.data_config['page_analytics_legacy']['invalid_start_time'])


@given("request is made to the legacy service endpoint with invalid end time")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_legacy_resource + "/" + context.data_config['page_analytics_legacy']['project']
        + "/" + context.data_config['page_analytics_legacy']['access']
        + "/" + context.data_config['page_analytics_legacy']['granularity']
        + "/" + context.data_config['page_analytics_legacy']['start_time'] + "/" +
        context.data_config['page_analytics_legacy']['end_time_invalid'],
        headers=header)


@step("verify that detail key from the error response displays invalid end time warning for legacy")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).contains(
        context.data_config['page_analytics_legacy']['invalid_end_time'])


@given("request is made to the legacy service endpoint without full month range")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_legacy_resource + "/" + context.data_config['page_analytics_legacy']['project'] + "/"
        + context.data_config['page_analytics_legacy']['access']
        + "/" + context.data_config['page_analytics_legacy']['granularity_monthly']
        + "/" + context.data_config['page_analytics_legacy']['start_time'] + "/" +
        context.data_config['page_analytics_legacy']['start_time'],
        headers=header)


@step("verify that detail key from the error response for no full month specified")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).contains(
        context.data_config['page_analytics_legacy']['no_full_month_range'])


@given("request is made to the legacy service endpoint with 24 hour specified with hourly granularity")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_legacy_resource + "/" + context.data_config['page_analytics_legacy']['project'] + "/"
        + context.data_config['page_analytics_legacy']['access']
        + "/" + context.data_config['page_analytics_legacy']['granularity']
        + "/" + context.data_config['page_analytics_legacy']['start_time'] + "/" +
        context.data_config['page_analytics_legacy']['end_time_24_hr'],
        headers=header)


@given("request is made to the legacy service endpoint with invalid start time format")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_legacy_resource + "/" + context.data_config['page_analytics_legacy']['project'] + "/"
        + context.data_config['page_analytics_legacy']['access']
        + "/" + context.data_config['page_analytics_legacy']['granularity']
        + "/" + context.data_config['page_analytics_legacy']['start_time_wrong_format'] + "/" +
        context.data_config['page_analytics_legacy']['end_time'],
        headers=header)


@given("request is made to the legacy service endpoint with invalid end time format")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_legacy_resource + "/" + context.data_config['page_analytics_legacy']['project'] + "/"
        + context.data_config['page_analytics_legacy']['access']
        + "/" + context.data_config['page_analytics_legacy']['granularity']
        + "/" + context.data_config['page_analytics_legacy']['start_time'] + "/" +
        context.data_config['page_analytics_legacy']['end_time_wrong_format'],
        headers=header)


@given("request is made to the legacy service endpoint with invalid start and end time format")
def step_impl(context):
    context.response = requests.get(
        uri + page_analytics_legacy_resource + "/" + context.data_config['page_analytics_legacy']['project'] + "/"
        + context.data_config['page_analytics_legacy']['access']
        + "/" + context.data_config['page_analytics_legacy']['granularity']
        + "/" + context.data_config['page_analytics_legacy']['start_time_wrong_format'] + "/" +
        context.data_config['page_analytics_legacy']['end_time_wrong_format'],
        headers=header)


@step("verify that detail key from the error response displays invalid start and end time format warning for legacy")
def step_impl(context):
    assertpy.assert_that(context.response.json()["detail"]).contains(
        context.data_config['page_analytics_legacy']['invalid_start_time'])