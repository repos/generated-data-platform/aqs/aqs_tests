#BDD steps for the unique devices test scenarios

@aqs_tests @aqs_tests.device_analytics

Feature: Tests for Unique Devices endpoint

    # Business Need: Positive Scenarios
    Scenario: Be able to run the AQS unique devices endpoints with all-sites
        Given request to the unique_devices endpoint with en.wikipedia.org, all-sites, daily, 20220701 and 20220929
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that items key contains project in list of arrays
        And verify that items key contains access-site in list of arrays
        And verify that items key contains granularity in list of arrays
        And verify that items key contains timestamp in list of arrays
        And verify that items key contains devices in list of arrays
        And verify that items key contains offset in list of arrays
        And verify that items key contains underestimate in list of arrays
        And verify that items key with sub_key project is of type string
        And verify that items key with sub_key access-site is of type string
        And verify that items key with sub_key granularity is of type string
        And verify that items key with sub_key timestamp is of type string
        And verify that items key with sub_key devices is of type integer
        And verify that items key with sub_key offset is of type integer
        And verify that items key with sub_key underestimate is of type integer
        And verify that project sub_key in items key is equal to en.wikipedia
        And verify that access-site sub_key in items key is equal to all-sites
        And verify that granularity sub_key in items key is equal to daily

    Scenario: Be able to run the AQS unique devices endpoints with desktop-site
        Given request is made to the unique_devices endpoint with en.wikipedia.org, desktop-site, daily, 20220701 and 20220929
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that items key contains access-site in list of arrays
        And verify that access-site sub_key in items key is equal to desktop-site

    Scenario: Be able to run the AQS unique devices endpoints with mobile-site
        Given request is made to the unique_devices endpoint with en.wikipedia.org, mobile-site, daily, 20220701 and 20220929
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that items key contains access-site in list of arrays
        And verify that access-site sub_key in items key is equal to mobile-site

    Scenario: Be able to run the AQS unique devices endpoints with all-sites and monthly
        Given request is made to the unique_devices endpoint with en.wikipedia.org, all-sites, monthly, 20220701 and 20220929
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that granularity sub_key in items key is equal to monthly

    Scenario: Be able to run the AQS unique devices endpoints with all-sites, daily and with a day interval
        Given request is made to the unique_devices endpoint with all-sites, daily and with a day interval
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that granularity sub_key in items key is equal to daily
        And verify the first object in the items key response using the start date timestamp
        And verify the last object in the items key response using the end date timestamp

  Scenario: Be able to run the AQS unique devices endpoints with all-sites, monthly and with a 30 day interval
        Given request is made to the unique_devices endpoint with all-sites, monthly and with a 30 day interval
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that granularity sub_key in items key is equal to monthly
        And verify the first object in the items key response for monthly granularity
        And verify the last object in the items key response for monthly granularity

       Scenario Outline: Be able to run the AQS unique devices endpoints with all-sites and different projects
        Given request is made to the unique_devices endpoint with <projects>, all-sites, daily, 20220701 and 20220929
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that items key contains project in list of arrays
        And verify that items key contains access-site in list of arrays
        And verify that items key contains granularity in list of arrays
        And verify that items key contains timestamp in list of arrays
        And verify that items key contains devices in list of arrays
        And verify that items key contains offset in list of arrays
        And verify that items key contains underestimate in list of arrays
        And verify that items key with sub_key project is of type string
        And verify that items key with sub_key access-site is of type string
        And verify that items key with sub_key granularity is of type string
        And verify that items key with sub_key timestamp is of type string
        And verify that items key with sub_key devices is of type integer
        And verify that items key with sub_key offset is of type integer
        And verify that items key with sub_key underestimate is of type integer
        And verify that project sub_key with .org domain in items key is equal to <projects>
        And verify that access-site sub_key in items key is equal to all-sites
        And verify that granularity sub_key in items key is equal to daily

           Examples:
               | projects |
               | es.wikisource.org |
               | zh.wikisource.org |
               | sw.wikipedia.org |
               | bcl.wikipedia.org |
               | it.wikibooks.org |
               | diq.wikipedia.org |
               | nl.wikibooks.org |
               | id.wikibooks.org |
               | ja.wikisource.org |
               | lmo.wikipedia.org |
               | ta.wiktionary.org |
               | tr.wikiquote.org |
               | pa.wikipedia.org |
               | he.wikiquote.org |
               | bh.wikipedia.org |
               | ban.wikipedia.org |
               | yo.wikipedia.org |
               | min.wikipedia.org |
               | pt.wikinews.org |
                | sv.wikibooks.org |
                | fr.wikipedia.org |
                | pl.wikipedia.org |
                | commons.wikimedia.org |

    # Business Need: Negative Scenarios
    Scenario: Be able to run the AQS unique devices endpoints with all-sites and invalid project
        Given request is made to the unique_devices endpoint with invalid_project, all-sites, monthly, 20220701 and 20220929
        Then the request should be unsuccessful with status code of 404
        And verify that the content-type is application/problem+json
        And verify that title key contains Not Found
        And verify that method key contains get
        And verify that the used project is in the returned uri

  Scenario: Be able to run the AQS unique devices endpoints with all-sites and wrong project
      Given request is made to the unique_devices endpoint with wrongwebsitefortesting.org, all-sites, monthly, 20220701 and 20220929
      Then the request should be unsuccessful with status code of 404
      And verify that the content-type is application/problem+json
      And verify that title key contains Not Found
      And verify that method key contains get

    Scenario: Be able to run the AQS unique devices endpoints with all-sites and wrong date
        Given request is made to the unique_devices endpoint with www.mediawiki.org, all-sites, wrongdate, 20220701 and 20220929
        Then the request should be unsuccessful with status code of 400
        And verify that the content-type is application/problem+json
        And verify that method key contains get
        And verify that detail key from the error response displays valid date warning

    Scenario: Be able to run the AQS unique devices endpoints with all-sites with start date later than end date
        Given request is made to the unique_devices endpoint with www.mediawiki.org, all-sites, daily, 20220929 and 20220701
        Then the request should be unsuccessful with status code of 400
        And verify that the content-type is application/problem+json
        And verify that method key contains get
        And verify that detail key from the error response displays start timestamp warning

    Scenario Outline: Be able to run the AQS unique devices endpoints with all-sites and different mobile projects
      Given request is made to the unique_devices endpoint with <projects>, all-sites, daily, 20220701 and 20220929
      Then the request should be successful with status code of 404
      And verify that the content-type is application/problem+json

      Examples:
        | projects |
         | en.m.wikisource.org |
         | zh.m.wikisource.org |
         | sw.m.wikipedia.org |
         | bcl.m.wikipedia.org |
        | fr.m.wikipedia.org |

   Scenario: Run the AQS unique devices endpoints and assert the first and last data returned matches dates and values
        Given request is made to the unique_devices endpoint with en.wikipedia.org, all-sites, daily, 20200101 and 20200110
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify the value of the devices key in the first json is equal to 64551398
        And verify the value of the offset key in the first json is equal to 12781183
        And verify the value of the underestimate key in the first json is equal to 51770215
        And verify the value of the devices key in the last json is equal to 69419942
        And verify the value of the offset key in the last json is equal to 13932549
        And verify the value of the underestimate key in the last json is equal to 55487393