#BDD steps for the Common Impact Metrics (CIM) Edits per category monthly API
@aqs_tests @aqs_tests.common_impact_metrics @aqs_tests.cim_edits_per_category_monthly

Feature: Tests for Common Impact Metrics : Edits per category Monthly
    # Business Need: Positive Scenarios

    # These are tests for Edits per category monthly

  Scenario Outline: Be able to run the CIM Edits per category Monthly endpoint with varying category
        Given request is made to the Edits per category monthly endpoint with varying category <category>, 20231101 and 20240601
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that response body with context key is of type dict
        And verify that the sub_key category in the context key response is equal to <category>
        And verify that the sub_key start in the context key response is equal to 2023110100
        And verify that the sub_key end in the context key response is equal to 2024060100
        Examples:
            | category |
            | Images_from_ArkDes |
            | Eilat_Museum |
            | Aerial_photographs_of_interchanges_in_Switzerland |
            | Aeroseum                                          |


  Scenario Outline: Be able to run the CIM Edits per category monthly endpoint with varying category scope
        Given request is made to the Edits per category monthly endpoint with varying scope <scope>, 20231101 and 20240601
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that response body with context key is of type dict
        And verify that the sub_key category-scope in the context key response is equal to <scope>
        And verify that the sub_key category in the context key response is equal to Arbetets_museum
        And verify that the sub_key start in the context key response is equal to 2023110100
        And verify that the sub_key end in the context key response is equal to 2024060100
        Examples:
            | scope |
            |shallow|
            | deep|

  Scenario Outline: Be able to run the CIM Edits per category monthly endpoint with varying edit type
        Given request is made to the Edits per category monthly endpoint with varying edit_type <edit_type>, 20231101 and 20240601
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that response body with context key is of type dict
        And verify that the sub_key category in the context key response is equal to Arbetets_museum
        And verify that the sub_key category-scope in the context key response is equal to shallow
        And verify that the sub_key edit-type in the context key response is equal to <edit_type>
        And verify that the sub_key start in the context key response is equal to 2023110100
        And verify that the sub_key end in the context key response is equal to 2024060100
        Examples:
            | edit_type |
            | create |
            | update |
            | all-edit-types |


  Scenario: Be able to run the CIM Edits per category monthly endpoint with invalid Route
        Given request is made to the Edits per category monthly endpoint with invalid route
        Then the request should be unsuccessful with status code of 404
        And verify that the content-type is application/problem+json
        And verify that title key contains Not Found
        And verify that the detail in response is equal to invalid route
        And verify that the method in response is equal to get


  Scenario: Be able to run the CIM Edits per category monthly endpoint with invalid category
        Given request is made to the Edits per category monthly endpoint with invalid category
        Then the request should be unsuccessful with status code of 404
        And verify that the content-type is application/problem+json
        And verify that title key contains Not Found
        And verify that the detail in response contains category you asked for is not loaded yet


  Scenario: Be able to run the CIM Edits per category monthly endpoint with invalid scope
        Given request is made to the Edits per category monthly endpoint with invalid scope
        Then the request should be unsuccessful with status code of 400
        And verify that the content-type is application/problem+json
        And verify that title key contains Bad Request
        And verify that the detail in response contains allowed values: [shallow,deep]


  Scenario: Be able to run the CIM Edits per category monthly endpoint with invalid edit type
        Given request is made to the Edits per category monthly endpoint with invalid edit type
        Then the request should be unsuccessful with status code of 400
        And verify that the content-type is application/problem+json
        And verify that title key contains Bad Request
        And verify that the detail in response contains allowed values: [create,update,all-edit-types]


  Scenario: Be able to run the CIM Edits per category monthly endpoint with invalid start time
        Given request is made to the Edits per category monthly endpoint with invalid start time
        Then the request should be unsuccessful with status code of 400
        And verify that the content-type is application/problem+json
        And verify that title key contains Bad Request
        And verify that the detail in response contains start timestamp is invalid, must be a valid date in YYYYMMDD format


  Scenario: Be able to run the CIM Edits per category monthly endpoint with invalid end time
        Given request is made to the Edits per category monthly endpoint with invalid end time
        Then the request should be unsuccessful with status code of 400
        And verify that the content-type is application/problem+json
        And verify that title key contains Bad Request
        And verify that the detail in response contains end timestamp is invalid, must be a valid date in YYYYMMDD format


  # DATA TEST
  Scenario: Be able to run the CIM Edits per category monthly endpoint to validate category rank and pageview count
        Given request is made to the Edits per category monthly endpoint to validate timestamp and edit count
        Then the request should be unsuccessful with status code of 200
        And verify that the sub_key edit-type in the context key response is equal to all-edit-types
        And verify that the sub_key category in the context key response is equal to Arbetets_museum
        And verify the value of the timestamp key in the items object first json is equal to text 2024030100
        And verify the value of the edit-count key in the first json is equal to 4
        And verify the value of the timestamp key in the items object last json is equal to text 2024030100
        And verify the value of the edit-count key in the last json is equal to 4