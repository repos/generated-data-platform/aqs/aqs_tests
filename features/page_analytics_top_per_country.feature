#BDD steps for the page_analytics_top_per_country endpoint test scenarios

  # Page analytics Top was formerly called Pageviews Top

@aqs_tests @aqs_tests.page_analytics @aqs_tests.page_analytics_top_per_country

Feature: Tests for pageviews_top_per_country endpoint

    Scenario Outline: Be able to run the AQS page analytics top per country endpoint with valid parameters
        Given request is made to the page analytics top per country endpoint with project, <country>, access, year, month, day
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that items key contains country in list of arrays
        And verify that items key contains country sub_key have upper case values
        And verify that items key contains country sub_key has a length of 2
        And verify that items key contains year in list of arrays
        And verify that items key contains year sub_key has a length of 4
        And verify that items key contains month in list of arrays
        And verify that items key contains month sub_key has a length of 2
        And verify that items key contains day in list of arrays
        And verify that items key contains day sub_key has a length of 2
        And verify that items key with sub_key articles contains article in the list
        And verify that items key with sub_key articles contains project in the list
        And verify that items key with sub_key articles contains views_ceil in the list
        And verify that items key with sub_key articles sub_key contains views_ceil of type int
        And verify that items key with sub_key articles contains rank in the list
        And verify that items key with sub_key articles sub_key contains rank of type int
        And verify that the country in the response is the same as the <country> in the endpoint
        And verify that the year in the response is the same as the year in the endpoint

      Examples:
        | country |
        | GB      |
        | FR |
        | US |
        | NG |
        | CA |
        | BE |
        | SE |
        | DE |
        | GE |
        | GH |
        | MO |
        | IN |

    Scenario Outline: Be able to run the AQS page analytics top per country endpoint with excluded countries
        Given request is made to the page analytics top per country endpoint with excluded <country>
        Then the request should be successful with status code of 404
        And verify that method key contains get
        And verify that title key contains Not found
        And verify that detail key from the error response displays error for the pageviews top per country project

      Examples:
        | country |
        | UK |
        | CN |

  Scenario Outline: Be able to run the AQS page analytics top per country endpoint with varying access
        Given request is made to the page analytics top per country endpoint with varying <access>
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify <access> in the request is the same as the access in the response

      Examples:
        | access |
        | all-access |
        | desktop |
        | mobile-app |
        | mobile-web |

  Scenario Outline: Be able to run the AQS page analytics top per country endpoint with varying valid year
        Given request is made to the page analytics top per country endpoint with valid varying <year>
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8

      Examples:
        | year |
        | 2021 |
        | 2022 |

  Scenario Outline: Be able to run the AQS page analytics top per country endpoint with varying invalid year
        Given request is made to the page analytics top per country endpoint with invalid varying <year_invalid>
        Then the request should be successful with status code of 404
        And verify that method key contains get
        And verify that title key contains Not found
        And verify that detail key from the error response displays error for the pageviews top per country project

      Examples:
        | year_invalid |
        | 2000 |
        | 2001 |
        | 2002 |
        | 2003 |
        | 2004 |
        | 2005 |
        | 2006 |
        | 2007 |
        | 2008 |
        | 2009 |
        | 2010 |
        | 2011 |
        | 2012 |
        | 2013 |
        | 2014 |
        | 2015 |
        | 2016 |
        | 2017 |
        | 2018 |
        | 2019 |
        | 2020 |


  Scenario: Be able to run the AQS page analytics top per country endpoint with invalid country
        Given request is made to the page analytics top per country endpoint with invalid country
        Then the request should be successful with status code of 404
        And verify that method key contains get
        And verify that title key contains Not found
        And verify that detail key from the error response displays error for the pageviews top per country project

  Scenario: Be able to run the AQS page analytics top per country endpoint with invalid access
        Given request is made to the page analytics top per country endpoint with invalid access
        Then the request should be successful with status code of 400
        And verify that method key contains get
        And verify that title key contains Bad Request
        And verify that detail key from the error response displays error for the pageviews top per country access

  Scenario: Be able to run the AQS page analytics top per country endpoint with invalid year
        Given request is made to the page analytics top per country endpoint with invalid year
        Then the request should be successful with status code of 400
        And verify that method key contains get
        And verify that detail key from the error response displays error for the pageviews top per country year

  Scenario: Be able to run the AQS page analytics top per country endpoint with invalid month
        Given request is made to the page analytics top per country endpoint with invalid month
        Then the request should be successful with status code of 400
        And verify that method key contains get
        And verify that detail key from the error response displays error for the pageviews top per country month

  Scenario: Be able to run the AQS page analytics top per country endpoint with invalid day
        Given request is made to the page analytics top per country endpoint with invalid day
        Then the request should be successful with status code of 400
        And verify that method key contains get
        And verify that detail key from the error response displays error for the pageviews top per country day

  Scenario: Be able to run the AQS page analytics top per country endpoint with invalid route
        Given request is made to the page analytics top per country endpoint with invalid route
        Then the request should be successful with status code of 404
        And verify that method key contains get
        And verify that title key contains Not found

    Scenario: Validate the first and last data in page analytics top per country for year 2021 using US
        Given request is made to page analytics top per country for year 2021 using US
        Then the request should be successful with status code of 200
        And verify that country sub_key in items key is equal to US
        And verify that year sub_key in items key is equal to 2021
        And verify the value of the views_ceil key in the first json of the articles key is equal to 2589300
        And verify the value of the rank key in the first json of the articles key is equal to 1
        And verify the value of the views_ceil key in the last json of the articles key is equal to 6300
        And verify the value of the rank key in the first json of the articles key is equal to 1000

    Scenario: Validate the first and last data in page analytics top per country for year 2022 using US
        Given request is made to page analytics top per country for year 2022 using US
        Then the request should be successful with status code of 200
        And verify that country sub_key in items key is equal to US
        And verify that year sub_key in items key is equal to 2022
        And verify the value of the views_ceil key in the first json of the articles key is equal to 1967300
        And verify the value of the rank key in the first json of the articles key is equal to 1
        And verify the value of the views_ceil key in the last json of the articles key is equal to 5700
        And verify the value of the rank key in the first json of the articles key is equal to 1000

    Scenario: Validate the first and last data in page analytics top per country for year 2023 using US
        Given request is made to page analytics top per country for year 2023 using US
        Then the request should be successful with status code of 200
        And verify that country sub_key in items key is equal to US
        And verify that year sub_key in items key is equal to 2023
        And verify the value of the views_ceil key in the first json of the articles key is equal to 2167800
        And verify the value of the rank key in the first json of the articles key is equal to 1
        And verify the value of the views_ceil key in the last json of the articles key is equal to 6000
        And verify the value of the rank key in the first json of the articles key is equal to 1000


    Scenario Outline: Verify invalid data projects for page analytics top per country
        Given request is made to page analytics top per country endpoint with invalid data <countries>
        Then the request should be successful with status code of 404
        And verify that the content-type is application/problem+json

        Examples:
            | countries |
            | de |
            | et |
            | fr |
            | ng |
            | be |
            | ca |
            | es |
            | et |
            | de |
            | fr |
            | be |
           | zh |
           | sw |
           | bcl |
            | diq |
           | nl |
           | id |
           | ja |
           | lmo |
           | ta |
           | tr |
           | pa |
           | he |
           | ban |
           | min |
           | pt |
            | sv |
            | fr |
            | pl |
            | in |
