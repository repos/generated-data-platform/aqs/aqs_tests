#BDD steps for the page analytics per_article endpoint test scenarios



@aqs_tests @aqs_tests.page_analytics @aqs_tests.page_analytics_per_article

Feature: Tests for Page analytics per article endpoint

    Scenario Outline: Be able to run the Page analytics per article endpoints with varying access and daily
        Given request is made to the page analytics per_article endpoint with en.wikipedia.org, <access>, all-agents, St. Louis City SC, daily, 20220701 and 20220929
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that items key contains project in list of arrays
        And verify that items key contains article in list of arrays
        And verify that items key contains granularity in list of arrays
        And verify that items key contains timestamp in list of arrays
        And verify that items key contains access in list of arrays
        And verify that items key contains article in list of arrays
        And verify that items key contains views in list of arrays
        And verify that items key with sub_key project is of type string
        And verify that items key with sub_key article is of type string
        And verify that items key with sub_key granularity is of type string
        And verify that items key with sub_key timestamp is of type string
        And verify that items key with sub_key access is of type string
        And verify that items key with sub_key article is of type string
        And verify that items key with sub_key views is of type integer
        And verify that granularity sub_key in items key is equal to daily
        And verify that access sub_key in items key is equal to <access>

        Examples:
            | access |
            | all-access |
            | desktop |
            | mobile-app |
            | mobile-web |


    Scenario Outline: Be able to run the Page analytics per article endpoints with varying agents and daily
        Given request is made to the page analytics per_article endpoint with en.wikipedia.org, access, varying <agents>, St. Louis City SC, daily, 20220701 and 20220929
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that items key contains project in list of arrays
        And verify that items key contains article in list of arrays
        And verify that items key contains granularity in list of arrays
        And verify that items key contains timestamp in list of arrays
        And verify that items key contains access in list of arrays
        And verify that items key contains article in list of arrays
        And verify that items key contains views in list of arrays
        And verify that items key with sub_key project is of type string
        And verify that items key with sub_key article is of type string
        And verify that items key with sub_key granularity is of type string
        And verify that items key with sub_key timestamp is of type string
        And verify that items key with sub_key access is of type string
        And verify that items key with sub_key article is of type string
        And verify that items key with sub_key views is of type integer
        And verify that granularity sub_key in items key is equal to daily
        And verify that access sub_key in items key is equal to all-access
        And verify that agent sub_key in items key is equal to <agents>

        Examples:
            | agents |
            | all-agents |
            | user |
            | spider |
            | automated |

    Scenario Outline: Be able to run the Page analytics per article endpoints with varying agents and monthly
        Given request is made to the page analytics per_article endpoint with en.wikipedia.org, <access>, all-agents, St. Louis City SC, monthly, 20220701 and 20220929
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that items key contains project in list of arrays
        And verify that items key contains article in list of arrays
        And verify that items key contains granularity in list of arrays
        And verify that items key contains timestamp in list of arrays
        And verify that items key contains access in list of arrays
        And verify that items key contains article in list of arrays
        And verify that items key contains views in list of arrays
        And verify that items key with sub_key project is of type string
        And verify that items key with sub_key article is of type string
        And verify that items key with sub_key granularity is of type string
        And verify that items key with sub_key timestamp is of type string
        And verify that items key with sub_key access is of type string
        And verify that items key with sub_key article is of type string
        And verify that items key with sub_key views is of type integer
        And verify that granularity sub_key in items key is equal to monthly
        And verify that agent sub_key in items key is equal to all-agents
        And verify that access sub_key in items key is equal to <access>

        Examples:
            | access |
            | all-access |
            | desktop |
            | mobile-app |
            | mobile-web |


    Scenario: Be able to run the Page analytics per article endpoints with Invalid project
        Given request is made to the page analytics per_article endpoint with invalid project, access, all-agents, St. Louis City SC, daily, 20220701 and 20220929
        Then the request should be successful with status code of 404
        And verify that method key contains get
        And verify that title key contains Not found
        And verify that detail key from the error response displays the invalid project error

    Scenario: Be able to run the Page analytics per article endpoints with Invalid character
        Given request is made to the page analytics per_article endpoint with special character, access, all-agents, St. Louis City SC, monthly, 20220701 and 20220929
        Then the request should be successful with status code of 400
        And verify that method key contains get
        And verify that detail key from the error response displays the invalid character error


    Scenario: Be able to run the Page analytics per article endpoints with Invalid access
        Given request is made to the page analytics per_article endpoint with project, invalid access, all-agents, St. Louis City SC, monthly, 20220701 and 20220929
        Then the request should be successful with status code of 400
        And verify that method key contains get
        And verify that title key contains Bad Request
        And verify that detail key from the error response displays the invalid access error

    Scenario: Be able to run the Page analytics per article endpoints with Invalid agent
        Given request is made to the page analytics per_article endpoint with project, access, invalid agent, St. Louis City SC, monthly, 20220701 and 20220929
        Then the request should be successful with status code of 400
        And verify that method key contains get
        And verify that title key contains Bad Request
        And verify that detail key from the error response displays the invalid agent error


    Scenario: Be able to run the Page analytics per article endpoints with Invalid article
        Given request is made to the page analytics per_article endpoint with project, access, agent, invalid article, monthly, 20220701 and 20220929
        Then the request should be successful with status code of 404
        And verify that method key contains get
        And verify that title key contains Not Found
        And verify that detail key from the error response displays the invalid article error

    Scenario: Be able to run the Page analytics per article endpoints with Invalid granularity
        Given request is made to the page analytics per_article endpoint with project, access, agent, article, invalid granularity, 20220701 and 20220929
        Then the request should be successful with status code of 400
        And verify that method key contains get
        And verify that title key contains Bad Request
        And verify that detail key from the error response displays the invalid granularity error

    Scenario: Be able to run the Page analytics per article endpoints with later date before early date with monthly granularity
        Given request is made to the page analytics per_article endpoint with project, access, agent, article, granularity, later date before early date
        Then the request should be successful with status code of 400
        And verify that method key contains get
        And verify that detail key from the error response displays the invalid start and end date timestamp error

    Scenario: Be able to run the APage analytics per article endpoints with later date before early date with daily granularity
        Given request is made to the page analytics per_article endpoint with project, access, agent, article, daily granularity, later date before early date
        Then the request should be successful with status code of 400
        And verify that method key contains get
        And verify that detail key from the error response displays the invalid start and end date timestamp error for daily


    Scenario: Validate the first and last data in Page analytics per article for year 2018 using en.wikipedia project
        Given request is made to page analytics per_article endpoint with en.wikipedia project, year 2018, month 01 and day 01
        Then the request should be successful with status code of 200
        And verify the value of the views key in the first json is equal to 3436
        And verify the value of the views key in the last json is equal to 3892

    Scenario: Validate the first and last data in Page analytics per article for year 2019 using en.wikipedia project
        Given request is made to page analytics per_article endpoint with en.wikipedia project, year 2019, month 01 and day 01
        Then the request should be successful with status code of 200
        And verify the value of the views key in the first json is equal to 3651
        And verify the value of the views key in the last json is equal to 4740

    Scenario: Validate the first and last data in Page analytics per article for year 2020 using en.wikipedia project
        Given request is made to page analytics per_article endpoint with en.wikipedia project, year 2020, month 01 and day 01
        Then the request should be successful with status code of 200
        And verify the value of the views key in the first json is equal to 3406
        And verify the value of the views key in the last json is equal to 4451

    Scenario Outline: Validate valid data projects for Page analytics per article on Mobile phone article
        Given request is made to page analytics per_article endpoint with valid data <projects>
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that project sub_key in items key is equal to <projects>

        Examples:
            | projects |
            | it.wikipedia |
            | yo.wikipedia |
            | bh.wikipedia |
            | commons.wikimedia |

    Scenario Outline: Validate invalid data projects for Page analytics per article on Mobile phone article
        Given request is made to page analytics per_article endpoint with invalid data <projects>
        Then the request should be successful with status code of 404
        And verify that the content-type is application/problem+json
        And verify that method key contains get

        Examples:
            | projects |
            | de.wikipedia.org |
            | et.wikipedia.org |
            | jp.wikipedia.org |
            | fr.wikipedia.org |
            | ke.wikipedia.org |
            | ng.wikipedia.org |
            | be.wikipedia.org |
            | ca.wikipedia.org |
            | td.wikipedia.org |
            | tz.wikipedia.org |
            | es.wikisource.org |
            | jp.wikisource.org |
            | et.wikisource.org |
            | de.wikisource.org |
            | fr.wikisource.org |
            | ke.wikisource.org |
            | ng.wikisource.org |
            | be.wikisource.org |
            | cd.wikisource.org |
            | td.wikisource.org |
           | zh.wikisource.org |
           | sw.wikipedia.org |
           | bcl.wikipedia.org |
            | diq.wikipedia.org |
           | nl.wikibooks.org |
           | id.wikibooks.org |
           | ja.wikisource.org |
           | lmo.wikipedia.org |
           | ta.wiktionary.org |
           | tr.wikiquote.org |
           | pa.wikipedia.org |
           | he.wikiquote.org |
           | ban.wikipedia.org |
           | min.wikipedia.org |
           | pt.wikinews.org |
            | sv.wikibooks.org |
            | fr.wikipedia.org |
            | pl.wikipedia.org |
            | bd.wikipedia |
            | bb.wikipedia |
            | by.wikipedia |
            | in.wikipedia |

