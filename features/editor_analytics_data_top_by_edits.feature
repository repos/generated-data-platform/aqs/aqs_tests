#BDD test steps for the editor data top-by-edits api scenarios

    # Editor Analytics data was formerly known as Editors Data

@aqs_tests @aqs_tests.editors_analytics_data @aqs_tests.editor_analytics_data_top_by_edits

Feature: Tests for Editor Analytics Data top-by-edits endpoint

    # Business Need: Positive Scenarios

    # This is to test for Editor data top-by-edits endpoint with en.wikipedia, diff editor types, diff page types, diff activity levels, diff granularity,
    # 20210302 and 20210901
    # Success cases
    Scenario Outline: Be able to run the Editor Analytics Data top-by-edits endpoint with various projects, editor types, page types, year, month and day
        Given request is made to the editors data top-by-edits endpoint with <project>, <editor_type>, <page_type>, <year>, <month> and <day>
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that items key contains project in list of arrays
        And verify that items key contains editor-type in list of arrays
        And verify that items key contains page-type in list of arrays
        And verify that items key with project in is equal to <response_project>
        And verify that items key with editor-type in is equal to <editor_type>
        And verify that items key with page-type in is equal to <page_type>
        And verify that items key with granularity in is equal to <granularity>
        And verify that top sub_key contains user_text of type string or none type
        And verify that top sub_key contains edits of type int
        And verify the value of user_text key of the first json of the top results array is a string and is equal to <first_user_text>
        And verify the value of edits key of the first json of the top results array is an integer and is equal to <first_edits>
        And verify the value of user_text key of the last json of the top results array is a string and is equal to <last_user_text>
        And verify the value of edits key of the last json of the top results array is an integer and is equal to <last_edits>

        Examples:
            | project                 | response_project        | editor_type      | page_type      | year | month | day      | granularity | first_user_text  | first_edits  | last_user_text            | last_edits |
            | all-projects            | all-projects            | user             | content        | 2021 | 01    | 01       | daily       | Luca.favorido           | 87414           | Renamerr                       | 480         |
            | all-wikipedia-projects  | all-wikipedia-projects  | all-editor-types | content        | 2021 | 04    | all-days | monthly     | Lsjbot       | 323204         | Narky Blert            | 5344          |
            | all-wiktionary-projects | all-wiktionary-projects | all-editor-types | content        | 2021 | 05    | all-days | monthly     | Sarri.greek       | 72567          | Diddy-sama6                    | 1011         |
            | ab.wikipedia            | ab.wikipedia            | user             | all-page-types | 2021 | 04    | all-days | monthly     | Pupsik-ipa       | 1121         | Xnet1234                  | 1          |
            | ab.wikipedia            | ab.wikipedia            | user             | content        | 2021 | 02    | 02       | daily       | Fraxinus.cs      | 24           | Fraxinus.cs               | 24         |
            | www.ab.wikipedia.org    | ab.wikipedia            | user             | content        | 2021 | 02    | 02       | daily       | Fraxinus.cs      | 24           | Fraxinus.cs               | 24         |
            | ro.wiktionary           | ro.wiktionary           | user             | non-content    | 2021 | 04    | 30       | daily       | Paloi Sciurala           | 3            | Paloi Sciurala                    | 3          |
            | sw.wikipedia            | sw.wikipedia            | user             | content        | 2020 | 12    | 31       | daily       | Kipala           | 6            | Shōten ＆ VTuber are best  | 1          |
            | sw.wikipedia.org        | sw.wikipedia            | user             | content        | 2020 | 12    | 31       | daily       | Kipala           | 6            | Shōten ＆ VTuber are best  | 1          |

    # Business Need: Negative Scenarios

    # Keep in mind that AQS 1.0 title error response for these cases is equal to "Invalid parameters".
    # Bad request when requesting invalid parameters (dates not included)
    Scenario Outline: Run the Editor Analytics Data top-by-edits endpoints with sw.wikipedia, editor types, page types, 2020, 12 and 30 using an invalid parameter for each case
        Given request is made to the editors data top-by-edits endpoint with sw.wikipedia, <editor_type>, <page_type>, 2020, 12 and 30
        Then the request should be unsuccessful with status code of 400
        And verify that the content-type is application/problem+json
        And verify that title key contains Bad Request
        And verify that method key contains get
        And verify that detail key contains <detail>

        Examples:
            | editor_type         | page_type         | detail                                                                                                                            |
            | invalid_editor_type | all-page-types    | editor-type should be equal to one of the allowed values: [all-editor-types, anonymous, group-bot, name-bot, user] |
            | anonymous           | invalid-content   | page-type should be equal to one of the allowed values: [all-page-types, content, non-content]                     |

    # Keep in mind that there is no 'title' key for errors when invalid dates. That part will fail with AQS 1.0
    # Keep in mind that AQS 1.0 title error response for these cases is equal to "Invalid parameters".
    # Bad request when requesting invalid dates
    Scenario Outline: Run the Editor Analytics Data top-by-edits endpoints with sw.wikipedia, all-editor-types, all-page-types, year, month and day using invalid dates for each case
        Given request is made to the editors data top-by-edits endpoint with sw.wikipedia, all-editor-types, all-page-types, <year>, <month> and <day>
        Then the request should be unsuccessful with status code of 400
        And verify that the content-type is application/problem+json
        And verify that title key contains Bad Request
        And verify that method key contains get
        And verify that detail key contains <detail>

    Examples:
        | year | month | day  | detail |
        | 202  | 12    | 30   | Given year/month/day is invalid date |
        | 2020 | 13    | 30   | Given year/month/day is invalid date |
        | 2020 | 12    | 32   | Given year/month/day is invalid date |
        | 2020 | 12    | 01a  | Given year/month/day is invalid date |

    # Invalid characters
    Scenario: Be able to run the Editor Analytics Data top-by-edits endpoints with an invalid project, all-editor-types, content, 2020, 12 and 30
        Given request is made to the editors data top-by-edits endpoint with sw.wi*kipedia, all-editor-types, content, 2020, 12 and 30
        Then the request should be unsuccessful with status code of 400
        And verify that the content-type is application/problem+json
        And verify that title key contains Bad Request
        And verify that method key contains get
        And verify that detail key contains the parameter `project` contains invalid characters

    # Keep in mind that AQS 1.0 responds with a 200 status code and zero values when no data is found due to an invalid project
    # Not Found when requesting a nonexistentproject project
    Scenario: Be able to run the Editor Analytics Data top-by-edits endpoints with an non-existent project, user, content, 2020, 12 and 31
    Given request is made to the editors data top-by-edits endpoint with nonexistentproject, user, content, 2020, 12 and 31
    Then the request should be unsuccessful with status code of 404
    And verify that the content-type is application/problem+json
    And verify that title key contains Not Found
    And verify that method key contains get
    And verify that detail key contains The date(s) you used are valid, but we either do not have data for those date(s), or the project you asked for is not loaded yet. Please check documentation for more information

    # No data found
    Scenario: Be able to run the Editor Analytics Data top-by-edits endpoints with a project, user, content, 9999, 12 and 31
    Given request is made to the editors data top-by-edits endpoint with sw.wikipedia, user, content, 9999, 12 and 31
    Then the request should be unsuccessful with status code of 404
    And verify that the content-type is application/problem+json
    And verify that title key contains Not Found
    And verify that method key contains get
    And verify that detail key contains The date(s) you used are valid, but we either do not have data for those date(s), or the project you asked for is not loaded yet. Please check documentation for more information

    # Invalid route
    Scenario: Be able to run the Editor Analytics Data top-by-edits endpoints with sw.wikipedia, all-editor-types, content, 2020, 12 and 31 using an invalid route
    Given request is made to the editors data top-by-edits endpoint using an invalid route
    Then the request should be unsuccessful with status code of 404
    And verify that the content-type is application/problem+json
    And verify that title key contains Not Found
    And verify that method key contains get
    And verify that detail key contains Invalid route
