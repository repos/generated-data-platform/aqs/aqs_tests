#BDD steps for the Common Impact Metrics (CIM) Top wikis per category monthly
@aqs_tests @aqs_tests.common_impact_metrics @aqs_tests.cim_top_wikis_per_category_monthly

Feature: Tests for Common Impact Metrics : Top wikis per category monthly
    # Business Need: Positive Scenarios

    # These are tests for Top wikis per category monthly

  Scenario Outline: Be able to run the CIM Top wikis per category monthly endpoint with varying category
        Given request is made to the Top wikis per category monthly endpoint with varying category <category>, 2023 and 11
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that response body with context key is of type dict
        And verify that the sub_key category in the context key response is equal to <category>
        And verify that the sub_key year in the context key response is equal to 2023
        And verify that the sub_key month in the context key response is equal to 11
        Examples:
            | category |
            | Aeroseum |
            | All_files_by_NeuroMat |
            |   Arbetets_museum           |
            |   Archives_at_NCBS |
            |   Archives_New_Zealand      |
            |   Archivio_Pietro_Pensa        |
            | Archivio_Storico_Ricordi |
            | Archivio_Storico_Ricordi |
            | Books_in_Balinese |
            | Content_produced_by_UNESCO |


  Scenario Outline: Be able to run the CIM Top wikis per category monthly endpoint with varying category scope
        Given request is made to the Top wikis per category monthly endpoint with varying scope <scope>, 2023 and 11
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that response body with context key is of type dict
        And verify that the sub_key category-scope in the context key response is equal to <scope>
        And verify that the sub_key year in the context key response is equal to 2023
        And verify that the sub_key month in the context key response is equal to 11
        Examples:
            | scope |
            |shallow|
            | deep|

  Scenario: Be able to run the CIM Top wikis per category monthly endpoint endpoint with invalid Route
        Given request is made to the Top wikis per category monthly endpoint with invalid route
        Then the request should be unsuccessful with status code of 404
        And verify that the content-type is application/problem+json
        And verify that title key contains Not Found
        And verify that the detail in response is equal to invalid route
        And verify that the method in response is equal to get


  Scenario: Be able to run the CIM Top wikis per category monthly endpoint endpoint with invalid category
        Given request is made to the Top wikis per category monthly endpoint with invalid category
        Then the request should be unsuccessful with status code of 404
        And verify that the content-type is application/problem+json
        And verify that title key contains Not Found
        And verify that the method in response is equal to get
        And verify that the detail in response contains the category you asked for is not loaded yet


  Scenario: Be able to run the CIM Top wikis per category monthly endpoint with invalid scope
        Given request is made to the Top wikis per category monthly endpoint with invalid scope
        Then the request should be unsuccessful with status code of 400
        And verify that the content-type is application/problem+json
        And verify that title key contains Bad Request
        And verify that the detail in response contains allowed values: [shallow,deep]


  Scenario: Be able to run the CIM Top wikis per category monthly endpoint with invalid year
        Given request is made to the Top wikis per category monthly endpoint with invalid year
        Then the request should be unsuccessful with status code of 400
        And verify that the content-type is application/problem+json
        And verify that title key contains bad request
        And verify that the detail in response contains given year/month is invalid date

  Scenario: Be able to run the CIM Top wikis per category monthly endpoint with invalid month
        Given request is made to the Top wikis per category monthly endpoint with invalid month
        Then the request should be unsuccessful with status code of 400
        And verify that the content-type is application/problem+json
        And verify that title key contains bad request
        And verify that the detail in response contains given year/month is invalid date


  # DATA TEST
  Scenario: Be able to run the CIM Top wikis per category monthly endpoint to validate category rank and pageview count
        Given request is made to the Top wikis per category monthly endpoint to validate category rank and pageview count
        Then the request should be unsuccessful with status code of 200
        And verify that the sub_key category-scope in the context key response is equal to shallow
        And verify that the sub_key category in the context key response is equal to All_files_by_NeuroMat
        And verify the value of the pageview-count key in the first json is equal to 3903265
        And verify the value of the wiki key in the items object first json is equal to text en.wikipedia
        And verify the value of the rank key in the first json is equal to 1
        And verify the value of the pageview-count key in the last json is equal to 1
        And verify the value of the wiki key in the items object last json is equal to text it.wikiquote
        And verify the value of the rank key in the last json is equal to 92
        And verify that the sub_key year in the context key response is equal to 2023
        And verify that the sub_key month in the context key response is equal to 11
        And verify the order of the rank key is numerical
