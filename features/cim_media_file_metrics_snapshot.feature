#BDD steps for the Common Impact Metrics (CIM) Media file metrics snapshot
@aqs_tests @aqs_tests.common_impact_metrics @aqs_tests.cim_media_file_metrics_snapshot

Feature: Tests for Common Impact Metrics : Media file metrics snapshot
    # Business Need: Positive Scenarios

    # These are tests for Media file metrics snapshot


   Scenario Outline: Be able to run the CIM Media file metrics snapshot endpoint with varying media file
        Given request is made to the Media file metrics snapshot endpoint with varying media file <media_file>, 20231101 and 20240601
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that response body with context key is of type dict
        And verify that the sub_key media-file in the context key response is equal to <media_file>
        And verify that the sub_key start in the context key response is equal to 2023110100
        And verify that the sub_key end in the context key response is equal to 2024060100
        Examples:
            | media_file |
            | !Nara.jpg |
            | !Sopot_Lipowa_9_entrance.jpg |
            | !!_Auberge_de_Castille_at_sunrise_!!.jpg |
            | !-2009-lgin-palac-abri.jpg         |
            | !-2011-debowa-leka-palac-abri.jpg |

   Scenario: Be able to run the CIM Media file metrics snapshot endpoint with invalid Route
        Given request is made to the Media file metrics snapshot endpoint with invalid route
        Then the request should be unsuccessful with status code of 404
        And verify that the content-type is application/problem+json
        And verify that title key contains Not Found
        And verify that the detail in response is equal to invalid route
        And verify that the method in response is equal to get


   Scenario: Be able to run the CIM Media file metrics snapshot endpoint with invalid file
        Given request is made to the Media file metrics snapshot endpoint with invalid file
        Then the request should be unsuccessful with status code of 404
        And verify that the content-type is application/problem+json
        And verify that title key contains Not Found
        And verify that the method in response is equal to get
        And verify that the detail in response contains media file you asked for is not loaded yet


   Scenario: Be able to run the CIM Media file metrics snapshot endpoint with invalid start time
        Given request is made to the Media file metrics snapshot endpoint with invalid start time
        Then the request should be unsuccessful with status code of 400
        And verify that the content-type is application/problem+json
        And verify that title key contains Bad Request
        And verify that the detail in response contains start timestamp is invalid, must be a valid date in YYYYMMDD format


  Scenario: Be able to run the CIM Media file metrics snapshot endpoint with invalid end time
        Given request is made to the Media file metrics snapshot endpoint with invalid end time
        Then the request should be unsuccessful with status code of 400
        And verify that the content-type is application/problem+json
        And verify that title key contains Bad Request
        And verify that the detail in response contains end timestamp is invalid, must be a valid date in YYYYMMDD format

# DATA Test
  Scenario: Be able to run the CIM Media file metrics snapshot endpoint to validate category rank and pageview count
        Given request is made to the Media file metrics snapshot endpoint to validate timestamp and edit count
        Then the request should be unsuccessful with status code of 200
        And verify that the sub_key media-file in the context key response is equal to !-2009-lgin-palac-abri.jpg
        And verify the value of the timestamp key in the items object first json is equal to text 2023110100
        And verify the value of the leveraging-wiki-count key in the first json is equal to 2
        And verify the value of the leveraging-page-count key in the first json is equal to 2
        And verify the value of the timestamp key in the items object last json is equal to text 2024050100
        And verify the value of the leveraging-wiki-count key in the last json is equal to 3
        And verify the value of the leveraging-page-count key in the last json is equal to 3