#BDD test steps for the edit data aggregate api scenarios
@aqs_tests @aqs_tests.edit_analytics @aqs_tests.edit_analytics_data_aggregate

Feature: Tests for Edit Data Aggregate endpoint

    # Business Need: Positive Scenarios


    # This is to test for Edit data aggregate endpoint with en.wikipedia, diff editor types, all-page-types, daily, 20200101 and 20201231
    Scenario Outline: Be able to run the Edit data aggregate endpoint with en.wikipedia, editor types, all-page-types, daily, 20200101 and 20201231
        Given request is made to the edit data aggregate endpoint with en.wikipedia, <editor_type>, all-page-types, daily, 20200101 and 20201231
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that items key contains project in list of arrays
        And verify that items key contains editor-type in list of arrays
        And verify that items key contains page-type in list of arrays
        And verify that items key contains granularity in list of arrays
        And verify that items key with project in is equal to en.wikipedia
        And verify that items key with page-type in is equal to all-page-types
        And verify that items key with granularity in is equal to daily
        And verify that items key with editor-type in is equal to <editor_type>
        And verify that items key with sub_key results sub_key contains edits of type int
        And verify that items key with sub_key results sub_key contains timestamp of type string

        Examples:
            | editor_type |
            | all-editor-types |
            | anonymous |
            | group-bot |
            | name-bot |
            | user |

        # This is to test for Edit data aggregate endpoint with en.wikipedia, all-editor-types, diff page types, daily, 20200101 and 20201231
    Scenario Outline: Be able to run the Edit data aggregate endpoint with en.wikipedia, all-editor-types, diff page-types, daily, 20200101 & 20201231
        Given request is made to the edit data aggregate endpoint using en.wikipedia, all_editor_types, <page_type>, daily, 20200101 & 20201231
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that items key contains project in list of arrays
        And verify that items key contains editor-type in list of arrays
        And verify that items key contains page-type in list of arrays
        And verify that items key contains granularity in list of arrays
        And verify that items key with project in is equal to en.wikipedia
        And verify that items key with page-type in is equal to <page_type>
        And verify that items key with granularity in is equal to daily
        And verify that items key with editor-type in is equal to all-editor-types
        And verify that items key with sub_key results sub_key contains edits of type int
        And verify that items key with sub_key results sub_key contains timestamp of type string

        Examples:
            | page_type |
            | all-page-types |
            | content |
            | non-content |


        # This is to test for Edit data aggregate endpoint with en.wikipedia, all-editor-types, all-editor-types, daily, 20200101 and 20201231
    Scenario Outline: Be able to run the Edit data aggregate endpoint with en.wikipedia, all-editor-types, diff page-types, daily, 20200101 and 20201231
        Given request is made to the edit data aggregate endpoint with en.wikipedia, all_editor_types, all-page-types, using diff. <granularity>, 20200101 and 20201231
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that items key contains project in list of arrays
        And verify that items key contains editor-type in list of arrays
        And verify that items key contains page-type in list of arrays
        And verify that items key contains granularity in list of arrays
        And verify that items key with project in is equal to en.wikipedia
        And verify that items key with page-type in is equal to all-page-types
        And verify that items key with granularity in is equal to <granularity>
        And verify that items key with editor-type in is equal to all-editor-types
        And verify that items key with sub_key results sub_key contains edits of type int
        And verify that items key with sub_key results sub_key contains timestamp of type string

        Examples:
            | granularity |
            | daily |
            | monthly |

        # This is to test for Edit data aggregate endpoint with same dates entered with daily granularity
    Scenario: Be able to run the Edit data aggregate endpoint with en.wikipedia, all-editor-types, all-page-types, daily, 20200101 and 20201231
        Given request is made to the edit data aggregate endpoint using en.wikipedia, all_editor_types, all-page-types, daily, 20200101 and 20201231
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8
        And verify that items key with project in is equal to en.wikipedia
        And verify that items key with page-type in is equal to all-page-types
        And verify that items key with editor-type in is equal to all-editor-types
        And verify that items key with granularity in is equal to daily
        And verify that items key with sub_key results is of type list

        # This is to test for Edit data aggregate endpoint with same dates entered with Monthly granularity
    Scenario: Be able to run the Edit data aggregate endpoint with en.wikipedia, all-editor-types, all-page-types, monthly, 20200101 and 20200101
        Given request is made to the edit data aggregate endpoint using en.wikipedia, all_editor_types, all-page-types, monthly, 20200101 and 20200101
        Then the request should be successful with status code of 400
        And verify that method key contains get
        And verify that detail key from the error response displays full month warning

        # This is to test for Edit data aggregate endpoint with end date before start date, daily granularity
    Scenario: Be able to run the Edit data aggregate endpoint with en.wikipedia, all-editor-types, all-page-types, daily, 20201231 and 20200101
        Given request is made to the edit data aggregate endpoint using en.wikipedia, all_editor_types, all-page-types, daily, 20201231 and 20200101
        Then the request should be successful with status code of 400
        And verify that method key contains get
        And verify that detail key from the error response displays start timestamp after end timestamp error

        # This is to test for Edits data aggregate endpoint with end date before start date, monthly granularity
    Scenario: Be able to run the Edit data aggregate endpoint with en.wikipedia, all-editor-types, all-page-types, monthly, 20201231 and 20200101
        Given request is made to the edit data aggregate endpoint using en.wikipedia, all_editor_types, all-page-types, monthly, 20201231 and 20200101
        Then the request should be successful with status code of 400
        And verify that method key contains get
        And verify that detail key from the error response displays start timestamp after end timestamp and full month errors

        # This is to test for Edits data aggregate endpoint with an invalid project
    Scenario: Be able to run the Edit data aggregate endpoint with invalideditsdataaggregate.org, all-editor-types, all-page-types, daily, 20200101 and 20201231
        Given request is made to the edit data aggregate endpoint with invalideditsdataaggregate.org, all_editor_types, all-page-types, daily, 20200101 and 20201231
        Then the request should be successful with status code of 404
        And verify that the content-type is application/problem+json
        And verify that method key contains get


        # This is to test for Edits data aggregate endpoint with an invalid editor type
    Scenario: Be able to run the Edit data aggregate endpoint with en.wikipedia, editor_type_invalid, all-page-types, daily, 20200101 and 20201231
        Given request is made to the edit data aggregate endpoint with en.wikipedia, using an invalid editor type editor_type_invalid
        Then the request should be successful with status code of 400
        And verify that method key contains get
        And verify that title key contains Bad Request
        And verify that detail key from the error response displays editor type errors

        # This is to test for Edits data aggregate endpoint with an invalid page type
    Scenario: Be able to run the Edit data aggregate endpoint with en.wikipedia, all-editor-types, page_type_invalid, daily, 20200101 and 20201231
        Given request is made to the edit data aggregate endpoint with en.wikipedia, using an invalid page type page_type_invalid
        Then the request should be successful with status code of 400
        And verify that method key contains get
        And verify that title key contains Bad Request
        And verify that detail key from the error response displays page type errors

        # This is to test for Edits data aggregate endpoint with an invalid granularity type
    Scenario: Be able to run the Edit data aggregate endpoint with en.wikipedia, all-editor-types, all-page-types, invalid_granularity, 20200101 and 20201231
        Given request is made to the edit data aggregate endpoint with en.wikipedia, using an invalid granularity type granularity_invalid
        Then the request should be successful with status code of 400
        And verify that method key contains get
        And verify that title key contains Bad Request
        And verify that detail key from the error response displays granularity type errors

        # This is to test for Edits data aggregate endpoint with a non leap year date with feb 29
    Scenario: Be able to run the Edit data aggregate endpoint with en.wikipedia, all-editor-types, all-page-types, daily, 20210229 and 20210229
        Given request is made to the edit data aggregate endpoint with en.wikipedia, using a non leap year date
        Then the request should be successful with status code of 400
        And verify that method key contains get
        And verify that detail key from the error response displays start timestamp and end timestamp errors

        # This is to test for Edits data aggregate endpoint with a leap year date with feb 29
    Scenario: Be able to run the Edit data aggregate endpoint with en.wikipedia, all-editor-types, all-page-types, daily, 20200229 and 20200229
        Given request is made to the edit data aggregate endpoint with en.wikipedia, using a leap year date
        Then the request should be successful with status code of 200
        And verify that the content-type is application/json; charset=utf-8