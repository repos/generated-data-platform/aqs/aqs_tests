# resources for all services

# API spec json for services
editor_analytics_schema_resource = "/metrics/editors"
media_analytics_schema_resource = "/metrics/mediarequests"
page_analytics_schema_resource = "/metrics/pageviews"
edit_analytics_schema_resource = "/metrics/edits"
geo_analytics_schema_resource = "/metrics/editors/by-country"
device_analytics_schema_resource = "/metrics/unique-devices"
cim_schema_resource = "/metrics/commons-analytics"

# page_analytics
page_analytics_per_article_resource = "/metrics/pageviews/per-article"
page_analytics_top = "/metrics/pageviews/top"
page_analytics_top_by_country = "/metrics/pageviews/top-by-country"
page_analytics_top_per_country = "/metrics/pageviews/top-per-country"
page_analytics_top_invalid_route = "/metrics/pageviews_invalid_route"
page_analytics_aggregate = "/metrics/pageviews/aggregate"

# Device Analytics
device_analytics_resource = "/metrics/unique-devices"

# Editor Analytics
editor_analytics_data_aggregate = "/metrics/editors/aggregate"
editor_analytics_data_top_by_edits = "/metrics/editors/top-by-edits"
editor_analytics_data_top_by_absolute_bytes_difference = "/metrics/editors/top-by-absolute-bytes-difference"
editor_analytics_data_top_by_net_bytes_difference = "/metrics/editors/top-by-net-bytes-difference"
editor_analytics_data_registered_users = "/metrics/registered-users/new"

# Geo Analytics
geo_analytics = "/metrics/editors/by-country"

# Edit Analytics
edit_data_aggregate = "/metrics/edits/aggregate"
edit_data_per_page = "/metrics/edits/per-page"
edit_bytes_diff_net_aggregate = "/metrics/bytes-difference/net/aggregate"
edit_bytes_diff_per_page = "/metrics/bytes-difference/net/per-page"
edits_bytes_diff_absolute_aggregate = "/metrics/bytes-difference/absolute/aggregate"
edit_bytes_diff_absolute_per_page = "/metrics/bytes-difference/absolute/per-page"
edit_edited_pages_new = "/metrics/edited-pages/new"
edit_edited_pages_aggregate = "/metrics/edited-pages/aggregate"
edit_edited_pages_top_by_edits = "/metrics/edited-pages/top-by-edits"
edit_edited_pages_top_by_net_byte_difference = "/metrics/edited-pages/top-by-net-bytes-difference"
edit_edited_pages_top_by_absolute_bytes_difference = "/metrics/edited-pages/top-by-absolute-bytes-difference"


# Media analytics
media_analytics_aggregate = "/metrics/mediarequests/aggregate"
media_analytics_top = "/metrics/mediarequests/top"
media_analytics_per_file = "/metrics/mediarequests/per-file"

# Page analytics Legacy
page_analytics_legacy_resource = "/metrics/legacy/pagecounts/aggregate"

# Common Impact Metrics (CIM)
cim_category_metrics_snapshot_resource = "/metrics/commons-analytics/category-metrics-snapshot"
cim_category_metrics_snapshot_resource_invalid = "/metrics/commons-analytics/category-metrics-snapshot_invalid"
cim_top_editors_monthly_resource = "/metrics/commons-analytics/top-editors-monthly"
cim_top_edited_categories_resource = "/metrics/commons-analytics/top-edited-categories-monthly"
cim_top_viewed_media_files_monthly_resource = "/metrics/commons-analytics/top-viewed-media-files-monthly"
cim_top_wikis_per_media_file_monthly_resource = "/metrics/commons-analytics/top-wikis-per-media-file-monthly"
cim_top_pages_per_media_file_monthly_resource = "/metrics/commons-analytics/top-pages-per-media-file-monthly"
cim_top_viewed_categories_monthly_resource = "/metrics/commons-analytics/top-viewed-categories-monthly"
cim_top_wikis_per_category_monthly_resource = "/metrics/commons-analytics/top-wikis-per-category-monthly"
cim_top_pages_per_category_monthly_resource = "/metrics/commons-analytics/top-pages-per-category-monthly"
cim_edits_per_user_monthly_resource = "/metrics/commons-analytics/edits-per-user-monthly"
cim_edits_per_category_monthly_resource = "/metrics/commons-analytics/edits-per-category-monthly"
cim_pageviews_per_media_file_monthly_resource = "/metrics/commons-analytics/pageviews-per-media-file-monthly"
cim_pageviews_per_category_monthly_resource = "/metrics/commons-analytics/pageviews-per-category-monthly"
cim_media_file_metrics_snapshot_resource = "/metrics/commons-analytics/media-file-metrics-snapshot"
