schema_device_analytics = {
  "title": "Generated schema for Root",
  "type": "object",
  "properties": {
    "schemes": {
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "swagger": {
      "type": "string"
    },
    "info": {
      "type": "object",
      "properties": {
        "description": {
          "type": "string"
        },
        "title": {
          "type": "string"
        },
        "termsOfService": {
          "type": "string"
        },
        "contact": {
          "type": "object",
          "properties": {},
          "required": []
        },
        "version": {
          "type": "string"
        }
      },
      "required": [
        "description",
        "title",
        "termsOfService",
        "contact",
        "version"
      ]
    },
    "host": {
      "type": "string"
    },
    "basePath": {
      "type": "string"
    },
    "paths": {
      "type": "object",
      "properties": {
        "/unique-devices/{project}/{access-site}/{granularity}/{start}/{end}": {
          "type": "object",
          "properties": {
            "get": {
              "type": "object",
              "properties": {
                "description": {
                  "type": "string"
                },
                "produces": {
                  "type": "array",
                  "items": {
                    "type": "string"
                  }
                },
                "summary": {
                  "type": "string"
                },
                "parameters": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "type": {
                        "type": "string"
                      },
                      "example": {
                        "type": "string"
                      },
                      "description": {
                        "type": "string"
                      },
                      "name": {
                        "type": "string"
                      },
                      "in": {
                        "type": "string"
                      },
                      "required": {
                        "type": "boolean"
                      },
                      "enum": {
                        "type": "array",
                        "items": {
                          "type": "string"
                        }
                      }
                    },
                    "required": [
                      "type",
                      "example",
                      "description",
                      "name",
                      "in",
                      "required"
                    ]
                  }
                },
                "responses": {
                  "type": "object",
                  "properties": {
                    "200": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "$ref": {
                              "type": "string"
                            }
                          },
                          "required": [
                            "$ref"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "400": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "type": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "type",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "404": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "type": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "type",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "500": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "type": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "type",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    }
                  },
                  "required": [
                    "200",
                    "400",
                    "404",
                    "500"
                  ]
                }
              },
              "required": [
                "description",
                "produces",
                "summary",
                "parameters",
                "responses"
              ]
            }
          },
          "required": [
            "get"
          ]
        }
      },
      "required": [
        "/unique-devices/{project}/{access-site}/{granularity}/{start}/{end}"
      ]
    },
    "definitions": {
      "type": "object",
      "properties": {
        "entities.UniqueDevices": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "access-site": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "devices": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "granularity": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "offset": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "project": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "timestamp": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "underestimate": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                }
              },
              "required": [
                "access-site",
                "devices",
                "granularity",
                "offset",
                "project",
                "timestamp",
                "underestimate"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.UniqueDevicesResponse": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "items": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "items": {
                      "type": "object",
                      "properties": {
                        "$ref": {
                          "type": "string"
                        }
                      },
                      "required": [
                        "$ref"
                      ]
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "items"
                  ]
                }
              },
              "required": [
                "items"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        }
      },
      "required": [
        "entities.UniqueDevices",
        "entities.UniqueDevicesResponse"
      ]
    }
  },
  "required": [
    "schemes",
    "swagger",
    "info",
    "host",
    "basePath",
    "paths",
    "definitions"
  ]
}