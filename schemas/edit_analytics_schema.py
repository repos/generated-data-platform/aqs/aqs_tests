schema_edit_analytics = {
  "title": "Generated schema for Root",
  "type": "object",
  "properties": {
    "schemes": {
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "swagger": {
      "type": "string"
    },
    "info": {
      "type": "object",
      "properties": {
        "description": {
          "type": "string"
        },
        "title": {
          "type": "string"
        },
        "termsOfService": {
          "type": "string"
        },
        "contact": {
          "type": "object",
          "properties": {},
          "required": []
        },
        "version": {
          "type": "string"
        }
      },
      "required": [
        "description",
        "title",
        "termsOfService",
        "contact",
        "version"
      ]
    },
    "host": {
      "type": "string"
    },
    "basePath": {
      "type": "string"
    },
    "paths": {
      "type": "object",
      "properties": {
        "/bytes-difference/absolute/aggregate/{project}/{editor-type}/{page-type}/{granularity}/{start}/{end}": {
          "type": "object",
          "properties": {
            "get": {
              "type": "object",
              "properties": {
                "description": {
                  "type": "string"
                },
                "produces": {
                  "type": "array",
                  "items": {
                    "type": "string"
                  }
                },
                "summary": {
                  "type": "string"
                },
                "parameters": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "type": {
                        "type": "string"
                      },
                      "example": {
                        "type": "string"
                      },
                      "description": {
                        "type": "string"
                      },
                      "name": {
                        "type": "string"
                      },
                      "in": {
                        "type": "string"
                      },
                      "required": {
                        "type": "boolean"
                      },
                      "enum": {
                        "type": "array",
                        "items": {
                          "type": "string"
                        }
                      }
                    },
                    "required": [
                      "type",
                      "example",
                      "description",
                      "name",
                      "in",
                      "required"
                    ]
                  }
                },
                "responses": {
                  "type": "object",
                  "properties": {
                    "200": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "$ref": {
                              "type": "string"
                            }
                          },
                          "required": [
                            "$ref"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "400": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "type": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "type",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "404": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "type": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "type",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "500": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "type": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "type",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    }
                  },
                  "required": [
                    "200",
                    "400",
                    "404",
                    "500"
                  ]
                }
              },
              "required": [
                "description",
                "produces",
                "summary",
                "parameters",
                "responses"
              ]
            }
          },
          "required": [
            "get"
          ]
        },
        "/bytes-difference/absolute/per-page/{project}/{page-title}/{editor-type}/{granularity}/{start}/{end}": {
          "type": "object",
          "properties": {
            "get": {
              "type": "object",
              "properties": {
                "description": {
                  "type": "string"
                },
                "produces": {
                  "type": "array",
                  "items": {
                    "type": "string"
                  }
                },
                "summary": {
                  "type": "string"
                },
                "parameters": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "type": {
                        "type": "string"
                      },
                      "example": {
                        "type": "string"
                      },
                      "description": {
                        "type": "string"
                      },
                      "name": {
                        "type": "string"
                      },
                      "in": {
                        "type": "string"
                      },
                      "required": {
                        "type": "boolean"
                      },
                      "enum": {
                        "type": "array",
                        "items": {
                          "type": "string"
                        }
                      }
                    },
                    "required": [
                      "type",
                      "example",
                      "description",
                      "name",
                      "in",
                      "required"
                    ]
                  }
                },
                "responses": {
                  "type": "object",
                  "properties": {
                    "200": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "$ref": {
                              "type": "string"
                            }
                          },
                          "required": [
                            "$ref"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "400": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "type": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "type",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "404": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "type": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "type",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "500": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "type": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "type",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    }
                  },
                  "required": [
                    "200",
                    "400",
                    "404",
                    "500"
                  ]
                }
              },
              "required": [
                "description",
                "produces",
                "summary",
                "parameters",
                "responses"
              ]
            }
          },
          "required": [
            "get"
          ]
        },
        "/bytes-difference/net/aggregate/{project}/{editor-type}/{page-type}/{granularity}/{start}/{end}": {
          "type": "object",
          "properties": {
            "get": {
              "type": "object",
              "properties": {
                "description": {
                  "type": "string"
                },
                "produces": {
                  "type": "array",
                  "items": {
                    "type": "string"
                  }
                },
                "summary": {
                  "type": "string"
                },
                "parameters": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "type": {
                        "type": "string"
                      },
                      "example": {
                        "type": "string"
                      },
                      "description": {
                        "type": "string"
                      },
                      "name": {
                        "type": "string"
                      },
                      "in": {
                        "type": "string"
                      },
                      "required": {
                        "type": "boolean"
                      },
                      "enum": {
                        "type": "array",
                        "items": {
                          "type": "string"
                        }
                      }
                    },
                    "required": [
                      "type",
                      "example",
                      "description",
                      "name",
                      "in",
                      "required"
                    ]
                  }
                },
                "responses": {
                  "type": "object",
                  "properties": {
                    "200": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "$ref": {
                              "type": "string"
                            }
                          },
                          "required": [
                            "$ref"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "400": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "type": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "type",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "404": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "type": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "type",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "500": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "type": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "type",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    }
                  },
                  "required": [
                    "200",
                    "400",
                    "404",
                    "500"
                  ]
                }
              },
              "required": [
                "description",
                "produces",
                "summary",
                "parameters",
                "responses"
              ]
            }
          },
          "required": [
            "get"
          ]
        },
        "/bytes-difference/net/per-page/{project}/{page-title}/{editor-type}/{granularity}/{start}/{end}": {
          "type": "object",
          "properties": {
            "get": {
              "type": "object",
              "properties": {
                "description": {
                  "type": "string"
                },
                "produces": {
                  "type": "array",
                  "items": {
                    "type": "string"
                  }
                },
                "summary": {
                  "type": "string"
                },
                "parameters": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "type": {
                        "type": "string"
                      },
                      "example": {
                        "type": "string"
                      },
                      "description": {
                        "type": "string"
                      },
                      "name": {
                        "type": "string"
                      },
                      "in": {
                        "type": "string"
                      },
                      "required": {
                        "type": "boolean"
                      },
                      "enum": {
                        "type": "array",
                        "items": {
                          "type": "string"
                        }
                      }
                    },
                    "required": [
                      "type",
                      "example",
                      "description",
                      "name",
                      "in",
                      "required"
                    ]
                  }
                },
                "responses": {
                  "type": "object",
                  "properties": {
                    "200": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "$ref": {
                              "type": "string"
                            }
                          },
                          "required": [
                            "$ref"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "400": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "type": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "type",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "404": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "type": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "type",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "500": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "type": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "type",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    }
                  },
                  "required": [
                    "200",
                    "400",
                    "404",
                    "500"
                  ]
                }
              },
              "required": [
                "description",
                "produces",
                "summary",
                "parameters",
                "responses"
              ]
            }
          },
          "required": [
            "get"
          ]
        },
        "/edited-pages/aggregate/{project}/{editor-type}/{page-type}/{activity-level}/{granularity}/{start}/{end}": {
          "type": "object",
          "properties": {
            "get": {
              "type": "object",
              "properties": {
                "description": {
                  "type": "string"
                },
                "produces": {
                  "type": "array",
                  "items": {
                    "type": "string"
                  }
                },
                "summary": {
                  "type": "string"
                },
                "parameters": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "type": {
                        "type": "string"
                      },
                      "example": {
                        "type": "string"
                      },
                      "description": {
                        "type": "string"
                      },
                      "name": {
                        "type": "string"
                      },
                      "in": {
                        "type": "string"
                      },
                      "required": {
                        "type": "boolean"
                      },
                      "enum": {
                        "type": "array",
                        "items": {
                          "type": "string"
                        }
                      }
                    },
                    "required": [
                      "type",
                      "example",
                      "description",
                      "name",
                      "in",
                      "required"
                    ]
                  }
                },
                "responses": {
                  "type": "object",
                  "properties": {
                    "200": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "$ref": {
                              "type": "string"
                            }
                          },
                          "required": [
                            "$ref"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "400": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "type": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "type",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "404": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "type": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "type",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "500": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "type": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "type",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    }
                  },
                  "required": [
                    "200",
                    "400",
                    "404",
                    "500"
                  ]
                }
              },
              "required": [
                "description",
                "produces",
                "summary",
                "parameters",
                "responses"
              ]
            }
          },
          "required": [
            "get"
          ]
        },
        "/edited-pages/new/{project}/{editor-type}/{page-type}/{granularity}/{start}/{end}": {
          "type": "object",
          "properties": {
            "get": {
              "type": "object",
              "properties": {
                "description": {
                  "type": "string"
                },
                "produces": {
                  "type": "array",
                  "items": {
                    "type": "string"
                  }
                },
                "summary": {
                  "type": "string"
                },
                "parameters": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "type": {
                        "type": "string"
                      },
                      "example": {
                        "type": "string"
                      },
                      "description": {
                        "type": "string"
                      },
                      "name": {
                        "type": "string"
                      },
                      "in": {
                        "type": "string"
                      },
                      "required": {
                        "type": "boolean"
                      },
                      "enum": {
                        "type": "array",
                        "items": {
                          "type": "string"
                        }
                      }
                    },
                    "required": [
                      "type",
                      "example",
                      "description",
                      "name",
                      "in",
                      "required"
                    ]
                  }
                },
                "responses": {
                  "type": "object",
                  "properties": {
                    "200": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "$ref": {
                              "type": "string"
                            }
                          },
                          "required": [
                            "$ref"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "400": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "type": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "type",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "404": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "type": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "type",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "500": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "type": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "type",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    }
                  },
                  "required": [
                    "200",
                    "400",
                    "404",
                    "500"
                  ]
                }
              },
              "required": [
                "description",
                "produces",
                "summary",
                "parameters",
                "responses"
              ]
            }
          },
          "required": [
            "get"
          ]
        },
        "/edited-pages/top-by-absolute-bytes-difference/{project}/{editor-type}/{page-type}/{year}/{month}/{day}": {
          "type": "object",
          "properties": {
            "get": {
              "type": "object",
              "properties": {
                "description": {
                  "type": "string"
                },
                "produces": {
                  "type": "array",
                  "items": {
                    "type": "string"
                  }
                },
                "summary": {
                  "type": "string"
                },
                "parameters": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "type": {
                        "type": "string"
                      },
                      "example": {
                        "type": "string"
                      },
                      "description": {
                        "type": "string"
                      },
                      "name": {
                        "type": "string"
                      },
                      "in": {
                        "type": "string"
                      },
                      "required": {
                        "type": "boolean"
                      },
                      "enum": {
                        "type": "array",
                        "items": {
                          "type": "string"
                        }
                      }
                    },
                    "required": [
                      "type",
                      "example",
                      "description",
                      "name",
                      "in",
                      "required"
                    ]
                  }
                },
                "responses": {
                  "type": "object",
                  "properties": {
                    "200": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "$ref": {
                              "type": "string"
                            }
                          },
                          "required": [
                            "$ref"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "400": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "type": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "type",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "404": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "type": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "type",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "500": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "type": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "type",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    }
                  },
                  "required": [
                    "200",
                    "400",
                    "404",
                    "500"
                  ]
                }
              },
              "required": [
                "description",
                "produces",
                "summary",
                "parameters",
                "responses"
              ]
            }
          },
          "required": [
            "get"
          ]
        },
        "/edited-pages/top-by-edits/{project}/{editor-type}/{page-type}/{year}/{month}/{day}": {
          "type": "object",
          "properties": {
            "get": {
              "type": "object",
              "properties": {
                "description": {
                  "type": "string"
                },
                "produces": {
                  "type": "array",
                  "items": {
                    "type": "string"
                  }
                },
                "summary": {
                  "type": "string"
                },
                "parameters": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "type": {
                        "type": "string"
                      },
                      "example": {
                        "type": "string"
                      },
                      "description": {
                        "type": "string"
                      },
                      "name": {
                        "type": "string"
                      },
                      "in": {
                        "type": "string"
                      },
                      "required": {
                        "type": "boolean"
                      },
                      "enum": {
                        "type": "array",
                        "items": {
                          "type": "string"
                        }
                      }
                    },
                    "required": [
                      "type",
                      "example",
                      "description",
                      "name",
                      "in",
                      "required"
                    ]
                  }
                },
                "responses": {
                  "type": "object",
                  "properties": {
                    "200": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "$ref": {
                              "type": "string"
                            }
                          },
                          "required": [
                            "$ref"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "400": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "type": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "type",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "404": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "type": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "type",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "500": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "type": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "type",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    }
                  },
                  "required": [
                    "200",
                    "400",
                    "404",
                    "500"
                  ]
                }
              },
              "required": [
                "description",
                "produces",
                "summary",
                "parameters",
                "responses"
              ]
            }
          },
          "required": [
            "get"
          ]
        },
        "/edited-pages/top-by-net-bytes-difference/{project}/{editor-type}/{page-type}/{year}/{month}/{day}": {
          "type": "object",
          "properties": {
            "get": {
              "type": "object",
              "properties": {
                "description": {
                  "type": "string"
                },
                "produces": {
                  "type": "array",
                  "items": {
                    "type": "string"
                  }
                },
                "summary": {
                  "type": "string"
                },
                "parameters": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "type": {
                        "type": "string"
                      },
                      "example": {
                        "type": "string"
                      },
                      "description": {
                        "type": "string"
                      },
                      "name": {
                        "type": "string"
                      },
                      "in": {
                        "type": "string"
                      },
                      "required": {
                        "type": "boolean"
                      },
                      "enum": {
                        "type": "array",
                        "items": {
                          "type": "string"
                        }
                      }
                    },
                    "required": [
                      "type",
                      "example",
                      "description",
                      "name",
                      "in",
                      "required"
                    ]
                  }
                },
                "responses": {
                  "type": "object",
                  "properties": {
                    "200": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "$ref": {
                              "type": "string"
                            }
                          },
                          "required": [
                            "$ref"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "400": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "type": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "type",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "404": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "type": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "type",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "500": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "type": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "type",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    }
                  },
                  "required": [
                    "200",
                    "400",
                    "404",
                    "500"
                  ]
                }
              },
              "required": [
                "description",
                "produces",
                "summary",
                "parameters",
                "responses"
              ]
            }
          },
          "required": [
            "get"
          ]
        },
        "/edits/aggregate/{project}/{editor-type}/{page-type}/{granularity}/{start}/{end}": {
          "type": "object",
          "properties": {
            "get": {
              "type": "object",
              "properties": {
                "description": {
                  "type": "string"
                },
                "produces": {
                  "type": "array",
                  "items": {
                    "type": "string"
                  }
                },
                "summary": {
                  "type": "string"
                },
                "parameters": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "type": {
                        "type": "string"
                      },
                      "example": {
                        "type": "string"
                      },
                      "description": {
                        "type": "string"
                      },
                      "name": {
                        "type": "string"
                      },
                      "in": {
                        "type": "string"
                      },
                      "required": {
                        "type": "boolean"
                      },
                      "enum": {
                        "type": "array",
                        "items": {
                          "type": "string"
                        }
                      }
                    },
                    "required": [
                      "type",
                      "example",
                      "description",
                      "name",
                      "in",
                      "required"
                    ]
                  }
                },
                "responses": {
                  "type": "object",
                  "properties": {
                    "200": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "$ref": {
                              "type": "string"
                            }
                          },
                          "required": [
                            "$ref"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "400": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "type": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "type",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "404": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "type": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "type",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "500": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "type": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "type",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    }
                  },
                  "required": [
                    "200",
                    "400",
                    "404",
                    "500"
                  ]
                }
              },
              "required": [
                "description",
                "produces",
                "summary",
                "parameters",
                "responses"
              ]
            }
          },
          "required": [
            "get"
          ]
        },
        "/edits/per-page/{project}/{page-title}/{editor-type}/{granularity}/{start}/{end}": {
          "type": "object",
          "properties": {
            "get": {
              "type": "object",
              "properties": {
                "description": {
                  "type": "string"
                },
                "produces": {
                  "type": "array",
                  "items": {
                    "type": "string"
                  }
                },
                "summary": {
                  "type": "string"
                },
                "parameters": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "type": {
                        "type": "string"
                      },
                      "example": {
                        "type": "string"
                      },
                      "description": {
                        "type": "string"
                      },
                      "name": {
                        "type": "string"
                      },
                      "in": {
                        "type": "string"
                      },
                      "required": {
                        "type": "boolean"
                      },
                      "enum": {
                        "type": "array",
                        "items": {
                          "type": "string"
                        }
                      }
                    },
                    "required": [
                      "type",
                      "example",
                      "description",
                      "name",
                      "in",
                      "required"
                    ]
                  }
                },
                "responses": {
                  "type": "object",
                  "properties": {
                    "200": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "$ref": {
                              "type": "string"
                            }
                          },
                          "required": [
                            "$ref"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "400": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "type": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "type",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "404": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "type": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "type",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "500": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "type": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "type",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    }
                  },
                  "required": [
                    "200",
                    "400",
                    "404",
                    "500"
                  ]
                }
              },
              "required": [
                "description",
                "produces",
                "summary",
                "parameters",
                "responses"
              ]
            }
          },
          "required": [
            "get"
          ]
        }
      },
      "required": [
        "/bytes-difference/absolute/aggregate/{project}/{editor-type}/{page-type}/{granularity}/{start}/{end}",
        "/bytes-difference/absolute/per-page/{project}/{page-title}/{editor-type}/{granularity}/{start}/{end}",
        "/bytes-difference/net/aggregate/{project}/{editor-type}/{page-type}/{granularity}/{start}/{end}",
        "/bytes-difference/net/per-page/{project}/{page-title}/{editor-type}/{granularity}/{start}/{end}",
        "/edited-pages/aggregate/{project}/{editor-type}/{page-type}/{activity-level}/{granularity}/{start}/{end}",
        "/edited-pages/new/{project}/{editor-type}/{page-type}/{granularity}/{start}/{end}",
        "/edited-pages/top-by-absolute-bytes-difference/{project}/{editor-type}/{page-type}/{year}/{month}/{day}",
        "/edited-pages/top-by-edits/{project}/{editor-type}/{page-type}/{year}/{month}/{day}",
        "/edited-pages/top-by-net-bytes-difference/{project}/{editor-type}/{page-type}/{year}/{month}/{day}",
        "/edits/aggregate/{project}/{editor-type}/{page-type}/{granularity}/{start}/{end}",
        "/edits/per-page/{project}/{page-title}/{editor-type}/{granularity}/{start}/{end}"
      ]
    },
    "definitions": {
      "type": "object",
      "properties": {
        "entities.BytesDifferenceAbsoluteAggregate": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "editor-type": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "granularity": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "page-type": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "project": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "results": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "items": {
                      "type": "object",
                      "properties": {
                        "$ref": {
                          "type": "string"
                        }
                      },
                      "required": [
                        "$ref"
                      ]
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "items"
                  ]
                }
              },
              "required": [
                "editor-type",
                "granularity",
                "page-type",
                "project",
                "results"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.BytesDifferenceAbsoluteAggregateResponse": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "items": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "items": {
                      "type": "object",
                      "properties": {
                        "$ref": {
                          "type": "string"
                        }
                      },
                      "required": [
                        "$ref"
                      ]
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "items"
                  ]
                }
              },
              "required": [
                "items"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.BytesDifferenceAbsoluteAggregateResult": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "abs_bytes_diff": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "timestamp": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                }
              },
              "required": [
                "abs_bytes_diff",
                "timestamp"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.BytesDifferenceAbsolutePerPage": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "editor-type": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "granularity": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "page-title": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "project": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "results": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "items": {
                      "type": "object",
                      "properties": {
                        "$ref": {
                          "type": "string"
                        }
                      },
                      "required": [
                        "$ref"
                      ]
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "items"
                  ]
                }
              },
              "required": [
                "editor-type",
                "granularity",
                "page-title",
                "project",
                "results"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.BytesDifferenceAbsolutePerPageResponse": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "items": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "items": {
                      "type": "object",
                      "properties": {
                        "$ref": {
                          "type": "string"
                        }
                      },
                      "required": [
                        "$ref"
                      ]
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "items"
                  ]
                }
              },
              "required": [
                "items"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.BytesDifferenceAbsolutePerPageResult": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "abs_bytes_diff": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "timestamp": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                }
              },
              "required": [
                "abs_bytes_diff",
                "timestamp"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.BytesDifferenceNetAggregate": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "editor-type": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "granularity": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "page-type": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "project": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "results": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "items": {
                      "type": "object",
                      "properties": {
                        "$ref": {
                          "type": "string"
                        }
                      },
                      "required": [
                        "$ref"
                      ]
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "items"
                  ]
                }
              },
              "required": [
                "editor-type",
                "granularity",
                "page-type",
                "project",
                "results"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.BytesDifferenceNetAggregateResponse": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "items": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "items": {
                      "type": "object",
                      "properties": {
                        "$ref": {
                          "type": "string"
                        }
                      },
                      "required": [
                        "$ref"
                      ]
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "items"
                  ]
                }
              },
              "required": [
                "items"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.BytesDifferenceNetAggregateResult": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "net_bytes_diff": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "timestamp": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                }
              },
              "required": [
                "net_bytes_diff",
                "timestamp"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.BytesDifferenceNetPerPage": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "editor-type": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "granularity": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "page-title": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "project": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "results": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "items": {
                      "type": "object",
                      "properties": {
                        "$ref": {
                          "type": "string"
                        }
                      },
                      "required": [
                        "$ref"
                      ]
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "items"
                  ]
                }
              },
              "required": [
                "editor-type",
                "granularity",
                "page-title",
                "project",
                "results"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.BytesDifferenceNetPerPageResponse": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "items": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "items": {
                      "type": "object",
                      "properties": {
                        "$ref": {
                          "type": "string"
                        }
                      },
                      "required": [
                        "$ref"
                      ]
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "items"
                  ]
                }
              },
              "required": [
                "items"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.BytesDifferenceNetPerPageResult": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "net_bytes_diff": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "timestamp": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                }
              },
              "required": [
                "net_bytes_diff",
                "timestamp"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.EditedPagesAggregate": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "activity-level": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "editor-type": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "granularity": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "page-type": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "project": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "results": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "items": {
                      "type": "object",
                      "properties": {
                        "$ref": {
                          "type": "string"
                        }
                      },
                      "required": [
                        "$ref"
                      ]
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "items"
                  ]
                }
              },
              "required": [
                "activity-level",
                "editor-type",
                "granularity",
                "page-type",
                "project",
                "results"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.EditedPagesAggregateResponse": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "items": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "items": {
                      "type": "object",
                      "properties": {
                        "$ref": {
                          "type": "string"
                        }
                      },
                      "required": [
                        "$ref"
                      ]
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "items"
                  ]
                }
              },
              "required": [
                "items"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.EditedPagesAggregateResult": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "edited_pages": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "timestamp": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                }
              },
              "required": [
                "edited_pages",
                "timestamp"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.EditedPagesNew": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "editor-type": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "granularity": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "page-type": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "project": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "results": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "items": {
                      "type": "object",
                      "properties": {
                        "$ref": {
                          "type": "string"
                        }
                      },
                      "required": [
                        "$ref"
                      ]
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "items"
                  ]
                }
              },
              "required": [
                "editor-type",
                "granularity",
                "page-type",
                "project",
                "results"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.EditedPagesNewResponse": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "items": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "items": {
                      "type": "object",
                      "properties": {
                        "$ref": {
                          "type": "string"
                        }
                      },
                      "required": [
                        "$ref"
                      ]
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "items"
                  ]
                }
              },
              "required": [
                "items"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.EditedPagesNewResult": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "new_pages": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "timestamp": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                }
              },
              "required": [
                "new_pages",
                "timestamp"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.EditsAggregate": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "editor-type": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "granularity": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "page-type": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "project": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "results": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "items": {
                      "type": "object",
                      "properties": {
                        "$ref": {
                          "type": "string"
                        }
                      },
                      "required": [
                        "$ref"
                      ]
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "items"
                  ]
                }
              },
              "required": [
                "editor-type",
                "granularity",
                "page-type",
                "project",
                "results"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.EditsAggregateResponse": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "items": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "items": {
                      "type": "object",
                      "properties": {
                        "$ref": {
                          "type": "string"
                        }
                      },
                      "required": [
                        "$ref"
                      ]
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "items"
                  ]
                }
              },
              "required": [
                "items"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.EditsAggregateResult": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "edits": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "timestamp": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                }
              },
              "required": [
                "edits",
                "timestamp"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.EditsPerPage": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "editor-type": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "granularity": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "page-title": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "project": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "results": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "items": {
                      "type": "object",
                      "properties": {
                        "$ref": {
                          "type": "string"
                        }
                      },
                      "required": [
                        "$ref"
                      ]
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "items"
                  ]
                }
              },
              "required": [
                "editor-type",
                "granularity",
                "page-title",
                "project",
                "results"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.EditsPerPageResponse": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "items": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "items": {
                      "type": "object",
                      "properties": {
                        "$ref": {
                          "type": "string"
                        }
                      },
                      "required": [
                        "$ref"
                      ]
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "items"
                  ]
                }
              },
              "required": [
                "items"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.EditsPerPageResult": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "edits": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "timestamp": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                }
              },
              "required": [
                "edits",
                "timestamp"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.TopByAbsoluteBytesDifference": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "editor-type": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "granularity": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "page-type": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "project": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "results": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "items": {
                      "type": "object",
                      "properties": {
                        "$ref": {
                          "type": "string"
                        }
                      },
                      "required": [
                        "$ref"
                      ]
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "items"
                  ]
                }
              },
              "required": [
                "editor-type",
                "granularity",
                "page-type",
                "project",
                "results"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.TopByAbsoluteBytesDifferenceResponse": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "items": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "items": {
                      "type": "object",
                      "properties": {
                        "$ref": {
                          "type": "string"
                        }
                      },
                      "required": [
                        "$ref"
                      ]
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "items"
                  ]
                }
              },
              "required": [
                "items"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.TopByAbsoluteBytesDifferenceResult": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "timestamp": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "top": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "items": {
                      "type": "object",
                      "properties": {
                        "$ref": {
                          "type": "string"
                        }
                      },
                      "required": [
                        "$ref"
                      ]
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "items"
                  ]
                }
              },
              "required": [
                "timestamp",
                "top"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.TopByAbsoluteBytesDifferenceTop": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "abs_bytes_diff": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "page_title": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "rank": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                }
              },
              "required": [
                "abs_bytes_diff",
                "page_title",
                "rank"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.TopByEdits": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "editor-type": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "granularity": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "page-type": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "project": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "results": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "items": {
                      "type": "object",
                      "properties": {
                        "$ref": {
                          "type": "string"
                        }
                      },
                      "required": [
                        "$ref"
                      ]
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "items"
                  ]
                }
              },
              "required": [
                "editor-type",
                "granularity",
                "page-type",
                "project",
                "results"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.TopByEditsResponse": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "items": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "items": {
                      "type": "object",
                      "properties": {
                        "$ref": {
                          "type": "string"
                        }
                      },
                      "required": [
                        "$ref"
                      ]
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "items"
                  ]
                }
              },
              "required": [
                "items"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.TopByEditsResult": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "timestamp": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "top": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "items": {
                      "type": "object",
                      "properties": {
                        "$ref": {
                          "type": "string"
                        }
                      },
                      "required": [
                        "$ref"
                      ]
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "items"
                  ]
                }
              },
              "required": [
                "timestamp",
                "top"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.TopByEditsTop": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "edits": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "page_title": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "rank": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                }
              },
              "required": [
                "edits",
                "page_title",
                "rank"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.TopByNetBytesDifference": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "editor-type": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "granularity": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "page-type": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "project": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "results": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "items": {
                      "type": "object",
                      "properties": {
                        "$ref": {
                          "type": "string"
                        }
                      },
                      "required": [
                        "$ref"
                      ]
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "items"
                  ]
                }
              },
              "required": [
                "editor-type",
                "granularity",
                "page-type",
                "project",
                "results"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.TopByNetBytesDifferenceResponse": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "items": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "items": {
                      "type": "object",
                      "properties": {
                        "$ref": {
                          "type": "string"
                        }
                      },
                      "required": [
                        "$ref"
                      ]
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "items"
                  ]
                }
              },
              "required": [
                "items"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.TopByNetBytesDifferenceResult": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "timestamp": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "top": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "items": {
                      "type": "object",
                      "properties": {
                        "$ref": {
                          "type": "string"
                        }
                      },
                      "required": [
                        "$ref"
                      ]
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "items"
                  ]
                }
              },
              "required": [
                "timestamp",
                "top"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.TopByNetBytesDifferenceTop": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "net_bytes_diff": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "page_title": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "rank": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                }
              },
              "required": [
                "net_bytes_diff",
                "page_title",
                "rank"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        }
      },
      "required": [
        "entities.BytesDifferenceAbsoluteAggregate",
        "entities.BytesDifferenceAbsoluteAggregateResponse",
        "entities.BytesDifferenceAbsoluteAggregateResult",
        "entities.BytesDifferenceAbsolutePerPage",
        "entities.BytesDifferenceAbsolutePerPageResponse",
        "entities.BytesDifferenceAbsolutePerPageResult",
        "entities.BytesDifferenceNetAggregate",
        "entities.BytesDifferenceNetAggregateResponse",
        "entities.BytesDifferenceNetAggregateResult",
        "entities.BytesDifferenceNetPerPage",
        "entities.BytesDifferenceNetPerPageResponse",
        "entities.BytesDifferenceNetPerPageResult",
        "entities.EditedPagesAggregate",
        "entities.EditedPagesAggregateResponse",
        "entities.EditedPagesAggregateResult",
        "entities.EditedPagesNew",
        "entities.EditedPagesNewResponse",
        "entities.EditedPagesNewResult",
        "entities.EditsAggregate",
        "entities.EditsAggregateResponse",
        "entities.EditsAggregateResult",
        "entities.EditsPerPage",
        "entities.EditsPerPageResponse",
        "entities.EditsPerPageResult",
        "entities.TopByAbsoluteBytesDifference",
        "entities.TopByAbsoluteBytesDifferenceResponse",
        "entities.TopByAbsoluteBytesDifferenceResult",
        "entities.TopByAbsoluteBytesDifferenceTop",
        "entities.TopByEdits",
        "entities.TopByEditsResponse",
        "entities.TopByEditsResult",
        "entities.TopByEditsTop",
        "entities.TopByNetBytesDifference",
        "entities.TopByNetBytesDifferenceResponse",
        "entities.TopByNetBytesDifferenceResult",
        "entities.TopByNetBytesDifferenceTop"
      ]
    }
  },
  "required": [
    "schemes",
    "swagger",
    "info",
    "host",
    "basePath",
    "paths",
    "definitions"
  ]
}