schema_commons_impact_metrics = {
  "title": "Generated schema for Root",
  "type": "object",
  "properties": {
    "schemes": {
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "swagger": {
      "type": "string"
    },
    "info": {
      "type": "object",
      "properties": {
        "description": {
          "type": "string"
        },
        "title": {
          "type": "string"
        },
        "termsOfService": {
          "type": "string"
        },
        "contact": {
          "type": "object",
          "properties": {},
          "required": []
        },
        "version": {
          "type": "string"
        }
      },
      "required": [
        "description",
        "title",
        "termsOfService",
        "contact",
        "version"
      ]
    },
    "host": {
      "type": "string"
    },
    "basePath": {
      "type": "string"
    },
    "paths": {
      "type": "object",
      "properties": {
        "/commons-analytics/category-metrics-snapshot/{category}/{start}/{end}": {
          "type": "object",
          "properties": {
            "get": {
              "type": "object",
              "properties": {
                "description": {
                  "type": "string"
                },
                "produces": {
                  "type": "array",
                  "items": {
                    "type": "string"
                  }
                },
                "summary": {
                  "type": "string"
                },
                "parameters": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "type": {
                        "type": "string"
                      },
                      "example": {
                        "type": "string"
                      },
                      "description": {
                        "type": "string"
                      },
                      "name": {
                        "type": "string"
                      },
                      "in": {
                        "type": "string"
                      },
                      "required": {
                        "type": "boolean"
                      }
                    },
                    "required": [
                      "type",
                      "example",
                      "description",
                      "name",
                      "in",
                      "required"
                    ]
                  }
                },
                "responses": {
                  "type": "object",
                  "properties": {
                    "200": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "$ref": {
                              "type": "string"
                            }
                          },
                          "required": [
                            "$ref"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "400": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "404": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "500": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    }
                  },
                  "required": [
                    "200",
                    "400",
                    "404",
                    "500"
                  ]
                }
              },
              "required": [
                "description",
                "produces",
                "summary",
                "parameters",
                "responses"
              ]
            }
          },
          "required": [
            "get"
          ]
        },
        "/commons-analytics/edits-per-category-monthly/{category}/{category-scope}/{edit-type}/{start}/{end}": {
          "type": "object",
          "properties": {
            "get": {
              "type": "object",
              "properties": {
                "description": {
                  "type": "string"
                },
                "produces": {
                  "type": "array",
                  "items": {
                    "type": "string"
                  }
                },
                "summary": {
                  "type": "string"
                },
                "parameters": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "type": {
                        "type": "string"
                      },
                      "example": {
                        "type": "string"
                      },
                      "description": {
                        "type": "string"
                      },
                      "name": {
                        "type": "string"
                      },
                      "in": {
                        "type": "string"
                      },
                      "required": {
                        "type": "boolean"
                      },
                      "enum": {
                        "type": "array",
                        "items": {
                          "type": "string"
                        }
                      }
                    },
                    "required": [
                      "type",
                      "example",
                      "description",
                      "name",
                      "in",
                      "required"
                    ]
                  }
                },
                "responses": {
                  "type": "object",
                  "properties": {
                    "200": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "$ref": {
                              "type": "string"
                            }
                          },
                          "required": [
                            "$ref"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "400": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "404": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "500": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    }
                  },
                  "required": [
                    "200",
                    "400",
                    "404",
                    "500"
                  ]
                }
              },
              "required": [
                "description",
                "produces",
                "summary",
                "parameters",
                "responses"
              ]
            }
          },
          "required": [
            "get"
          ]
        },
        "/commons-analytics/edits-per-user-monthly/{user-name}/{edit-type}/{start}/{end}": {
          "type": "object",
          "properties": {
            "get": {
              "type": "object",
              "properties": {
                "description": {
                  "type": "string"
                },
                "produces": {
                  "type": "array",
                  "items": {
                    "type": "string"
                  }
                },
                "summary": {
                  "type": "string"
                },
                "parameters": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "type": {
                        "type": "string"
                      },
                      "example": {
                        "type": "string"
                      },
                      "description": {
                        "type": "string"
                      },
                      "name": {
                        "type": "string"
                      },
                      "in": {
                        "type": "string"
                      },
                      "required": {
                        "type": "boolean"
                      },
                      "enum": {
                        "type": "array",
                        "items": {
                          "type": "string"
                        }
                      }
                    },
                    "required": [
                      "type",
                      "example",
                      "description",
                      "name",
                      "in",
                      "required"
                    ]
                  }
                },
                "responses": {
                  "type": "object",
                  "properties": {
                    "200": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "$ref": {
                              "type": "string"
                            }
                          },
                          "required": [
                            "$ref"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "400": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "404": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "500": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    }
                  },
                  "required": [
                    "200",
                    "400",
                    "404",
                    "500"
                  ]
                }
              },
              "required": [
                "description",
                "produces",
                "summary",
                "parameters",
                "responses"
              ]
            }
          },
          "required": [
            "get"
          ]
        },
        "/commons-analytics/media-file-metrics-snapshot/{media-file}/{start}/{end}": {
          "type": "object",
          "properties": {
            "get": {
              "type": "object",
              "properties": {
                "description": {
                  "type": "string"
                },
                "produces": {
                  "type": "array",
                  "items": {
                    "type": "string"
                  }
                },
                "summary": {
                  "type": "string"
                },
                "parameters": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "type": {
                        "type": "string"
                      },
                      "example": {
                        "type": "string"
                      },
                      "description": {
                        "type": "string"
                      },
                      "name": {
                        "type": "string"
                      },
                      "in": {
                        "type": "string"
                      },
                      "required": {
                        "type": "boolean"
                      }
                    },
                    "required": [
                      "type",
                      "example",
                      "description",
                      "name",
                      "in",
                      "required"
                    ]
                  }
                },
                "responses": {
                  "type": "object",
                  "properties": {
                    "200": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "$ref": {
                              "type": "string"
                            }
                          },
                          "required": [
                            "$ref"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "400": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "404": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "500": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    }
                  },
                  "required": [
                    "200",
                    "400",
                    "404",
                    "500"
                  ]
                }
              },
              "required": [
                "description",
                "produces",
                "summary",
                "parameters",
                "responses"
              ]
            }
          },
          "required": [
            "get"
          ]
        },
        "/commons-analytics/pageviews-per-category-monthly/{category}/{category-scope}/{wiki}/{start}/{end}": {
          "type": "object",
          "properties": {
            "get": {
              "type": "object",
              "properties": {
                "description": {
                  "type": "string"
                },
                "produces": {
                  "type": "array",
                  "items": {
                    "type": "string"
                  }
                },
                "summary": {
                  "type": "string"
                },
                "parameters": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "type": {
                        "type": "string"
                      },
                      "example": {
                        "type": "string"
                      },
                      "description": {
                        "type": "string"
                      },
                      "name": {
                        "type": "string"
                      },
                      "in": {
                        "type": "string"
                      },
                      "required": {
                        "type": "boolean"
                      },
                      "enum": {
                        "type": "array",
                        "items": {
                          "type": "string"
                        }
                      }
                    },
                    "required": [
                      "type",
                      "example",
                      "description",
                      "name",
                      "in",
                      "required"
                    ]
                  }
                },
                "responses": {
                  "type": "object",
                  "properties": {
                    "200": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "$ref": {
                              "type": "string"
                            }
                          },
                          "required": [
                            "$ref"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "400": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "404": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "500": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    }
                  },
                  "required": [
                    "200",
                    "400",
                    "404",
                    "500"
                  ]
                }
              },
              "required": [
                "description",
                "produces",
                "summary",
                "parameters",
                "responses"
              ]
            }
          },
          "required": [
            "get"
          ]
        },
        "/commons-analytics/pageviews-per-media-file-monthly/{media-file}/{wiki}/{start}/{end}": {
          "type": "object",
          "properties": {
            "get": {
              "type": "object",
              "properties": {
                "description": {
                  "type": "string"
                },
                "produces": {
                  "type": "array",
                  "items": {
                    "type": "string"
                  }
                },
                "summary": {
                  "type": "string"
                },
                "parameters": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "type": {
                        "type": "string"
                      },
                      "example": {
                        "type": "string"
                      },
                      "description": {
                        "type": "string"
                      },
                      "name": {
                        "type": "string"
                      },
                      "in": {
                        "type": "string"
                      },
                      "required": {
                        "type": "boolean"
                      }
                    },
                    "required": [
                      "type",
                      "example",
                      "description",
                      "name",
                      "in",
                      "required"
                    ]
                  }
                },
                "responses": {
                  "type": "object",
                  "properties": {
                    "200": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "$ref": {
                              "type": "string"
                            }
                          },
                          "required": [
                            "$ref"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "400": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "404": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "500": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    }
                  },
                  "required": [
                    "200",
                    "400",
                    "404",
                    "500"
                  ]
                }
              },
              "required": [
                "description",
                "produces",
                "summary",
                "parameters",
                "responses"
              ]
            }
          },
          "required": [
            "get"
          ]
        },
        "/commons-analytics/top-edited-categories-monthly/{category-scope}/{edit-type}/{year}/{month}": {
          "type": "object",
          "properties": {
            "get": {
              "type": "object",
              "properties": {
                "description": {
                  "type": "string"
                },
                "produces": {
                  "type": "array",
                  "items": {
                    "type": "string"
                  }
                },
                "summary": {
                  "type": "string"
                },
                "parameters": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "enum": {
                        "type": "array",
                        "items": {
                          "type": "string"
                        }
                      },
                      "type": {
                        "type": "string"
                      },
                      "example": {
                        "type": "string"
                      },
                      "description": {
                        "type": "string"
                      },
                      "name": {
                        "type": "string"
                      },
                      "in": {
                        "type": "string"
                      },
                      "required": {
                        "type": "boolean"
                      }
                    },
                    "required": [
                      "type",
                      "example",
                      "description",
                      "name",
                      "in",
                      "required"
                    ]
                  }
                },
                "responses": {
                  "type": "object",
                  "properties": {
                    "200": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "$ref": {
                              "type": "string"
                            }
                          },
                          "required": [
                            "$ref"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "400": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "404": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "500": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    }
                  },
                  "required": [
                    "200",
                    "400",
                    "404",
                    "500"
                  ]
                }
              },
              "required": [
                "description",
                "produces",
                "summary",
                "parameters",
                "responses"
              ]
            }
          },
          "required": [
            "get"
          ]
        },
        "/commons-analytics/top-editors-monthly/{category}/{category-scope}/{edit-type}/{year}/{month}": {
          "type": "object",
          "properties": {
            "get": {
              "type": "object",
              "properties": {
                "description": {
                  "type": "string"
                },
                "produces": {
                  "type": "array",
                  "items": {
                    "type": "string"
                  }
                },
                "summary": {
                  "type": "string"
                },
                "parameters": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "type": {
                        "type": "string"
                      },
                      "example": {
                        "type": "string"
                      },
                      "description": {
                        "type": "string"
                      },
                      "name": {
                        "type": "string"
                      },
                      "in": {
                        "type": "string"
                      },
                      "required": {
                        "type": "boolean"
                      },
                      "enum": {
                        "type": "array",
                        "items": {
                          "type": "string"
                        }
                      }
                    },
                    "required": [
                      "type",
                      "example",
                      "description",
                      "name",
                      "in",
                      "required"
                    ]
                  }
                },
                "responses": {
                  "type": "object",
                  "properties": {
                    "200": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "$ref": {
                              "type": "string"
                            }
                          },
                          "required": [
                            "$ref"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "400": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "404": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "500": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    }
                  },
                  "required": [
                    "200",
                    "400",
                    "404",
                    "500"
                  ]
                }
              },
              "required": [
                "description",
                "produces",
                "summary",
                "parameters",
                "responses"
              ]
            }
          },
          "required": [
            "get"
          ]
        },
        "/commons-analytics/top-pages-per-category-monthly/{category}/{category-scope}/{wiki}/{year}/{month}": {
          "type": "object",
          "properties": {
            "get": {
              "type": "object",
              "properties": {
                "description": {
                  "type": "string"
                },
                "produces": {
                  "type": "array",
                  "items": {
                    "type": "string"
                  }
                },
                "summary": {
                  "type": "string"
                },
                "parameters": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "type": {
                        "type": "string"
                      },
                      "example": {
                        "type": "string"
                      },
                      "description": {
                        "type": "string"
                      },
                      "name": {
                        "type": "string"
                      },
                      "in": {
                        "type": "string"
                      },
                      "required": {
                        "type": "boolean"
                      },
                      "enum": {
                        "type": "array",
                        "items": {
                          "type": "string"
                        }
                      }
                    },
                    "required": [
                      "type",
                      "example",
                      "description",
                      "name",
                      "in",
                      "required"
                    ]
                  }
                },
                "responses": {
                  "type": "object",
                  "properties": {
                    "200": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "$ref": {
                              "type": "string"
                            }
                          },
                          "required": [
                            "$ref"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "400": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "404": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "500": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    }
                  },
                  "required": [
                    "200",
                    "400",
                    "404",
                    "500"
                  ]
                }
              },
              "required": [
                "description",
                "produces",
                "summary",
                "parameters",
                "responses"
              ]
            }
          },
          "required": [
            "get"
          ]
        },
        "/commons-analytics/top-pages-per-media-file-monthly/{media-file}/{wiki}/{year}/{month}": {
          "type": "object",
          "properties": {
            "get": {
              "type": "object",
              "properties": {
                "description": {
                  "type": "string"
                },
                "produces": {
                  "type": "array",
                  "items": {
                    "type": "string"
                  }
                },
                "summary": {
                  "type": "string"
                },
                "parameters": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "type": {
                        "type": "string"
                      },
                      "example": {
                        "type": "string"
                      },
                      "description": {
                        "type": "string"
                      },
                      "name": {
                        "type": "string"
                      },
                      "in": {
                        "type": "string"
                      },
                      "required": {
                        "type": "boolean"
                      }
                    },
                    "required": [
                      "type",
                      "example",
                      "description",
                      "name",
                      "in",
                      "required"
                    ]
                  }
                },
                "responses": {
                  "type": "object",
                  "properties": {
                    "200": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "$ref": {
                              "type": "string"
                            }
                          },
                          "required": [
                            "$ref"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "400": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "404": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "500": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    }
                  },
                  "required": [
                    "200",
                    "400",
                    "404",
                    "500"
                  ]
                }
              },
              "required": [
                "description",
                "produces",
                "summary",
                "parameters",
                "responses"
              ]
            }
          },
          "required": [
            "get"
          ]
        },
        "/commons-analytics/top-viewed-categories-monthly/{category-scope}/{wiki}/{year}/{month}": {
          "type": "object",
          "properties": {
            "get": {
              "type": "object",
              "properties": {
                "description": {
                  "type": "string"
                },
                "produces": {
                  "type": "array",
                  "items": {
                    "type": "string"
                  }
                },
                "summary": {
                  "type": "string"
                },
                "parameters": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "enum": {
                        "type": "array",
                        "items": {
                          "type": "string"
                        }
                      },
                      "type": {
                        "type": "string"
                      },
                      "example": {
                        "type": "string"
                      },
                      "description": {
                        "type": "string"
                      },
                      "name": {
                        "type": "string"
                      },
                      "in": {
                        "type": "string"
                      },
                      "required": {
                        "type": "boolean"
                      }
                    },
                    "required": [
                      "type",
                      "example",
                      "description",
                      "name",
                      "in",
                      "required"
                    ]
                  }
                },
                "responses": {
                  "type": "object",
                  "properties": {
                    "200": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "$ref": {
                              "type": "string"
                            }
                          },
                          "required": [
                            "$ref"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "400": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "404": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "500": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    }
                  },
                  "required": [
                    "200",
                    "400",
                    "404",
                    "500"
                  ]
                }
              },
              "required": [
                "description",
                "produces",
                "summary",
                "parameters",
                "responses"
              ]
            }
          },
          "required": [
            "get"
          ]
        },
        "/commons-analytics/top-viewed-media-files-monthly/{category}/{category-scope}/{wiki}/{year}/{month}": {
          "type": "object",
          "properties": {
            "get": {
              "type": "object",
              "properties": {
                "description": {
                  "type": "string"
                },
                "produces": {
                  "type": "array",
                  "items": {
                    "type": "string"
                  }
                },
                "summary": {
                  "type": "string"
                },
                "parameters": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "type": {
                        "type": "string"
                      },
                      "example": {
                        "type": "string"
                      },
                      "description": {
                        "type": "string"
                      },
                      "name": {
                        "type": "string"
                      },
                      "in": {
                        "type": "string"
                      },
                      "required": {
                        "type": "boolean"
                      },
                      "enum": {
                        "type": "array",
                        "items": {
                          "type": "string"
                        }
                      }
                    },
                    "required": [
                      "type",
                      "example",
                      "description",
                      "name",
                      "in",
                      "required"
                    ]
                  }
                },
                "responses": {
                  "type": "object",
                  "properties": {
                    "200": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "$ref": {
                              "type": "string"
                            }
                          },
                          "required": [
                            "$ref"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "400": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "404": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "500": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    }
                  },
                  "required": [
                    "200",
                    "400",
                    "404",
                    "500"
                  ]
                }
              },
              "required": [
                "description",
                "produces",
                "summary",
                "parameters",
                "responses"
              ]
            }
          },
          "required": [
            "get"
          ]
        },
        "/commons-analytics/top-wikis-per-category-monthly/{category}/{category-scope}/{year}/{month}": {
          "type": "object",
          "properties": {
            "get": {
              "type": "object",
              "properties": {
                "description": {
                  "type": "string"
                },
                "produces": {
                  "type": "array",
                  "items": {
                    "type": "string"
                  }
                },
                "summary": {
                  "type": "string"
                },
                "parameters": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "type": {
                        "type": "string"
                      },
                      "example": {
                        "type": "string"
                      },
                      "description": {
                        "type": "string"
                      },
                      "name": {
                        "type": "string"
                      },
                      "in": {
                        "type": "string"
                      },
                      "required": {
                        "type": "boolean"
                      },
                      "enum": {
                        "type": "array",
                        "items": {
                          "type": "string"
                        }
                      }
                    },
                    "required": [
                      "type",
                      "example",
                      "description",
                      "name",
                      "in",
                      "required"
                    ]
                  }
                },
                "responses": {
                  "type": "object",
                  "properties": {
                    "200": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "$ref": {
                              "type": "string"
                            }
                          },
                          "required": [
                            "$ref"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "400": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "404": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "500": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    }
                  },
                  "required": [
                    "200",
                    "400",
                    "404",
                    "500"
                  ]
                }
              },
              "required": [
                "description",
                "produces",
                "summary",
                "parameters",
                "responses"
              ]
            }
          },
          "required": [
            "get"
          ]
        },
        "/commons-analytics/top-wikis-per-media-file-monthly/{media-file}/{year}/{month}": {
          "type": "object",
          "properties": {
            "get": {
              "type": "object",
              "properties": {
                "description": {
                  "type": "string"
                },
                "produces": {
                  "type": "array",
                  "items": {
                    "type": "string"
                  }
                },
                "summary": {
                  "type": "string"
                },
                "parameters": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "type": {
                        "type": "string"
                      },
                      "example": {
                        "type": "string"
                      },
                      "description": {
                        "type": "string"
                      },
                      "name": {
                        "type": "string"
                      },
                      "in": {
                        "type": "string"
                      },
                      "required": {
                        "type": "boolean"
                      }
                    },
                    "required": [
                      "type",
                      "example",
                      "description",
                      "name",
                      "in",
                      "required"
                    ]
                  }
                },
                "responses": {
                  "type": "object",
                  "properties": {
                    "200": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "$ref": {
                              "type": "string"
                            }
                          },
                          "required": [
                            "$ref"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "400": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "404": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "500": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    }
                  },
                  "required": [
                    "200",
                    "400",
                    "404",
                    "500"
                  ]
                }
              },
              "required": [
                "description",
                "produces",
                "summary",
                "parameters",
                "responses"
              ]
            }
          },
          "required": [
            "get"
          ]
        }
      },
      "required": [
        "/commons-analytics/category-metrics-snapshot/{category}/{start}/{end}",
        "/commons-analytics/edits-per-category-monthly/{category}/{category-scope}/{edit-type}/{start}/{end}",
        "/commons-analytics/edits-per-user-monthly/{user-name}/{edit-type}/{start}/{end}",
        "/commons-analytics/media-file-metrics-snapshot/{media-file}/{start}/{end}",
        "/commons-analytics/pageviews-per-category-monthly/{category}/{category-scope}/{wiki}/{start}/{end}",
        "/commons-analytics/pageviews-per-media-file-monthly/{media-file}/{wiki}/{start}/{end}",
        "/commons-analytics/top-edited-categories-monthly/{category-scope}/{edit-type}/{year}/{month}",
        "/commons-analytics/top-editors-monthly/{category}/{category-scope}/{edit-type}/{year}/{month}",
        "/commons-analytics/top-pages-per-category-monthly/{category}/{category-scope}/{wiki}/{year}/{month}",
        "/commons-analytics/top-pages-per-media-file-monthly/{media-file}/{wiki}/{year}/{month}",
        "/commons-analytics/top-viewed-categories-monthly/{category-scope}/{wiki}/{year}/{month}",
        "/commons-analytics/top-viewed-media-files-monthly/{category}/{category-scope}/{wiki}/{year}/{month}",
        "/commons-analytics/top-wikis-per-category-monthly/{category}/{category-scope}/{year}/{month}",
        "/commons-analytics/top-wikis-per-media-file-monthly/{media-file}/{year}/{month}"
      ]
    },
    "definitions": {
      "type": "object",
      "properties": {
        "entities.CategoryEdits": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "edit-count": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "timestamp": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                }
              },
              "required": [
                "edit-count",
                "timestamp"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.CategoryEditsContext": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "category": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "category-scope": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "edit-type": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "end": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "endpoint": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "start": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                }
              },
              "required": [
                "category",
                "category-scope",
                "edit-type",
                "end",
                "endpoint",
                "start"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.CategoryEditsResponse": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "context": {
                  "type": "object",
                  "properties": {
                    "$ref": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "$ref"
                  ]
                },
                "items": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "items": {
                      "type": "object",
                      "properties": {
                        "$ref": {
                          "type": "string"
                        }
                      },
                      "required": [
                        "$ref"
                      ]
                    }
                  },
                  "required": [
                    "type",
                    "items"
                  ]
                }
              },
              "required": [
                "context",
                "items"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.CategoryMetric": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "leveraging-page-count": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "leveraging-page-count-deep": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "leveraging-wiki-count": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "leveraging-wiki-count-deep": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "media-file-count": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "media-file-count-deep": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "timestamp": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "used-media-file-count": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "used-media-file-count-deep": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                }
              },
              "required": [
                "leveraging-page-count",
                "leveraging-page-count-deep",
                "leveraging-wiki-count",
                "leveraging-wiki-count-deep",
                "media-file-count",
                "media-file-count-deep",
                "timestamp",
                "used-media-file-count",
                "used-media-file-count-deep"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.CategoryMetricResponse": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "context": {
                  "type": "object",
                  "properties": {
                    "$ref": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "$ref"
                  ]
                },
                "items": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "items": {
                      "type": "object",
                      "properties": {
                        "$ref": {
                          "type": "string"
                        }
                      },
                      "required": [
                        "$ref"
                      ]
                    }
                  },
                  "required": [
                    "type",
                    "items"
                  ]
                }
              },
              "required": [
                "context",
                "items"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.CategoryMetricsSnapshotContext": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "category": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "end": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "endpoint": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "start": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                }
              },
              "required": [
                "category",
                "end",
                "endpoint",
                "start"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.CategoryPageviews": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "pageview-count": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "timestamp": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                }
              },
              "required": [
                "pageview-count",
                "timestamp"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.CategoryPageviewsContext": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "category": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "category-scope": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "end": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "endpoint": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "start": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "wiki": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                }
              },
              "required": [
                "category",
                "category-scope",
                "end",
                "endpoint",
                "start",
                "wiki"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.CategoryPageviewsResponse": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "context": {
                  "type": "object",
                  "properties": {
                    "$ref": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "$ref"
                  ]
                },
                "items": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "items": {
                      "type": "object",
                      "properties": {
                        "$ref": {
                          "type": "string"
                        }
                      },
                      "required": [
                        "$ref"
                      ]
                    }
                  },
                  "required": [
                    "type",
                    "items"
                  ]
                }
              },
              "required": [
                "context",
                "items"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.MediaFileMetricsSnapshot": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "leveraging-page-count": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "leveraging-wiki-count": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "timestamp": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                }
              },
              "required": [
                "leveraging-page-count",
                "leveraging-wiki-count",
                "timestamp"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.MediaFileMetricsSnapshotContext": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "end": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "endpoint": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "media-file": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "start": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                }
              },
              "required": [
                "end",
                "endpoint",
                "media-file",
                "start"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.MediaFileMetricsSnapshotResponse": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "context": {
                  "type": "object",
                  "properties": {
                    "$ref": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "$ref"
                  ]
                },
                "items": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "items": {
                      "type": "object",
                      "properties": {
                        "$ref": {
                          "type": "string"
                        }
                      },
                      "required": [
                        "$ref"
                      ]
                    }
                  },
                  "required": [
                    "type",
                    "items"
                  ]
                }
              },
              "required": [
                "context",
                "items"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.MediaFilePageviews": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "pageview-count": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "timestamp": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                }
              },
              "required": [
                "pageview-count",
                "timestamp"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.MediaFilePageviewsContext": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "end": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "endpoint": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "media-file": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "start": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "wiki": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                }
              },
              "required": [
                "end",
                "endpoint",
                "media-file",
                "start",
                "wiki"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.MediaFilePageviewsResponse": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "context": {
                  "type": "object",
                  "properties": {
                    "$ref": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "$ref"
                  ]
                },
                "items": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "items": {
                      "type": "object",
                      "properties": {
                        "$ref": {
                          "type": "string"
                        }
                      },
                      "required": [
                        "$ref"
                      ]
                    }
                  },
                  "required": [
                    "type",
                    "items"
                  ]
                }
              },
              "required": [
                "context",
                "items"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.TopEditedCategories": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "category": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "edit-count": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "rank": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                }
              },
              "required": [
                "category",
                "edit-count",
                "rank"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.TopEditedCategoriesContext": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "category-scope": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "edit-type": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "endpoint": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "month": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "year": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                }
              },
              "required": [
                "category-scope",
                "edit-type",
                "endpoint",
                "month",
                "year"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.TopEditedCategoriesResponse": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "context": {
                  "type": "object",
                  "properties": {
                    "$ref": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "$ref"
                  ]
                },
                "items": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "items": {
                      "type": "object",
                      "properties": {
                        "$ref": {
                          "type": "string"
                        }
                      },
                      "required": [
                        "$ref"
                      ]
                    }
                  },
                  "required": [
                    "type",
                    "items"
                  ]
                }
              },
              "required": [
                "context",
                "items"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.TopEditors": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "edit-count": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "rank": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "user-name": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                }
              },
              "required": [
                "edit-count",
                "rank",
                "user-name"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.TopEditorsContext": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "category": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "category-scope": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "edit-type": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "endpoint": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "month": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "year": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                }
              },
              "required": [
                "category",
                "category-scope",
                "edit-type",
                "endpoint",
                "month",
                "year"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.TopEditorsResponse": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "context": {
                  "type": "object",
                  "properties": {
                    "$ref": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "$ref"
                  ]
                },
                "items": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "items": {
                      "type": "object",
                      "properties": {
                        "$ref": {
                          "type": "string"
                        }
                      },
                      "required": [
                        "$ref"
                      ]
                    }
                  },
                  "required": [
                    "type",
                    "items"
                  ]
                }
              },
              "required": [
                "context",
                "items"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.TopPagesByCategory": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "page-title": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "pageview-count": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "rank": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                }
              },
              "required": [
                "page-title",
                "pageview-count",
                "rank"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.TopPagesByCategoryContext": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "category": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "category-scope": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "endpoint": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "month": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "wiki": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "year": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                }
              },
              "required": [
                "category",
                "category-scope",
                "endpoint",
                "month",
                "wiki",
                "year"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.TopPagesByCategoryResponse": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "context": {
                  "type": "object",
                  "properties": {
                    "$ref": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "$ref"
                  ]
                },
                "items": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "items": {
                      "type": "object",
                      "properties": {
                        "$ref": {
                          "type": "string"
                        }
                      },
                      "required": [
                        "$ref"
                      ]
                    }
                  },
                  "required": [
                    "type",
                    "items"
                  ]
                }
              },
              "required": [
                "context",
                "items"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.TopPagesByMediaFile": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "page-title": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "pageview-count": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "rank": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                }
              },
              "required": [
                "page-title",
                "pageview-count",
                "rank"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.TopPagesByMediaFileContext": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "endpoint": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "media-file": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "month": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "wiki": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "year": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                }
              },
              "required": [
                "endpoint",
                "media-file",
                "month",
                "wiki",
                "year"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.TopPagesByMediaFileResponse": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "context": {
                  "type": "object",
                  "properties": {
                    "$ref": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "$ref"
                  ]
                },
                "items": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "items": {
                      "type": "object",
                      "properties": {
                        "$ref": {
                          "type": "string"
                        }
                      },
                      "required": [
                        "$ref"
                      ]
                    }
                  },
                  "required": [
                    "type",
                    "items"
                  ]
                }
              },
              "required": [
                "context",
                "items"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.TopViewedCategories": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "category": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "pageview-count": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "rank": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                }
              },
              "required": [
                "category",
                "pageview-count",
                "rank"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.TopViewedCategoriesContext": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "category-scope": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "endpoint": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "month": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "wiki": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "year": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                }
              },
              "required": [
                "category-scope",
                "endpoint",
                "month",
                "wiki",
                "year"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.TopViewedCategoriesResponse": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "context": {
                  "type": "object",
                  "properties": {
                    "$ref": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "$ref"
                  ]
                },
                "items": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "items": {
                      "type": "object",
                      "properties": {
                        "$ref": {
                          "type": "string"
                        }
                      },
                      "required": [
                        "$ref"
                      ]
                    }
                  },
                  "required": [
                    "type",
                    "items"
                  ]
                }
              },
              "required": [
                "context",
                "items"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.TopViewedMediaFiles": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "media-file": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "pageview-count": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "rank": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                }
              },
              "required": [
                "media-file",
                "pageview-count",
                "rank"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.TopViewedMediaFilesContext": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "category": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "category-scope": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "endpoint": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "month": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "wiki": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "year": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                }
              },
              "required": [
                "category",
                "category-scope",
                "endpoint",
                "month",
                "wiki",
                "year"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.TopViewedMediaFilesResponse": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "context": {
                  "type": "object",
                  "properties": {
                    "$ref": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "$ref"
                  ]
                },
                "items": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "items": {
                      "type": "object",
                      "properties": {
                        "$ref": {
                          "type": "string"
                        }
                      },
                      "required": [
                        "$ref"
                      ]
                    }
                  },
                  "required": [
                    "type",
                    "items"
                  ]
                }
              },
              "required": [
                "context",
                "items"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.TopWikisByCategory": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "pageview-count": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "rank": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "wiki": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                }
              },
              "required": [
                "pageview-count",
                "rank",
                "wiki"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.TopWikisByCategoryContext": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "category": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "category-scope": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "endpoint": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "month": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "year": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                }
              },
              "required": [
                "category",
                "category-scope",
                "endpoint",
                "month",
                "year"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.TopWikisByCategoryResponse": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "context": {
                  "type": "object",
                  "properties": {
                    "$ref": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "$ref"
                  ]
                },
                "items": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "items": {
                      "type": "object",
                      "properties": {
                        "$ref": {
                          "type": "string"
                        }
                      },
                      "required": [
                        "$ref"
                      ]
                    }
                  },
                  "required": [
                    "type",
                    "items"
                  ]
                }
              },
              "required": [
                "context",
                "items"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.TopWikisByMediaFile": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "pageview-count": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "rank": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "wiki": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                }
              },
              "required": [
                "pageview-count",
                "rank",
                "wiki"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.TopWikisByMediaFileContext": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "endpoint": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "media-file": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "month": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "year": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                }
              },
              "required": [
                "endpoint",
                "media-file",
                "month",
                "year"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.TopWikisByMediaFileResponse": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "context": {
                  "type": "object",
                  "properties": {
                    "$ref": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "$ref"
                  ]
                },
                "items": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "items": {
                      "type": "object",
                      "properties": {
                        "$ref": {
                          "type": "string"
                        }
                      },
                      "required": [
                        "$ref"
                      ]
                    }
                  },
                  "required": [
                    "type",
                    "items"
                  ]
                }
              },
              "required": [
                "context",
                "items"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.UserEdits": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "edit-count": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "timestamp": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                }
              },
              "required": [
                "edit-count",
                "timestamp"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.UserEditsContext": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "edit-type": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "end": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "end-point": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "start": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "user-name": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                }
              },
              "required": [
                "edit-type",
                "end",
                "end-point",
                "start",
                "user-name"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.UserEditsResponse": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "context": {
                  "type": "object",
                  "properties": {
                    "$ref": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "$ref"
                  ]
                },
                "items": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "items": {
                      "type": "object",
                      "properties": {
                        "$ref": {
                          "type": "string"
                        }
                      },
                      "required": [
                        "$ref"
                      ]
                    }
                  },
                  "required": [
                    "type",
                    "items"
                  ]
                }
              },
              "required": [
                "context",
                "items"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        }
      },
      "required": [
        "entities.CategoryEdits",
        "entities.CategoryEditsContext",
        "entities.CategoryEditsResponse",
        "entities.CategoryMetric",
        "entities.CategoryMetricResponse",
        "entities.CategoryMetricsSnapshotContext",
        "entities.CategoryPageviews",
        "entities.CategoryPageviewsContext",
        "entities.CategoryPageviewsResponse",
        "entities.MediaFileMetricsSnapshot",
        "entities.MediaFileMetricsSnapshotContext",
        "entities.MediaFileMetricsSnapshotResponse",
        "entities.MediaFilePageviews",
        "entities.MediaFilePageviewsContext",
        "entities.MediaFilePageviewsResponse",
        "entities.TopEditedCategories",
        "entities.TopEditedCategoriesContext",
        "entities.TopEditedCategoriesResponse",
        "entities.TopEditors",
        "entities.TopEditorsContext",
        "entities.TopEditorsResponse",
        "entities.TopPagesByCategory",
        "entities.TopPagesByCategoryContext",
        "entities.TopPagesByCategoryResponse",
        "entities.TopPagesByMediaFile",
        "entities.TopPagesByMediaFileContext",
        "entities.TopPagesByMediaFileResponse",
        "entities.TopViewedCategories",
        "entities.TopViewedCategoriesContext",
        "entities.TopViewedCategoriesResponse",
        "entities.TopViewedMediaFiles",
        "entities.TopViewedMediaFilesContext",
        "entities.TopViewedMediaFilesResponse",
        "entities.TopWikisByCategory",
        "entities.TopWikisByCategoryContext",
        "entities.TopWikisByCategoryResponse",
        "entities.TopWikisByMediaFile",
        "entities.TopWikisByMediaFileContext",
        "entities.TopWikisByMediaFileResponse",
        "entities.UserEdits",
        "entities.UserEditsContext",
        "entities.UserEditsResponse"
      ]
    }
  },
  "required": [
    "schemes",
    "swagger",
    "info",
    "host",
    "basePath",
    "paths",
    "definitions"
  ]
}