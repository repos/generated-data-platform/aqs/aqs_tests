schema_page_analytics = {
  "title": "Generated schema for Root",
  "type": "object",
  "properties": {
    "schemes": {
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "swagger": {
      "type": "string"
    },
    "info": {
      "type": "object",
      "properties": {
        "description": {
          "type": "string"
        },
        "title": {
          "type": "string"
        },
        "termsOfService": {
          "type": "string"
        },
        "contact": {
          "type": "object",
          "properties": {},
          "required": []
        },
        "version": {
          "type": "string"
        }
      },
      "required": [
        "description",
        "title",
        "termsOfService",
        "contact",
        "version"
      ]
    },
    "host": {
      "type": "string"
    },
    "basePath": {
      "type": "string"
    },
    "paths": {
      "type": "object",
      "properties": {
        "/legacy/pagecounts/aggregate/{project}/{access-site}/{granularity}/{start}/{end}": {
          "type": "object",
          "properties": {
            "get": {
              "type": "object",
              "properties": {
                "description": {
                  "type": "string"
                },
                "produces": {
                  "type": "array",
                  "items": {
                    "type": "string"
                  }
                },
                "summary": {
                  "type": "string"
                },
                "parameters": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "type": {
                        "type": "string"
                      },
                      "example": {
                        "type": "string"
                      },
                      "description": {
                        "type": "string"
                      },
                      "name": {
                        "type": "string"
                      },
                      "in": {
                        "type": "string"
                      },
                      "required": {
                        "type": "boolean"
                      },
                      "enum": {
                        "type": "array",
                        "items": {
                          "type": "string"
                        }
                      }
                    },
                    "required": [
                      "type",
                      "example",
                      "description",
                      "name",
                      "in",
                      "required"
                    ]
                  }
                },
                "responses": {
                  "type": "object",
                  "properties": {
                    "200": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "$ref": {
                              "type": "string"
                            }
                          },
                          "required": [
                            "$ref"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "400": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "type": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "type",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "404": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "type": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "type",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "500": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "type": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "type",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    }
                  },
                  "required": [
                    "200",
                    "400",
                    "404",
                    "500"
                  ]
                }
              },
              "required": [
                "description",
                "produces",
                "summary",
                "parameters",
                "responses"
              ]
            }
          },
          "required": [
            "get"
          ]
        },
        "/pageviews/aggregate/{project}/{access}/{agent}/{granularity}/{start}/{end}": {
          "type": "object",
          "properties": {
            "get": {
              "type": "object",
              "properties": {
                "description": {
                  "type": "string"
                },
                "produces": {
                  "type": "array",
                  "items": {
                    "type": "string"
                  }
                },
                "summary": {
                  "type": "string"
                },
                "parameters": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "type": {
                        "type": "string"
                      },
                      "example": {
                        "type": "string"
                      },
                      "description": {
                        "type": "string"
                      },
                      "name": {
                        "type": "string"
                      },
                      "in": {
                        "type": "string"
                      },
                      "required": {
                        "type": "boolean"
                      },
                      "enum": {
                        "type": "array",
                        "items": {
                          "type": "string"
                        }
                      }
                    },
                    "required": [
                      "type",
                      "example",
                      "description",
                      "name",
                      "in",
                      "required"
                    ]
                  }
                },
                "responses": {
                  "type": "object",
                  "properties": {
                    "200": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "$ref": {
                              "type": "string"
                            }
                          },
                          "required": [
                            "$ref"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "400": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "type": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "type",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "404": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "type": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "type",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "500": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "type": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "type",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    }
                  },
                  "required": [
                    "200",
                    "400",
                    "404",
                    "500"
                  ]
                }
              },
              "required": [
                "description",
                "produces",
                "summary",
                "parameters",
                "responses"
              ]
            }
          },
          "required": [
            "get"
          ]
        },
        "/pageviews/per-article/{project}/{access}/{agent}/{article}/{granularity}/{start}/{end}": {
          "type": "object",
          "properties": {
            "get": {
              "type": "object",
              "properties": {
                "description": {
                  "type": "string"
                },
                "produces": {
                  "type": "array",
                  "items": {
                    "type": "string"
                  }
                },
                "summary": {
                  "type": "string"
                },
                "parameters": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "type": {
                        "type": "string"
                      },
                      "example": {
                        "type": "string"
                      },
                      "description": {
                        "type": "string"
                      },
                      "name": {
                        "type": "string"
                      },
                      "in": {
                        "type": "string"
                      },
                      "required": {
                        "type": "boolean"
                      },
                      "enum": {
                        "type": "array",
                        "items": {
                          "type": "string"
                        }
                      }
                    },
                    "required": [
                      "type",
                      "example",
                      "description",
                      "name",
                      "in",
                      "required"
                    ]
                  }
                },
                "responses": {
                  "type": "object",
                  "properties": {
                    "200": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "$ref": {
                              "type": "string"
                            }
                          },
                          "required": [
                            "$ref"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "400": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "type": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "type",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "404": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "type": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "type",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "500": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "type": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "type",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    }
                  },
                  "required": [
                    "200",
                    "400",
                    "404",
                    "500"
                  ]
                }
              },
              "required": [
                "description",
                "produces",
                "summary",
                "parameters",
                "responses"
              ]
            }
          },
          "required": [
            "get"
          ]
        },
        "/pageviews/top-by-country/{project}/{access}/{year}/{month}": {
          "type": "object",
          "properties": {
            "get": {
              "type": "object",
              "properties": {
                "description": {
                  "type": "string"
                },
                "produces": {
                  "type": "array",
                  "items": {
                    "type": "string"
                  }
                },
                "summary": {
                  "type": "string"
                },
                "parameters": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "type": {
                        "type": "string"
                      },
                      "example": {
                        "type": "string"
                      },
                      "description": {
                        "type": "string"
                      },
                      "name": {
                        "type": "string"
                      },
                      "in": {
                        "type": "string"
                      },
                      "required": {
                        "type": "boolean"
                      },
                      "enum": {
                        "type": "array",
                        "items": {
                          "type": "string"
                        }
                      }
                    },
                    "required": [
                      "type",
                      "example",
                      "description",
                      "name",
                      "in",
                      "required"
                    ]
                  }
                },
                "responses": {
                  "type": "object",
                  "properties": {
                    "200": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "$ref": {
                              "type": "string"
                            }
                          },
                          "required": [
                            "$ref"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "400": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "type": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "type",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "404": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "type": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "type",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "500": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "type": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "type",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    }
                  },
                  "required": [
                    "200",
                    "400",
                    "404",
                    "500"
                  ]
                }
              },
              "required": [
                "description",
                "produces",
                "summary",
                "parameters",
                "responses"
              ]
            }
          },
          "required": [
            "get"
          ]
        },
        "/pageviews/top-per-country/{country}/{access}/{year}/{month}/{day}": {
          "type": "object",
          "properties": {
            "get": {
              "type": "object",
              "properties": {
                "description": {
                  "type": "string"
                },
                "produces": {
                  "type": "array",
                  "items": {
                    "type": "string"
                  }
                },
                "summary": {
                  "type": "string"
                },
                "parameters": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "type": {
                        "type": "string"
                      },
                      "example": {
                        "type": "string"
                      },
                      "description": {
                        "type": "string"
                      },
                      "name": {
                        "type": "string"
                      },
                      "in": {
                        "type": "string"
                      },
                      "required": {
                        "type": "boolean"
                      },
                      "enum": {
                        "type": "array",
                        "items": {
                          "type": "string"
                        }
                      }
                    },
                    "required": [
                      "type",
                      "example",
                      "description",
                      "name",
                      "in",
                      "required"
                    ]
                  }
                },
                "responses": {
                  "type": "object",
                  "properties": {
                    "200": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "$ref": {
                              "type": "string"
                            }
                          },
                          "required": [
                            "$ref"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "400": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "type": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "type",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "404": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "type": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "type",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "500": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "type": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "type",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    }
                  },
                  "required": [
                    "200",
                    "400",
                    "404",
                    "500"
                  ]
                }
              },
              "required": [
                "description",
                "produces",
                "summary",
                "parameters",
                "responses"
              ]
            }
          },
          "required": [
            "get"
          ]
        },
        "/pageviews/top/{project}/{access}/{year}/{month}/{day}": {
          "type": "object",
          "properties": {
            "get": {
              "type": "object",
              "properties": {
                "description": {
                  "type": "string"
                },
                "produces": {
                  "type": "array",
                  "items": {
                    "type": "string"
                  }
                },
                "summary": {
                  "type": "string"
                },
                "parameters": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "type": {
                        "type": "string"
                      },
                      "example": {
                        "type": "string"
                      },
                      "description": {
                        "type": "string"
                      },
                      "name": {
                        "type": "string"
                      },
                      "in": {
                        "type": "string"
                      },
                      "required": {
                        "type": "boolean"
                      },
                      "enum": {
                        "type": "array",
                        "items": {
                          "type": "string"
                        }
                      }
                    },
                    "required": [
                      "type",
                      "example",
                      "description",
                      "name",
                      "in",
                      "required"
                    ]
                  }
                },
                "responses": {
                  "type": "object",
                  "properties": {
                    "200": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "$ref": {
                              "type": "string"
                            }
                          },
                          "required": [
                            "$ref"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "400": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "type": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "type",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "404": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "type": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "type",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "500": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "type": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "type",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    }
                  },
                  "required": [
                    "200",
                    "400",
                    "404",
                    "500"
                  ]
                }
              },
              "required": [
                "description",
                "produces",
                "summary",
                "parameters",
                "responses"
              ]
            }
          },
          "required": [
            "get"
          ]
        }
      },
      "required": [
        "/legacy/pagecounts/aggregate/{project}/{access-site}/{granularity}/{start}/{end}",
        "/pageviews/aggregate/{project}/{access}/{agent}/{granularity}/{start}/{end}",
        "/pageviews/per-article/{project}/{access}/{agent}/{article}/{granularity}/{start}/{end}",
        "/pageviews/top-by-country/{project}/{access}/{year}/{month}",
        "/pageviews/top-per-country/{country}/{access}/{year}/{month}/{day}",
        "/pageviews/top/{project}/{access}/{year}/{month}/{day}"
      ]
    },
    "definitions": {
      "type": "object",
      "properties": {
        "entities.Aggregate": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "access": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "agent": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "granularity": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "project": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "timestamp": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "views": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                }
              },
              "required": [
                "access",
                "agent",
                "granularity",
                "project",
                "timestamp",
                "views"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.AggregateResponse": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "items": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "items": {
                      "type": "object",
                      "properties": {
                        "$ref": {
                          "type": "string"
                        }
                      },
                      "required": [
                        "$ref"
                      ]
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "items"
                  ]
                }
              },
              "required": [
                "items"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.Article": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "article": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "rank": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "views": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                }
              },
              "required": [
                "article",
                "rank",
                "views"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.Country": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "country": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "rank": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "views": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "views_ceil": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                }
              },
              "required": [
                "country",
                "rank",
                "views",
                "views_ceil"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.Legacy": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "access-site": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "count": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "granularity": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "project": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "timestamp": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                }
              },
              "required": [
                "access-site",
                "count",
                "granularity",
                "project",
                "timestamp"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.LegacyResponse": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "items": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "items": {
                      "type": "object",
                      "properties": {
                        "$ref": {
                          "type": "string"
                        }
                      },
                      "required": [
                        "$ref"
                      ]
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "items"
                  ]
                }
              },
              "required": [
                "items"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.PerArticle": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "access": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "agent": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "article": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "granularity": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "project": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "timestamp": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "views": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                }
              },
              "required": [
                "access",
                "agent",
                "article",
                "granularity",
                "project",
                "timestamp",
                "views"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.PerArticleResponse": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "items": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "items": {
                      "type": "object",
                      "properties": {
                        "$ref": {
                          "type": "string"
                        }
                      },
                      "required": [
                        "$ref"
                      ]
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "items"
                  ]
                }
              },
              "required": [
                "items"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.PerCountryArticle": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "article": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "project": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "rank": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "views_ceil": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                }
              },
              "required": [
                "article",
                "project",
                "rank",
                "views_ceil"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.Top": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "access": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "articles": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "items": {
                      "type": "object",
                      "properties": {
                        "$ref": {
                          "type": "string"
                        }
                      },
                      "required": [
                        "$ref"
                      ]
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "items"
                  ]
                },
                "day": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "month": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "project": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "year": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                }
              },
              "required": [
                "access",
                "articles",
                "day",
                "month",
                "project",
                "year"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.TopByCountry": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "access": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "countries": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "items": {
                      "type": "object",
                      "properties": {
                        "$ref": {
                          "type": "string"
                        }
                      },
                      "required": [
                        "$ref"
                      ]
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "items"
                  ]
                },
                "month": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "project": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "year": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                }
              },
              "required": [
                "access",
                "countries",
                "month",
                "project",
                "year"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.TopByCountryResponse": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "items": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "items": {
                      "type": "object",
                      "properties": {
                        "$ref": {
                          "type": "string"
                        }
                      },
                      "required": [
                        "$ref"
                      ]
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "items"
                  ]
                }
              },
              "required": [
                "items"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.TopPerCountry": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "access": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "articles": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "items": {
                      "type": "object",
                      "properties": {
                        "$ref": {
                          "type": "string"
                        }
                      },
                      "required": [
                        "$ref"
                      ]
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "items"
                  ]
                },
                "country": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "day": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "month": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                },
                "year": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "example"
                  ]
                }
              },
              "required": [
                "access",
                "articles",
                "country",
                "day",
                "month",
                "year"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.TopPerCountryResponse": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "items": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "items": {
                      "type": "object",
                      "properties": {
                        "$ref": {
                          "type": "string"
                        }
                      },
                      "required": [
                        "$ref"
                      ]
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "items"
                  ]
                }
              },
              "required": [
                "items"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.TopResponse": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "items": {
                  "type": "object",
                  "properties": {
                    "description": {
                      "type": "string"
                    },
                    "type": {
                      "type": "string"
                    },
                    "items": {
                      "type": "object",
                      "properties": {
                        "$ref": {
                          "type": "string"
                        }
                      },
                      "required": [
                        "$ref"
                      ]
                    }
                  },
                  "required": [
                    "description",
                    "type",
                    "items"
                  ]
                }
              },
              "required": [
                "items"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        }
      },
      "required": [
        "entities.Aggregate",
        "entities.AggregateResponse",
        "entities.Article",
        "entities.Country",
        "entities.Legacy",
        "entities.LegacyResponse",
        "entities.PerArticle",
        "entities.PerArticleResponse",
        "entities.PerCountryArticle",
        "entities.Top",
        "entities.TopByCountry",
        "entities.TopByCountryResponse",
        "entities.TopPerCountry",
        "entities.TopPerCountryResponse",
        "entities.TopResponse"
      ]
    }
  },
  "required": [
    "schemes",
    "swagger",
    "info",
    "host",
    "basePath",
    "paths",
    "definitions"
  ]
}