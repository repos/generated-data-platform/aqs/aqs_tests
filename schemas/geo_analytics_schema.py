schema_geo_analytics = {
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "object",
  "properties": {
    "schemes": {
      "type": "array",
      "items": [
        {
          "type": "string"
        }
      ]
    },
    "swagger": {
      "type": "string"
    },
    "info": {
      "type": "object",
      "properties": {
        "title": {
          "type": "string"
        },
        "termsOfService": {
          "type": "string"
        },
        "contact": {
          "type": "object"
        },
        "version": {
          "type": "string"
        }
      },
      "required": [
        "title",
        "termsOfService",
        "contact",
        "version"
      ]
    },
    "host": {
      "type": "string"
    },
    "basePath": {
      "type": "string"
    },
    "paths": {
      "type": "object",
      "properties": {
        "/editors/by-country/{project}/{activity-level}/{year}/{month}": {
          "type": "object",
          "properties": {
            "get": {
              "type": "object",
              "properties": {
                "description": {
                  "type": "string"
                },
                "produces": {
                  "type": "array",
                  "items": [
                    {
                      "type": "string"
                    }
                  ]
                },
                "summary": {
                  "type": "string"
                },
                "parameters": {
                  "type": "array",
                  "items": [
                    {
                      "type": "object",
                      "properties": {
                        "type": {
                          "type": "string"
                        },
                        "example": {
                          "type": "string"
                        },
                        "description": {
                          "type": "string"
                        },
                        "name": {
                          "type": "string"
                        },
                        "in": {
                          "type": "string"
                        },
                        "required": {
                          "type": "boolean"
                        }
                      },
                      "required": [
                        "type",
                        "example",
                        "description",
                        "name",
                        "in",
                        "required"
                      ]
                    },
                    {
                      "type": "object",
                      "properties": {
                        "enum": {
                          "type": "array",
                          "items": [
                            {
                              "type": "string"
                            },
                            {
                              "type": "string"
                            }
                          ]
                        },
                        "type": {
                          "type": "string"
                        },
                        "example": {
                          "type": "string"
                        },
                        "description": {
                          "type": "string"
                        },
                        "name": {
                          "type": "string"
                        },
                        "in": {
                          "type": "string"
                        },
                        "required": {
                          "type": "boolean"
                        }
                      },
                      "required": [
                        "enum",
                        "type",
                        "example",
                        "description",
                        "name",
                        "in",
                        "required"
                      ]
                    },
                    {
                      "type": "object",
                      "properties": {
                        "type": {
                          "type": "string"
                        },
                        "example": {
                          "type": "string"
                        },
                        "description": {
                          "type": "string"
                        },
                        "name": {
                          "type": "string"
                        },
                        "in": {
                          "type": "string"
                        },
                        "required": {
                          "type": "boolean"
                        }
                      },
                      "required": [
                        "type",
                        "example",
                        "description",
                        "name",
                        "in",
                        "required"
                      ]
                    },
                    {
                      "type": "object",
                      "properties": {
                        "type": {
                          "type": "string"
                        },
                        "example": {
                          "type": "string"
                        },
                        "description": {
                          "type": "string"
                        },
                        "name": {
                          "type": "string"
                        },
                        "in": {
                          "type": "string"
                        },
                        "required": {
                          "type": "boolean"
                        }
                      },
                      "required": [
                        "type",
                        "example",
                        "description",
                        "name",
                        "in",
                        "required"
                      ]
                    }
                  ]
                },
                "responses": {
                  "type": "object",
                  "properties": {
                    "200": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "$ref": {
                              "type": "string"
                            }
                          },
                          "required": [
                            "$ref"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "400": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "404": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    },
                    "500": {
                      "type": "object",
                      "properties": {
                        "description": {
                          "type": "string"
                        },
                        "schema": {
                          "type": "object",
                          "properties": {
                            "type": {
                              "type": "string"
                            },
                            "properties": {
                              "type": "object",
                              "properties": {
                                "detail": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "method": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "status": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "title": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                },
                                "uri": {
                                  "type": "object",
                                  "properties": {
                                    "type": {
                                      "type": "string"
                                    }
                                  },
                                  "required": [
                                    "type"
                                  ]
                                }
                              },
                              "required": [
                                "detail",
                                "method",
                                "status",
                                "title",
                                "uri"
                              ]
                            }
                          },
                          "required": [
                            "type",
                            "properties"
                          ]
                        }
                      },
                      "required": [
                        "description",
                        "schema"
                      ]
                    }
                  },
                  "required": [
                    "200",
                    "400",
                    "404",
                    "500"
                  ]
                }
              },
              "required": [
                "description",
                "produces",
                "summary",
                "parameters",
                "responses"
              ]
            }
          },
          "required": [
            "get"
          ]
        }
      },
      "required": [
        "/editors/by-country/{project}/{activity-level}/{year}/{month}"
      ]
    },
    "definitions": {
      "type": "object",
      "properties": {
        "entities.Country": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "country": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "editors-ceil": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "integer"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                }
              },
              "required": [
                "country",
                "editors-ceil"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.EditorsByCountry": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "activity-level": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "countries": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "items": {
                      "type": "object",
                      "properties": {
                        "$ref": {
                          "type": "string"
                        }
                      },
                      "required": [
                        "$ref"
                      ]
                    }
                  },
                  "required": [
                    "type",
                    "items"
                  ]
                },
                "month": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "project": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                },
                "year": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "example": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "type",
                    "example"
                  ]
                }
              },
              "required": [
                "activity-level",
                "countries",
                "month",
                "project",
                "year"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        },
        "entities.EditorsByCountryResponse": {
          "type": "object",
          "properties": {
            "type": {
              "type": "string"
            },
            "properties": {
              "type": "object",
              "properties": {
                "items": {
                  "type": "object",
                  "properties": {
                    "type": {
                      "type": "string"
                    },
                    "items": {
                      "type": "object",
                      "properties": {
                        "$ref": {
                          "type": "string"
                        }
                      },
                      "required": [
                        "$ref"
                      ]
                    }
                  },
                  "required": [
                    "type",
                    "items"
                  ]
                }
              },
              "required": [
                "items"
              ]
            }
          },
          "required": [
            "type",
            "properties"
          ]
        }
      },
      "required": [
        "entities.Country",
        "entities.EditorsByCountry",
        "entities.EditorsByCountryResponse"
      ]
    }
  },
  "required": [
    "schemes",
    "swagger",
    "info",
    "host",
    "basePath",
    "paths",
    "definitions"
  ]
}