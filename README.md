# Test framework for AQS 2.0 Endpoints

Framework is designed to test the functionality of the AQS 2.0 endpoints. 

The endpoints are:

- Page Analytics:
  -   Aggregate
  -   Legacy
  -   Per Article
  -   Top
  -   Top By Country
  -   Top Per Country
- Device Analytics
- Edits Analytics:
  -   Aggregate
  -   Bytes Difference Absolute Aggregate
  -   Bytes Difference Absolute Per Page
  -   Bytes Difference Net Aggregate
  -   Bytes Difference Net Per Page
  -   Edited Pages Aggregate
  -   Edited Pages New
  -   Edited Pages Top By Absolute Byte Difference
  -   Edited Pages Top By Edits
  -   Edited Pages Top By Net Byte Difference
  -   Per Page
- Editor Analytics 
  -   Data Aggregate
  -   Data Registered Users
  -   Data Top By Absolute Bytes Difference
  -   Data Top By Edits
  -   Data Top By Net Bytes
- Media Analytics:
  -   Aggregate
  -   Per File
  -   Top
- API Spec Json
- Commons Analytics:
  -   Category Metrics Snapshot
  -   Media file metrics snapshot
  -   Pageviews per category monthly
  -   Pageviews per media file monthly
  -   Edits per category monthly
  -   Edits per user monthly
  -   Top pages per category monthly
  -   Top wikis per category monthly
  -   Top viewed categories monthly
  -   Top pages per media file monthly
  -   Top wikis by media file monthly
  -   Top viewed media files monthly
  -   Top edited categories monthly
  -   Top editors monthly



### Language / Framework / Packages
Python scripting language is used for this design. Python 3 should already be installed on your local machines to be 
able to run these tests. Pip3 should automatically be installed with python3. Cucumber BDD framework was used here as 
well to provide readable english syntax that elaborates what exactly each line is doing (See reference at the end of 
readme).

### IDE
This framework is built on python language, so it's recommended to use the Pycharm Professional IDE to make it easier to
use. Other IDEs can be used as well but there might be additional plugins yet unknown. There are available licenses within
WMF.

### Clone Repo

Use the command below to clone the repo to any directory:

```bash
git clone https://gitlab.wikimedia.org/repos/generated-data-platform/aqs/aqs_tests.git
```


### Virtual Environment Set up ( Not required but recommended ) 

It is recommended to set up a virtual env and keep it inside the project. This is done to isolates the packages and possibly the package versions should the need arise.

Steps to set up the virtual environment:

``` bash
# From the root project directory, find python path
which python3

### Create virtual environment
virtualenv --python=<above python3 path> <name desired of virtual env>

### Run the below if you encounter the error "zsh: command not found: virtualenv"
pip3 install virtualenv # then after run the command to create the virtual env above

### ensure env is created in project directory
ls

### Activate the python3 virtual env
source <name of virtual env>/bin/activate
```


### Install the project packages using the requirements.txt file
``` bash
pip3 install -r requirements.txt

```

### Tests
Tests for the endpoints are written and driven by the cucumber(gherkin statements)
in the feature files.


### Running all tests in the framework
At the root directory ( aqs_tests ) of the project use the command to run all tests: 

```bash
behave -D env=prod
```

### Running a specific test per feature file
The specific test is run and the rest of the tests are skipped and the summary is displayed. Each feature file is tagged
such that they can be run independent of all other tests . Example below

The specific test is run and the rest of the tests are skipped and the summary of other tests are displayed. 
Example below:

```bash
behave -D env=prod --tags @aqs_tests.geo_analytics_editors_by_country 
```

The specific test is run and the rest of the tests are skipped and the summary is NOT displayed. Example below:

```bash
behave -D env=prod --tags @aqs_tests.geo_analytics_editors_by_country --no-skipped --summary  
```

### Local testing
In order to run the tests locally, Please see the link below under 'Local Testing' section to set up the qa env and also
the 'Quick start (for QA engineers)' section to set up the dockers and run the tests locally.

Example of a local test endpoint:

```bash
 "http://localhost:8090/metrics/unique-devices/en.wikipedia.org/all-sites/daily/20180101/20180131"
```

[Local Testing Reference link](https://wikitech.wikimedia.org/wiki/AQS_2.0#Quick_start_(for_QA_engineers))

### Contributing
Pull/Merge requests are welcomed from different participants. 

### CI/CD
Gitlab CI/CD test tools will be used for this. More research needs to be carried out and more details will be provided.  TBD

### Test Report
Allure reporter will be used to send test report. This will be integrated with the Gitlab CI/CD



### Documentation Reference
[Cucumber / Gherkin Syntax](https://cucumber.io/docs/gherkin/reference/)

[Behave Command Line Arguments](https://behave.readthedocs.io/en/stable/behave.html)

[Python Requests library](https://requests.readthedocs.io/en/latest/)

[Assertpy Python library](https://pypi.org/project/assertpy/)

[Python 3.0](https://www.python.org/downloads/)

[Allure Behave](https://allurereport.org/docs/behave/)

[Allure Report with Gitlab CI/CD](https://docs.qameta.io/allure-testops/integrations/gitlab/)

[ReadMe](https://www.makeareadme.com/)
