# define the requests header

from fake_useragent import UserAgent

ua = UserAgent()

header_random = ua.random

# the header_random will pick any of safari or chrome or firefox browser attributes

header = {"accept": "application/json",
          "user-agent": header_random}
