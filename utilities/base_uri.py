""" This is the base uri for all the tests """

uri = "https://wikimedia.org/api/rest_v1"

uri_local_test = "http://localhost:8080"

uri_unique_devices_test = "http://localhost:8089"

uri_geo_analytics_local_test = "http://localhost:8091"

uri_media_analytics_local_test = "http://localhost:8092"

uri_page_analytics_local_test = "http://localhost:8093"

uri_editors_analytics_local_test = "http://localhost:8094"

uri_edits_analytics_local = "http://localhost:8095"

uri_common_impact_metrics_local = "http://localhost:8096"
